﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameDataMgr : MonoBehaviour
{

    #region Singleton

    private static GameDataMgr _instance = null;

    public static GameDataMgr Instance
    {
        get
        {
            return _instance;
        }
    }

    #endregion Singleton


    protected Dictionary<string, LocalDataBase>         _localDataList = new Dictionary<string, LocalDataBase>();
    protected Dictionary<string, InfoBase>              _infoList = new Dictionary<string, InfoBase>();
    protected Dictionary<string, DataBase>              _dataList = new Dictionary<string, DataBase>();


    public T GetLocalData<T>() where T : LocalDataBase
    {
        LocalDataBase localDataBase;

        if (!_localDataList.TryGetValue(typeof(T).ToString(), out localDataBase))
        {
            return null;
        }

        return localDataBase as T;
    }

    public IEnumerable GetLocalData()
    {
        foreach (LocalDataBase localDataBase in _localDataList.Values)
        {
            yield return localDataBase;
        }
    }

    public T GetInfo<T>() where T : InfoBase
    {
        InfoBase infoBase;

        if (!_infoList.TryGetValue(typeof(T).ToString(), out infoBase))
        {
            return null;
        }

        return infoBase as T;
    }

    public IEnumerable GetInfo()
    {
        foreach (InfoBase infoBase in _infoList.Values)
        {
            yield return infoBase;
        }
    }

    public T GetData<T>() where T : DataBase
    {
        DataBase dataBase;

        if (!_dataList.TryGetValue(typeof(T).ToString(), out dataBase))
        {
            return null;
        }

        return dataBase as T;
    }

    public IEnumerable GetData()
    {
        foreach (DataBase dataBase in _dataList.Values)
        {
            yield return dataBase;
        }
    }


    protected void Awake()
    {
        _instance = this;


        /////////////////////////////////////////////////////////////////////////
        // Register LocalData
        /////////////////////////////////////////////////////////////////////////

		RegisterLocalData<EnvLocalData>();


        /////////////////////////////////////////////////////////////////////////
        // Register Info
        /////////////////////////////////////////////////////////////////////////

		RegisterInfo<TextInfoMgr>();
        RegisterInfo<PlacementInfoMgr>();
        RegisterInfo<ReballPlacementInfoMgr>();
		RegisterInfo<StageInfoMgr>();
		RegisterInfo<StageMapInfoMgr>();
        RegisterInfo<ItemInfoMgr>();
        RegisterInfo<PackageInfoMgr>();
        RegisterInfo<ShopInfoMgr>();
		RegisterInfo<RandomboxInfoMgr> ();
		RegisterInfo<TipInfoMgr> ();
		RegisterInfo<ScenarioInfoMgr> ();

        /////////////////////////////////////////////////////////////////////////
        // Register Data
        /////////////////////////////////////////////////////////////////////////
    }

    protected void OnDestroy()
    {
        _instance = null;
    }


    protected bool RegisterLocalData<T>() where T : LocalDataBase
    {
        T localData = ScriptableObject.CreateInstance<T>();

        if (null == localData)
        {
            return false;
        }

        _localDataList.Add(localData.GetType().ToString(), localData);

        return true;
    }

    protected bool RegisterData<T>() where T : DataBase
    {
        T serverTable = ScriptableObject.CreateInstance<T>();

        if ( null == serverTable )
        {
            return false;
        }

        _dataList.Add(serverTable.GetType().ToString(), serverTable);

        return true;
    }

    protected bool RegisterInfo<T>() where T : InfoBase
    {
        T info = ScriptableObject.CreateInstance<T>();

        if (null == info)
        {
            return false;
        }

        _infoList.Add(info.GetType().ToString(), info);

        return true;
    }

}
