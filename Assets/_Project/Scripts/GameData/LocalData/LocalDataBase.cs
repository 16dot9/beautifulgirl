﻿using UnityEngine;


public abstract class LocalDataBase : ScriptableObject
{

    public abstract bool Load(string pFile);
    public abstract bool Save(string pFile);

}

