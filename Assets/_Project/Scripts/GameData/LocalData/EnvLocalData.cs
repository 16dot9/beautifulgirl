﻿using UnityEngine;


public class EnvLocalData : LocalDataBase
{

	public bool IsTakingDayReward
	{
		get;
		set;
	}

	public bool IsCompleteTutorialIntro = false;	// 인트로 튜토리얼을 완료했는지..

	public override bool Load(string pFile)
	{
		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

