﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MyInfoMgr
{
    #region singleton

    private static MyInfoMgr m_Instance = null;

    public MyInfoMgr()
    {
        if (m_Instance != null)
        {
            Debug.LogError("Cannot create two instances of singleton. MyInfoMgr !");
            return;
        }

        m_Instance = this;
    }

    public static MyInfoMgr Instance
    {
        get
        {
            if (m_Instance == null)
                new MyInfoMgr();

            return m_Instance;
        }
    }

    #endregion singleton

    public static int _gachaFunnySu = 5000;
    public static int _UnlockStageGemSu = 1;

	public enum ITEM
	{
		Power = 0 	,
		Funny 		,
		Gem 		,
		Roulette 	, 
		BestStage 	, 
		AttendSeq 	,
	}

	static public uint[]     		m_Item = new uint[6];

    public int m_user_status = 0;
    // 보유 재화
    public int             m_Power = 10;
    public int             m_Funny = 5000;
    public int             m_Gem = 5;

    //                        1,094,965,532
    //int -> -2,147,483,648 ~ 2,147,483,647

	public uint				m_Roulette = 0;

    // 완료 스테이지들 정보
    public uint             m_BestStage = 30;
    public ArrayList        m_arStageInfo;

    public List<MailBox> _Mail = new List<MailBox>();

    public uint             m_AttendSeq = 3;

    // 소유 아이템들 정보( max 10 ? ), {item_index, su }

    public Dictionary<uint, uint> m_dicItemsAll = new Dictionary<uint, uint>()
    {
		//{4, 3}, {5, 2}, {6, 3}, {7, 2}, {8, 4}, {17,1} , {16,1}		원본
		{4, 3}, {6, 3}, {7, 2}, {8, 4}, {16,1}
    };

	public Dictionary<uint, uint> m_dicItemsTime = new Dictionary<uint, uint>()
	{
		{4, 3}, {7, 2}, {8, 4}, {16,1}
	};

	public Dictionary<uint, uint> m_dicItemsCount = new Dictionary<uint, uint>()
	{
		{4, 3}, {6, 3}, {8, 4}, {16,1}
	};

	public Dictionary<uint, uint> m_dicItemsAllNotFever = new Dictionary<uint, uint>()
	{
		{4, 3}, {6, 3}, {7, 2}, {8, 4}
	};
	
	public Dictionary<uint, uint> m_dicItemsTimeNotFever = new Dictionary<uint, uint>()
	{
		{4, 3},{7, 2}, {8, 4}
	};
	
	public Dictionary<uint, uint> m_dicItemsCountNotFever = new Dictionary<uint, uint>()
	{
		{4, 3}, {6, 3}, {8, 4}
	};

    // 보유 Keypet 들( 26 종류 ? )

    // 선택한 아이템 정보들( 3개 )
    public List<uint> m_UseItems = new List<uint>();
//    public uint[] use_items = new uint[3];  // item_index

    public uint             m_CurPlayStage = 3;
    
    //상점 셋팅 (요기서 하는게 낫겠지?)
    public List<Shop_Table> _shop = new List<Shop_Table>();

    // 인밴 아이템.
    public List<Inventory_Item> _Inven = new List<Inventory_Item>();
    public void Reset_Inven()
    {
//        Debug.Log(" ~~~~~~~~~~~~~~~~~~~~_Inven.Clear() -> _Inven count: " + _Inven.Count);
        _Inven.Clear();           
    }

    public void Inven_AddItem(Inventory_Item _Inv)
    {
//        Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ _Inv._itemid:" + _Inv._itemid + ", qty:" + _Inv._qty);
        _Inven.Add(_Inv);

//        Debug.Log(" ~~~~~~~~~~~~~~~~~~~~add -> _Inven count: " + _Inven.Count);
    }
    public Inventory_Item Get_Inventory_Item(int ix)
    {
//        Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ _Inven count:" + _Inven.Count + " ix:" + ix);
        Inventory_Item rtn = null;
        for(int i=0; i<_Inven.Count; i++) //  Inventory_Item itm in )
        {
            if (_Inven[i]._itemid == ix)
            {
                rtn = _Inven[i];
                break;
            }
        }
        //if (rtn != null)
        //{
        //    Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ rtn._itemid:" + rtn._itemid + " su:" + rtn._qty);
        //}
        return rtn;
    }
}
