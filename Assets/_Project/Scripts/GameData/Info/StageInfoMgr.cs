﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;


public enum GameMode
{
	Invalid,

	Time,
	Count,
	TimeAndCount,

	Max,
}


public class StageInfo
{
	public uint													si = 0;

	public GameMode												game_mode = GameMode.Invalid;

	public string												mode_desc = "";

	public int												    attr_1 = 0;
	public int												    attr_2 = 0;

	public int													fever_time = 0;

	public uint												    pi_si = 0;			// PlacementInfo Talbe.

	public uint													ri_si_1 = 0;					// Reward item Talbe 1.
	public int													ri_si_1_su = 0;
	public uint													ri_si_2 = 0;					// Reward item Talbe 2.
	public int													ri_si_2_su = 0;
	public uint													ri_si_3 = 0;					// Reward item Talbe 3.
	public int													ri_si_3_su = 0;
	public uint													ri_si_4 = 0;					// Reward item Talbe 4.
	public int													ri_si_4_su = 0;
	public uint													ri_si_5 = 0;					// Reward item Talbe 5.
	public int													ri_si_5_su = 0;
}


public class StageInfoMgr : InfoBase
{

	protected Dictionary<uint, StageInfo>						_data = new Dictionary<uint, StageInfo>();


	public StageInfo GetStageInfo(uint pSI)
	{
		StageInfo stageInfo;

		if (!_data.TryGetValue(pSI, out stageInfo))
		{
			return null;
		}

		return stageInfo;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t');

			StageInfo stageInfo = new StageInfo();

			int tokenIndex = 0;

			stageInfo.si = uint.Parse(split[tokenIndex++].Trim());

			stageInfo.game_mode = (GameMode)System.Enum.Parse(typeof(GameMode), split[tokenIndex++].Trim());
		
			stageInfo.mode_desc = split[tokenIndex++].Trim();

            stageInfo.attr_1 = int.Parse(split[tokenIndex++].Trim());
            stageInfo.attr_2 = int.Parse(split[tokenIndex++].Trim());

			stageInfo.fever_time = int.Parse(split[tokenIndex++].Trim());

			stageInfo.pi_si = uint.Parse(split[tokenIndex++].Trim());

			stageInfo.ri_si_1 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_1_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_2 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_2_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_3 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_3_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_4 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_4_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_5 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_5_su = int.Parse(split[tokenIndex++].Trim());

			_data.Add(stageInfo.si, stageInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			StageInfo stageInfo = new StageInfo();

			int tokenIndex = 0;

			stageInfo.si = uint.Parse(split[tokenIndex++].Trim());

			stageInfo.game_mode = (GameMode) System.Enum.Parse(typeof(GameMode), split[tokenIndex++].Trim());

			stageInfo.mode_desc = split[tokenIndex++].Trim();

			stageInfo.attr_1 = int.Parse(split[tokenIndex++].Trim());
			stageInfo.attr_2 = int.Parse(split[tokenIndex++].Trim());

			stageInfo.fever_time = int.Parse(split[tokenIndex++].Trim());

			stageInfo.pi_si = uint.Parse(split[tokenIndex++].Trim());
			
			stageInfo.ri_si_1 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_1_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_2 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_2_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_3 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_3_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_4 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_4_su = int.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_5 = uint.Parse(split[tokenIndex++].Trim());
			stageInfo.ri_si_5_su = int.Parse(split[tokenIndex++].Trim());

			_data.Add(stageInfo.si, stageInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

