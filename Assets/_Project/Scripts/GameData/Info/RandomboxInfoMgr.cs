﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// 어디에서 사용할 렌덤박스 데이터인지 구분...
public enum RandomboxType
{
	Invalid	= 0,
	ShopGacha,
	KeypetRoulette,
	GemGacha,
	Max
}


// 하나의 랜덤박스가 가지고 있는 PackagesetInfo의 수량 (csv 파일에서 현재 10개임).. 
public enum Max_PackagesetInfo
{
	count = 10
}


// 랜덤박스가 보유하고 있는 패키지 하나하나의 정보...
public struct PackagesetInfo
{
	public uint	package_si;		// 참조하는 패키지 아이템의 인덱스..
	public uint item_su;		// 패키지 아이템의 수량..
	public uint drop_percent;	// 드랍 확률..
};


// 랜덤박스 정보...
public class RandomboxInfo
{
	public uint				si = 0;										// 해당 랜덤박스의 고유 인덱스..
	public RandomboxType 	randombox_type = RandomboxType.Invalid;		// 어디에서 사용할 랜덤박스인가..
	public uint 			attr_1 = 0;									// 랜덤박스를 뽑는데 필요한 퍼니 or  키펫 or 젬의 수량..
	public string			desc = "";									// 관리자용 랜덤박스 설명문..
	public PackagesetInfo[] packagesetInfo = null;						// 랜덤박스가 참조하는 패키지 리스트..
};


// 랜덤박스를 관리하는 메니저 클래스...
public class RandomboxInfoMgr : InfoBase 
{
	// RandomboxInfo를 저장한 딕셔너리..
	protected Dictionary<uint, RandomboxInfo> _data = new Dictionary<uint, RandomboxInfo>();

	// RandomboxInfo 데이터 얻기...
	public RandomboxInfo GetRandomoxInfo(uint pSI)
	{
		RandomboxInfo randomboxInfo;

		if (!_data.TryGetValue (pSI, out randomboxInfo)) 
		{
				return null;
		}

		return randomboxInfo;
	}

	public IEnumerable GetRandomboxInfo()
	{
		foreach (RandomboxInfo randomboxInfo in _data.Values)
			yield return randomboxInfo;
	}

		
	//
	public override bool SetJsonData(string pJSONdata)
	{
		return true;
	}


	// csv 파일에서 RandomboxInfo 데이터 만들기...
	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader (pCSVData);
		bool isReading = true;

		while (isReading)
		{
			if(reader.Peek() < 0)
				break;

			string line = reader.ReadLine();

			if(string.IsNullOrEmpty(line))
				continue;

			string[] split = line.Split('\t'); // line.Split(',');

			RandomboxInfo randomboxInfo = new RandomboxInfo();
			int tokenIndex = 0;

			randomboxInfo.si = uint.Parse(split[tokenIndex++].Trim());
			randomboxInfo.randombox_type = (RandomboxType)System.Enum.Parse(typeof(RandomboxType), split[tokenIndex++].Trim());
			randomboxInfo.attr_1 = uint.Parse(split[tokenIndex++].Trim());
			randomboxInfo.desc = split[tokenIndex++].Trim();

			randomboxInfo.packagesetInfo = new PackagesetInfo[(int)Max_PackagesetInfo.count];

			for(int i = 0; i < (int)Max_PackagesetInfo.count; i++)
			{
				randomboxInfo.packagesetInfo[i] = new PackagesetInfo();
				randomboxInfo.packagesetInfo[i].package_si = uint.Parse(split[tokenIndex++].Trim());
				randomboxInfo.packagesetInfo[i].item_su = uint.Parse(split[tokenIndex++].Trim());
				randomboxInfo.packagesetInfo[i].drop_percent = uint.Parse(split[tokenIndex++].Trim());
			}
	
			// 딕셔너리에 데이터 저장..
			_data.Add(randomboxInfo.si, randomboxInfo);
		}

		reader.Close ();
		return true;
	}


	public override bool Load(string pFile)
	{
		return true;
	}


	public override bool Save(string pFile)
	{
		return true;
	}
}
