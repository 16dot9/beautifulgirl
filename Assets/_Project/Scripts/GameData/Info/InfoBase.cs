﻿using UnityEngine;


public abstract class InfoBase : ScriptableObject
{

    public abstract bool SetJsonData(string pJSONData);
	public abstract bool Import(string pCSVData);
	public abstract bool Load(string pFile);
    public abstract bool Save(string pFile);

}

