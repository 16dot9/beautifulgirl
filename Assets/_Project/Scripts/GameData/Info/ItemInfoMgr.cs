﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;



public class ItemInfo
{
	public uint													si = 0;

	public string												name = "";
    public string                                               desc = "";                                     

	public uint												    type = 0;
	public int												    attr_1 = 0;
    public int                                                  attr_2 = 0;
    public int                                                  attr_3 = 0;

	public string           									icon_image = "";
}


public class ItemInfoMgr : InfoBase
{

	protected Dictionary<uint, ItemInfo>						_data = new Dictionary<uint, ItemInfo>();


	public ItemInfo GetItemInfo(uint pSI)
	{
		ItemInfo itemInfo;

		if (!_data.TryGetValue(pSI, out itemInfo))
		{
			return null;
		}

		return itemInfo;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			ItemInfo itemInfo = new ItemInfo();

			int tokenIndex = 0;

            itemInfo.si = uint.Parse(split[tokenIndex++].Trim());

            itemInfo.name = split[tokenIndex++].Trim();
            itemInfo.desc = split[tokenIndex++].Trim();

            itemInfo.type = uint.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_1 = int.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_2 = int.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_3 = int.Parse(split[tokenIndex++].Trim());

            itemInfo.icon_image = split[tokenIndex++].Trim();

			_data.Add(itemInfo.si, itemInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

            ItemInfo itemInfo = new ItemInfo();

            int tokenIndex = 0;

            itemInfo.si = uint.Parse(split[tokenIndex++].Trim());

            itemInfo.name = split[tokenIndex++].Trim();
            itemInfo.desc = split[tokenIndex++].Trim();

            itemInfo.type = uint.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_1 = int.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_2 = int.Parse(split[tokenIndex++].Trim());
            itemInfo.attr_3 = int.Parse(split[tokenIndex++].Trim());

            itemInfo.icon_image = split[tokenIndex++].Trim();

            _data.Add(itemInfo.si, itemInfo);
        }

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

