﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;


public enum PackageType
{
	Invalid,

	Shop,
	Operator,
	Event,

	Max,
}

public enum PriceType
{
    Invalid,
    Gem,
    Funny,
    //Keypet,
	FriendInvite,	// 친구 초대...
    Max
}

public class PackageInfo
{
	public uint				si = 0;

    public PackageType      type = PackageType.Invalid;

    public string           name = "";
    public string           desc = "";
    public string           icon_image = "";

    public uint             item_1_idx = 0;
    public uint             item_1_su = 0;
    public uint             item_2_idx = 0;
    public uint             item_2_su = 0;
    public uint             item_3_idx = 0;
    public uint             item_3_su = 0;
    public uint             item_4_idx = 0;
    public uint             item_4_su = 0;
    public uint             item_5_idx = 0;
    public uint             item_5_su = 0;

    public PriceType        price_type = 0;
    public float             price_value = 0;	// 친구 초대시 초대 인원 수를 나타냄...
}


public class PackageInfoMgr : InfoBase
{

    protected Dictionary<uint, PackageInfo>     _data = new Dictionary<uint, PackageInfo>();


    public PackageInfo GetPackageInfo(uint pSI)
	{
        PackageInfo packageInfo;

        if (!_data.TryGetValue(pSI, out packageInfo))
		{
			return null;
		}

        return packageInfo;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

            PackageInfo packageInfo = new PackageInfo();

			int tokenIndex = 0;

            packageInfo.si = uint.Parse(split[tokenIndex++].Trim());

            packageInfo.type = (PackageType)System.Enum.Parse(typeof(PackageType), split[tokenIndex++].Trim());
            packageInfo.name = split[tokenIndex++].Trim();
            packageInfo.desc = split[tokenIndex++].Trim();
            packageInfo.icon_image = split[tokenIndex++].Trim();

            packageInfo.item_1_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_1_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_2_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_2_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_3_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_3_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_4_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_4_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_5_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_5_su = uint.Parse(split[tokenIndex++].Trim());
            

            packageInfo.price_type = (PriceType)System.Enum.Parse(typeof(PriceType), split[tokenIndex++].Trim());
            packageInfo.price_value = float.Parse(split[tokenIndex++].Trim());

            _data.Add(packageInfo.si, packageInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

            PackageInfo packageInfo = new PackageInfo();

            int tokenIndex = 0;

            packageInfo.si = uint.Parse(split[tokenIndex++].Trim());

            packageInfo.type = (PackageType)System.Enum.Parse(typeof(PackageType), split[tokenIndex++].Trim());
            packageInfo.name = split[tokenIndex++].Trim();
            packageInfo.desc = split[tokenIndex++].Trim();
            packageInfo.icon_image = split[tokenIndex++].Trim();

            packageInfo.item_1_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_1_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_2_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_2_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_3_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_3_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_4_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_4_su = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_5_idx = uint.Parse(split[tokenIndex++].Trim());
            packageInfo.item_5_su = uint.Parse(split[tokenIndex++].Trim());

            packageInfo.price_type = (PriceType)System.Enum.Parse(typeof(PriceType), split[tokenIndex++].Trim());
            packageInfo.price_value = uint.Parse(split[tokenIndex++].Trim());

            _data.Add(packageInfo.si, packageInfo);
        }

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

