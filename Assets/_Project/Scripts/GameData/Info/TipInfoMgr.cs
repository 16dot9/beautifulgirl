﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// 어디에서 사용할 툴팁 에시지인지 구분...
public enum TipType
{
	Invalid	= 0,
	LoadingTipMsg,
	Other,
	Max
}


// 툴팁 메시지 정보...
public class TipInfo
{
	public uint				si = 0;										// 고유 인덱스..
	public TipType 			tip_type = TipType.Invalid;					// 어디에서 사용할 툴팁인가..
	public int 				attr_1 = 0;									// 로딩 툴팁의 경우에는 출력되는 순서라고 함...
	public string			desc = "";									// 메시지 내용..
};


// 툴팁 관리하는 메니저 클래스...
public class TipInfoMgr : InfoBase 
{
	// RandomboxInfo를 저장한 딕셔너리..
	protected Dictionary<uint, TipInfo> _data = new Dictionary<uint, TipInfo>();

	// TipType.LoadingTipMsg 타입의 데이터 수량 체크..
	protected int _LoadingTipCount = 0;
	
	// RandomboxInfo 데이터 얻기...
	public TipInfo GetTipInfo(uint pSI)
	{
		TipInfo tipInfo;
		
		if (!_data.TryGetValue (pSI, out tipInfo)) 
		{
			return null;
		}
		
		return tipInfo;
	}
	
	public IEnumerable GetTipInfo()
	{
		foreach (TipInfo tipInfo in _data.Values)
			yield return tipInfo;
	}


	// 
	public int LoadingTipCount
	{
		get
		{
			return _LoadingTipCount;
		}
	}
	
	
	//
	public override bool SetJsonData(string pJSONdata)
	{
		return true;
	}
	
	
	// csv 파일에서 TipInfo 데이터 만들기...
	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader (pCSVData);
		bool isReading = true;
		
		while (isReading)
		{
			if(reader.Peek() < 0)
				break;
			
			string line = reader.ReadLine();
			
			if(string.IsNullOrEmpty(line))
				continue;
			
			string[] split = line.Split('\t'); // line.Split(',');
			
			TipInfo tipInfo = new TipInfo();
			int tokenIndex = 0;
			
			tipInfo.si = uint.Parse(split[tokenIndex++].Trim());
			tipInfo.tip_type = (TipType)System.Enum.Parse(typeof(TipType), split[tokenIndex++].Trim());
			tipInfo.attr_1 = int.Parse(split[tokenIndex++].Trim());
			tipInfo.desc = split[tokenIndex++].Trim();

			// 딕셔너리에 데이터 저장..
			_data.Add(tipInfo.si, tipInfo);

			// 로딩시에 나오는 툴팁 메시지의 수량 체크..
			if(tipInfo.tip_type == TipType.LoadingTipMsg)
				++_LoadingTipCount;
		}
		
		reader.Close ();
		return true;
	}
	
	
	public override bool Load(string pFile)
	{
		return true;
	}
	
	
	public override bool Save(string pFile)
	{
		return true;
	}
}
