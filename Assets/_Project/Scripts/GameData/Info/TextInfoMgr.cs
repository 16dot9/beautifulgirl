﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;


public class TextInfo
{
	public uint										si = 0;

	public string									text = "";
}


public class TextInfoMgr : InfoBase
{

	protected Dictionary<uint, TextInfo>			_data = new Dictionary<uint, TextInfo>();


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			TextInfo textInfo = new TextInfo();

			int tokenIndex = 0;

			textInfo.si = uint.Parse(split[tokenIndex++].Trim());

			textInfo.text = split[tokenIndex++].Trim();

			_data.Add(textInfo.si, textInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split('\t'); // line.Split(',');

			TextInfo textInfo = new TextInfo();

			int tokenIndex = 0;

			textInfo.si = uint.Parse(split[tokenIndex++].Trim());

			textInfo.text = split[tokenIndex++].Trim();

			_data.Add(textInfo.si, textInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

