﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MaxObjectInfo
{
	count = 2
}

public enum MaxLinkedStageInfo
{
	count = 20
}


public class LinkedStageInfo
{
	public uint										stage_si = 0;				// 스테이지 시리얼 아이디.
	public float									pos_x = 0.0f;				// 맵에서의 x 위치.
	public float									pos_y = 0.0f;				// 맵에서의 y 위치.
}

public class ObjectInfo
{
	public string									object_res = "";			// 마을에 대한 리소스 정보.
	public float									pos_x = 0.0f;				// 맵에서의 x 위치.
	public float									pos_y = 0.0f;				// 맵에서의 y 위치.
}


public class StageMapInfo
{

	public uint										si = 0;						// 시리얼 아이디.

	public string									vill_name = "";
	public string									vill_desc = "";

	public string									background_01 = "";
	public string									background_02 = "";

    public Vector2                                  SignPos = Vector2.zero;
    public Vector2                                  nextVillPos = Vector2.zero;

	public List<ObjectInfo>							object_info = new List<ObjectInfo>();				// 마을 리스트.
	public List<LinkedStageInfo>					linked_stage_info = new List<LinkedStageInfo>();	// 연결된 스테이지 리스트.

	public uint										enter_dir_si = 0;			// 시작 연출에 대한 시리얼 아이디.
	public uint										random_dir_01_si = 0;		// 종료 연출에 대한 시리얼 아이디.
	public uint										random_dir_02_si = 0;		// 종료 연출에 대한 시리얼 아이디.
	public uint										random_dir_03_si = 0;		// 종료 연출에 대한 시리얼 아이디.
}


public class StageMapInfoMgr : InfoBase
{

    protected Dictionary<uint, StageMapInfo> _data = new Dictionary<uint, StageMapInfo>();

    public int CountOfStageMapInfo
	{
		get { return _data.Count; }
	}


	public IEnumerable GetStageMapInfo()
	{
        foreach (StageMapInfo staGemapInfo in _data.Values)
		{
			yield return staGemapInfo;
		}
	}

    public StageMapInfo GetStageMapInfo(uint pSI)
	{
        StageMapInfo info;
		if (!_data.TryGetValue(pSI, out info))
		{
		    return null;
		}
		   
		return info;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}


	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

            StageMapInfo staGemapInfo = new StageMapInfo();

			int tokenIndex = 0;

			staGemapInfo.si = uint.Parse(split[tokenIndex++].Trim());

			staGemapInfo.vill_name = split[tokenIndex++].Trim();
			staGemapInfo.vill_desc = split[tokenIndex++].Trim();

			staGemapInfo.background_01 = split[tokenIndex++].Trim();
			staGemapInfo.background_02 = split[tokenIndex++].Trim();

            staGemapInfo.SignPos.x = float.Parse(split[tokenIndex++].Trim());
            staGemapInfo.SignPos.y = float.Parse(split[tokenIndex++].Trim());
            staGemapInfo.nextVillPos.x = float.Parse(split[tokenIndex++].Trim());
			staGemapInfo.nextVillPos.y = float.Parse(split[tokenIndex++].Trim());

			for (int i = 0; i < (int)MaxObjectInfo.count; ++i)
			{
				ObjectInfo objectInfo = new ObjectInfo();

				objectInfo.object_res = split[tokenIndex++].Trim();
				objectInfo.pos_x = float.Parse(split[tokenIndex++].Trim());
				objectInfo.pos_y = float.Parse(split[tokenIndex++].Trim());

				staGemapInfo.object_info.Add(objectInfo);
			}

			for (int i = 0; i < (int)MaxLinkedStageInfo.count; ++i)
			{
				LinkedStageInfo linkedStageInfo = new LinkedStageInfo();

				linkedStageInfo.stage_si = uint.Parse(split[tokenIndex++].Trim());
				linkedStageInfo.pos_x = float.Parse(split[tokenIndex++].Trim());
				linkedStageInfo.pos_y = float.Parse(split[tokenIndex++].Trim());

				staGemapInfo.linked_stage_info.Add(linkedStageInfo);
			}

			staGemapInfo.enter_dir_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_01_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_02_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_03_si = uint.Parse(split[tokenIndex++].Trim());

			_data.Add(staGemapInfo.si, staGemapInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

            StageMapInfo staGemapInfo = new StageMapInfo();

			int tokenIndex = 0;

			staGemapInfo.si = uint.Parse(split[tokenIndex++].Trim());

			staGemapInfo.vill_name = split[tokenIndex++].Trim();
			staGemapInfo.vill_desc = split[tokenIndex++].Trim();

			staGemapInfo.background_01 = split[tokenIndex++].Trim();
			staGemapInfo.background_02 = split[tokenIndex++].Trim();

            staGemapInfo.SignPos.x = float.Parse(split[tokenIndex++].Trim());
            staGemapInfo.SignPos.y = float.Parse(split[tokenIndex++].Trim());
            staGemapInfo.nextVillPos.x = float.Parse(split[tokenIndex++].Trim());
			staGemapInfo.nextVillPos.y = float.Parse(split[tokenIndex++].Trim());

			for (int i = 0; i < (int)MaxObjectInfo.count; ++i)
			{
				ObjectInfo objectInfo = new ObjectInfo();

				objectInfo.object_res = split[tokenIndex++].Trim();
				objectInfo.pos_x = float.Parse(split[tokenIndex++].Trim());
				objectInfo.pos_y = float.Parse(split[tokenIndex++].Trim());

				staGemapInfo.object_info.Add(objectInfo);
			}

			for (int i = 0; i < (int)MaxLinkedStageInfo.count; ++i)
			{
				LinkedStageInfo linkedStageInfo = new LinkedStageInfo();

				linkedStageInfo.stage_si = uint.Parse(split[tokenIndex++].Trim());
				linkedStageInfo.pos_x = float.Parse(split[tokenIndex++].Trim());
				linkedStageInfo.pos_y = float.Parse(split[tokenIndex++].Trim());

				staGemapInfo.linked_stage_info.Add(linkedStageInfo);
			}

			staGemapInfo.enter_dir_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_01_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_02_si = uint.Parse(split[tokenIndex++].Trim());
			staGemapInfo.random_dir_03_si = uint.Parse(split[tokenIndex++].Trim());

			_data.Add(staGemapInfo.si, staGemapInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

