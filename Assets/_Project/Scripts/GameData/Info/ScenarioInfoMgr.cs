﻿using System.IO;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public enum ScenarioType
{
	Invalid = 0,
	NormalScenario,		// 통상..
	RandomScenario,		// 랜덤..
	EventScenario,		// 이벤트..
	Max
}

public enum KeypetDirect
{
	Invalid = 0,
	KoNya,			// 고양이..
	DorongYi,		// 개..
	Max
}

public class ScenarioInfo
{
	public int si	= 0;								// 인덱스..
	public ScenarioType scenarioType = ScenarioType.Invalid;		// 시나리오 타입..
	public int scenarioNumber = 0;						// 해당 넘버를 스테이지 데이터에 넣는다.. 즉, 특정 스테이지에서 보여지는 시나리오는 공통된 scenarioNumber를 갖는 ScenarioInfo를 플레이 시키는 것이다..
	public int scenarioSequence = 0;					// scenarioNumber 안에서 대사의 순서..
	public string bgName = "";							// 배경 그림파일 이름..
	public KeypetDirect keypetDirect = KeypetDirect.Invalid;		// 말풍선 방향..(누가 말하는 것인지 결정)..
	public string keypetFace = "";						// 키펫 얼굴 파일 이름..
	public string keypetScript01 = "";					// keypetDirect에 해당하는 키펫(지금 말하고 있는 주체)의 대사..
	public string keypetScript02 = "";					// keypetDirect가 아닌 키펫의 대사..
	public int keypetTime = 0;							// 시간..
}

public class ScenarioGroup
{
	public List<ScenarioInfo> scenarioInfoList = null;
}


// 시나리오 관리하는 메니저 클래스...
public class ScenarioInfoMgr : InfoBase 
{
	// ScenarioGroup을 저장한 딕셔너리..
	protected Dictionary<int, ScenarioGroup> _data = new Dictionary<int, ScenarioGroup>();

	public ScenarioGroup GetScenarioGroup(int pSI)
	{
		ScenarioGroup group;
		
		if (!_data.TryGetValue (pSI, out group))
		{
			return null;
		}
		
		return group;
	}
	
/*	public ScenarioInfo GetScenarioInfo(int pSI)
	{
		ScenarioInfo info;
		
		if (_data.TryGetValue (pSI, out info))
		{
			return null;
		}
		
		return info;
	}
	
	public IEnumerable GetScenarioInfo ()
	{
		foreach (ScenarioInfo info in _data.Values)
			yield return info;
	}
*/	
	
	
	// TipType.LoadingTipMsg 타입의 데이터 수량 체크..
	//	protected int _LoadingTipCount = 0;
	//
	//	// 
	//	public int LoadingTipCount
	//	{
	//		get
	//		{
	//			return _LoadingTipCount;
	//		}
	//	}
	
	
	//
	public override bool SetJsonData(string pJSONdata)
	{
		return true;
	}
	
	
	// csv 파일에서 데이터 만들기...
	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader (pCSVData);
		bool isReading = true;

//		int currentScenarioNumber = 0;
//		int scenarioCount = 0;

		List<ScenarioInfo> scenarioInfoList = new List<ScenarioInfo> ();
		
		while (isReading)
		{
			if(reader.Peek() < 0)
			{
				//scenarioGroup.scenarioInfoList = scenarioList;
			//	_data.Add(currentScenarioNumber, scenarioGroup);
				
				//scenarioList.Clear();

				break;
			}
			
			string line = reader.ReadLine();
			
			if(string.IsNullOrEmpty(line))
				continue;
			
			string[] split = line.Split('\t'); // line.Split(',');
			
#if UNITY_EDITOR
			if (split.Length != 10)
				Debug.LogError (string.Format( "시나리오 데이터 파일 내에 {0}번째 시나리오 데이터의 길이가 10개 이상입니다.(중간에 ,가 있을 수도 있음..)", split[0] ) );
#endif
			
			
			ScenarioInfo scenarioInfo = new ScenarioInfo();
			int tokenIndex = 0;
			
			scenarioInfo.si = int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.scenarioType = (ScenarioType)System.Enum.Parse(typeof(ScenarioType), split[tokenIndex++].Trim());
			scenarioInfo.scenarioNumber = int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.scenarioSequence =  int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.bgName = split[tokenIndex++].Trim();
			scenarioInfo.keypetDirect = (KeypetDirect)System.Enum.Parse(typeof(KeypetDirect), split[tokenIndex++].Trim());
			scenarioInfo.keypetFace = split[tokenIndex++].Trim();
			scenarioInfo.keypetScript01 = split[tokenIndex++].Trim();
			scenarioInfo.keypetScript02 = split[tokenIndex++].Trim();
			scenarioInfo.keypetTime =  int.Parse(split[tokenIndex++].Trim());

			scenarioInfoList.Add (scenarioInfo);

/*			if (scenarioInfo.scenarioNumber != currentScenarioNumber)
			{
				++currentScenarioNumber;
			}
			else
			{
			
				_data.Add(currentScenarioNumber, scenarioGroup);


				++currentScenarioNumber;
			}
	*/					
			// 로딩시에 나오는 툴팁 메시지의 수량 체크..
			//if(tipInfo.tip_type == TipType.LoadingTipMsg)
			//	++_LoadingTipCount;
		}
		
		reader.Close ();

		{
			int currentScenarioNumber = 1;


			int scenarioCount = scenarioInfoList[scenarioInfoList.Count-1].scenarioNumber;

			ScenarioGroup[] scenarioGroup = new ScenarioGroup[scenarioCount];
			for (int i = 0; i < scenarioCount; i++)
			{
				scenarioGroup[i] = new ScenarioGroup();
				scenarioGroup[i].scenarioInfoList = new List<ScenarioInfo>();
				_data.Add (i, scenarioGroup[i]);
			}



			foreach (ScenarioInfo scenarioInfo in scenarioInfoList)
			{
				if (scenarioInfo.scenarioNumber != currentScenarioNumber)
				{
					currentScenarioNumber++;
				}

				scenarioGroup[currentScenarioNumber-1].scenarioInfoList.Add (scenarioInfo);
			}
		}

		return true;
	}
	
	
	public override bool Load(string pFile)
	{
		return true;
	}
	
	
	public override bool Save(string pFile)
	{
		return true;
	}
}


/*
// 시나리오 관리하는 메니저 클래스...
public class ScenarioInfoMgr : InfoBase 
{
	// ScenarioInfo를 저장한 딕셔너리..
	protected Dictionary<int, ScenarioInfo> _data = new Dictionary<int, ScenarioInfo>();

	//protected Dictionary<int, ScenarioGroup> _data = new Dictionary<int, ScenarioGroup>;

	public ScenarioInfo GetScenarioInfo(int pSI)
	{
		ScenarioInfo info;

		if (_data.TryGetValue (pSI, out info))
		{
			return null;
		}

		return info;
	}

	public IEnumerable GetScenarioInfo ()
	{
		foreach (ScenarioInfo info in _data.Values)
			yield return info;
	}



	// TipType.LoadingTipMsg 타입의 데이터 수량 체크..
//	protected int _LoadingTipCount = 0;
//
//	// 
//	public int LoadingTipCount
//	{
//		get
//		{
//			return _LoadingTipCount;
//		}
//	}

	
	//
	public override bool SetJsonData(string pJSONdata)
	{
		return true;
	}
	
	
	// csv 파일에서 데이터 만들기...
	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader (pCSVData);
		bool isReading = true;
		
		while (isReading)
		{
			if(reader.Peek() < 0)
				break;
			
			string line = reader.ReadLine();
			
			if(string.IsNullOrEmpty(line))
				continue;
			
			string[] split = line.Split(',');

#if UNITY_EDITOR
			if (split.Length != 10)
				Debug.LogError (string.Format( "시나리오 데이터 파일 내에 {0}번째 시나리오 데이터의 길이가 10개 이상입니다.(중간에 ,가 있을 수도 있음..)", split[0] ) );
#endif

			
			ScenarioInfo scenarioInfo = new ScenarioInfo();
			int tokenIndex = 0;
			
			scenarioInfo.si = int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.scenarioType = (ScenarioType)System.Enum.Parse(typeof(ScenarioType), split[tokenIndex++].Trim());
			scenarioInfo.scenarioNumber = int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.scenarioSequence =  int.Parse(split[tokenIndex++].Trim());
			scenarioInfo.bgName = split[tokenIndex++].Trim();
			scenarioInfo.keypetDirect = (KeypetDirect)System.Enum.Parse(typeof(KeypetDirect), split[tokenIndex++].Trim());
			scenarioInfo.keypetFace = split[tokenIndex++].Trim();
			scenarioInfo.keypetScript01 = split[tokenIndex++].Trim();
			scenarioInfo.keypetScript02 = split[tokenIndex++].Trim();
			scenarioInfo.keypetTime =  int.Parse(split[tokenIndex++].Trim());
						
			// 딕셔너리에 데이터 저장..
			_data.Add(scenarioInfo.si, scenarioInfo);
			
			// 로딩시에 나오는 툴팁 메시지의 수량 체크..
			//if(tipInfo.tip_type == TipType.LoadingTipMsg)
			//	++_LoadingTipCount;
		}
		
		reader.Close ();
		return true;
	}
	
	
	public override bool Load(string pFile)
	{
		return true;
	}
	
	
	public override bool Save(string pFile)
	{
		return true;
	}
}
*/
