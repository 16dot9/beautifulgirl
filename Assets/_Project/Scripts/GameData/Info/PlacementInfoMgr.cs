﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;


public class PlacementInfo
{
	public uint													si = 0;									// Serial Id.

	public string												attr_1 = "";							// 속성 1.
	public string												attr_2 = "";							// 속성 2.
	public int													plane_width = 0;						// 퍼즐 판 가로 크기.
	public int													plane_height = 0;						// 퍼즐 판 세로 크기.
	public AlgorithmOfPlacingButtons.PlacedButtonInfo[]			placed_button_info_list = null;			// 퍼즐 판 슬롯 정보.
}


public class PlacementInfoMgr : InfoBase
{

	protected Dictionary<uint, PlacementInfo>					_data = new Dictionary<uint, PlacementInfo>();


	public void SetData(List<PlacementInfo> pPlacementInfoList)
	{
		_data.Clear();

		foreach (PlacementInfo placementInfo in pPlacementInfoList)
		{
			_data.Add(placementInfo.si, placementInfo);
		}
	}

	public PlacementInfo GetPlacementInfo(uint pSI)
	{
		PlacementInfo placementInfo;

		if (!_data.TryGetValue(pSI, out placementInfo))
		{
			return null;
		}

		return placementInfo;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split =  line.Split('\t'); // line.Split(',');

			PlacementInfo placementInfo = new PlacementInfo();

			int tokenIndex = 0;

			placementInfo.si = uint.Parse(split[tokenIndex++].Trim());

			placementInfo.attr_1 = split[tokenIndex++].Trim();
			placementInfo.attr_2 = split[tokenIndex++].Trim();

			placementInfo.plane_width = int.Parse(split[tokenIndex++].Trim());
			placementInfo.plane_height = int.Parse(split[tokenIndex++].Trim());

			int maxCount = placementInfo.plane_width * placementInfo.plane_height;

			placementInfo.placed_button_info_list = new AlgorithmOfPlacingButtons.PlacedButtonInfo[maxCount];

			for (int i = 0; i < maxCount; ++i)
			{
				placementInfo.placed_button_info_list[i] = new AlgorithmOfPlacingButtons.PlacedButtonInfo();

				placementInfo.placed_button_info_list[i].placementType = (AlgorithmOfPlacingButtons.PlacementType)System.Enum.Parse(typeof(AlgorithmOfPlacingButtons.PlacementType), split[tokenIndex++].Trim());
				placementInfo.placed_button_info_list[i].iD = int.Parse(split[tokenIndex++].Trim());
			}

			_data.Add(placementInfo.si, placementInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			PlacementInfo placementInfo = new PlacementInfo();

			int tokenIndex = 0;

			placementInfo.si = uint.Parse(split[tokenIndex++].Trim());

			placementInfo.attr_1 = split[tokenIndex++].Trim();
			placementInfo.attr_2 = split[tokenIndex++].Trim();

			placementInfo.plane_width = int.Parse(split[tokenIndex++].Trim());
			placementInfo.plane_height = int.Parse(split[tokenIndex++].Trim());

			int maxCount = placementInfo.plane_width * placementInfo.plane_height;

			placementInfo.placed_button_info_list = new AlgorithmOfPlacingButtons.PlacedButtonInfo[maxCount];

			for (int i = 0; i < maxCount; ++i)
			{
				placementInfo.placed_button_info_list[i] = new AlgorithmOfPlacingButtons.PlacedButtonInfo();

				placementInfo.placed_button_info_list[i].placementType = (AlgorithmOfPlacingButtons.PlacementType)System.Enum.Parse(typeof(AlgorithmOfPlacingButtons.PlacementType), split[tokenIndex++].Trim());
				placementInfo.placed_button_info_list[i].iD = int.Parse(split[tokenIndex++].Trim());
			}

			_data.Add(placementInfo.si, placementInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		StreamWriter writer = File.CreateText(pFile);

		if (null == writer)
		{
			return false;
		}

		string line;

		foreach (PlacementInfo placementInfo in _data.Values)
		{
			line = placementInfo.si.ToString();						line += ",";

			line += placementInfo.attr_1;							line += ",";
			line += placementInfo.attr_2;							line += ",";

			line += placementInfo.plane_width.ToString();			line += ",";
			line += placementInfo.plane_height.ToString();			line += ",";

			for (int i = 0; i < placementInfo.placed_button_info_list.Length; ++i)
			{
				line += placementInfo.placed_button_info_list[i].placementType.ToString(); line += ",";
				line += placementInfo.placed_button_info_list[i].iD.ToString(); line += ",";
			}

			writer.WriteLine(line);
		}

		writer.Flush();

		writer.Close();

		return true;
	}

}

