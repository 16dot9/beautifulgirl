﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ShopType
{
	Invalid,

	Item,
	Funny,
	Gem,
    Power,

	Max
}


public class ShopInfo
{
	public uint				si = 0;

	public string			desc = "";
	public ShopType			type = ShopType.Invalid;

   	public uint			    pack_idx = 0;
	public string			pack_desc = "";
}


public class ShopInfoMgr : InfoBase
{

	protected Dictionary<uint, ShopInfo>			_data = new Dictionary<uint, ShopInfo>();

    public IEnumerable GetShopInfo()
    {
        foreach (ShopInfo shopInfo in _data.Values)
        {
            yield return shopInfo;
        }
    }

	public ShopInfo GetShopInfo(uint pSI)
	{
		ShopInfo shopInfo;

		if (!_data.TryGetValue(pSI, out shopInfo))
		{
			return null;
		}

		return shopInfo;
	}


	public override bool SetJsonData(string pJSONData)
	{
		return true;
	}

	public override bool Import(string pCSVData)
	{
		StringReader reader = new StringReader(pCSVData);

		bool isReading = true;

		while (isReading)
		{
			if (reader.Peek() < 0)
			{
				break;
			}

			string line = reader.ReadLine();

            
			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			ShopInfo shopInfo = new ShopInfo();

			int tokenIndex = 0;

			shopInfo.si = uint.Parse(split[tokenIndex++].Trim());
            shopInfo.desc = split[tokenIndex++].Trim();
			shopInfo.type = (ShopType)System.Enum.Parse(typeof(ShopType), split[tokenIndex++].Trim());

			shopInfo.pack_idx = uint.Parse(split[tokenIndex++].Trim());
			shopInfo.pack_desc = split[tokenIndex++].Trim();

			_data.Add(shopInfo.si, shopInfo);
        }

		reader.Close();

		return true;
	}

	public override bool Load(string pFile)
	{
		StreamReader reader = File.OpenText(pFile);

		if (null == reader)
		{
			return false;
		}

		bool isReading = true;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] split = line.Split('\t'); // line.Split(',');

			ShopInfo shopInfo = new ShopInfo();

			int tokenIndex = 0;

			shopInfo.si = uint.Parse(split[tokenIndex++].Trim());

			shopInfo.desc = split[tokenIndex++].Trim();
			shopInfo.type = (ShopType)System.Enum.Parse(typeof(ShopType), split[tokenIndex++].Trim());

			shopInfo.pack_idx = uint.Parse(split[tokenIndex++].Trim());
			shopInfo.pack_desc = split[tokenIndex++].Trim();

			_data.Add(shopInfo.si, shopInfo);
		}

		reader.Close();

		return true;
	}

	public override bool Save(string pFile)
	{
		return true;
	}

}

