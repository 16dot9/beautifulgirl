﻿using UnityEngine;
using System.Collections;


public class AutoDeleteCtrl : MonoBehaviour
{

    public float        autoDelTime = 0.5f;


    protected void Start()
    {
        StartCoroutine(AutoDelete());
    }


    protected IEnumerator AutoDelete()
    {
        yield return new WaitForSeconds(autoDelTime);
        GameObject.Destroy(gameObject);
    }

}
