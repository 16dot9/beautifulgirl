﻿using System.Text;


public class Helper
{

	public static void ConvertMoneyType(ref string pValue)
	{
		if ( null == pValue || 0 == pValue.Length )
		{
			return;
		}

		StringBuilder ret = new StringBuilder(pValue.Length * 2);

		int length = pValue.Length;

		for (int i = 0; i < length; ++i)
		{
			if ( 0 != i && (length - i) % 3 == 0)
			{
				ret.Append(',');
				ret.Append(pValue[i]);
			}
			else
			{
				ret.Append(pValue[i]);
			}
		}

		pValue = ret.ToString();
	}

}
