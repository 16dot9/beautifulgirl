﻿using UnityEngine;
using System.Collections;


public class FeverBGDirCtrl : MonoBehaviour
{

	public UISprite				Yellow = null;
	public UITexture			Green = null;
	public UITexture			Red = null;
	public float				Speed = 0.0f;


	protected bool				_isPause = false;

	protected bool				_isSrcColor = true;
	protected float				_colorChangeTime = 0.0f;
	protected Color				_srcColor = new Color(0f, 1.0f, 0.7f);
	protected Color				_destColor = new Color(0f, 0.75f, 0.6f);

	protected Vector2			_geenOffset = Vector2.zero;
	protected Vector2			_redOffset = Vector2.zero;


	public void Pause(bool pPause)
	{
		_isPause = pPause;
	}


	public void FixedUpdate()
	{
		if (_isPause)
		{
			return;
		}

		_colorChangeTime -= Time.deltaTime * 2f;

		if (_colorChangeTime < 0.0f)
		{
			_colorChangeTime = 0.2f;

			if (_isSrcColor)
			{
				_isSrcColor = false;

				Yellow.color = _srcColor;
			}
			else
			{
				_isSrcColor = true;

				Yellow.color = _destColor;
			}
		}

		_geenOffset.x = _geenOffset.x + Time.fixedDeltaTime * Speed;
		_geenOffset.y = _geenOffset.y + Time.fixedDeltaTime * Speed;

		Rect uvRect = Green.uvRect;

		uvRect.x = _geenOffset.x;
		uvRect.y = _geenOffset.y;

		Green.uvRect = uvRect;

		_redOffset.x = _redOffset.x + Time.fixedDeltaTime * -Speed;
		_redOffset.y = _redOffset.y + Time.fixedDeltaTime * -Speed;

		uvRect = Red.uvRect;

		uvRect.x = _redOffset.x;
		uvRect.y = _redOffset.y;

		Red.uvRect = uvRect;
	}

}
