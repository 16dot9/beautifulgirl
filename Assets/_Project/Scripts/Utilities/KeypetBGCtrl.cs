﻿using UnityEngine;
using System.Collections;


public class KeypetBGCtrl : MonoBehaviour
{

    protected Transform         _bgNormal = null;
    protected Transform         _bgPress = null;
	protected Transform         _bgSelected = null;
	protected Transform         _bgRescue = null;


    public void Selected(bool pIsSelected)
    {
        if (pIsSelected)
        {
            _bgSelected.gameObject.SetActive(true);
        }
        else
        {
            _bgSelected.gameObject.SetActive(false);
        }
    }

    public void Press(bool pIsPress)
    {
        if (pIsPress)
        {
            _bgNormal.gameObject.SetActive(false);
            _bgPress.gameObject.SetActive(true);
        }
        else
        {
            _bgNormal.gameObject.SetActive(true);
            _bgPress.gameObject.SetActive(false);
        }
    }

	public void Rescue(bool pIsRescue)
	{
		if (pIsRescue)
		{
			_bgRescue.gameObject.SetActive(true);
		}
		else
		{
			_bgRescue.gameObject.SetActive(false);
		}
	}

	public void FeverMode(bool pIsFeverMode)
	{
		if (pIsFeverMode)
		{
			transform.Find("BGPress").GetComponent<UISprite>().spriteName = "block_feverPress";
			transform.Find("BGSelected").GetComponent<UISprite>().spriteName = "block_feverHint";
		}
		else
		{
			transform.Find("BGPress").GetComponent<UISprite>().spriteName = "block_press";
			transform.Find("BGSelected").GetComponent<UISprite>().spriteName = "block_hint";
		}
	}

    protected void Awake()
    {
        _bgNormal = transform.Find("BGNormal");
        _bgPress = transform.Find("BGPress");
		_bgSelected = transform.Find("BGSelected");
		_bgRescue = transform.Find("BGRescue");

        _bgNormal.gameObject.SetActive(true);
        _bgPress.gameObject.SetActive(false);
		_bgSelected.gameObject.SetActive(false);
		_bgRescue.gameObject.SetActive(false);
    }

}
