﻿using UnityEngine;
using System.Collections;


public class KeypetCtrl : MonoBehaviour
{

	public float curTime = 1.0f;	// 마지막 키펫이 점수화 되는 딜레이 시간 
	public float moveToNextKeypet = 6.0f;		// 다음 키펫 까지의 시간
    public bool  IsBlank;


    public delegate void DELEGATEVOID();

    /*
	public bool IsBlank
	{
		get;
		set;
	}*/
	
    protected class MoveParamInfo
    {
		public bool					IsFeverMode = false;
        public Vector3              MoveSource = Vector3.zero;
        public Vector3              MoveTarget = Vector3.zero;
        public System.Action         MoveFinished = null;
    }


	protected Animator				_animator = null;
    protected SpriteRenderer        _spriteRenderer = null;
	protected Vector3				_localScale = Vector3.one;


	public int CurSlotIndex
	{
		get;
		set;
	}

	public KeypetType CurKeypetType
	{
		get;
		set;
	}

	public void Restore()
	{
		transform.localPosition = Vector3.zero;
		transform.localScale = _localScale;
		_spriteRenderer.material.color = Color.white;
	}

	public void ActIdle()
	{
		if (IsBlank)
		{
			return;
		}

        StopAllCoroutines();
        StartCoroutine(ActDirIdle());
	}

    public void ActPress(bool pIsInfinity)
	{
		if (IsBlank)
		{
			return;
		}

        StopAllCoroutines();
        StartCoroutine(ActDirPress(pIsInfinity));
	}

	public void ActMoveToNextKeypet(Vector3 pMoveSrc, Vector3 pMoveTarget, bool pIsFeverMode, System.Action pMoveFinished)
    {
		if (IsBlank)
		{
			return;
		}

        MoveParamInfo moveParamInfo = new MoveParamInfo();
		moveParamInfo.IsFeverMode = pIsFeverMode;
        moveParamInfo.MoveSource = pMoveSrc;
        moveParamInfo.MoveTarget = pMoveTarget;
        moveParamInfo.MoveFinished = pMoveFinished;


        StopAllCoroutines();
		StartCoroutine(ActDirMoveToNextKeypet(moveParamInfo));
    }

    public void ActMoveToScore(Vector3 pMoveSrc, Vector3 pMoveTarget, bool pIsFeverMode, System.Action pMoveFinished)
    {
		if (IsBlank)
		{
			return;
		}

        MoveParamInfo moveParamInfo = new MoveParamInfo();
		moveParamInfo.IsFeverMode = pIsFeverMode;
		moveParamInfo.MoveSource = pMoveSrc;
        moveParamInfo.MoveTarget = pMoveTarget;
        moveParamInfo.MoveFinished = pMoveFinished;

        StopAllCoroutines();
        StartCoroutine(ActDirMoveToScore(moveParamInfo));
    }

    public void ActFail()
    {
		if (IsBlank)
		{
			return;
		}

        StopAllCoroutines();
        StartCoroutine(ActDirFail());
    }


	protected void Start()
	{
		_animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
		_localScale = transform.localScale;
	}


    protected IEnumerator ActDirIdle()
    {
		if (IsBlank)
		{
			yield break;
		}

        _animator.SetTrigger("Normal");

        yield return null;
    }

    protected IEnumerator ActDirPress(bool pIsInfinity)
    {
		if (IsBlank)
		{
			yield break;
		}
		
		_animator.SetTrigger("Press");

        if (!pIsInfinity)
        {
            yield return new WaitForSeconds(1.0f);

            _animator.SetTrigger("Stay");
        }
    }

    protected IEnumerator ActDirMoveToNextKeypet(MoveParamInfo pMoveParamInfo)
    {
		if (IsBlank)
		{
			yield break;
		}

		Camera backCam = GuiMgr.Instance.GetBackCamera();

		var FindObjects = GameObject.Find("EffectCamera");

		if (null == FindObjects)
		{
			Debug.Log($"FindObject is Null");
		}

		Camera effectCam = FindObjects.GetComponent<Camera>();

        int curOrder = _spriteRenderer.sortingOrder;
        _spriteRenderer.sortingOrder = curOrder + 1;

        Vector3 dir = pMoveParamInfo.MoveTarget - pMoveParamInfo.MoveSource;
        float dist = dir.magnitude;
        dir.Normalize();

        if (dist > 0.0001f)
        {
			Vector3 effectSrcPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(transform.position));
			effectSrcPos.z = 0.0f;

			if (pMoveParamInfo.IsFeverMode)
			{
				GameObject.Instantiate(Resources.Load("Effects/feverEmpty"), effectSrcPos, Quaternion.identity);
			}
			else
			{
				GameObject.Instantiate(Resources.Load("Effects/keypetEmpty"), effectSrcPos, Quaternion.identity);
			}
			
			float curDist = 0.0f;

            while (curDist <= dist)
            {
                transform.position = pMoveParamInfo.MoveSource + dir * curDist;
				curDist += Time.deltaTime * moveToNextKeypet;		// 다음 키펫 까지의 시간

                yield return null;
            }

            transform.position = pMoveParamInfo.MoveTarget;
        }

        _animator.SetTrigger("Done");

		Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(transform.position));
		effectTargetPos.z = 0.0f;

		if (pMoveParamInfo.IsFeverMode)
		{
			GameObject.Instantiate(Resources.Load("Effects/feverEscape"), effectTargetPos, Quaternion.identity);
		}
		else
		{
			GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), effectTargetPos, Quaternion.identity);
		}

        yield return new WaitForSeconds(0.1f);

        _animator.SetTrigger("Normal");

        _spriteRenderer.sortingOrder = curOrder;

        pMoveParamInfo.MoveFinished?.Invoke();
    }

    protected IEnumerator ActDirMoveToScore(MoveParamInfo pMoveParamInfo)
    {
		if (IsBlank)
		{
			yield break;
		}

        yield return new WaitForSeconds(0.5f);

        int curOrder = _spriteRenderer.sortingOrder;
        _spriteRenderer.sortingOrder = curOrder + 1;

        Vector3 curPos = pMoveParamInfo.MoveSource;
        Vector3 tarPos = pMoveParamInfo.MoveTarget;

        Vector3 curScale = transform.localScale;
        Vector3 tarScale = curScale * 0.5f;

        Color curColor = _spriteRenderer.material.color;
        Color tarColor = curColor;
        tarColor.a = 0.0f;

        while (curTime >= 0.0f)
        {
            curTime -= Time.deltaTime;

            curPos = Vector3.Lerp(curPos, tarPos, (1.0f - curTime));
            curScale = Vector3.Lerp(curScale, tarScale, (1.0f - curTime));
            curColor = Color.Lerp(curColor, tarColor, (1.0f - curTime));

            transform.position = curPos;
            transform.localScale = curScale;
            _spriteRenderer.material.color = curColor;

			if ((tarPos - curPos).magnitude < 0.01f)
			{
				break;
			}

            yield return null;
        }

        transform.position = tarPos;
        transform.localScale = tarScale;
        _spriteRenderer.material.color = tarColor;

        _spriteRenderer.sortingOrder = curOrder;

        pMoveParamInfo.MoveFinished?.Invoke();
    }

    protected IEnumerator ActDirFail()
    {
		if (IsBlank)
		{
			yield break;
		}

        _animator.SetTrigger("Fail");

        yield return new WaitForSeconds(1.0f);

        _animator.SetTrigger("Normal");
    }

}
