﻿using UnityEngine;
using System.Collections;


public class UVAnimCtrl : MonoBehaviour
{

	public UITexture			Tex = null;
	public Vector2				Speed = Vector2.zero;


	protected Vector2			_offset = Vector2.zero;


	public void FixedUpdate()
	{
		_offset.x = _offset.x + Time.fixedDeltaTime * Speed.x;
		_offset.y = _offset.y + Time.fixedDeltaTime * Speed.y;

		Rect uvRect = Tex.uvRect;

		uvRect.x = _offset.x;
		uvRect.y = _offset.y;

		Tex.uvRect = uvRect;
	}

}
