using System;
using System.IO;
using System.Security.Cryptography;
 
public static class AES {
    private static string KEY = "sdfasdfjklqeruzxvndsqeraxcvzca23";
    private static string IV = "1234dudkaqlfeld6mieinaegames5837";
	//private static string KEY = "rudrlehtjdskatlqnsekdrntjgusehd2";
	//private static string IV = "1234dudkaqlfeld6mieinaegames5837";

	public static string encrypt(string clearText) {
		//if (string.IsNullOrEmpty(clearText)) return null;

        return EncryptRJ256(KEY, IV, clearText);
	}
	
	public static string decrypt(string encryptText) {
		//if (string.IsNullOrEmpty(encryptText)) return null;

		return DecryptRJ256(KEY, IV, encryptText);
	}
	
	public static string EncryptRJ256(string pkey, string piv, string clearText) {
		RijndaelManaged myRijndael = new RijndaelManaged();
		myRijndael.Padding = PaddingMode.Zeros;
		myRijndael.Mode = CipherMode.CBC;
		myRijndael.KeySize = 256;
		myRijndael.BlockSize = 256;
		
		byte[] key = System.Text.Encoding.UTF8.GetBytes(pkey);
		byte[] IV = System.Text.Encoding.UTF8.GetBytes(piv);
		
		ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);
		MemoryStream ms = new MemoryStream();
		CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
		
		byte[] encryptByte = System.Text.Encoding.UTF8.GetBytes(clearText);
		
		cs.Write (encryptByte, 0, encryptByte.Length);
		cs.FlushFinalBlock();
		
		byte[] encrypted = ms.ToArray ();
		
		return Convert.ToBase64String (encrypted);
	}
	
	public static string DecryptRJ256(string pkey, string piv, string encryptedText) {
		RijndaelManaged myRijndael = new RijndaelManaged();
		myRijndael.Padding = PaddingMode.Zeros;
		myRijndael.Mode = CipherMode.CBC;
		myRijndael.KeySize = 256;
		myRijndael.BlockSize = 256;
		
		byte[] key = System.Text.Encoding.UTF8.GetBytes(pkey);
		byte[] iv = System.Text.Encoding.UTF8.GetBytes(piv);
		
		ICryptoTransform decryptor = myRijndael.CreateDecryptor(key, iv);
		byte[] encryptByte = Convert.FromBase64String(encryptedText);
		byte[] encrypted = new byte[encryptByte.Length];
		
		MemoryStream ms = new MemoryStream(encryptByte);
		CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);
		cs.Read(encrypted, 0, encrypted.Length);
		
		return (System.Text.Encoding.UTF8.GetString(encrypted));
	}
}
