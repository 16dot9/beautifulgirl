﻿using UnityEngine;
using SimpleJSON;

// OK를 제외한 성공 코드는 0 보다 작아야 한다.
// 서버에서 정의된 에러 이외의 에러 코드는 MAX_SERVER_ERROR 보다 큰 값이어야 한다.
public enum CmdResultType
{
	OK_ALREADY_SETUP = -10000, // 패킷 받기 성공, 패킷을 보낸 곳에서 따로 처리할 필요가 없는 패킷

	OK = 0,

	ERROR_UUID,
	ERROR_NOT_FOUND, // 데이터가 없음.
	ERROR_UNKNOWN,
	ERROR_QUERY, // 중복 정보를 허용되지 않는 파라미터 오류. (예:id,uuid,nickname)
	ERROR_INVALID_PARAM,

	ERROR_NOT_COMPLETION,
	ERROR_NOT_LOGIN,
	ERROR_INSUFFICIENT_GAME_MONEY,
	ERROR_INSUFFICIENT_CASH,
	ERROR_INSUFFICIENT_BUDDY_FOOD,

	ERROR_INSUFFICIENT_MANA,
	ERROR_INSUFFICIENT_COOKIE,
	ERROR_INTERNAL_ERROR,
	ERROR_REQUIRED_LEVEL, // 레벨이 부족해서 물건은 사지 못함
	ERROR_GUEST_LEVEL,

	MAX_SERVER_ERROR,

	ERROR_UNKNOWN_ERROR = 1000,
	ERROR_ERROR_CODE, // www 객체의 error에 에러가 있는 경우
	ERROR_NULL_TEXTURE_ERROR,
	ERROR_EMPTY_TEXT,
	ERROR_NOT_RECEIVED_PACKET,

	// 클라이언트 자체 검사 에러
	CLIENT_ERROR_INVAILD_JSON_FORMAT = 2000,
	CLIENT_ERROR_INVAILD_RE_UPDATE_FORMAT,
	CLIENT_NOT_INIT,

	ERROR_INVALID_RECEIPT, // 27- 구매오류(해킹)
	ERROR_INVALID_ORDERID, // 28- 구매오류(해킹)
}

public class CmdResult
{
	public WWW m_www;
	public CmdResultType m_cmdResult;
	public JSONNode m_jsonNode;
}

