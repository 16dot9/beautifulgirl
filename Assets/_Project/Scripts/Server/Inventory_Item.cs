﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Inventory_Item
{
    public int _itemid;
    public int _itemtype;
    public int _equipped;
    public int _qty;

    public void inven_item(int _itemtype, int _itemid, int _equipped, int _qty)
    {
        this._itemid = _itemid;
        this._itemtype = _itemtype;
        this._equipped = _equipped;
        this._qty = _qty;
    }
}
