﻿

public sealed class CmdConfig
{
	public const float kFirstServerWatiTime = 40.0f; // 해당 시간 동안 서버에 응답이 없으면 팝업을 출력한다.
	public const float kLastServerWaitTime = 20.0f; // 이 시간 서버로 부터 응답이 없다면 로그인 화면으로 돌아온다
}
