﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;
using System.Security.Cryptography;
using SimpleJSON;
using System.Net.NetworkInformation;
using System.Xml;



public class Cmd_Handler_CS : GameStateBase
{
    private static Cmd_Handler_CS _Instance = null;

    public static Cmd_Handler_CS Instance
    {
        get
        {
            return  _Instance;
        }
    }


    public string _nickname = "";
    public int _nScoreCurrent = 0;
    public string _sStage = "";

    public string _userid = "";
    public string _assesToken = "";

    public const int USER_STATE_NEW_USER = 0;
    public const int USER_STATE_ACTIVATED = 1;
    public const int USER_STATE_BLOCKED=2;
    public const int USER_STATE_ALLOW_MSG=3;   
    public const int USER_STATE_ALLOW_MAIL=4;
    public const int USER_STATE_ATTENDANCE = 5; 

    // 카카오 서버의 개인 데이터 버퍼 를 이용 할 것인가?
    public static bool _isKakaoDataMode = false;
    // 서버와의 연결이 끊어지는 것을 판단하기 위한 내용.
    public static bool connectState = true; // 서버와 연결된 상태
    public static int curClientTime = 0;
    public static int curServerTime = 0; // 서버와 연결된 최근 시간
	
    static readonly object padlock = new object();

    public GuiMessage guiMessage;

    public static ProtoCal previousPacket = ProtoCal.None; // 이전에 통신한 패킷. 

    public static bool isServerDown = false; // 서버 연결 실패시 true.
    public class CmdStack : IDisposable
    {
        private readonly Cmd_Handler_CS m_cmd;
        private readonly ProtoCal m_sendPacket;
        private readonly CmdResult m_cmdResult;

        public CmdStack(Cmd_Handler_CS cmd, ProtoCal packet, CmdResult cmdResult)
        {
            m_cmd = cmd;
            cmd.m_isSendPacket = true;
            m_sendPacket = packet;
            m_cmdResult = cmdResult;
        }

        public void Dispose()
        {
            //debug.log("received packet : " + m_sendPacket.ToString());
            previousPacket = m_sendPacket;
            m_cmd.m_isSendPacket = false;

            if ((int)m_cmdResult.m_cmdResult > (int)CmdResultType.OK)
            {
                //debug.log("packet(" + m_sendPacket + ") error = " + m_cmdResult.m_www.text);
                isServerDown = true;
            }
            else
            {
                isServerDown = false;
            }
        }
    }
    
    private GUISkin customSkin;

    //cookie
    // will be set to some session id after login
    Hashtable session_ident = new Hashtable();

    // and some helper functions and properties
    void  ClearSessionCookie (){

        try
        {
            session_ident["Cookie"] = null;
        }
        catch (Exception e)
        {
            Debug.Log(" e : " + e.ToString());
        }

    }

    void  SetSessionCookie (string s){
        session_ident["Cookie"] = s;
    }

    Hashtable  SessionCookie ()
    {
        return session_ident;
    }

    //Hashtable  GetSessionCookie ()
    //{
    //    return session_ident["Cookie"];
    //}

    //Hashtable  SessionCookieIsSet ()
    //{
    //    return session_ident["Cookie"] != null;
    //}

    //FIXME_VAR_TYPE session_cookie;

    //--------------------
    // ID/PASS Login
    //--------------------
    public int g_authType= 2;				        // 1:kakao 2:id/pass 3:google plus 4:snail
    public string g_userid = "12345678901234567";	// kakao일 경우 최대 18자리, id/passwd일 경우에는 40자리 가능.
    public string g_passwd = "p4ssword";
    public string g_accessToken = "";
    public string g_nickname = "하youjong活动♡";
    public string g_email = "youjong@test.com";

    //--------------------
    // KAKAO Login
    //--------------------
    /* //android
    private FIXME_VAR_TYPE g_authType= 1;						// 1:kakao 2:id/pass 0:guest
    private FIXME_VAR_TYPE g_userid= ""; 
    private FIXME_VAR_TYPE g_accessToken= "8AI424SB9fUva53PF2lzZFzFI5p-RkdT79GpvW_AZ_UKPIq2ZBZ7JHioyS0PYFQOK0ZTkl1mJZM7sJ0-4a6iGg"; //로그인할 때마다 바뀜
    private FIXME_VAR_TYPE g_passwd= "";						// id/passwd일 경우에만 셋팅
    */

    private int g_device_type= 2;						// 1:android, 2:ios
    //device_token : gcm registration id(android) 또는 APNS device token (ios)입니다.
    //	APNS device token은 binary format이기 때문에 base64로 encoding해서 전송해야 합니다.
    //	(gcm registration id는 base64로 encoding 되어 있음)
    //  http://answers.unity3d.com/questions/40568/base64-encodedecoding.html
    public string g_device_id; //= "APA91bHN_n8WxcOBZNBS6XeJseL0G0V54pXT7a1LXKkxV_KyWvJdkjC7AlQsdMZnZ9CcSGTkGKbaZuxV5v_psM4n19B2kjy0WqS0N1PGeYb0Iq7ULvBJEKI8GNdU0W_EwHCfVqRjVatwQr2fmZKWGWoWNymbr9NUsZTm0QBrPAXhwi9qDvth6uA";
    private string formHash= "KeyPet"; 	// client 구분용

    //-------------------------------- 
    // gem 구매 sample data
    //---------------------------------
    // 구글 구매데이터.(g_devicetype = 1)
    private string g_purchaseData = "purchaseData Test!";//'{"orderId":"12999763169054705758.1335602951508756f","packageName":"kr.co.vivagame.thedaydf2.forkakao","productId":"kr.co.vivagame.thedaydf2.crystal001001","purchaseTime":1386307505587,"purchaseState":0,"purchaseToken":"zjvkmvchvtzwoxrikfycwtxl.AO-J1OyEJ0vW68_hUObxMcsvhYFhnuxDgTrT5WxtRN4qLWgad4nA4cEfl_G15j9nX4NMpl9b7ci0xaX69DWjejx0LAqgyVbmsrCVU-RfcWRjog5JGChTHbEFofNgpBmZMYMgrhzaLQsWRRxSzbFTcEOemHVcnhAajj5mIC6AC_s3Nh3GCBNifig"}';
    private string g_dataSignature= "HjKSQVM2zPcCke94btm+qfpZ8d/wuceTp2McAhcfcEuSm/9M+qWJ9EwF2DPRBDtW0BiPq4D7KlC/bjofRaDrAkHrd3D2yUtrwEMlX/IcPpNrjVju874Jv8A9Bhzv7bfSUu9WoySGbCTz7UpgKlTJztzL2sjAjjXa0BqbpfyjGGu8pTZ4nrWszPw7mMbGLtIs11qB6K6guN+kp8mItkPMKF+IlPTiIDkwLpP2nBhtC/VG3KAbjLrxvMZiWb68for6OOFhIzjZj8kg2+oQgsCjIwUg/42vWdCoyNoGHASDQolUatlEdN7hUub6tQLHgx4tVLVl/otGxr9K+Gc9AzdnVQ==";

    //애플 구매데이터 (앱스토어에서 검사된  receipt 데이터)
    /* 
    { "receipt":{"original_purchase_date_pst":"2013-12-03 23:58:16 America/Los_Angeles", 
        "purchase_date_ms":"1386143896604", "unique_identifier":"d7e8a867fce730114609d79e71aeae517c6846af", 
        "original_transaction_id":"1000000095494691", "bvrs":"1.0f", "transaction_id":"1000000095494691", "quantity":"1", 
        "unique_vendor_identifier":"AE8DF95A-2FC4-43E5-816E-A3A81683823E", "item_id":"769256887", 
        "product_id":"kr.co.vivagame.TheDayDF2.Crystal250", "purchase_date":"2013-12-04 07:58:16 Etc/GMT", 
        "original_purchase_date":"2013-12-04 07:58:16 Etc/GMT", "purchase_date_pst":"2013-12-03 23:58:16 
        America/Los_Angeles", "bid":"kr.co.vivagame.TheDayDF2", 
        "original_purchase_date_ms":"1386143896604"}, "status":0}
    */
    //app sotre recipt.(g_devicetype = 2)
    //private FIXME_VAR_TYPE g_app_store_receipt= "ewoJInNpZ25hdHVyZSIgPSAiQXFONEZlb0ZuT2JpNWJOUEFGYjh3TWtFU1BkT1VWUzZGOUY4VGcvZUFOSnRYd1JWK1NQOVJoSkNHUkxtTUdudTU0aHk0SDBhZzhYZVF0VGZIczhsb084ZmdhV2tjOGxvSE1TUHJENVc4OTF2MnZMZFpkMWwrbEU0aHZyNUdJYmxHTzBWZWQ3dHl1c0dwa2JhNHFRbFRzZ0s2YjhVdVpVMUJTWHN0LzBFUjNYaEFBQURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RRRUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlFRjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIzSmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5USXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVIVnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVrRndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NXNWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZMEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1YvcnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRkS1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNqQndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBnRVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERnUVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0JnVUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lCM1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNlVFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIwN2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FRVnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xvSHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5qK2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2JwMGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJwdXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREV6TFRFeUxUQXpJREl6T2pVNE9qRTJJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0prTjJVNFlUZzJOMlpqWlRjek1ERXhORFl3T1dRM09XVTNNV0ZsWVdVMU1UZGpOamcwTm1GbUlqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTVRBd01EQXdNREE1TlRRNU5EWTVNU0k3Q2draVluWnljeUlnUFNBaU1TNHdJanNLQ1NKMGNtRnVjMkZqZEdsdmJpMXBaQ0lnUFNBaU1UQXdNREF3TURBNU5UUTVORFk1TVNJN0Nna2ljWFZoYm5ScGRIa2lJRDBnSWpFaU93b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGJYTWlJRDBnSWpFek9EWXhORE00T1RZMk1EUWlPd29KSW5WdWFYRjFaUzEyWlc1a2IzSXRhV1JsYm5ScFptbGxjaUlnUFNBaVFVVTRSRVk1TlVFdE1rWkROQzAwTTBVMUxUZ3hOa1V0UVROQk9ERTJPRE00TWpORklqc0tDU0p3Y205a2RXTjBMV2xrSWlBOUlDSnJjaTVqYnk1MmFYWmhaMkZ0WlM1VWFHVkVZWGxFUmpJdVEzSjVjM1JoYkRJMU1DSTdDZ2tpYVhSbGJTMXBaQ0lnUFNBaU56WTVNalUyT0RnM0lqc0tDU0ppYVdRaUlEMGdJbXR5TG1OdkxuWnBkbUZuWVcxbExsUm9aVVJoZVVSR01pSTdDZ2tpY0hWeVkyaGhjMlV0WkdGMFpTMXRjeUlnUFNBaU1UTTROakUwTXpnNU5qWXdOQ0k3Q2draWNIVnlZMmhoYzJVdFpHRjBaU0lnUFNBaU1qQXhNeTB4TWkwd05DQXdOem8xT0RveE5pQkZkR012UjAxVUlqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbExYQnpkQ0lnUFNBaU1qQXhNeTB4TWkwd015QXlNem8xT0RveE5pQkJiV1Z5YVdOaEwweHZjMTlCYm1kbGJHVnpJanNLQ1NKdmNtbG5hVzVoYkMxd2RYSmphR0Z6WlMxa1lYUmxJaUE5SUNJeU1ERXpMVEV5TFRBMElEQTNPalU0T2pFMklFVjBZeTlIVFZRaU93cDkiOwoJImVudmlyb25tZW50IiA9ICJTYW5kYm94IjsKCSJwb2QiID0gIjEwMCI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9";

     
    //-------------------- 
    // xdebug support
    //--------------------
    public bool DEBUG= false;

    //--------------------
    // BASE URL 현재 키펫 주소
    // http://keypet.open-b.com/keypet
    // http://192.168.0.45/keypet
    //--------------------
    public string GAME_SERVER_BASE_URL= "http://115.68.25.250/keypat";

    public bool _isFail_Last = false;

    //--------------------
    //GUI
    //--------------------

    private int margin= 5;

    //cmd buttons
    private float buttonWidth= 120;
    private float buttonHeight= 20;
    //scroll view
//        string sOutputString= "insert very long text here";
    private Vector2 scrollPosition = Vector2.zero;
    private float viewWidth= 0;
    private float viewHeight= 0;
    private GUIStyle style;
    private Texture2D bmOutputTexture;
 
    //--------------------
    //id
    //--------------------
    //texture output
    private int g_uid= 0; //login할 때 셋팅됨.
    private int g_power;
    private int g_nfunny;
    private int gem;
    public int g_new_msg;
    private int g_dayCnt;
    public int g_invCnt;
    private int g_slot1;
    private int g_slot2;
    private int g_slot3;
    private int g_OpenStage;

    public int g_inv_cnt;

    private int cmdid= 0; //protocol number
    private int g_postid= 0;	//메시지 조회때 셋팅됨. 마지막 것.

    public List<int> g_PackHeartCount = new List<int>(); //패키지 29번 갯수를 카운트 한다.

    //--------------------
    //protcol constants
    //--------------------

    public enum ProtoCal 
    {
        None = 0,
        PROTOCOL_LOGIN_REQ,
        PROTOCOL_LOGIN_RES,

        PROTOCOL_UNLOCK_STAGE_REQ,
        PROTOCOL_UNLOCK_STAGE_RES,       
                   
        PROTOCOL_LIST_ITEM_REQ,
        PROTOCOL_LIST_ITEM_RES,       
                   
        PROTOCOL_RESET_ITEM_REQ,
        PROTOCOL_RESET_ITEM_RES,       
                   
        PROTOCOL_SEND_POWER_REQ,
        PROTOCOL_SEND_POWER_RES = 10,       
                   
        PROTOCOL_GET_GAMEINFO_REQ,
        PROTOCOL_GET_GAMEINFO_RES,       
                   
        PROTOCOL_LIST_SHOP_REQ,
        PROTOCOL_LIST_SHOP_RES,       
                   
        PROTOCOL_BUY_PACK_REQ,
        PROTOCOL_BUY_PACK_RES,       
                   
        PROTOCOL_BUY_GEM_REQ,
        PROTOCOL_BUY_GEM_RES,       
                   
        PROTOCOL_GACHA_REQ,
        PROTOCOL_GACHA_RES = 20,       
                   
        PROTOCOL_SHOW_ME_THE_MONEY_REQ,
        PROTOCOL_SHOW_ME_THE_MONEY_RES,       
                   
        PROTOCOL_LIST_MSG_REQ,
        PROTOCOL_LIST_MSG_RES,       
                   
        PROTOCOL_READ_MSG_REQ,
        PROTOCOL_READ_MSG_RES,       
                   
        PROTOCOL_READ_MSG_ALL_REQ,
        PROTOCOL_READ_MSG_ALL_RES,       
                   
        PROTOCOL_START_GAME_REQ,
        PROTOCOL_START_GAME_RES = 30,       
                   
        PROTOCOL_END_GAME_REQ,
        PROTOCOL_END_GAME_RES,       
                   
        PROTOCOL_PUSH_REGISTER_REQ,
        PROTOCOL_PUSH_REGISTER_RES,       
                   
        PROTOCOL_PUSH_UNREGISTER_REQ,
        PROTOCOL_PUSH_UNREGISTER_RES,       
                   
        PROTOCOL_MSG_ON_REQ,
        PROTOCOL_MSG_ON_RES,       
                   
        PROTOCOL_UNREGISTER_REQ,
        PROTOCOL_UNREGISTER_RES = 40,       
                   
        PROTOCOL_LOGOUT_REQ,      
        PROTOCOL_LOGOUT_RES,      
                   
        PROTOCOL_INVITE_FRIEND_REQ,       
        PROTOCOL_INVITE_FRIEND_RES,       
                   
        PROTOCOL_UNLOCK_STAGE_GEM_REQ,       
        PROTOCOL_UNLOCK_STAGE_GEM_RES,

        PROTOCOL_LIST_HELP_REQ,
        PROTOCOL_LIST_HELP_RES,

        PROTOCOL_USE_ITEM_REQ,
        PROTOCOL_USE_ITEM_RES,

        PROTOCOL_TUTORIAL_REQ,
        PROTOCOL_TUTORIAL_RES = 52,

        PROTOCOL_UPDATE_STAGE_GAME_REQ = 59,
        PROTOCOL_UPDATE_STAGE_GAME_RES = 59,
    }

    void Awake() 
    {
        _Instance = this;
    }

    void  Start ()
    {
        
    }

    void  Update (){
	        
    }

    string Md5_Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    private string aes_key = "abcdefgghnklmnopqrstuvwxyz123456";

    private string AESMD5_Decode(string input)
    {
        //debug.log("input : " + input);

        RijndaelManaged aes = new RijndaelManaged();
        aes.KeySize = 256;
        aes.BlockSize = 128;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;
        aes.Key = Encoding.UTF8.GetBytes(aes_key);
        aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


        var decrypt = aes.CreateDecryptor();
        byte[] xBuff = null;

        using (var ms = new MemoryStream())
        {
            using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
            {
                byte[] xXml = Convert.FromBase64String(input);

                //debug.log("xXml Length : " + xXml.Length);

                cs.Write(xXml, 0, xXml.Length);
            }

            xBuff = ms.ToArray();
        }

        String Output = Encoding.UTF8.GetString(xBuff);

        string outputValue = Output.Substring(0, Output.Length - 32);
        string outputValueMd5 = Md5_Sum(outputValue);

        string outputMd5 = Output.Substring(Output.Length - 32, 32);

        ////debug.log(outputValue+" "+outputValueMd5+" "+outputMd5);

        if (outputValueMd5 != outputMd5)
            return "";
        return outputValue;
    }


    public bool _bLoginComplete = false;

    public IEnumerator Send_Refresh_Login_Req()
    {
        if (_isKakaoDataMode == false)
        {
            _sOutputString = "";
            
            var form = new WWWForm();

            form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LOGIN_REQ));
            form.AddField("hash", formHash);
#if UNITY_EDITOR
            g_authType = 2;
            form.AddField("authtype", g_authType);
            form.AddField("userid", g_userid);
            form.AddField("passwd", AES.encrypt(ACToken));
            form.AddField("nickname", AES.encrypt(_nickname));
            form.AddField("email", "Test@test.com");
            form.AddField("app_ver", "1");
            form.AddField("device_type", 1);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", "TESTPC");

#elif UNITY_ANDROID
                g_authType = 1;
            form.AddField("authtype",       g_authType);
            form.AddField("userid",         _userid);
            form.AddField("passwd",         AES.encrypt(ACToken));
            form.AddField("nickname",       AES.encrypt(_nickname));
            form.AddField("email",          "Test@test.com");
            form.AddField("app_ver",        "1");
            form.AddField("device_type",    1);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", "");
#elif UNITY_IPHONE
            g_authType = 1;
            form.AddField("authtype",      g_authType);
            form.AddField("userid",        _userid);
            form.AddField("passwd",        AES.encrypt(ACToken));
            form.AddField("nickname",      AES.encrypt(_nickname));
            form.AddField("email",         "Test@test.com");
            form.AddField("app_ver",       "1");
            form.AddField("device_type",   2);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", "TESTIPHONE");

#endif
            string _pc = SystemInfo.deviceModel.ToString();
            string Test = "";

            if (_pc.Length > 16)
            {
                Test = _pc.Substring(0, 16);
            }
            else 
            {
                Test = _pc;
            }

            form.AddField("dev_model", Test);
            form.AddField("dev_mac", "68:A8:6D:C9:88:68");
            form.AddField("dev_os", "AndroidOS");

            string url = GAME_SERVER_BASE_URL + "/login.php";

            WWW www = new WWW(GAME_SERVER_BASE_URL + "/login.php", form.data);
            yield return www;

            ClearSessionCookie();

            try
            {
                if (www.responseHeaders.ContainsKey("SET-COOKIE"))
                {
                    char[] splitter = { ';' };
                    string[] v = www.responseHeaders["SET-COOKIE"].Split(splitter);
                    foreach (string s in v)
                    {
                        if (string.IsNullOrEmpty(s))
                            continue;
                        if (s.Contains("PHPSESSID"))
                        {
                            // found it
                            SetSessionCookie(s);
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("Error :" + e.ToString());
            }

            // end 
            if (www.error != null)
            {
                PlayErrorMessage(_DefaultMessage,0, true);
            }
            else
            {
                _sOutputString = www.text;

                //JSON parsing
                JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

                var resultCode = int.Parse(j["rc"]);

//                    //debug.log("RESULT CODE :" + resultCode);

                if (resultCode == 0)
                {
                    //GuiMgr.Instance.Show<GuiRoulette>(GuiMgr.ELayerType.Front, false, null); // 룰렛 창이 있다면 끈다.

                    //this.GetComponent<KakaoMain>().GoToStageMap();	//게임 맵으로 전환...

                    _bLoginComplete = true;
                    g_uid           = int.Parse(j["uid"]);

                    g_power         = int.Parse(j["power"]);
                    g_nfunny        = int.Parse(j["Funny"]);
                    gem             = int.Parse(j["Gem"]);
                    g_new_msg       = int.Parse(j["new_msg"]);
                    g_dayCnt        = int.Parse(j["day_cnt"]);
                    g_invCnt        = int.Parse(j["inv_cnt"]);
                    g_slot1         = int.Parse(j["slot1"]);
                    g_slot2         = int.Parse(j["slot2"]);
                    g_slot3         = int.Parse(j["slot3"]);
                    g_OpenStage     = int.Parse(j["open_stage"]);


                    Debug.Log( g_power    );    
                    Debug.Log( g_nfunny   );    
                    Debug.Log( gem        );    
                    Debug.Log( g_new_msg  );    
                    Debug.Log( g_dayCnt   );    
                    Debug.Log( g_invCnt   );    
                    Debug.Log( g_slot1    );    
                    Debug.Log( g_slot2    );    
                    Debug.Log( g_slot3    );
                    Debug.Log(g_OpenStage);    

                    MyInfoMgr._gachaFunnySu = int.Parse(j["gacha_cost"]);
                    //                        //debug.log("~~~~~~~~~~~~~~~~~~~~ gacha_cost:" + MyInfoMgr._gachaFunnySu);
                    // 룰렛 수 설정.
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu = uint.Parse(j["roulette_cnt"]);

                    int user_status = int.Parse(j["user_status"]);

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = g_OpenStage;
                    MyInfoMgr.Instance.m_Funny = (int)g_nfunny;
                    MyInfoMgr.Instance.m_Gem = (int)gem;
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().getDay = g_dayCnt % GameObject.Find("InfoMgr").GetComponent<InfoSave>()._maxAttendanceDay;
                        //===========g_dayCnt;

                    MyInfoMgr.Instance.m_user_status = user_status;

                    KakaoFriendsController.Instance.LoadKakaoFriends();

                    yield return new WaitForSeconds(0.1f);

                    StartCoroutine(Send_PROTOCOL_LIST_ITEM_REQ());
                }
                else 
                {
                    PlayErrorMessage(_DefaultMessage,0,true);
                }
            }
        }
        else
        {
            // 카톡 서버의 개인 버퍼이용 데이터 처리.
            string publicData = KakaoGameUserInfo.Instance.publicData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.publicData);
            string privateData = KakaoGameUserInfo.Instance.privateData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.privateData);
        }
    

    }

    private string ACToken = "";
    public bool isLogin = false;

    //---------------------------------------------------------------------------------
    // protocol handler functions
    //---------------------------------------------------------------------------------
    public IEnumerator  Send_PROTOCOL_LOGIN_REQ(string _UserNickName,string _UserId, string accessToken,int stage)
    {
          Debug.Log("Send Login");

//        #region TNk_Setting <- tnk 제거 할거면 이거 제거 하면 됩니다.
//#if UNITY_EDITOR
//        Debug.Log("TNK Setting");
//#elif UNITY_ANDROID 
//        TnkAd.Plugin.Instance.setUserName(_UserId);
//        TnkAd.Plugin.Instance.prepareInterstitialAd("notice_start");
//        TnkAd.Plugin.Instance.showInterstitialAd();

//#endif
//        #endregion

        _bLoginComplete = false;

        _nickname = _UserNickName;
        _userid = _UserId;

        ACToken = accessToken;

        if (_isKakaoDataMode == false)
        {
            // 카톡 서버의 개인 버퍼이용 데이터 처리.
            string publicData = KakaoGameUserInfo.Instance.publicData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.publicData);
            string privateData = KakaoGameUserInfo.Instance.privateData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.privateData);

            _sOutputString = "";
            
            var form = new WWWForm();

            form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LOGIN_REQ));
            form.AddField("hash", formHash);
            //    1:kakao 2:id/pass 3:google plus 4:snail
//#if UNITY_EDITOR
            /*
#elif UNITY_ANDROID
                g_authType = 2;
            form.AddField("authtype",       g_authType);
            form.AddField("userid",         _userid);
            form.AddField("passwd",         AES.encrypt("FacebookLogin"));
            form.AddField("nickname",       AES.encrypt(_nickname));
            form.AddField("email",          "Test@test.com");
            form.AddField("app_ver",        "1");
            form.AddField("device_type",    1);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", GCM.Instance.senderIdsStr);
//            form.AddField("device_id", "TESTANDROID" ); // GCM.Instance.senderIdsStr);

#elif UNITY_IPHONE
            g_authType = 1;
            form.AddField("authtype",      g_authType);
            form.AddField("userid",        _UserId);
            form.AddField("passwd",        accessToken);
            form.AddField("nickname",      _UserNickName);
            form.AddField("email",         "Test@test.com");
            form.AddField("app_ver",       "1");
            form.AddField("device_type",   2);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", "TESTIPHONE");

#endif*/

            g_authType = 2;
            form.AddField("authtype", g_authType);
            form.AddField("userid", _userid);
            form.AddField("passwd", AES.encrypt("FacebookLogin"));
            form.AddField("nickname", AES.encrypt(_nickname));
            form.AddField("email", "Test@test.com");
            form.AddField("app_ver", "1");
            form.AddField("device_type", 1);//SystemInfo.deviceType.ToString());
            form.AddField("device_id", "TESTPC");
            form.AddField("stage", stage);

            string _pc = SystemInfo.deviceModel.ToString();
            string Test = "";

            if (_pc.Length > 16)
            {
                Test = _pc.Substring(0, 16);
            }
            else 
            {
                Test = _pc;
            }

            form.AddField("dev_model", Test);
            form.AddField("dev_mac", "68:A8:6D:C9:88:68");
            form.AddField("dev_os", "AndroidOS");

            Debug.Log(GAME_SERVER_BASE_URL + "/login.php");

            string url = GAME_SERVER_BASE_URL + "/login.php";
            WWW www = new WWW("115.71.233.219/fbkeypet/keypet/game"  + "/login.php", form.data);
            yield return www;

            ClearSessionCookie();

            // check if session cookie was send, if not, well, no use to continue then
            try
            {
                if (www.responseHeaders.ContainsKey("SET-COOKIE"))
                {
                    char[] splitter = { ';' };
                    string[] v = www.responseHeaders["SET-COOKIE"].Split(splitter);
                    foreach (string s in v)
                    {
                        if (string.IsNullOrEmpty(s))
                            continue;
                        if (s.Contains("PHPSESSID"))
                        {
                            // found it
                            SetSessionCookie(s);
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("Error :" + e.ToString());
            }

            // end 
            if (www.error != null)
            {
                PlayErrorMessage(_DefaultMessage,0, true);
            }
            else
            {
                _sOutputString = www.text;
                Debug.Log(_sOutputString);

                //JSON parsing
                JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

                var resultCode = int.Parse(j["rc"]);
                
                if (resultCode == 0)
                {
                   // this.GetComponent<KakaoMain>().GoToStageMap();	//게임 맵으로 전환...

                    _bLoginComplete = true;
                    g_uid = int.Parse(j["uid"]);

                    g_power = int.Parse(j["power"]);
                    g_nfunny = int.Parse(j["Funny"]);
                    gem = int.Parse(j["Gem"]);
                    g_new_msg = int.Parse(j["new_msg"]);
                    g_dayCnt = int.Parse(j["day_cnt"]);
                    g_invCnt = int.Parse(j["inv_cnt"]);
                    g_slot1 = int.Parse(j["slot1"]);
                    g_slot2 = int.Parse(j["slot2"]);
                    g_slot3 = int.Parse(j["slot3"]);
                    g_OpenStage = int.Parse(j["open_stage"]);
                    MyInfoMgr._gachaFunnySu = int.Parse(j["gacha_cost"]);
                    
                    //                        //debug.log("~~~~~~~~~~~~~~~~~~~~ gacha_cost:" + MyInfoMgr._gachaFunnySu);
                    // 룰렛 수 설정.
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu = uint.Parse(j["roulette_cnt"]);

                    int user_status = int.Parse(j["user_status"]);
                    int stageidx = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber ;

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = g_OpenStage;
                    MyInfoMgr.Instance.m_Funny =    (int)g_nfunny;
                    MyInfoMgr.Instance.m_Gem =      (int)gem;
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().getDay = g_dayCnt % GameObject.Find("InfoMgr").GetComponent<InfoSave>()._maxAttendanceDay;
                        //=== g_dayCnt;

                    MyInfoMgr.Instance.m_user_status = user_status;

                    isLogin = true;

                    if (stageidx > g_OpenStage)
                        yield return StartCoroutine(Send_STAGE_UPDATE_REQ(stageidx));
                    
                    yield return new WaitForSeconds(0.1f);

                    StartCoroutine(Send_PROTOCOL_LIST_ITEM_REQ());
                }
                else 
                {
                    PlayErrorMessage(_DefaultMessage,0,true);
                }
                    
                //foreach (string str in www.responseHeaders.Keys)
                //{
                //    //debug.log("KEY:" + str + ", VALUE:" + www.responseHeaders[str]);
                //}
            }
        }
        else
        {
            // 카톡 서버의 개인 버퍼이용 데이터 처리.
            string publicData = KakaoGameUserInfo.Instance.publicData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.publicData);
            string privateData = KakaoGameUserInfo.Instance.privateData == null ? null : Encoding.UTF8.GetString(KakaoGameUserInfo.Instance.privateData);

//                //debug.log("~~~~~~~~~~~~~~~~~~~~ publicData : [" +publicData + "] privateData : ["+publicData +"]");

        }

        // 로딩 화면 닫기.
        // 씬 로딩 할때 다시 로딩 화면을 오픈 하고, 크로즈 함으로 여기서는 크로즈 하지 않는다.
        // GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, false, null);
    }

    public bool m_isSendPacket = false;
        
    public IEnumerator Send(ProtoCal packet, WWWForm wwwForm, string urlAppend, CmdResult cmdResult)
    {
        // 보낸 패킷이 올 때 가지 기다린다
        while (m_isSendPacket)
        {
            yield return null;
        }

        using (new CmdStack(this, packet, cmdResult))
        {
            cmdResult.m_cmdResult = CmdResultType.CLIENT_NOT_INIT;

            string url = GAME_SERVER_BASE_URL + urlAppend;
            //string url = CmdHandler.IP + urlAppend;
#if UNITY_EDITOR
            switch (packet)
            {
                case ProtoCal.PROTOCOL_LOGIN_REQ:
                    break;
                case ProtoCal.PROTOCOL_LOGOUT_REQ:
                    url += "?&DEBUG_SESSION_START";
                    break;
                default:
                    url += "&DEBUG_SESSION_START";
                    break;
            }
#endif

            cmdResult.m_www = (packet == ProtoCal.PROTOCOL_LOGIN_REQ)
                                                    ? new WWW(url, wwwForm.data)
                                                    : new WWW(url, wwwForm.data, SessionCookie());

            // 로그아웃 예외처리.
            if (packet == ProtoCal.PROTOCOL_LOGOUT_REQ)
                yield break;

            float waitTime = Time.time + CmdConfig.kFirstServerWatiTime;
            while (cmdResult.m_www.isDone == false && waitTime > Time.time)
            {
                yield return null;
            }

            // 일정 시간 동안 서버로 부터 패킷을 받지 못한 경우
            if (cmdResult.m_www.isDone == false)
            {
                //debug.log("not received packet!!");
                cmdResult.m_cmdResult = CmdResultType.ERROR_NOT_RECEIVED_PACKET;
                yield break;
            }

            if (packet == ProtoCal.PROTOCOL_LOGIN_REQ)
            {
                // 일반 패킷과 처리 과정이 다르므로 예외 처리를 한다.
                ClearSessionCookie();
                if (cmdResult.m_www.responseHeaders.ContainsKey("SET-COOKIE"))
                {
                    ////debug.log("ContainsKey cookie : ");
                    char[] splitter = { ';' };
                    string[] v = cmdResult.m_www.responseHeaders["SET-COOKIE"].Split(splitter);
                    foreach (string s in v)
                    {
                        if (string.IsNullOrEmpty(s))
                            continue;
                        if (s.Contains("PHPSESSID"))
                        {
                            // found it
                            SetSessionCookie(s);
                            break;
                        }
                    }
                }
            }

            if (cmdResult.m_www.error != null)
            {
                cmdResult.m_cmdResult = CmdResultType.ERROR_ERROR_CODE;

                //debug.logError(packet + " is error : " + cmdResult.m_www.error);
                yield break;
            }

            if (cmdResult.m_www.text.Length <= 1)
            {
                cmdResult.m_cmdResult = CmdResultType.ERROR_EMPTY_TEXT;
                //debug.logError("packet (" + packet + ")'s www.text(length : " +
                //                                    cmdResult.m_www.text.Length + " ) = " + cmdResult.m_www.text);

                yield break;
            }

            JSONNode root = JSON.Parse(cmdResult.m_www.text);

            cmdResult.m_jsonNode = root;
            if (root == null || root.Count == 0)
            {
                cmdResult.m_cmdResult = CmdResultType.CLIENT_ERROR_INVAILD_JSON_FORMAT;

                string outputText = "can't parse (" + packet + ")'s www.text = " + cmdResult.m_www.text;
                //debug.logError(outputText);

                yield break;
            }

            var resultCode = int.Parse(root["rc"]);

            if (resultCode < (int)CmdResultType.OK || resultCode >= (int)CmdResultType.MAX_SERVER_ERROR)
            {
                cmdResult.m_cmdResult = CmdResultType.ERROR_UNKNOWN_ERROR;
            }
            else
            {
                cmdResult.m_cmdResult = (CmdResultType)resultCode;
            }


            if (resultCode == 1) connectState = false;					// 서버와의 연결이 안되어 있는 경우, 다시 로그인 하도록 함.
            else if (resultCode == 27) cmdResult.m_cmdResult = CmdResultType.ERROR_INVALID_RECEIPT;// 27- 구매오류(해킹)
            else if (resultCode == 28) cmdResult.m_cmdResult = CmdResultType.ERROR_INVALID_ORDERID;// 28- 구매오류(해킹)

            // 오랫동안 서버와 통신을 안하면, 서버 접속을 불가능하게 함.(서버에서는 20분으로 설정되어있음)
            // 서버와 마지막으로 통신했던 시간을 체크하기 위한 변수.
            curServerTime = (int)System.DateTime.Now.TimeOfDay.TotalSeconds;
        }
    }

    private bool is_locked_stage(int idx)
    {
        return ((idx != 1) && (idx % 20 == 1)); //21,41,61.. - locked stage로 이동하기 위해서는 조건이 필요하다.
    }
    public IEnumerator Send_PROTOCOL_UNLOCK_STAGE_REQ(string _FriendUserid, KakaoMessage.MessageType messageType, MyFriend myKakaoFriend)
    {
        int _nStage = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber;

        if (is_locked_stage(_nStage+1))
        {
            _nStage = _nStage + 1;

            //debug.log("<----------->");
            //debug.log("Log True :" + _nStage);
            //debug.log("<----------->");
        }
        else
        {
            //debug.log("<----------->");
            //debug.log("Log True :" + _nStage);
            //debug.log("<----------->");
        }

        string url= GAME_SERVER_BASE_URL+"/game.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";	  
        //debug.log("url="+url);

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_UNLOCK_STAGE_REQ));
        form.AddField("uid", g_uid);
        form.AddField("fuserid", _FriendUserid);     //친구 카톡id
        form.AddField("stage_idx", _nStage);                        //잠겨있는 스테이지 인덱스
	
        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            
            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
                KakaoFriendsController.Instance.SendMessageToMyKakaoFriend(myKakaoFriend, messageType, null);	// 카카오 친구에게 메시지 보내기...
            }
            else 
            {
		        JSONNode msgParam = new JSONClass();

                msgParam["message"] = "같은 친구에게 두 번 도움요청을 드릴수 없어요. 다른 친구에게 요청해 보세요.";
		        msgParam["show_type"] = "one";		// "ok", "cancel" two button...
		
		        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

                guiMessageBox.OnButtonClick = (bool pIsOk) =>
                {
                    if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
                    {
                    }
                };
            }
             
            //debug.log(_sOutputString); 
        }
    }

    public IEnumerator Send_PROTOCOL_LIST_ITEM_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/user.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";	  
	
        WWWForm form= new WWWForm();
        form.AddField("id",  System.Convert.ToInt32(ProtoCal.PROTOCOL_LIST_ITEM_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //NGUI//debug.log(sOutputString);

            JSONNode root = JSON.Parse(www.text);

            var rc = int.Parse(root["rc"]);

            if (rc == 0) 
            {
                int count = int.Parse(root["count"]);

                MyInfoMgr.Instance.Reset_Inven();
                for (int i = 0; i < count; i++) 
                {
                    var o =  (root[i.ToString()]);
                    //debug.log("[" + i + "]" + o.ToString());
                    Inventory_AddItem(o.ToString());
                }

                //StartCoroutine(Send_PROTOCOL_LIST_MSG_REQ());
            }
        }
    }

    public void Inventory_AddItem(string _itemNode) 
    {
        JSONNode root = JSON.Parse(_itemNode);

        Inventory_Item _Inv = new Inventory_Item();

        _Inv.inven_item(int.Parse(root["item_type"]),
            int.Parse(root["item_idx"]), int.Parse(root["equipped"]), int.Parse(root["qty"]));
        MyInfoMgr.Instance.Inven_AddItem(_Inv);

    }

    public Inventory_Item Get_Inventory_Item(int ix)
    {
        return MyInfoMgr.Instance.Get_Inventory_Item(ix);
    }

    public void PackItem_SetGemFunny(string _itemNode)
    {
        JSONNode root = JSON.Parse(_itemNode);

        try
        {
            int _gem = int.Parse(root["Gem"]);
            MyInfoMgr.Instance.m_Gem = (int)_gem;
        }
        catch { }
        try
        {
            int _funny = int.Parse(root["Funny"]);
            MyInfoMgr.Instance.m_Funny = (int)_funny;
        }
        catch { }
    }

    public void ShopList(string _itemNode)
    {
        JSONNode root = JSON.Parse(_itemNode);

        Shop_Table Shop = new Shop_Table();

        int _Idx = int.Parse(root["idx"]);
        int _shop_type = int.Parse(root["shop_type"]);
        int _Pack_idx = int.Parse(root["pack_idx"]);
        int _sale_ratio = int.Parse(root["sale_ratio"]);
        string _DName = root["dname"].ToString();

        Shop.ShopSetting(_shop_type, _Pack_idx, _DName, _sale_ratio);

        MyInfoMgr.Instance._shop.Add(Shop);
    } 

    public void MailSystem(string _itemNode) 
    {
        JSONNode root = JSON.Parse(_itemNode);

        MailBox MessageBox = new MailBox();

        int seial = int.Parse(root["serial"]);
        int msg_type = int.Parse(root["msg_type"]);
        string receiver = root["receiver"].ToString();
        int sender = int.Parse(root["sender"]);
        int pack_idx = int.Parse(root["pack_idx"]);
        int pack_qty = int.Parse(root["pack_qty"]);
        string send_ts = root["send_ts"].ToString();
        int confirmed = int.Parse(root["confirmed"]);

        MessageBox.Set_Mail(seial, msg_type, receiver, sender
            , pack_idx, pack_qty, send_ts, confirmed);

        MyInfoMgr.Instance._Mail.Add(MessageBox);
    }
    public IEnumerator Send_PROTOCOL_RESET_ITEM_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/user.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";	  
	
        WWWForm form= new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_RESET_ITEM_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }
    public IEnumerator Send_PROTOCOL_SEND_POWER_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/msg.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";	  
	
        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_SEND_POWER_REQ));
        form.AddField("uid", g_uid);
        form.AddField("fuserid", 123456); //친구 카톡 아이디

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }
    public IEnumerator Send_PROTOCOL_GET_GAMEINFO_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/user.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_GET_GAMEINFO_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            
            //debug.log(_sOutputString);

            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
                MyInfoMgr.Instance.m_Funny = int.Parse(root["Funny"]);
                MyInfoMgr.Instance.m_Gem = int.Parse(root["Gem"]);

                g_OpenStage = int.Parse(root["open_stage"]);

                int _nOldStage = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber;

                //debug.log("g_OpenStage :" + g_OpenStage + "_nOld Stage :" + _nOldStage);

                if (_nOldStage < g_OpenStage)
                {
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = g_OpenStage;

                    GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
                }
            }

            /*
             * "uid":3,"
             * day_cnt":5,
             * "day_reward_ts":"2014-10-24 00:00:51",
             * "inv_cnt":0,
             * "inv_reward":0,
             * "heart":51,
             * "heart_ts":"2000-01-01 00:00:00",
             * "Jóias":11,
             * "Funny":46600,
             * "friend_butak":0,
             * "play_time":17,
             * "open_stage":2,
             * "last_stage":1,
             * "user_lv":1,
             * "roulette_cnt":0,
             * "slot1":0,
             * "slot2":0,
             * "slot3":0,
             * "rc":0,
             * "id":12,
             * "gacha_cost":5000
             */
        }
    }         

    public IEnumerator  Send_PROTOCOL_LIST_SHOP_REQ (){
        string url= GAME_SERVER_BASE_URL+"/shop.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LIST_SHOP_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 

            JSONNode root = JSON.Parse(_sOutputString);

            var resultCode = int.Parse(root["rc"]);

            if (resultCode == 0)
            {
                int count = int.Parse(root["count"]);

                for (int i = 0; i < count; i++)
                {
                    var o = (root[i.ToString()]);
                    //debug.log("[" + i + "]" + o.ToString());
                    ShopList(o.ToString());
                }
            }
        }
    }
    public IEnumerator Send_PROTOCOL_BUY_PACK_REQ(int _nItemidx)
    {
        string url= GAME_SERVER_BASE_URL+"/shop.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_BUY_PACK_REQ));
        form.AddField("uid", g_uid);
        form.AddField("pack_idx", _nItemidx); // 구입한 인덱스를 넣어준다.

        //+------------------+
        //141004 send Buy Pack
        //+------------------+

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;

        if (www.error != null)
        {
            PlayErrorMessage(_DefaultMessage,0);
            _isFail_Last = true;
        }
        else
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);

            JSONNode root = JSON.Parse(www.text);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
                int count = int.Parse(root["count"]);

                for (int i = 0; i < count; i++)
                {
                    var o = (root[i.ToString()]);
                    //debug.log("[" + i + "]" + o.ToString());
                }

                PackItem_SetGemFunny(www.text);

                yield return StartCoroutine(Send_PROTOCOL_LIST_ITEM_REQ());
            }
            else 
            {
                _isFail_Last = true;
            }
        }
    }

    public IEnumerator Send_PROTOCOL_BUY_GEM_REQ(string _strChaseData , string _signature)
    {
        string url = "http://lixkeypet.open-b.com/PlaySpot/single_shop.php";
        // "http://61.100.184.235/PlaySpot/single_shop.php";
        //if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();

        form.AddField("device", 1);
        form.AddField("data",       _strChaseData);
        form.AddField("signature",  _signature);
            
        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            
            Debug.Log(_sOutputString);

            JSONNode root = JSON.Parse(_sOutputString);

            var resultCode = int.Parse(root["rc"]);

            if (resultCode == 0)
            {
                int qty = int.Parse(root["qty"]);

                Debug.Log("Qty :" + qty);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().GemSuPlus(qty);
            }

            //StartCoroutine(Send_PROTOCOL_GET_GAMEINFO_REQ());
        }
    }
    public IEnumerator Send_PROTOCOL_GACHA_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/shop.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_GACHA_REQ));
        form.AddField("uid", g_uid);
        form.AddField("idx", 1); //1=funny,2=gem 소비량은 r_random 테이블에서 읽음.

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString   = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }
    public IEnumerator Send_PROTOCOL_SHOW_ME_THE_MONEY_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/user.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_SHOW_ME_THE_MONEY_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;

        if (www.error != null)
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);

            JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

            #region dummy you want deleted log this here.

            try
            {
                var resultCode = int.Parse(j["rc"]);

                if (resultCode == 0)
                {
                    g_power = int.Parse(j["power"]);
                    g_nfunny = int.Parse(j["Funny"]);
                    gem = int.Parse(j["Gem"]);

                    //yInfoMgr.Instance.m_Power += (uint)g_power;
                        
                    MyInfoMgr.Instance.m_Funny += (int)g_nfunny;
                    MyInfoMgr.Instance.m_Gem += (int)gem;
                }

                //NGUI//debug.log("ShowmeTheMoney" + _sOutputString);
            }
            catch (Exception e) 
            {
                Debug.Log("e :" + e.ToString());
            }

            //int addCount = 50;
            /*
            byte[] _publicData =
        KakaoGameUserInfo.Instance.publicData == null ? null : (KakaoGameUserInfo.Instance.publicData);
            byte[] _privateData = KakaoGameUserInfo.Instance.privateData == null ? null : (KakaoGameUserInfo.Instance.privateData);

            KakaoNativeExtension.Instance.updateUser(
                addCount, _publicData, _privateData,
                this.onUpdateUserComplete,
                this.onUpdateUserError);
            */
            #endregion
        }
    }

    //UpdateUser,
    public void onUpdateUserComplete()
    {
        //debug.log("onUpdateUserComplete");

        // You must call the KakaoNativeExtension::loadGameUserInfo method
        KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);
    }
    private void onUpdateUserError(string status, string message)
    {
        //debug.log("onUpdateUserError");
        //debug.log(status + message);
    }

    private void onGameUserInfoComplete()
    {
        //debug.log("onGameUserInfoComplete");
        string alertMessage = "";

        MyInfoMgr.Instance.m_Power = (int)KakaoGameUserInfo.Instance.heart;
        string heart_regen_starts_at = KakaoGameUserInfo.Instance.heart_regen_starts_at.ToString();

        //if (KakaoGameUserInfo.Instance.user_id != null && KakaoGameUserInfo.Instance.user_id.Length > 0)
        //{
        //    KakaoFriendsController.Instance.LoadKakaoFriends();
        //}
        //else 
        //{
        //    NGUI//debug.log("친구 목록을 아직 못불러 왔네요.");
        //}
 //       KakaoNativeExtension.Instance.ShowAlertMessage("onGameUserInfoComplete 확인...");
    }


    private void onGameUserInfoError(string status, string message)
    {
        //debug.log("onGameUserInfoError");
        //debug.log(status + message);
    }

    public IEnumerator Send_PROTOCOL_LIST_MSG_REQ()
    {
        string url = GAME_SERVER_BASE_URL + "/msg.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new  WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LIST_MSG_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);

            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);
                
            //JSON parsing
            JSONNode root = JSON.Parse(www.text);
            var resultCode = int.Parse(root["rc"]);
            if (resultCode == 0)
            {
                int count= int.Parse(root["count"]);

                //debug.log("+----------------+");
                //debug.log("**** msg box ****"); 
                //debug.log("count="+count);

                g_new_msg = count;

                if (guiMessage != null)
                {
                    GuiSettingMenu _guiSettingMenu;

                    _guiSettingMenu = GameObject.Find("GuiSettingMenu").GetComponent<GuiSettingMenu>();
                    //GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, true, null);

                    _guiSettingMenu.KeypetMessageLoadCount();

                    PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();
                    
                    GameObject guiMessageItemGO;
                    GuiMessageItem guiMessageItem;

                    g_PackHeartCount.Clear(); //하트가 남아있다면 딜리트
                    g_PackHeartCount = new List<int>();

                    for (int i = 0; i < count; i++)
                    {
                        var o = (root[i.ToString()]);

                        JSONNode rc = JSON.Parse(o.ToString());

                        int seial = int.Parse(rc["serial"]);
                        int msg_type = int.Parse(rc["msg_type"]);
                        string receiver = rc["receiver"].ToString();
                        int sender = int.Parse(rc["sender"]);
                        int pack_idx = int.Parse(rc["pack_idx"]);
                        int pack_qty = int.Parse(rc["pack_qty"]);
                        string send_ts = rc["send_ts"].ToString();
                        int confirmed = int.Parse(rc["confirmed"]);

                        if (pack_idx == 120 || pack_idx == 29) 
                        {
                            g_PackHeartCount.Add(pack_idx);
                        }

                        guiMessageItemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiMessageItem"));
                        guiMessageItem = guiMessageItemGO.GetComponent<GuiMessageItem>();
                        guiMessageItem.SetItemInfo((uint)seial, pack_idx,
                            (Texture2D)Resources.Load("Images/Item/" + packageInfoMgr.GetPackageInfo((uint)pack_idx).icon_image),
                            packageInfoMgr.GetPackageInfo((uint)pack_idx).name + "도착"
                            , packageInfoMgr.GetPackageInfo((uint)pack_idx).desc,
                            packageInfoMgr.GetPackageInfo((uint)pack_idx).name);

                        guiMessage.AddMessageItem(guiMessageItem);
                    }
                }
                else 
                {
                    guiMessage.ClearItems();
                }

                StartCoroutine(Send_PROTOCOL_GET_GAMEINFO_REQ());

                GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
            }
            else
            {
                //debug.log("+----------------+");
                //debug.log("error rc:" + resultCode);
                //debug.log("+----------------+");
            } 
        }
    }
    public IEnumerator Send_PROTOCOL_READ_MSG_REQ(int _nSerial,GameObject _DesOb)
    {
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
                
        string url= GAME_SERVER_BASE_URL+"/msg.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_READ_MSG_REQ));
        form.AddField("uid",    g_uid);
        form.AddField("serial", _nSerial);     //메시지 고유번호

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);

            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
               // GameObject.Find("GuiMessage").GetComponent<GuiMessage>().Clearitem((uint)_nSerial);
                GameObject.Find("GuiMessage").GetComponent<GuiMessage>().ClearItems();

                GSStageMap _gsstagemap = GameObject.Find("GSStageMap").GetComponent<GSStageMap>();

                _gsstagemap.KeypetMessageLoadCount();
            }
        }
    }
    public IEnumerator Send_PROTOCOL_READ_MSG_ALL_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/msg.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id",  System.Convert.ToInt32(ProtoCal.PROTOCOL_READ_MSG_ALL_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);

            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0) 
            {
                StartCoroutine(Send_PROTOCOL_LIST_MSG_REQ());
            }
        }
    }

    public IEnumerator Send_PROTOCOL_START_GAME_REQ(int _nStage, int _slot1, int _slot2, int _slot3)
    {
        string url= GAME_SERVER_BASE_URL+"/game.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        //debug.log("_nStage :" + _nStage);

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_START_GAME_REQ));
        form.AddField("uid", g_uid);
        form.AddField("stage_idx", _nStage);
        form.AddField("slot1", _slot1); //슬롯장착 아이템 idx
        form.AddField("slot2", _slot2);
        form.AddField("slot3", _slot3);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            Debug.Log(_sOutputString); 
        }

    }
    public IEnumerator Send_PROTOCOL_USE_ITEM_REQ(int item_idx)
    {
        string url = GAME_SERVER_BASE_URL + "/game.php";
        if (DEBUG) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_USE_ITEM_REQ));
        form.AddField("uid", g_uid);
        form.AddField("item_idx", item_idx);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;

        if (www.error != null)
        {
            // print(www.error);
            PlayErrorMessage("아이템 사용중 "+ _DefaultMessage,0);

        }
        else
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString);

            StartCoroutine(Send_PROTOCOL_LIST_ITEM_REQ());
        }

    }

    public string _sOutputString;
    public IEnumerator Send_PROTOCOL_END_GAME_REQ(bool _bClear, int _nScore, int _nStage, int _slot1, int _slot2, int _slot3)
    {
        Debug.Log("send endgame");

        string url = GAME_SERVER_BASE_URL + "/game.php";
        //if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();

        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_END_GAME_REQ));
        form.AddField("uid", g_uid);

        if (_bClear)
        {
            form.AddField("clear", 1);    //스테이지 클리어(0=false,1=true)
        }
        else
        {
            form.AddField("clear", 0);    //스테이지 클리어(0=false,1=true)
        }

        //debug.log("_Stage :" + _nStage);

        //퍼니가 없을 수 있다.
        form.AddField("Funny", 0); //얻은 퍼니수
        form.AddField("score", _nScore);
        form.AddField("stage_idx", _nStage);
        form.AddField("slot1", _slot1); //슬롯장착 아이템 idx
        form.AddField("slot2", _slot2);
        form.AddField("slot3", _slot3); 
            

        //debug.log("url=" + url);
        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }

    }
    public IEnumerator Send_PROTOCOL_PUSH_REGISTER_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/setting.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_PUSH_REGISTER_REQ));
        form.AddField("uid", g_uid);
        form.AddField("device_id", g_device_id); //gcm device id 또는 APNS device token
        form.AddField("device_id", g_device_id); // 애플은 binary format이기 때문에 base64로 encoding해서 전송해야 합니다.
                                                    // gcm device id는 원래 base64로 인코딩 되어 있었는지 확인필요. 일단 인코딩 없이 그냥 보냄.

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }
    public IEnumerator Send_PROTOCOL_PUSH_UNREGISTER_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/setting.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_PUSH_UNREGISTER_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            
            Debug.Log(_sOutputString); 
            
            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
			}
        }
    }

    
    /// <summary>
    /// 탈퇴 성공시 호출...
    /// </summary>
    private void onUnregisterComplete()
    {
        Application.Quit();
    }


    /// <summary>
    /// 탈퇴 실패시 호출...
    /// </summary>
    private void onUnregisterError(string status, string message)
    {
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    public IEnumerator Send_PROTOCOL_MSG_ON_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/setting.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_MSG_ON_REQ));
        form.AddField("uid", g_uid);
        form.AddField("flag", 0);  //0:off,1:on

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }

    public IEnumerator Send_PROTOCOL_UNREGISTER_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/setting.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_UNREGISTER_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;

            Debug.Log(_sOutputString);

            JSONNode root = JSON.Parse(_sOutputString);

            var rc = int.Parse(root["rc"]);

            if (rc == 0)
            {
                KakaoNativeExtension.Instance.deleteUser(this.onDeleteUserInfoComplete, this.onDeleteUserInfoError);
		
			}
        }
    }           

    public IEnumerator  Send_PROTOCOL_LOGOUT_REQ (){
        string url= GAME_SERVER_BASE_URL+"/logout.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LOGOUT_REQ));
        form.AddField("uid", g_uid);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage,0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }

    public IEnumerator Send_PROTOCOL_INVITE_FRIEND_REQ(string _friendId, Kakao_InviteFriend_Window window)
    {
        string url= GAME_SERVER_BASE_URL+"/user.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_INVITE_FRIEND_REQ));
        form.AddField("uid", g_uid);
        form.AddField("fuserid", _friendId); //친구 카톡iD

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString = www.text;
            bmOutputTexture = null;

            Debug.Log("Invite :" + _sOutputString);
            
            JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

            var resultCode = int.Parse(j["rc"]);

            if (resultCode == 0) 
            {
                g_invCnt = int.Parse(j["inv_cnt"]);
            }

            window.ShowFriendCount("초대한 친구 수 : " + g_invCnt.ToString());

            //debug.log(_sOutputString); 
        }
    }

    //type 2
    public IEnumerator Send_PROTOCOL_ROULETTE_REQ()
    {
        string url= GAME_SERVER_BASE_URL+"/shop.php";
        if( DEBUG ) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_GACHA_REQ));
        form.AddField("uid", g_uid);
        form.AddField("idx", 2); //type 2

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;
	
        if( www.error != null )
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else 
        {
            _sOutputString   = www.text;
            bmOutputTexture = null;
            //debug.log(_sOutputString); 
        }
    }

    public IEnumerator Send_PROTOCOL_LIST_HELP_REQ(List<MyFriend> _Friend, Kakao_NextVillagePopup_RequestFriend_Window window)
    {
        int _nStage = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber;

        if (is_locked_stage(_nStage + 1))
        {
            _nStage = _nStage + 1;

            string url = GAME_SERVER_BASE_URL + "/game.php";
            if (DEBUG) url = url + "?XDEBUG_SESSION_START";

            WWWForm form = new WWWForm();
            form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_LIST_HELP_REQ));
            form.AddField("uid", g_uid);
            form.AddField("stage_idx", _nStage);

            WWW www = new WWW(url, form.data, SessionCookie());
            yield return www;

            if (www.error != null)
            {
                PlayErrorMessage(_DefaultMessage, 0);

                //debug.logError("WWW Errro :" + www.error);
            }
            else
            {
                _sOutputString = www.text;
                bmOutputTexture = null;
                //debug.log(_sOutputString);

                JSONNode root = JSON.Parse(www.text);

                var resultCode = int.Parse(root["rc"]);

                if (resultCode == 0)
                {
                    int count = int.Parse(root["count"]);

                    //debug.log("count :" + count);

                    for (int i = 0; i < count; i++)
                    {
                        var o = (root[i.ToString()]);

                        JSONNode rc = JSON.Parse(o.ToString());

                        string receiver = rc["receiver"].ToString();

                        string _str = receiver.Substring(1, 17);

                        for (int j = 0; j < _Friend.Count; j++)
                        {
                            if (_Friend[j].userid == _str)
                            {
                                window.SetFriendImage(i, _Friend[j].profileImage, _Friend[j].nickname);
                            }
                        }
                    }

                    if (count > 2)
                    {
                        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

                        yield return new WaitForSeconds(1.0f);

                        window.ClearItems();
                        window.ExitBtn.onClick = null;
                        window = null;

                        GuiMgr.Instance.Show<Kakao_NextVillagePopup_RequestFriend_Window>(GuiMgr.ELayerType.Front, false, null);

                        GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);

                        g_OpenStage = int.Parse(root["open_stage"]);

                        GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = g_OpenStage;

                        GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);

                        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
                    }
                }
            }
        }
        else
        {
            //debug.log("ClearStage : over");
        }
    }

    public IEnumerator Send_PROTOCOL_UNLOCK_STAGE_GEM_REQ()
    {
        Debug.Log("unlock");

        int _nStage = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber;

        if (is_locked_stage(_nStage + 1))
        {
            _nStage = _nStage + 1;

            //debug.log("<----------->");
            //debug.log("Log True :" + _nStage);
            //debug.log("<----------->");

            //debug.log("System.Convert.ToInt32(ProtoCal.PROTOCOL_UNLOCK_STAGE_GEM_REQ) : " +
            //    System.Convert.ToInt32(ProtoCal.PROTOCOL_UNLOCK_STAGE_GEM_REQ));

            string url = GAME_SERVER_BASE_URL + "/shop.php";
            if (DEBUG) url = url + "?XDEBUG_SESSION_START";

            WWWForm form = new WWWForm();
            form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_UNLOCK_STAGE_GEM_REQ));
            form.AddField("uid", g_uid);
            form.AddField("stage_idx", _nStage); //gem 5를 소모하여 다음 스테이지(open_stage + 1)해금.

            WWW www = new WWW(url, form.data, SessionCookie());
            yield return www;

            Debug.Log("www" + www.text);

            if (www.error != null)
            {
                PlayErrorMessage(_DefaultMessage,0);
            }
            else
            {
                _sOutputString = www.text;
                bmOutputTexture = null;

                Debug.Log(_sOutputString);

                JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

                var resultCode = int.Parse(j["rc"]);

                if (resultCode == 0)
                {
                    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

                    gem = int.Parse(j["Gem"]);
                    g_OpenStage = int.Parse(j["open_stage"]);

                    MyInfoMgr.Instance.m_Gem = (int)gem;
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = g_OpenStage;

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().gemLockClear = true;
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().ClearStageNumberSave();

                    GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
                }
                else
                {
                    PlayErrorMessage(_DefaultMessage, 0);
                }
                //NGUI//debug.log("ShowmeTheMoney" + _sOutputString);
            }
        }
        else
        {
            ////debug.log("<----------->");
            ////debug.log("Log True :" + _nStage);
            ////debug.log("<----------->");
        }


    }

    public string _DefaultMessage = "접속이 원활하지 않습니다\n잠시후 재시도 해주십시오";
    public void PlayErrorMessage(string strMessage, int _nindex, bool _bLogin = false)
    {
        //Hwang Wark Message Popup Start
        JSONNode msgParam = new JSONClass();
        msgParam["message"] = strMessage;
        msgParam["show_type"] = "one";

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
                if (_bLogin)
                {
                    Application.Quit();
                }
                else 
                {
                    if (_nindex == 1)
                    {
                        StartCoroutine(Send_Refresh_Login_Req());
                    }
                    // Application.LoadLevel(Application.loadedLe
                }
            }
        };
    }

    public bool _bLoadInfoSave = false;


    public IEnumerator Send_PROTOCOL_NOTICE() 
    {
        //현재 꺼 : http://event.open-b.com/img/event001.jpg 
        
        //옛날꺼 : http://keypet.open-b.com/event/index.json;

        string url = "http://event.open-b.com/index.json";

        WWW www = new WWW(url);

        yield return www;

        Debug.Log("Notice !");

        if (www.error != null)
        {
            Debug.Log("Error :" + www.error);
        }
        else
        {

            _sOutputString = www.text;
            bmOutputTexture = null;
            Debug.Log("www :" + _sOutputString);
            JSONNode root = JSON.Parse(www.text);

            int count = int.Parse(root["count"]);

            int VersionCount = int.Parse(root["ver_count"]);

            Debug.Log("Version Count :" + VersionCount);

            /*
            for (int i = 0; i < VersionCount; i++)
            {
                var o = (root["ver_" + i.ToString()]);

                JSONNode rcVer = JSON.Parse(o.ToString());

                int authtype    = int.Parse(rcVer["authtype"].ToString());
                int ver_minor   = int.Parse(rcVer["ver_minor"].ToString());
                int ver_major   = int.Parse(rcVer["ver_major"].ToString());

                Debug.Log("Version " + i + " : "+ authtype + "." + ver_major + "." + ver_minor);
            }
            */

            GameObject guinoticeitemgo;
            List<GuiNotice> guiNoticeitem = new List<GuiNotice>();

            Debug.Log("출석 !");

            string strNow = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Dictionary<int, string> lastLoginDate = GameObject.Find("InfoMgr").GetComponent<InfoSave>()._snotice;

            string lastDate = "";

            for (int i = 0; i < count; i++)
            {
                if (lastLoginDate.ContainsKey(i) == false)
                {
                    lastLoginDate.Add(i, lastDate);
                }
                else 
                {
                    Debug.Log("_sNotice[" + i + "] :" + lastLoginDate.TryGetValue(i, out lastDate));

                    Debug.Log("lastDate :" + lastDate);
                }

                if (lastDate.Length > 10)
                {
                    string strtmp1 = lastDate.Substring(0, 10);
                    string strtmp2 = strNow.Substring(0, 10);

                    if (strtmp1 != strtmp2)
                    {
                        var o = (root[i.ToString()]);

                        JSONNode rc = JSON.Parse(o.ToString());

                        string receiver = rc["url"].ToString();

                        Debug.Log("receiver :" + receiver);

                        guinoticeitemgo = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiNotice"));
                        guinoticeitemgo.GetComponent<GuiNotice>().SettingItem(receiver, i);
                        guinoticeitemgo.GetComponent<UIPanel>().depth = 200 - i;
                        guinoticeitemgo.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
                        guinoticeitemgo.transform.localPosition = Vector3.zero;
                        guinoticeitemgo.transform.localScale = Vector3.one;

                        guiNoticeitem.Add(guinoticeitemgo.GetComponent<GuiNotice>());
                    }
                }
                else
                {
                    Debug.Log("출석--2 !");

                    var o = (root[i.ToString()]);

                    JSONNode rc = JSON.Parse(o.ToString());

                    string receiver = rc["url"].ToString();

                    Debug.Log("receiver :" + receiver);

                    guinoticeitemgo = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiNotice"));
                    guinoticeitemgo.GetComponent<GuiNotice>().SettingItem(receiver, i);
                    guinoticeitemgo.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
                    guinoticeitemgo.transform.localPosition = Vector3.zero;
                    guinoticeitemgo.transform.localScale = Vector3.one;

                    guiNoticeitem.Add(guinoticeitemgo.GetComponent<GuiNotice>());
                }
            }
        }
    }

    //< 튜토리얼 보상 프로토콜
    public IEnumerator Send_PROTOCOL_TUTORIAL_REQ()
    {
        string url = GAME_SERVER_BASE_URL + "/user.php";
        if (DEBUG) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_TUTORIAL_REQ));
        form.AddField("uid", g_uid);
        
        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;

        if (www.error != null)
        {
            Debug.Log("Tutorial Error :" + www.error);

            PlayErrorMessage(_DefaultMessage, 0);
        }
        else
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            Debug.LogError(_sOutputString); 
        }
    }

    public IEnumerator Send_STAGE_UPDATE_REQ(int _indexStage)
    {
        string url = GAME_SERVER_BASE_URL + "/game.php";
        if (DEBUG) url = url + "?XDEBUG_SESSION_START";

        WWWForm form = new WWWForm();
        form.AddField("id", System.Convert.ToInt32(ProtoCal.PROTOCOL_UPDATE_STAGE_GAME_REQ));
        form.AddField("uid", g_uid);
        form.AddField("stage_idx", _indexStage);

        WWW www = new WWW(url, form.data, SessionCookie());
        yield return www;

        if (www.error != null)
        {
            PlayErrorMessage(_DefaultMessage, 0);
        }
        else
        {
            _sOutputString = www.text;
            bmOutputTexture = null;
            Debug.LogError(_sOutputString);

            //JSON parsing
            JSONNode j = JSONNode.Parse(www.text);//json.fromString(www.text);

            var resultCode = int.Parse(j["rc"]);

            if (resultCode == 0)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber = int.Parse(j["open_stage"]);
            }
        }
    }

    private void onDeleteUserInfoComplete() 
    {
        KakaoNativeExtension.Instance.Unregister(onUnregisterComplete, onUnregisterError);
    }
    private void onDeleteUserInfoError(string status, string message)
    {
        Debug.Log("onDeleteUserInfoError");
    }
}
   
