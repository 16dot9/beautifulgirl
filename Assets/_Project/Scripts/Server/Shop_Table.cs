﻿using UnityEngine;
using System.Collections;

public class Shop_Table
{
    public int shop_type;
    public int pack_idx;
    public string _dname;

    public int sale_ratio;

    public void ShopSetting(int _shoptype, int _packidx, string _dname, int saleratio)
    {
        this.shop_type = _shoptype;
        this.pack_idx = _packidx;
        this._dname = _dname;
        this.sale_ratio = saleratio;
    }
}
