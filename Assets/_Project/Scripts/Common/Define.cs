﻿using UnityEngine;


public enum KeypetType
{
	Alien,
	Chicken,
	Lesserpanda,
	Pig,
	Rabbit,
	Sleepy,

	Max,

	blank = -1,
}


public class KeypetResInfo
{
	public KeypetType			KeypetResType = KeypetType.Max;
	public Object				KeypetResObj = null;
}
