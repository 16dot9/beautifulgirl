﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoMain : MonoBehaviour
{
    //Reball Check True
    public bool isReballPlay = false;

    public bool bVersionCheck = false; //버전업 검사를 진행하고, 버전업을 할 필요가 없다면 true!

    public Texture2D DefaultProfileImage;	// 유저, 카카오친구들의 기본 프로필 이미지...

    public bool _bTutorailStart = false;

    private static KakaoMain _Instance;
    public static KakaoMain Instance
    {
        get
        {
            //if (_Instance == null)
            //{
            //    _Instance = GameObject.FindObjectOfType<KakaoMain>();
            //    if (_Instance == null)
            //    {
            //        GameObject container = new GameObject();
            //        container.name = "KakaoMain";
            //        _Instance = container.AddComponent<KakaoMain>();
            //        DontDestroyOnLoad(_Instance);
            //    }
            //}

            return _Instance;
        }
    }

    void Awake()
    {
        _Instance = this;
    }

    public string _SenderId;

    /// <summary>
    /// 카카오 초기화...
    /// </summary>
    public void Init()
    {
        Debug.Log("Init ! ");
        // 초기화 성공시 onInitComplete() 호출함...
        // 토큰 변경시 onTokens() 호출함...

        Cmd_Handler_CS cmd = this.GetComponent<Cmd_Handler_CS>();

        StartCoroutine(cmd.Send_PROTOCOL_NOTICE());

        KakaoNativeExtension.Instance.Init(onInitComplete, onTokens);
    }

    void OnApplicationQuit()
    {
        Destroy(this.gameObject);
        
    }

    /// <summary>
    /// KakaoSDK 초기화 성공시 호출...
    /// </summary>
    private void onInitComplete()
    {
        Debug.Log("onInitComplete ! ");

        // 카카오 계정으로 로그인 인증 처리...
        KakaoNativeExtension.Instance.Authorized(onAuthorized);
    }

    void Start() 
    {
        //StartCoroutine(IE_RechargeHeart());
    }

    /// <summary>
    /// <----------------------->
    /// _nCnt Dummy 삭제해도 무방
    /// <----------------------->
    /// </summary>
    private int _nCnt = 0;

    private void onLocalUserComplete()
    {
#if UNITY_EDITOR
        StartSend_PROTOCOL_LOGIN_REQ();
#elif UNITY_ANDROID || UNITY_IPHONE    
        StartKakaoloadGameInfo();
#endif

    }
    void StartSend_PROTOCOL_LOGIN_REQ()
    {
        //StartCoroutine(this.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_LOGIN_REQ(
        //    KakaoLocalUser.Instance.nickName, KakaoLocalUser.Instance.userId,
        //    PlayerPrefs.GetString(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs)));//KakaoLocalUser.Instance.userId));
        
//        CheckUpdateHeart();
    }

    void StartKakaoloadGameInfo()
    {
        KakaoNativeExtension.Instance.loadGameInfo(this.onGameInfoComplete, this.onGameInfoError);
        // onGameInfoComplete --> KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);
        // onGameUserInfoComplete_StartType --> StartSend_PROTOCOL_LOGIN_REQ

    }

    private void onGameInfoComplete()
    {
        Debug.Log("onGameInfoComplete");
        KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete_StartType, this.onGameUserInfoError);
    }
    //Load Leaderboard
    private void onLoadLeaderboardComplete()
    {
        Debug.Log("onLoadLeaderboardComplete");
        // test
        //KakaoLeaderboards.Instance.printToConsole();
    }
    private void onLoadLeaderboardError(string status, string message)
    {
        Debug.Log("onLoadLeaderboardError");
        KakaoReConnectMessage(_DefaultMessage);
    }
    private void onGameUserInfoComplete_StartType()
    {
        // 유저 갱신 및 하트 갱신 확인.
        onGameUserInfoComplete();
        _StartCheckUpdateHeart = true;
        // 디폴트 리더보드 호출 체크.
        //KakaoNativeExtension.Instance.loadLeaderboard("DEFAULT", this.onLoadLeaderboardComplete, this.onLoadLeaderboardError);

        // 하트 이외의 유저 정보 로긴 처리.
        StartSend_PROTOCOL_LOGIN_REQ();
    }
    private void onGameInfoError(string status, string message)
    {
        Debug.Log("onGameInfoError");
        KakaoReConnectMessage(_DefaultMessage);
    }
    public string _DefaultMessage = "접속이 원활하지 않습니다.\n잠시후 재시도 해주십시오";
    public void KakaoReConnectMessage(string strMessage)
    {
        //Hwang Wark Message Popup Start
        JSONNode msgParam = new JSONClass();
        msgParam["message"] = strMessage;
        msgParam["show_type"] = "one";

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...
        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
                // onInitComplete();
                Application.Quit();
            }
        };
    }

    private void onLocalUserError(string status, string message)
    {
        showAlertErrorMessage(status, message);
    }
    // 중복 사용 체크 필요.
    public void onGameUserInfoComplete()
    {
        //NGUIDebug.Log("onGameUserInfoComplete");
//        _dRegenStartAt = KakaoGameUserInfo.Instance.heart_regen_starts_at;
        MyInfoMgr.Instance.m_Power = KakaoGameUserInfo.Instance.heart;

        if (MyInfoMgr.Instance.m_Power < 5)
        {
            StartCoroutine(IE_RechargeHeart());
        }

        //NGUIDebug.Log("heart:" + MyInfoMgr.Instance.m_Power);
    }
    
    bool _StartCheckUpdateHeart = false;
    float _CheckUpdateHeartTime = 3f;
    float _updateTime = 0f;
    void Update()
    {
        /*if (_StartCheckUpdateHeart == true)
        {
            _updateTime += Time.deltaTime;
            if (_CheckUpdateHeartTime < _updateTime)
            {
                _updateTime = 0f;
                CheckUpdateHeart();
            }
        }*/
    }

    public void CheckUpdateHeart(GuiPlayerInfo guiPlayerInfo)
    {
        if (MyInfoMgr.Instance.m_Power < 10) 
        {
            TimeStamp TSpan = new TimeStamp();

            TSpan.SetTime(System.DateTime.Now);

            Debug.Log("하트사용된 시간" + ES3.Load<int>("HeartReganTime") + ":" + GameObject.Find("InfoMgr").GetComponent<InfoSave>().HeartReganTime.ToString());

            int DataTime = GameObject.Find("InfoMgr").GetComponent<InfoSave>().HeartReganTime = ES3.Load<int>("HeartReganTime");

            int TimeCheck = (TSpan.getTimeStampINT() - DataTime);

            if (TimeCheck >= 420 && TimeCheck < 840)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(1);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 420)));
            }
            else if (TimeCheck >= 840 && TimeCheck < 1260)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(2);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 840)));
            }
            else if (TimeCheck >= 1260 && TimeCheck < 1680)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(3);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 1260)));
            }
            else if (TimeCheck >= 1680 && TimeCheck < 2100)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(4);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 1680)));
            }
            else if (TimeCheck >= 2100 && TimeCheck < 2520)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(5);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 2100)));
            }
            else if (TimeCheck >= 2520 && TimeCheck < 2940)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(6);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 2520)));
            }
            else if (TimeCheck >= 2940 && TimeCheck < 3360)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(7);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 2940)));
            }
            else if (TimeCheck >= 3360 && TimeCheck < 3780)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(8);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 3360)));
            }
            else if (TimeCheck >= 3780 && TimeCheck < 4200)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(9);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 3780)));
            }
            else if (TimeCheck >= 4200)
            {
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlusTime(10);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime((TSpan.getTimeStampINT() - (TimeCheck - 4200)));
            }

            StartCoroutine(IE_TimeCheck());
        }

        //KakaoNativeExtension.Instance.loadGameInfo(this.onGameHeartCheckComplete, this.onGameInfoError);
    }

    public void onGameUserInfoError(string status, string message)
    {
        Debug.Log("onGameUserInfoError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    /// <summary>
    /// 카카오 계정 인증 처리를 위해 호출...
    /// </summary>
    private void onAuthorized(bool _authorized)
    {
        Debug.Log("authorized : " + _authorized);

        if (_authorized)	// 카카오 계정으로 이미 로그인 프로세스가 진행되었다면 게임으로 진입...
        {
            KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
        }
        else   // 카카오 계정으로 로그인을 하지 않은 것이라면 약관 동의(및 로그인) 유도 화면으로 진입...
        {
             GameStateMgr.Instance.ChangeState<GSLoginAgreement>(0, null);
             GameObject.Find("InfoMgr").GetComponent<InfoSave>().infoReset = true; // update에서 초기화후 false 됨.
        }
    }

    /// <summary>
    /// token이 변경되는 시점에 호출되는 델리게이트 함수 (로그인, 로그아웃, 탈퇴, 토근만료 및 자동갱신 등)...
    /// </summary>
    private void onTokens(string accessToken, string refreshToken)
    {
        // 인증 토큰을 최신 상태로 유지...
        KakaoNativeExtension.Instance.updateTokenCache(accessToken, refreshToken);

        if (KakaoNativeExtension.Instance.hasValidTokenCache() == true)	// 인증 토큰이 유효하다면 게임으로 진입...
        {
            Debug.LogError("<---------------->");
            Debug.LogError("2. Token Local");
            Debug.LogError("<---------------->");
            KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
        }
        else   // 인증 토큰이 유효하지 않다면 약관 동의(및 로그인)을 유도시키는 화면으로 진입...
        {
            GameStateMgr.Instance.ChangeState<GSLoginAgreement>(0, null);
        }
    }


    /// <summary>
    /// 게임 맵 화면으로 전환 및 카카오 유저정보/친구정보 로딩...
    /// </summary>
    public void GoToStageMap()
    {
        #region from Server

        bool BlockUser = false;	// 재가입이 금지되었는지 여부는 서버에서 받아야 하는 값임 (일단은 테스트로 세팅함).....

        #endregion

        // 재가입이 금지된 유저의 경우 다시 탈퇴 처리 시킴...
        if (BlockUser)
        {
            JSONNode msgParam = new JSONClass();

            msgParam["message"] = "재가입이 불가능한 회원입니다. \n 나중에 다시 시도해 주세요.";
            msgParam["show_type"] = "one";		// "ok" button...

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk)
                {
                    Unregister();
                }
            };
            return;
        }

        //KakaoLocalUserController.Instance.LoadUserProfile();		// 카카오 유저 정보 로드...	
        InvokeRepeating("RefreshKakaoFriends", 0f, 60f * 60f);		// 18분마다 카카오 서버에서 카카오 친구 목록 로드...

        GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);	// 맵 화면으로 전환...
    }


    /// <summary>
    /// 카카오 친구 목록을 카카오 캐싱주기(10분)를 참고해서 갱신함...
    /// </summary>
    private void RefreshKakaoFriends()
    {
        // 친구 목록을 사용하고 있지 않을 때  		---> 친구 목록을 사용 중일 때 데이터를 변경하면 안되므로... 
        if (KakaoFriendsController.Instance.UsingKakaoFriends == true)
        {
            KakaoNativeExtension.Instance.ShowAlertMessage("친구리스트를 사용 중이므로 업데이트 안 함...");
            return;
        }

        // 게임 플레이를 하고 있지 않을 때 			--> 플레이를 방해하지 않기 위해...
        GSPlay play = GameStateMgr.Instance.GetComponentInChildren<GSPlay>();
        if (play != null)
        {
            if (play.IsPlayState() == true)
            {
                KakaoNativeExtension.Instance.ShowAlertMessage("플레이 중이므로 친구리스트 업데이트 안 함...");
                return;
            }
        }

    }



    /// <summary>
    /// "카카오 로그인" 버튼 클릭시 호출...
    /// </summary>
    public void Login()
    {
        KakaoNativeExtension.Instance.Login(onLoginComplete, onLoginError);
    }


    /// <summary>
    /// "카카오 계정 등록" 버튼 클릭시 호출...
    /// </summary>
    public void WebLogin()
    {
        KakaoResponseHandler.Instance.loginComplete = this.onLoginComplete;
        KakaoResponseHandler.Instance.loginError = this.onLoginError;
        KakaoNativeExtension.Instance.LoginWithWebview(onLoginComplete, onLoginError);
    }


    /// <summary>
    /// 로그인 성공시 호출...
    /// </summary>
    private void onLoginComplete()
    {
        KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
//        KakaoNativeExtension.Instance.ShowAlertMessage("6. 카카오 로그인 성공");
    }


    /// <summary>
    /// 로그인 에러시 호출...
    /// </summary>
    private void onLoginError(string status, string message)
    {
        KakaoMain.Instance.showAlertErrorMessage(status, message);

        // 로딩 화면 닫기.
        GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, false, null);
    }


    /// <summary>
    /// "로그아웃" 버튼 클릭시 호출...
    /// </summary>
    public void Logout()
    {
        KakaoNativeExtension.Instance.Logout(onLogoutComplete, onLogoutError);
    }

    /// <summary>
    /// 로그아웃 성공시 호출됨...
    /// </summary>
    private void onLogoutComplete()
    {
        // KakaoNativeExtension.Instance.ShowAlertMessage ("2. 카카오 로그아웃 성공.");

        // 로그아웃했으므로 이후에 토큰의 유효성을 확인하기 위해 onTokens()가 자동 호출됨...
    }


    /// <summary>
    /// 로그아웃 에러시 호출됨...
    /// </summary>
    private void onLogoutError(string status, string message)
    {
        showAlertErrorMessage(status, message);
    }


    /// <summary>
    /// 탈퇴시 호출...
    /// </summary>
    public void Unregister()
    {
        StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_UNREGISTER_REQ());
    }

    /// <summary>
    /// Shows the alert error message.
    /// </summary>
    public void showAlertErrorMessage(string code, string message)
    {
        string alertMessage = "";
        if (code != null)
        {
            alertMessage += ("Error Code : " + code);
        }

        if (message != null)
        {
            alertMessage += "\n";
            alertMessage += ("Error Message : " + message);
        }

        //KakaoNativeExtension.Instance.ShowAlertMessage(alertMessage);
    }


    /*	private void onSendMessageComplete() 
        {
            KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendMessage");
        }

        private void onSendMessageError(string status, string message) 
        {
            KakaoNativeExtension.Instance.ShowAlertMessage("Send Message Error test");
            showAlertErrorMessage(status,message);
        }
    */

    /// <summary>
    /// Shows the kakao message box.
    /// </summary>
    public void ShowKakaoMessageBox(string errorCode, string errorMessage, string from)
    {
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

        string msg = null;

        if (errorCode == "-31")
        {
            // test 용으로 호출해 본 것.....
            // SaveInvitedUserInfo (_invitedUserID, true);	

            if (from == "초대 메시지")
            {
                msg = "동일한 친구에게는 31일 이후에 다시 초대 메시지를 보낼 수 있습니다.";
            }
            else
            {
                msg = "정성이 가득 담김 파워 고마워요. 하지만, 동일한 친구에게 아직 보낼 수 없어요. 나중에 보내주세요.";
            }
        }
        else if (errorCode == "-32")
        {
            msg = "하루 최대 30명 이상 초대 메시지를 보낼 수 없습니다.";
        }
        else if (errorCode == "-17")
        {
            msg = "수신자가 카카오톡 초대 메시지를 차단한 상태입니다.";
        }
        else if (errorCode == "-500") // 일단 네트워크 오류일 경우에 확인이 필요해서...
        {
            msg = from + " : 알 수 없는 에러입니다. 네트워크 연결 상태를 확인해 주세요.";
        }
        else if (errorCode == "카카오톡 메시지수신 차단 확인")
        {
            msg = "우선 \"카카오톡 > 더보기 > 설정 > 카카오계정 > 연결된 앱 관리 > 키펫브레이크\"의 메시지 수신을 체크해야 카카오톡 메시지를 받을 수 있습니다.";
        }
        else if (errorCode == "조르기 메시지 에러")
        {
            msg = from + " : 조르기 메시지 전송 실패. " + errorMessage;
        }
        else
            msg = " 카카오톡 메시지 수신 거부한 유저입니다."; //+ errorMessage;

        JSONNode msgParam = new JSONClass();
        msgParam["message"] = msg;
        msgParam["show_type"] = "one";		// "ok" button...

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
            }
        };
    }

    private double _dServerTime;
    private int _dRegenStartAt = 0;
    int _MaxHeart = 0;
    float _HeartTime = 0.0f;
    private int _Heart_Interval;

    public GuiPlayerInfo _guiPlayerInfo;

    public  bool _bLoding = false;

    bool IE_RechargeHeart_Loop = false;

    public void UseHeart() 
    {
        if (IE_RechargeHeart_Loop == false)
        {
            TimeStamp TSpan = new TimeStamp();

            TSpan.SetTime(System.DateTime.Now);

            GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime(TSpan.getTimeStampINT());
        }

        GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuMinus();

        //if (MyInfoMgr.Instance.m_Power < 10) 
        //{
        //    StartCoroutine(IE_TimeCheck());
        //}
    }

    double _fDate;

    //클라이언트에서 하트 돌아가는 시간
    private IEnumerator IE_TimeCheck()
    {
        Debug.Log("IE TiemCheck! :" + IE_RechargeHeart_Loop);

        if (IE_RechargeHeart_Loop == false && _guiPlayerInfo != null)
        {
            IE_RechargeHeart_Loop = true;

            _guiPlayerInfo = GameObject.Find("GuiPlayerInfo").GetComponent<GuiPlayerInfo>();

            TimeStamp TSpan = new TimeStamp();

            TSpan.SetTime(System.DateTime.Now);

            double ServerTime = TSpan.getTimeStampDou();

            _fDate = ServerTime;

            _dRegenStartAt = GameObject.Find("InfoMgr").GetComponent<InfoSave>().HeartReganTime;

            while (IE_RechargeHeart_Loop)
            {
                _fDate += Time.deltaTime;

                ServerTime = (int)_fDate;

                int _dTime = (int)ServerTime - _dRegenStartAt;

                int _acheckTime = 420 - (int)_dTime;

                string strtime = string.Format("{0:0#}:{1:0#}",
                    _acheckTime / 60, _acheckTime % 60);

                if (MyInfoMgr.Instance.m_Power >= 10)
                {
                    IE_RechargeHeart_Loop = false;
                    _guiPlayerInfo.SetPowerRechargeCount("");
                    break;
                }

                if (_acheckTime <= 0.0f)
                {
                    IE_RechargeHeart_Loop = false;

                    TSpan.SetTime(System.DateTime.Now);

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetHeartTime(TSpan.getTimeStampINT());

                    _guiPlayerInfo.SetPowerRechargeCount("");

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().PowerSuPlus(1);

                    if (MyInfoMgr.Instance.m_Power < 10 && GameStateMgr.Instance.GetStatusStage() == "GSStageMap")
                    {
                        StartCoroutine(IE_TimeCheck());
                    }

                    break;
                }
                else
                {
                    _guiPlayerInfo.SetPowerRechargeCount(strtime);
                }

                if (_bFocus)
                {
                    IE_RechargeHeart_Loop = false;
                    _bFocus = false;

                    Debug.Log("Focus Check Update");

                    if (MyInfoMgr.Instance.m_Power < 10)
                    {
                        CheckUpdateHeart(_guiPlayerInfo);
                    }

                    break;
                }

                yield return new WaitForFixedUpdate();
            }
        }
        else
        {
            if (_guiPlayerInfo != null)
            {
                if (_guiPlayerInfo.GetPowerRecharge() == "")
                {
                    IE_RechargeHeart_Loop = false;

                    CheckUpdateHeart(_guiPlayerInfo);
                }
            }
            else
            {
                if (GameStateMgr.Instance.GetStatusStage() == "GSStageMap")
                {
                    _guiPlayerInfo = GuiMgr.Instance.Show<GuiPlayerInfo>(GuiMgr.ELayerType.Back, true, null);

                    IE_RechargeHeart_Loop = false;

                    CheckUpdateHeart(_guiPlayerInfo);
                }
            }
        }
    }

    IEnumerator IE_RechargeHeart()
    {
        if (IE_RechargeHeart_Loop == false)
        {
            IE_RechargeHeart_Loop = true;
            Debug.Log("RechargeHeart");

            while(true)
            {
                if (_bLoding) 
                {
                    break;
                }
                yield return new WaitForFixedUpdate();
            }

            _guiPlayerInfo = GameObject.Find("GuiPlayerInfo").GetComponent<GuiPlayerInfo>();
            
            //_dServerTime = KakaoGameUserInfo.Instance.server_time;

            Debug.Log("_dServerTime :" + _dServerTime);
            
            while (true)
            {
                if (MyInfoMgr.Instance.m_Power >= 5 || KakaoMain.Instance._bLoding == false)
                {
                    Debug.Log("MaxHeart");
                    IE_RechargeHeart_Loop = false;
                    _guiPlayerInfo.SetPowerRechargeCount("");
                    break;
                }
                _dServerTime += Time.fixedDeltaTime;

                double _dTime = _dServerTime - _dRegenStartAt;
                int _acheckTime = _Heart_Interval - (int)_dTime;

                string strtime = string.Format("{0:0#}:{1:0#}",
                    _acheckTime / 60, _acheckTime % 60);

                if (_acheckTime <= 0.0f || _bFocus)
                {
                    _guiPlayerInfo.SetPowerRechargeCount("");

                    KakaoNativeExtension.Instance.loadGameInfo(this.onGameHeartCheckComplete, this.onGameInfoError);
                    
                    IE_RechargeHeart_Loop = false;
                    _bFocus = false;
                    break;
                }
                else 
                {
                    _guiPlayerInfo.SetPowerRechargeCount(strtime);
                }
                
                yield return new WaitForFixedUpdate();
            }
        }
    }

    void onGameHeartCheckComplete() 
    {
        _Heart_Interval = KakaoGameInfo.Instance.heart_regen_interval;

        Debug.Log("HeartInterval:" + _Heart_Interval);
        
        KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);
    }

    bool _bFocus = false;

    //버전 - 1
    void OnApplicationFocus(bool focusStatus)
    {
        if (focusStatus && GameStateMgr.Instance.GetStatusStage() == "GSStageMap")
        {
            _bFocus = true;
            Debug.Log("Focus");
            CheckUpdateHeart(_guiPlayerInfo);
        }
    }

    public void UseMapHelpNextViliage()
    {
        TimeStamp TSpan = new TimeStamp();

        TSpan.SetTime(System.DateTime.Now);

        if (GameObject.Find("InfoMgr").GetComponent<InfoSave>().NextViliageTime == 0)
        {
            GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetNextVillageTime(TSpan.getTimeStampINT());
        }
    }

    /*
     
                if (CheckTime <= 0)
                {
                    ES3.DeleteKey("NextViliageTime");

                    break;
                }

     */
    //무료 마을 이동까지 남은 시간
    bool _bRegen = false;
    bool isRebenCheck = false;
    public void SetcheckTime(bool isOK) 
    {
        _bRegen = false;
        isRebenCheck = isOK;
    }

    public int CheckTime = 10;
    public  IEnumerator IE_RegenNextViliage()
    {
        UseMapHelpNextViliage();

        if (_bRegen == false && GameObject.Find("InfoMgr").GetComponent<InfoSave>().NextViliageTime > 0)
        {
            _bRegen = true;

            TimeStamp TSpan = new TimeStamp();

            TSpan.SetTime(System.DateTime.Now);

            double ServerTime = TSpan.getTimeStampDou();

            int RegenStartAt = GameObject.Find("InfoMgr").GetComponent<InfoSave>().NextViliageTime;

            while (true)
            {
                ServerTime += Time.deltaTime;

                int Timecheck = (int)ServerTime - RegenStartAt;

                CheckTime = 86400 - Timecheck;

                if (CheckTime <= 0) 
                {
                    break;
                }

                if (isRebenCheck) 
                {
                    isRebenCheck = false;
                    _bRegen = false;
                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetNextVillageTime(0);
                    break;
                }

                yield return new WaitForFixedUpdate();
            }
        }
    }


}
