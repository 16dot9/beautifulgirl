﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class RequestInfo : MonoBehaviour {

	#region Server	
	// 나중에는 서버에서 날짜 정보를 가지고 있어야 한다...
	private Dictionary<string, bool> _requestFriendInfoDictionary;	// 일정 시간 내에 조르기 메시지를 한 번이라도 보낸 유저의 리스트...	
	#endregion
	
	public string RequestFriendID { get; set; }			// 조르기 메시지를 보낸 유저의 아이디...
	
	
	void Awake ()
	{
		ClearRequestFriendInfo ();
		_requestFriendInfoDictionary = new Dictionary<string, bool> ();
	}
	
	
	void OnDestroy ()
	{
		ClearRequestFriendInfo ();
	}
	
	
	/// <summary>
	/// 일정 시간 내에 조르기 메시지를 보낸 모든 친구 리스트 초기화...
	/// </summary>
	private void ClearRequestFriendInfo()
	{
		//ES3.DeleteKey ("InviteInfo");
		
		if (_requestFriendInfoDictionary != null)
			_requestFriendInfoDictionary.Clear ();
	}
	
	
	#region 
	// 로컬, 서버에 저장할 경우 암호화해야 함...
	// 현재는 임시로 로컬에 저장함...
	
	/// <summary>
	/// 조르기 메시지를 보낸 친구 정보를 _requestFriendInfoDictionary 리스트에 저장함...
	/// 현재는 isInvite 변수는 무조건 true 값이 들어옴...
	/// </summary>
	public void SaveRequestFriendInfo (bool isInvite)
	{
		if (_requestFriendInfoDictionary.ContainsKey (RequestFriendID))
		{
			_requestFriendInfoDictionary.Remove (RequestFriendID);
		}
		
		_requestFriendInfoDictionary.Add (RequestFriendID, isInvite);
		
		ES3.Save<Dictionary<string, bool>> ("RequestInfo", _requestFriendInfoDictionary);
		
//		////KakaoNativeExtension.Instance.ShowAlertMessage("SaveRequestFriendInfo : " + RequestFriendID);
	}
	
	
	/// <summary>
	/// 일정 시간 내에 조르기 메시지를 보낸 모든 친구 리스트(현재는 로컬에 저장되어 있음)를 불러들임...
	/// </summary>
	public void LoadRequestFriendInfo()
	{
		if (ES3.KeyExists ("RequestInfo"))
		{
			_requestFriendInfoDictionary = ES3.Load<Dictionary<string, bool>>("RequestInfo");
			
	//		////KakaoNativeExtension.Instance.ShowAlertMessage("loadRequestFriendInfo");
		}
	}
	
	#endregion
	
	
	/// <summary>
	/// 친구에게 조르기 메시지를 보낸 적이 있는지 확인...
	/// </summary>
	public bool FindRequestFriend(string userID)
	{
		bool isRequest = false;
		
		if (_requestFriendInfoDictionary.ContainsKey (userID))
		{
			_requestFriendInfoDictionary.TryGetValue (userID, out isRequest);
			
			//////KakaoNativeExtension.Instance.ShowAlertMessage("FindRequestFriend : true : " + userID);
		//	////KakaoNativeExtension.Instance.ShowAlertMessage("Find RequestFriend : true");
			
			return isRequest;
		}
		
		return isRequest;
	}
	
	
	/// <summary>
	/// 조르기 메시지를 보낸 유저의 수...
	/// </summary>
	public int GetRequestFriendCount()
	{
		if (_requestFriendInfoDictionary == null)
			return -1;

		return _requestFriendInfoDictionary.Count;
	}
}
