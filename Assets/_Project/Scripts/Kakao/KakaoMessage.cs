﻿using UnityEngine;
using System.Collections;

public class KakaoMessage {

	public enum MessageType
	{
		InviteFriend = 1,			// 친구 초대...
		RequestFriend_Gem,			// 젬 요청...
		RequestFriend_Power,		// 파워 요청...
		RequestFriend_Funny,		// 퍼니 요청...
		BoastFriend	,				// 자랑하기...
		GiftFriend_Gem,				// 선물하기...
        SendFriend_Power,
	    NextVillage
    };

	public class RecieveMsg
	{
        public static string InviteFriend_Msg = "귀여운 키펫들과 함께 핸드폰 안에서 즐기는 여행! 키펫 브레이크! 지금 바로 한판 즐겨볼까요?";
        public static string RequestFriend_Gem_Msg = "젬 요청이 왔습니다. 친구를 도와주세요.";
        public static string RequestFriend_Power_Msg = KakaoGameUserInfo.Instance.nickname + "님이 파워가 부족하시대요~돕고 사는 세상!!버튼을 눌러서 파워를 보낼수 있어요! ";
        public static string RequestFriend_Funny_Msg = "퍼니 요청이 왔습니다. 친구를 도와주세요.";
        public static string BoastFriend_Msg = "키펫 브레이크에서" + KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._nickname + "님이"
            + KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._sStage + "판에서" + KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._nScoreCurrent +"점을 획득했어요! 우리도 힘내볼까요?";
        public static string GiftFriend_Gem_Msg = "젬 선물이 도착했어요.";
        public static string NextVillage_Msg = "님이 다음 마을로 가고 싶어해요. 버튼을 눌러서 도움을 주세요~ㅠㅠ!";
    }
	
	public class SendMsg
	{
		public static string InviteFriend_Msg				= " 님에게 카카오톡으로 초대장을 보내시겠습니까";
		public static string RequestFriend_Gem_Msg			= " 님에게 카카오톡으로 젬 요청을 하시겠습니까?";
        public static string RequestFriend_Power_Msg        = " 님에게 카카오톡으로 파워 요청을 하시겠습니까?";
        public static string SendFriend_Power_Msg           = " 님에게 카카오톡으로 파워를 보내시겠습니까?";
		public static string RequestFriend_Funny_Msg		= " 님에게 카카오톡으로 퍼니 요청을 하시겠습니까?";
		public static string BoastFriend_Msg				= " 님에게 카카오톡으로 자랑을 하시겠습니까?";
		public static string GiftFriend_Gem_Msg				= " 님에게 젬을 선물하시겠습니까?";
        public static string NextVillage_Msg                = " 님께 도움요청 메시지를 카카오톡으로 보내시겠습니까?";
	
    }

}
