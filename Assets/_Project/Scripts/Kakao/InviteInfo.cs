﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// 게임을 설치하지 않은 10명을 초대했는데 그 중 2명이 게임을 설치했다..
// InviteInfo 리스트에는 여전히 10명으로 되어 있다...
// 8명으로 수정해야 하나? 
// 그냥 단순히 초대했던 친구 리스트만 가지고 있을 수 있지만, 리스트에서 제거해야 하나?...


public class InviteInfo : MonoBehaviour {

	#region Server	
	// 나중에는 서버에서 날짜 정보를 가지고 있어야 한다(31일 이후에 해제시키려면)....
	private Dictionary<string, bool> _inviteFriendInfoDictionary;	// 31일 이내에 초대 메시지를 한 번이라도 보낸 유저의 리스트...	
	#endregion
	
	public string InvitedFriendID { get; set; }			// 초대 메시지를 보낸 유저의 아이디...


	void Awake ()
	{
		ClearInvitedFriendInfo ();
		_inviteFriendInfoDictionary = new Dictionary<string, bool> ();
	}


	void OnDestroy ()
	{
		ClearInvitedFriendInfo ();
	}


	/// <summary>
	///  31일 이내에 초대 메시지를 보낸 모든 친구 리스트 초기화...
	/// </summary>
	private void ClearInvitedFriendInfo()
	{
		//ES3.DeleteKey ("InviteInfo");
		
		if (_inviteFriendInfoDictionary != null)
			_inviteFriendInfoDictionary.Clear ();
	}


	#region 
	// 로컬, 서버에 저장할 경우 암호화해야 함...
	// 현재는 임시로 로컬에 저장함...

	/// <summary>
	/// 초대 메시지를 보낸 친구 정보를 _inviteFriendInfoDictionary 리스트에 저장함...
	/// 현재는 isInvite 변수는 무조건 true 값이 들어옴...
	/// </summary>
	public void SaveInvitedFriendInfo (bool isInvite)
	{
		if (_inviteFriendInfoDictionary.ContainsKey (InvitedFriendID))
		{
			_inviteFriendInfoDictionary.Remove (InvitedFriendID);
		}
		
		_inviteFriendInfoDictionary.Add (InvitedFriendID, isInvite);
		
		ES3.Save<Dictionary<string, bool>> ("InviteInfo", _inviteFriendInfoDictionary);
		
//		////KakaoNativeExtension.Instance.ShowAlertMessage("SaveInvitedFriendInfo : " + InvitedFriendID);	// 아직 초대 중인 유저는 아이디 앞에 -가 표시됨...
	}


	/// <summary>
	/// 31일 이내에 초대 메시지를 보낸 모든 친구 리스트(현재는 로컬에 저장되어 있음)를 불러들임...
	/// </summary>
	public void LoadInvitedFriendInfo()
	{
		if (ES3.KeyExists ("InviteInfo"))
		{
			_inviteFriendInfoDictionary = ES3.Load<Dictionary<string,bool>>("InviteInfo");
			
//			////KakaoNativeExtension.Instance.ShowAlertMessage("LoadInvitedUserInfo");
		}
	}

	#endregion


	/// <summary>
	/// 친구에게 초대 메시지를 보낸 적이 있는지 확인...
	/// </summary>
	public bool FindInvitedFriend(string userID)
	{
		bool isInvite = false;
		
		if (_inviteFriendInfoDictionary.ContainsKey (userID))
		{
			_inviteFriendInfoDictionary.TryGetValue (userID, out isInvite);
			
			//////KakaoNativeExtension.Instance.ShowAlertMessage("FindInvitedFriend : true : " + userID);
//			////KakaoNativeExtension.Instance.ShowAlertMessage("Find InvitedFriend : true");
			
			return isInvite;
		}
		
		return isInvite;
	}


	/// <summary>
	/// 초대장을 보낸 유저의 수...
	/// </summary>
	public int GetInvitedFriendCount()
	{
		if (_inviteFriendInfoDictionary == null)
			return -1;

		return _inviteFriendInfoDictionary.Count;
	}
}
