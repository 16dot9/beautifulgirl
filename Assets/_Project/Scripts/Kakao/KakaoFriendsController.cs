﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;


public class KakaoFriendsController : MonoBehaviour 
{
    public bool _bGameFriendLoad;
    public bool _bLeaderBoard;
    public float _BoastTime = 0;
    public int _nStageidx= 0;

	
	public List<MyFriend> _myKakaoKeypetFriends;			// 키펫 설치된 친구들...
	public List<MyFriend> _myKakaoNoKeypetFriends;			// 키펫 설치 안 한 친구들...

	public bool UsingKakaoFriends { get; set; }				// 카카오 친구 목록을 어디선가 사용 중이면 true... 사용이 끝난 이후에는 false...
	public bool LoadingKakaoFriends { get; set; }			// 카카오 서버에서 친구 목록을 로드 중이면 true... 로드가 끝난 이후에는 false...

	private InviteInfo		_inviteInfo;

	private RequestInfo 	_requestInfo;

    public KakaoWindowPopup _kakaoWindowPopup;

	private static KakaoFriendsController _Instance;
	public static KakaoFriendsController Instance
	{
		get 
		{
			if (_Instance == null)
			{
				_Instance = GameObject.FindObjectOfType<KakaoFriendsController>();
				if (_Instance == null)
				{
					GameObject container = new GameObject();
					container.name = "KakaoFriendsController";
					_Instance = container.AddComponent<KakaoFriendsController>();
					DontDestroyOnLoad(_Instance);
				}
			}
			
			return _Instance;
		}
	}


	/// <summary>
	/// 모든 카카오 친구 리스트 초기화...
	/// </summary>
	public void ClearMyFriends()
	{
        try
        {
            _bLeaderBoard = false;
            if (_myKakaoKeypetFriends != null)
            {
                _myKakaoKeypetFriends.Clear();
            }

            if (_myKakaoNoKeypetFriends != null)
            {
                //foreach (MyFriend myFriend in _myKakaoNoKeypetFriends)
                //{
                //    DestroyImmediate(myFriend.profileImage);
                //}
                _myKakaoNoKeypetFriends.Clear();
            }

            UsingKakaoFriends = false;
            LoadingKakaoFriends = false;
        }
        catch(Exception e) 
        {
            Debug.Log("Error Load :" + e.ToString());
        }
    }


	/// <summary>
	/// 카카오 친구가 있는지 확인...
	/// </summary>
	public bool HasMyFriends() {
		if( _myKakaoKeypetFriends.Count > 0 )
			return true;
		
		if( _myKakaoNoKeypetFriends.Count > 0 )
			return true;
		
		return false;
	}


	/// <summary>
	/// 카카오 서버에서 카카오 친구 목록 로드...
	/// </summary>
	public void LoadKakaoFriends()
	{
        StartCoroutine(IE_LoadKakaoFriends());
    }
    IEnumerator IE_LoadKakaoFriends()
    {
        yield return new WaitForSeconds(0.5f);

        Debug.Log("Load KakaoFriends");

#if UNITY_EDITOR

        #region 유니티 상에서 친구 목록 테스트하기 위해서 만든 목록 Delete 해도 무방

        StartCoroutine(IE_EDITOR_FRIEND());
        #endregion

#endif
        //       //KakaoNativeExtension.Instance.ShowAlertMessage("친구리스트 확인...");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
        if (KakaoGameUserInfo.Instance.user_id != null && KakaoGameUserInfo.Instance.user_id.Length > 0)
        {
            KakaoNativeExtension.Instance.loadGameFriends(this.onLoadGameFriendsComplete, this.onLoadGameFriendsError);
        }
        else
        {
 //           //KakaoNativeExtension.Instance.ShowAlertMessage("친구리스트 불러오기 실패 확인...");
//            Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
//            cmd.onUpdateUserComplete();
        }

        yield return null;
    }


    public void onGameUserInfoComplete()
    {
        Debug.Log("onGameUserInfoComplete");
    }

    public void onGameUserInfoError(string status, string message)
    {
        Debug.Log("onGameUserInfoError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

	/// <summary>
	/// 카카오 친구 목록 로드 성공시 호출...
	/// </summary>
    public void onLoadGameFriendsComplete()
    {
        Debug.Log("onLoadGameFriendsComplete");

		// _myKakaoNoKeypetFriends 리스트에 카카오 친구들 저장하기...
	    StartCoroutine(	SetMyKakaoFriends());
	}


	/// <summary>
	/// 카카오 친구 목록 로드 실패시 호출..
	/// </summary>
    public void onLoadGameFriendsError(string status, string message) 
	{
        Debug.Log("status + message :" + status + message);

		//KakaoMain.Instance.ShowKakaoMessageBox (status, message, "Kakao Friends : Loading Error");

		KakaoMain.Instance.showAlertErrorMessage(status,message);
	}

    /// <summary>
    ///  다운 로딩 리스트
    /// </summary>
    Dictionary<string, MyFriend> _DownloadList = new Dictionary<string, MyFriend>();
    Queue<string> _DownoladQ = new Queue<string>();
    void DownloadFriendsImage_Add(string str_url, MyFriend myKakaoFriend)
    {
        _DownoladQ.Enqueue(str_url);
        if (_DownloadList.ContainsKey(str_url) == false)
        {
            _DownloadList.Add(str_url, myKakaoFriend);
        }
        StartCoroutine(IE_DownloadFriendsImage());
    }
    bool bDownloading = false;
    IEnumerator IE_DownloadFriendsImage()
    {
        if (bDownloading == false)
        {
            bDownloading = true;
            while (_DownoladQ.Count > 0)
            {
                string url = _DownoladQ.Dequeue();
                if (_DownloadList.ContainsKey(url) == true)
                {
                    WWW www = new WWW(url);
                    yield return www;
                    if (!String.IsNullOrEmpty(www.error))
                    {
                        continue;
                    }
                    else
                    {
                        MyFriend myobj = _DownloadList[url];
                        myobj.profileImage = (www.texture);
                        myobj._isUpdateTexture = true;
                    }
                }
                yield return null;
            }
 //           //KakaoNativeExtension.Instance.ShowAlertMessage("image download ok");
            _DownoladQ.Clear();
            _DownloadList.Clear();
            bDownloading = false;
        }
    }


    private IEnumerator IE_EDITOR_FRIEND() 
    {
        ClearMyFriends();	// 저장된 친구가 있다면 다 지우기...

        Texture2D defaultTexture = KakaoMain.Instance.DefaultProfileImage;

        //친구 목록 초기화
        for(int  i = 0; i < 300; i++)
        {
            // MyFriend 클래스 생성...
            MyFriend myKakaoFriend = new MyFriend(defaultTexture, false);	// 친구 이미지 파일이 없는 경우도 있으므로 디폴트 이미지로 세팅...

            myKakaoFriend.nickname = "Invite_";
            myKakaoFriend.userid = "88219865535413120"; // Ahn 카카오톡 UserId
            myKakaoFriend.messageBlocked = false;
            myKakaoFriend.profileImageUrl = "";
            myKakaoFriend.SupportendDevice = true;

            // StartCoroutine(FriendProfileImageLoad(myKakaoFriend.profileImageUrl, myKakaoFriend));
            DownloadFriendsImage_Add(myKakaoFriend.profileImageUrl, myKakaoFriend);

            // 리스트에 myKakaoFriend 클래스 저장...
            _myKakaoNoKeypetFriends.Add(myKakaoFriend);

            yield return null;
        }
    }



	/// <summary>
	/// _myKakaoNoKeypetFriends, _myKakaoKeypetFriends 리스트에 카카오 친구들 저장하기...
	/// </summary>
    ///
	private IEnumerator SetMyKakaoFriends ()
	{
		ClearMyFriends ();	// 카카오 친구 목록을 가져오기 전에 저장된 데이터 지우기... 
		LoadingKakaoFriends = true;

		Texture2D defaultTexture = KakaoMain.Instance.DefaultProfileImage;
	    
		#region _myKakaoNoKeypetFriends (키펫 설치 안 함) 리스트 생성... 
        
        int friendsCount = KakaoGameFriends.Instance.kakaotalkFriends.Count;

//        //KakaoNativeExtension.Instance.ShowAlertMessage("native kakao NoKeypet friends count : " + friendsCount.ToString());

        //int firstLoadCount = 500; // 10 명 만 test
        //int cnt = 0;
        foreach (var pair in KakaoGameFriends.Instance.kakaotalkFriends)
        {
            KakaoGameFriends.KakaotalkFriend kakaotalkFriend = pair.Value;
            if (kakaotalkFriend == null)
                continue;

			// MyFriend 클래스 생성...
			MyFriend myKakaoFriend = new MyFriend (defaultTexture, false);	// 친구 이미지 파일이 없는 경우도 있으므로 디폴트 이미지로 세팅...

            myKakaoFriend.nickname = kakaotalkFriend.nickname;
            myKakaoFriend.userid = kakaotalkFriend.userid;
            myKakaoFriend.messageBlocked = kakaotalkFriend.messageBlocked;
            myKakaoFriend.profileImageUrl = kakaotalkFriend.profileImageUrl;
            myKakaoFriend.SupportendDevice = kakaotalkFriend.supportedDevice;
            
            // StartCoroutine(FriendProfileImageLoad(myKakaoFriend.profileImageUrl, myKakaoFriend));
            DownloadFriendsImage_Add(myKakaoFriend.profileImageUrl, myKakaoFriend);

			// 리스트에 myKakaoFriend 클래스 저장...
			_myKakaoNoKeypetFriends.Add (myKakaoFriend);

            yield return null; // new WaitForEndOfFrame();

            //cnt++;
            //if (firstLoadCount < cnt)
            //{
            //    break;
            //}
		}

        yield return null;

		if (_myKakaoNoKeypetFriends.Count != friendsCount)
		{
//			string msg = string.Format("카카오 친구 수 : {0} 와 내 카카오 친구 수 : {1} 가 다릅니다{0}, {1}", friendsCount, _myKakaoNoKeypetFriends.Count);
//			//KakaoNativeExtension.Instance.ShowAlertMessage(msg);
		}

		#endregion

		#region _myKakaoKeypetFriends (키펫 설치) 리스트 생성... 

        friendsCount = KakaoGameFriends.Instance.leaderboardFriends.Count;

//~~~~~~~        //KakaoNativeExtension.Instance.ShowAlertMessage("카카오 친구 로드 완료.");
//        _bGameFriendLoad = true;
//        LoadingKakaoFriends = false;

 //       //KakaoNativeExtension.Instance.ShowAlertMessage("native kakao Keypet friends count : " + friendsCount.ToString());
        int _nCount = 1;

        foreach (var pair in KakaoGameFriends.Instance.leaderboardFriends)
        {
            KakaoGameFriends.LeaderboardFriend leaderboardFriend = pair.Value;
            if (leaderboardFriend == null && leaderboardFriend.userid == KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._userid)
                continue;
			
			// MyFriend 클래스 생성...
			MyFriend myKakaoFriend = new MyFriend (defaultTexture, false);	// 친구 이미지 파일이 없는 경우도 있으므로 디폴트 이미지로 세팅...

            myKakaoFriend.nickname = leaderboardFriend.nickname;
            myKakaoFriend.userid = leaderboardFriend.userid;
            myKakaoFriend.messageBlocked = leaderboardFriend.messageBlocked;
            myKakaoFriend.profileImageUrl = leaderboardFriend.profileImageUrl;
            
            // StartCoroutine(FriendProfileImageLoad(myKakaoFriend.profileImageUrl, myKakaoFriend));
            DownloadFriendsImage_Add(myKakaoFriend.profileImageUrl, myKakaoFriend);

            // 리스트에 myKakaoFriend 클래스 저장...
            _myKakaoKeypetFriends.Add(myKakaoFriend);

            _nCount++;
//            yield return null;
		}

        //if (_myKakaoKeypetFriends.Count != friendsCount)
        //{
        //    string msg = string.Format("카카오 설치 친구 수 : {0} 와 내 카카오 설치 친구 수 : {1} 가 다릅니다{0}, {1}", friendsCount, _myKakaoKeypetFriends.Count);
        //    //KakaoNativeExtension.Instance.ShowAlertMessage(msg);
        //}

		#endregion

        _bGameFriendLoad = true;
        LoadingKakaoFriends = false;
    }
    IEnumerator FriendProfileImageLoad(string url, MyFriend _go)
    {
        WWW www = new WWW(url);

        yield return www;

        if (!String.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
        }
        else
        {
            _go.profileImage = (www.texture);
        }
    }

    private string _userid = "";

	/// <summary>
	/// 카카오톡으로 친구에게 메시지 보내기...
	/// </summary>
	public void SendMessageToMyKakaoFriend (MyFriend myKakaoFriend, KakaoMessage.MessageType messageType, Dictionary<string,string> metaInfo)
	{
		switch(messageType)
		{ 
            case KakaoMessage.MessageType.SendFriend_Power:

                _requestInfo.RequestFriendID = myKakaoFriend.userid;
                    string publicData = "itemId|0000101";

                    KakaoNativeExtension.Instance.sendGameMessage(
                        myKakaoFriend.userid,
                        KakaoLocalUser.Instance.nickName + " 님이 파워를 보내셨어요! 다시 힘내서 키펫 세계로 떠나볼까요?",
                        "This is game message. You can check this message in game.",
                        1,
                        null,
                        Encoding.UTF8.GetBytes(publicData),
                        onSendGameMessageComplete,
                        onSendGameMessageError);

                break;
			case KakaoMessage.MessageType.InviteFriend :			// 친구 초대 메시지 보내기...		
			{
                GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
				_inviteInfo.InvitedFriendID = myKakaoFriend.userid;

                #region UNITY_ 친구 초대 테스트 < Editor 지워도 무방함

#if UNITY_EDITOR
                StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_INVITE_FRIEND_REQ(_inviteInfo.InvitedFriendID, KakaoFriendsController.Instance._kakaoWindowPopup.window));
#elif UNITY_ANDROID || UNITY_IPHONE
				KakaoNativeExtension.Instance.SendInviteImageMessage ("1622", myKakaoFriend.userid, "itemid=02", metaInfo, onSendInviteImageMessageComplete, onSendInviteImageMessageError);
#endif
                #endregion
                break;
			}
			case KakaoMessage.MessageType.RequestFriend_Gem :		// 젬 요청 메시지 보내기...		
			{
				_requestInfo.RequestFriendID = myKakaoFriend.userid;
				KakaoNativeExtension.Instance.SendMessage (KakaoMessage.RecieveMsg.RequestFriend_Gem_Msg, myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
				break;
			}
			case KakaoMessage.MessageType.RequestFriend_Power :		// 파워 요청 메시지 보내기...		
			{
				_requestInfo.RequestFriendID = myKakaoFriend.userid;
				
				KakaoNativeExtension.Instance.SendMessage (KakaoMessage.RecieveMsg.RequestFriend_Power_Msg, myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
				break;
			}
			case KakaoMessage.MessageType.RequestFriend_Funny :		// 퍼니 요청 메시지 보내기...		
			{
				_requestInfo.RequestFriendID = myKakaoFriend.userid;
				
				KakaoNativeExtension.Instance.SendMessage (KakaoMessage.RecieveMsg.RequestFriend_Funny_Msg, myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
				break;
			}
			case KakaoMessage.MessageType.BoastFriend :				// 자랑하기 메시지 보내기...		
			{
				_requestInfo.RequestFriendID = myKakaoFriend.userid;
				
				KakaoNativeExtension.Instance.SendImageMessage("1672", myKakaoFriend.userid, "", "itemid=02", metaInfo, onSendRequestMessageComplete, onSendRequestMessageError);

                StartCoroutine(myKakaoFriend.IE_BoastTime());
                break;
			}
			case KakaoMessage.MessageType.GiftFriend_Gem :		// 젬 선물 메시지 보내기...		
			{
				_requestInfo.RequestFriendID = myKakaoFriend.userid;
				
				KakaoNativeExtension.Instance.SendMessage (KakaoMessage.RecieveMsg.GiftFriend_Gem_Msg, myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
				break;
			}
            case KakaoMessage.MessageType.NextVillage:
            {
                _requestInfo.RequestFriendID = myKakaoFriend.userid;

                KakaoNativeExtension.Instance.SendMessage(KakaoLocalUser.Instance.nickName + KakaoMessage.RecieveMsg.NextVillage_Msg, myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
				
                break;
            }
		}
	}

	/*
	/// <summary>
	/// 카카오톡으로 친구에게 메시지 보내기...
	/// </summary>
	public void SendMessageToMyKakaoFriend (MyFriend myKakaoFriend, string messageType)
	{
		if (messageType == "InviteFriend")			// 친구 초대...
		{
			Dictionary<string,string> metaInfo = new Dictionary<string, string>();
			metaInfo.Add("nickname","A Good Friend");
			
			//_invitedFriendID = myKakaoFriend.userid;
			_inviteInfo.InvitedFriendID = myKakaoFriend.userid;
			
			// 카카오톡으로 초대 메시지 발송...
			KakaoNativeExtension.Instance.SendInviteImageMessage ("1622", myKakaoFriend.userid, "itemid=02",metaInfo, onSendInviteImageMessageComplete, onSendInviteImageMessageError);
		}
		else if (messageType == "RequestFriend")		// 조르기...
		{
			_requestInfo.RequestFriendID = myKakaoFriend.userid;
			
			KakaoNativeExtension.Instance.SendMessage ("조르기 요청이 왔습니다. 친구를 도와주세요.", myKakaoFriend.userid, "itemid=001&count=1", onSendRequestMessageComplete, onSendRequestMessageError);
		}
	}*/


	#region 초대....

	/// <summary>
	/// 초대 메시지 발송 성공시...
	/// </summary>
	private void onSendInviteImageMessageComplete() 
	{
		// 초대 메시지를 발송한 친구를 저장...
		_inviteInfo.SaveInvitedFriendInfo(true);
		
        /*
		// 초대 받은 친구 수를 표시...
		int count = GetInvitedFriendCount();

        Kakao_InviteFriend_Window window = KakaoFriendsController.Instance._kakaoWindowPopup.window;

        window.ShowFriendCount("초대한 친구 수 : " + count.ToString());
        */

        StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_INVITE_FRIEND_REQ(_inviteInfo.InvitedFriendID, KakaoFriendsController.Instance._kakaoWindowPopup.window));

//		//KakaoNativeExtension.Instance.ShowAlertMessage("Succeed Send_InviteImaGemessage");
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
	}
	
	
	/// <summary>
	/// 초대 메시지 발송 실패시...
	/// </summary>
	private void onSendInviteImageMessageError(string status, string message) 
	{
		//_invitedUserID = null;	// 굳이 필요없지만 null로 만듬...
		
		KakaoMain.Instance.ShowKakaoMessageBox (status, message, "초대 메시지");
		
		KakaoMain.Instance.showAlertErrorMessage(status,message);
	}


	/// <summary>
	/// 친구에게 초대 메시지를 보낸 적이 있는지 확인...
	/// </summary>
	public bool FindInvitedFriend(string userID)
	{
		return _inviteInfo.FindInvitedFriend (userID);
	}


	/// <summary>
	/// 초대장을 보낸 유저의 수...
	/// </summary>
	public int GetInvitedFriendCount()
	{
		return _inviteInfo.GetInvitedFriendCount ();
	}


	/// <summary>
	/// 31일 이내에 초대 메시지를 보낸 모든 친구 리스트 로드 (현재는 로컬에 저장되어 있음)...
	/// </summary>
	public void LoadInvitedFriendInfo()
	{
		_inviteInfo.LoadInvitedFriendInfo ();
	}

	#endregion


	#region 요청....

	/// <summary>
	/// 요청 메시지 발송 성공시 호출됨...
	/// </summary>
	private void onSendRequestMessageComplete()
	{
		// 메시지를 발송한 친구를 저장...
		//_requestInfo.SaveRequestFriendInfo(true);
		
		// 메시지를 받은 친구 수를 표시...
        //int count = GetRequestFriendCount();
        //GameObject.Find ("GuiKakaotalkReq").GetComponent<GuiKakaotalkReq> ().ShowInvitedFriendCount ("요청한 친구 수 : " + count.ToString());
		
//		//KakaoNativeExtension.Instance.ShowAlertMessage("Succeed Send_RequestMessage");
	}
	
	
	/// <summary>
	/// 요청 메시지 발송 실패시 호출됨...
	/// </summary>
	private void onSendRequestMessageError(string status, string message)
	{
		KakaoMain.Instance.ShowKakaoMessageBox ("요청 메시지 에러", status + message, "요청 메시지");
		
		KakaoMain.Instance.showAlertErrorMessage(status,message);
	}


	/// <summary>
	/// 요청 메시지를 보낸 적이 있는지 확인...
	/// </summary>
	public bool FindRequestFriend(string userID)
	{
		return _requestInfo.FindRequestFriend (userID);
	}
	
	
	/// <summary>
	/// 요청 메시지를 보낸 유저의 수...
	/// </summary>
	public int GetRequestFriendCount()
	{
		return _requestInfo.GetRequestFriendCount ();
	}
	
	
	/// <summary>
	/// 일정 시간 내에 요청 메시지를 보낸 모든 친구 리스트 로드 (현재는 로컬에 저장되어 있음)...
	/// </summary>
	public void LoadRequestFriendInfo()
	{
		_requestInfo.LoadRequestFriendInfo ();
	}
	#endregion


	#region 친구 리스트 윈도우...
	/// <summary>
	/// 초대하기 위해 '친구 리스트 윈도우' 보여주기...
	/// </summary>
	public void ShowKakaoInviteWindow()
	{
	    StartCoroutine(	_kakaoWindowPopup.InviteKakaoFriends());

        ////~~~~~~~~~~~~~~~~~~~~~~~~~~~  test
        //ClearMyFriends();
        //LoadKakaoFriends();
	}
    
	/// <summary>
	/// 젬 요청을 하기 위해 '친구 리스트 윈도우' 보여주기...
	/// </summary>
	public void ShowKakaoRequestWindowForGem()
	{
		StartCoroutine (_kakaoWindowPopup.RequestKakaoKeypetFriendsForGem ());
	}


	/// <summary>
	/// 파워 요청을 하기 위해 '친구 리스트 윈도우' 보여주기...
	/// </summary>
	public void ShowKakaoRequestWindowForPower()
	{
		StartCoroutine (_kakaoWindowPopup.RequestKakaoKeypetFriendsForPower ());
	}


	/// <summary>
	/// 다음 마을로 이동하기 위채 젬 요청 '친구 리스트 윈도우' 보여주기...
	/// </summary>
	public void ShowKakaoRequestWindowForNextVillage()
	{
		StartCoroutine (_kakaoWindowPopup.RequestKakaoKeypetFriendsForNextVillage ());
	}


	/// <summary>
	/// 젬 선물하기 위해 '친구 리스트 윈도우' 보여주기...
	/// </summary>
	public void ShowKakaoGiftWindowForGem()
	{
		StartCoroutine (_kakaoWindowPopup.GiftKakaoKeypetFriendsForGem ());
	}
	#endregion


	void Awake ()
	{
		//ClearMyFriends ();
		_myKakaoKeypetFriends = new List<MyFriend>();
		_myKakaoNoKeypetFriends = new List<MyFriend>();
		
		_inviteInfo = GetComponent<InviteInfo> ();
		_requestInfo = GetComponent<RequestInfo> ();
		_kakaoWindowPopup = GetComponent<KakaoWindowPopup> ();

        _bGameFriendLoad = false;
	}


	void OnDestroy ()
	{
		ClearMyFriends ();
	}

    private void onSendGameMessageComplete()
    {
        Debug.Log("Success");

        //        //KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendGameMessage");
    }
    private void onSendGameMessageError(string status, string message)
    {
        KakaoMain.Instance.ShowKakaoMessageBox(status, message, "하트 보내기");
    }
}
