﻿using UnityEngine;
using System.Collections;

public class KakaoLocalUserController : MonoBehaviour {

	public GameObject UserProfilePrefab;
	public string UserNickName 	{ get; set;	}

	private static KakaoLocalUserController _Instance;
	public static KakaoLocalUserController Instance
	{
		get 
		{
			if (_Instance == null)
			{
				_Instance = GameObject.FindObjectOfType<KakaoLocalUserController>();
				if (_Instance == null)
				{
					GameObject container = new GameObject();
					container.name = "KakaoLocalUserController";
					_Instance = container.AddComponent<KakaoLocalUserController>();
					DontDestroyOnLoad(_Instance);
				}
			}
			
			return _Instance;
		}
	}


	/// <summary>
	/// 카카오 유저 고유의 닉네임과 프로필 사진 보여주기...
	/// </summary>
	public void LoadUserProfile()
	{
		KakaoNativeExtension.Instance.LocalUser (onLocalUserComplete, onLocalUserError);
	}
	
	
	/// <summary>
	/// 카카오 유저 고유 정보 로드 성공시 호출...
	/// </summary>
	private void onLocalUserComplete() 
	{	
		string nickName 		= KakaoLocalUser.Instance.nickName;
		string profileImageUrl 	= KakaoLocalUser.Instance.profileImageUrl;
		bool messageBlocked 	= KakaoLocalUser.Instance.messageBlocked;	// "카카오톡>설정>카카오계정>연결된앱관리>키펫브레이크>카카오톡 메시지 수신설정"의 값이 반영된 것임... 

		// 사용하지 않는 정보는 주석처리...
		//		string hashedTalkUserId = KakaoLocalUser.Instance.hashedTalkUserId;
		//      string userId			= KakaoLocalUser.Instance.userId;
		//		string countryIso 		= KakaoLocalUser.Instance.countryIso;
						
		UserNickName = nickName;
		//StartCoroutine( GetUserProfile (profileImageUrl, nickName) );

		//////KakaoNativeExtension.Instance.ShowAlertMessage("카톡 메시지 블럭 상태인가? : " + messageBlocked.ToString() );
	}
	

	/// <summary>
	/// 카카오 유저 고유 정보 로드 실패시 호출...
	/// </summary>
	private void onLocalUserError(string status, string message) 
	{
		KakaoMain.Instance.showAlertErrorMessage (status,message);
	}
	
	
	/// <summary>
	/// 유저 정보 화면에 보여주기... 임시...
	/// </summary>
	private IEnumerator GetUserProfile(string profileImageUrl, string nickName)
	{
		GameObject userProfile = Instantiate (UserProfilePrefab) as GameObject;
		
		userProfile.transform.name = "UserProfile";
		userProfile.transform.parent = GuiMgr.Instance.GetBackCamera ().transform;
		userProfile.transform.localPosition = new Vector3(0, Screen.height / 4f, 0);
		userProfile.transform.localScale = Vector3.one;
		userProfile.transform.localRotation = Quaternion.identity;
		
		WWW www = new WWW (profileImageUrl);
		
		yield return www;
		
		if (nickName != null && nickName.Length > 0)
			userProfile.GetComponentInChildren<UILabel> ().text = "환영합니다!" + "\n" + nickName;
		
		if( profileImageUrl != null && profileImageUrl.Length > 0 )
			userProfile.transform.Find("ProfileImage").GetComponentInChildren<UITexture> ().mainTexture = www.texture;
		
		
		// or ....
		//	Texture2D downTexture;
		//	www.LoadImageIntoTexture (downTexture);
		//	profile.GetComponentInChildren<UITexture> ().mainTexture = downTexture;
		
		www.Dispose ();
	}

	/// <summary>
	/// 유저 계정 정보 넘겨 주기...
	/// </summary>
	public string GetUserAccount()
	{
		return KakaoLocalUser.Instance.userId;
	}

	/// <summary>
	/// 카카오톡 메시지 수신을 거부한 상태인지...
	/// </summary>
	public bool MessageBlocked()
	{
		return KakaoLocalUser.Instance.messageBlocked;	// "카카오톡>설정>카카오계정>연결된앱관리>키펫브레이크>카카오톡 메시지 수신설정"의 값이 반영된 것임...
	}

}
