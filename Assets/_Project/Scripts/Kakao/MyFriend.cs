﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MyFriend
{
    public float _fBoastTime = new float();

    public bool SupportendDevice {get; set;}

    public int[] _nLeaderboardStage = new int[330];

    public string profileImageUrl { get; set; }

    public string nickname { get; set; }
    public string userid { get; set; }
    public bool messageBlocked { get; set; }			// 카카오 계정 연동에서 게임 메시지 수신 거부 유저의 경우에는 false값을 가짐... 초대 메시지 거부 유저의 경우는 확인할 방법이 없으므로 리턴코드 -17을 반환하는지 확인할 것..
    public Texture2D profileImage { get; set; }
    //public bool isSendInviteMessage { get; set; }		// 해당 친구에게 초대 메시지를 보냈다면 true... 서버에서 관리해야

    public bool _isUpdateTexture = false; // _isUpdateTexture  가 true 일 때, 사진을 업데이트 한다. 보이기 아이템 상에서.
    public MyFriend(Texture2D defaultImage, bool a_isUpdateTexture)
    {
        _isUpdateTexture = a_isUpdateTexture;
        profileImage = defaultImage;					// 기본 프로필 이미지를 세팅...
    }

    public  IEnumerator IE_BoastTime() 
    {
        while (true) 
        {
            if (_fBoastTime >= 60.0f)
            {
                _fBoastTime = 0.0f;

                break;
            }
            else 
            {
                _fBoastTime += Time.deltaTime; 
            }

            yield return new WaitForFixedUpdate();
        }
    }
}
