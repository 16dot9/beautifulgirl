﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class KakaoWindowPopup : MonoBehaviour {

	public GameObject	_prefab_InviteFriendItem = null;					// 친구초대 아이템바...
	public GameObject	_prefab_RequestFriendItem = null;					// 젬, 파워 요청 아이템바... 	
	public GameObject	_prefab_NextVillagePopup_RequestFriendItem = null;	// 다음 마을로 이동 요청 아이템바...
	public GameObject	_prefab_GiftFriendItem = null;						// 젬 선물 아이템바... 	

    private bool _bSetup = false;

    public Kakao_InviteFriend_Window window;


	/// <summary>
	/// 카카오 친구 초대 윈도우 나타내기...
	/// </summary>
    float _defaultOffsetY = -92;
    IEnumerator SetItem_InviteKakaoFriends(List<MyFriend> myKakaoFriends)
    {
        if (myKakaoFriends == null)
        {
            for (int i = 0; i < 300; i++)
            {
                GameObject itemGO = GameObject.Instantiate(_prefab_InviteFriendItem) as GameObject;
                itemGO.name = string.Format("A_{0:0000}", i); // "A_000" + i.ToString();
                Kakao_InviteFriend_Item item = itemGO.GetComponent<Kakao_InviteFriend_Item>();
                
                // 리스트 윈도우에 친구 추가하기...
                item.SetItemInfo(i, true, false,
                    (Texture2D)Resources.Load("Images/Friends/profile_0001")
                    , "invite_0" + i.ToString(), null);
                window.Add_KakaoFriendItem(item, i * _defaultOffsetY);

                yield return null;
            }
        }
        else
        {
            int icount = 0;
            foreach (var pair in myKakaoFriends)
            {
                GameObject itemGO = GameObject.Instantiate(_prefab_InviteFriendItem) as GameObject;
                itemGO.name = string.Format("A_{0:0000}", icount);   // "A_000" + _Count.ToString();

                Kakao_InviteFriend_Item item = itemGO.GetComponent<Kakao_InviteFriend_Item>();

                // 리스트 윈도우에 친구 추가하기...
                item.SetItemInfo(icount,pair.SupportendDevice, pair.messageBlocked,
                    pair.profileImage
                    , pair.nickname
                    , pair
                    );

                window.Add_KakaoFriendItem(item, icount * _defaultOffsetY);

                bool isAlreadySendMessage = KakaoFriendsController.Instance.FindInvitedFriend(pair.userid);	// 이미 초대 메시지를 보낸 친구인지 확인...

                if (isAlreadySendMessage)	// 초대 메시지를 한 번이라도 보낸 유저라면 체크버튼 비활성화...
                {
                    UIToggle toggle = item.Button;
                    toggle.value = true;									// 체크표시 나타내고...
                    toggle.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
                }

                // 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
                if (pair.messageBlocked == true)
                {
                    item.GetComponentInChildren<UILabel>().color = Color.red;
                }

                icount++;
                yield return null; // new WaitForFixedUpdate();
            }

            //window.ScrollView_RepositionNow();
        }
        yield return null;
    }
    public IEnumerator InviteKakaoFriends()
	{
        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();

        

		window = GuiMgr.Instance.Show<Kakao_InviteFriend_Window>(GuiMgr.ELayerType.Front, true, null);

        // "나가기" 버튼 클릭시 처리...
        window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
        {
            window.ClearItems();

            window._Panel.transform.localPosition =
                new Vector3(0.0f, 0.0f, 0.0f);

            window._Panel.clipOffset = new Vector2(0.0f, 0.0f);

            GuiMgr.Instance.Show<Kakao_InviteFriend_Window>(GuiMgr.ELayerType.Front, false, null);

        };

        #region KakaoInvite Load

        // 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
        while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
            yield return null;



        // 처리중 창 열기.
        GuiMgr.Instance.Show<GuiProcessing_BoardType>(GuiMgr.ELayerType.Front, true, null);

        List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoNoKeypetFriends;	// _myKakaoNoKeypetFriends 리스트 가져오기...
        int myKakaoFriendsCount = myKakaoFriends.Count;
        KakaoFriendsController.Instance.LoadInvitedFriendInfo();	// 이미 초대 메시지를 보낸 유저 리스트 가져오기...
        int count = KakaoFriendsController.Instance.GetInvitedFriendCount();
        window.ShowFriendCount("초대한 친구 수 : " + cmd.g_invCnt.ToString());      // 초대 메시지를 보낸 친구 수 보여주기...
#if UNITY_EDITOR

        _bSetup = false;
///////////////////////////////////////////////////////////////////////////

        StartCoroutine(SetItem_InviteKakaoFriends(null) );
//        window.ScrollView_RepositionNow();
        _bSetup = true;
        yield return new WaitForFixedUpdate();
        // 처리중 창 닫기.
        GuiMgr.Instance.Show<GuiProcessing_BoardType>(GuiMgr.ELayerType.Front, false, null);

        //while (true) 
        //{
        //    if (_bSetup) 
        //    {
        //        window.PanelInfo();
        //    }

        //    yield return new WaitForFixedUpdate();
        //}


 #elif UNITY_ANDROID
        StartCoroutine(SetItem_InviteKakaoFriends(myKakaoFriends));
        yield return new WaitForFixedUpdate();
        _bSetup = true;
        // 처리중 창 닫기.
        GuiMgr.Instance.Show<GuiProcessing_BoardType>(GuiMgr.ELayerType.Front, false, null);

#endif

        #endregion
        yield return null;
    }


	/// <summary>
	/// 키펫 설치한 카카오 친구 윈도우 나타내고 "젬 요청"...
	/// </summary>
	public IEnumerator RequestKakaoKeypetFriendsForGem()
	{
		KakaoFriendsController.Instance.UsingKakaoFriends = true;

		Kakao_RequestFriend_Window window = GuiMgr.Instance.Show<Kakao_RequestFriend_Window>(GuiMgr.ELayerType.Front, true, null);
		
		// 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
		while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
			yield return null;
		
		List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;	// _myKakaoKeypetFriends 리스트 가져오기...
		int myKakaoFriendsCount = myKakaoFriends.Count;
		
//		//KakaoNativeExtension.Instance.ShowAlertMessage ("myKakaoKeypetFriendsCount : " + myKakaoFriendsCount.ToString());
		
		KakaoFriendsController.Instance.LoadRequestFriendInfo ();	// 이미 조르기 메시지를 보낸 유저 리스트 가져오기...
		
		int count = KakaoFriendsController.Instance.GetRequestFriendCount ();
		window.ShowFriendCount ("조르기한 친구 수 : " + count.ToString());							// 조르기 메시지를 보낸 친구 수 보여주기...
		
        Debug.Log("myKakaoFriendsCount :" + myKakaoFriendsCount);

		for (int i = 0; i < myKakaoFriendsCount; i++)
		{
            if(myKakaoFriends[i].userid != KakaoLocalUserController.Instance.GetUserAccount ())
            { 
			    GameObject itemGO 				= GameObject.Instantiate (_prefab_RequestFriendItem) as GameObject;
			    Kakao_RequestFriend_Item item	= itemGO.GetComponent<Kakao_RequestFriend_Item>();
			
			    // 리스트 윈도우에 친구 추가하기...
			    item.SetItemInfo (i, myKakaoFriends[i].profileImage, myKakaoFriends[i].nickname );			
			    window.Add_KakaoFriendItem(item);
			
			    bool isAlreadySendMessage = KakaoFriendsController.Instance.FindRequestFriend(myKakaoFriends[i].userid);	// 이미 조르기 메시지를 보낸 친구인지 확인...
			
			    if (isAlreadySendMessage)	// 조르기 메시지를 한 번이라도 보낸 유저라면 체크버튼 비활성화...
			    {
				    UIToggle toggle = item.Button;
				    toggle.value = true;									// 체크표시 나타내고...
				    toggle.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
			    }
			
			    // 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
			    if (myKakaoFriends[i].messageBlocked == true)
			    {
				    item.GetComponentInChildren<UILabel>().color = Color.red;
                }
            }
		}
		
		window.ScrollView_RepositionNow ();
		
		////KakaoNativeExtension.Instance.ShowAlertMessage("그리드 내 아이템 수량 :  " + inviteWindow.GetGridChild ().ToString());
		
		// "나가기" 버튼 클릭시 처리...
		window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
		{
			KakaoFriendsController.Instance.UsingKakaoFriends = false;
			
			window.ClearItems();
			window.ExitBtn.onClick = null;
			window = null;
			
			GuiMgr.Instance.Show<Kakao_RequestFriend_Window>(GuiMgr.ELayerType.Front, false, null);	
		};
	}

	
	/// <summary>
	/// 키펫 설치한 카카오 친구 윈도우 나타내고 "파워 요청"...
	/// </summary>
	public IEnumerator RequestKakaoKeypetFriendsForPower()
	{
		KakaoFriendsController.Instance.UsingKakaoFriends = true;
				
		Kakao_RequestFriend_Window window = GuiMgr.Instance.Show<Kakao_RequestFriend_Window>(GuiMgr.ELayerType.Front, true, null);
		
		// 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
		while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
			yield return null;
		
		List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;	// _myKakaoKeypetFriends 리스트 가져오기...
		int myKakaoFriendsCount = myKakaoFriends.Count;
		
//		//KakaoNativeExtension.Instance.ShowAlertMessage ("myKakaoKeypetFriendsCount : " + myKakaoFriendsCount.ToString());
		
		KakaoFriendsController.Instance.LoadRequestFriendInfo ();	// 이미 조르기 메시지를 보낸 유저 리스트 가져오기...
		
		int count = KakaoFriendsController.Instance.GetRequestFriendCount ();
		window.ShowFriendCount ("조르기한 친구 수 : " + count.ToString());							// 조르기 메시지를 보낸 친구 수 보여주기...

        Debug.Log("Count :" + count.ToString());

		for (int i = 0; i < myKakaoFriendsCount; i++)
		{
            if(myKakaoFriends[i].userid != KakaoLocalUserController.Instance.GetUserAccount ())
            { 
			GameObject itemGO 				= GameObject.Instantiate (_prefab_RequestFriendItem) as GameObject;
			Kakao_RequestFriend_Item item 	= itemGO.GetComponent<Kakao_RequestFriend_Item>();
			
			// 리스트 윈도우에 친구 추가하기...
			item.SetItemInfo (i, myKakaoFriends[i].profileImage, myKakaoFriends[i].nickname );			
			window.Add_KakaoFriendItem(item);
			
			bool isAlreadySendMessage = KakaoFriendsController.Instance.FindRequestFriend(myKakaoFriends[i].userid);	// 이미 조르기 메시지를 보낸 친구인지 확인...
			
			if (isAlreadySendMessage)	// 조르기 메시지를 한 번이라도 보낸 유저라면 체크버튼 비활성화...
			{
				UIToggle toggle = item.Button;
				toggle.value = true;									// 체크표시 나타내고...
				toggle.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
			}
			
			// 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
			if (myKakaoFriends[i].messageBlocked == true)
			{
				item.GetComponentInChildren<UILabel>().color = Color.red;
            }
            }
		}
		
		window.ScrollView_RepositionNow ();
		
		////KakaoNativeExtension.Instance.ShowAlertMessage("그리드 내 아이템 수량 :  " + inviteWindow.GetGridChild ().ToString());
		
		// "나가기" 버튼 클릭시 처리...
		window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
		{
			KakaoFriendsController.Instance.UsingKakaoFriends = false;
			
			window.ClearItems();
			window.ExitBtn.onClick = null;
			window = null;
			
			GuiMgr.Instance.Show<Kakao_RequestFriend_Window>(GuiMgr.ELayerType.Front, false, null);
		};
	}


	/// <summary>
	/// 다음 마을로 이동 버튼 클릭시, 키펫 설치 및 미설치한 카카오 친구 윈도우 나타내고 "젬 요청"...
	/// </summary>
	public IEnumerator RequestKakaoKeypetFriendsForNextVillage()
	{
		KakaoFriendsController.Instance.UsingKakaoFriends = true;
		
		Kakao_NextVillagePopup_RequestFriend_Window window = GuiMgr.Instance.Show<Kakao_NextVillagePopup_RequestFriend_Window>(GuiMgr.ELayerType.Front, true, null);

        // "나가기" 버튼 클릭시 처리...
        window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
        {
            KakaoFriendsController.Instance.UsingKakaoFriends = false;

            window.ClearItems();
            window.ExitBtn.onClick = null;
            window = null;

            GuiMgr.Instance.Show<Kakao_NextVillagePopup_RequestFriend_Window>(GuiMgr.ELayerType.Front, false, null);
        };

		// 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
		while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
			yield return null;
		
		#region 키펫 설치한 친구들 리스트...
		//////////////
		List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;	// _myKakaoKeypetFriends 리스트 가져오기...
		int myKakaoFriendsCount = myKakaoFriends.Count;
		
//		//KakaoNativeExtension.Instance.ShowAlertMessage ("myKakaoKeypetFriendsCount : " + myKakaoFriendsCount.ToString());
		
		KakaoFriendsController.Instance.LoadRequestFriendInfo ();	// 이미 조르기 메시지를 보낸 유저 리스트 가져오기...
		
		int count = KakaoFriendsController.Instance.GetRequestFriendCount ();
		window.ShowFriendCount ("조르기한 친구 수 : " + count.ToString());							// 조르기 메시지를 보낸 친구 수 보여주기...
		
		for (int i = 0; i < myKakaoFriendsCount; i++)
		{
            if(myKakaoFriends[i].userid != KakaoLocalUserController.Instance.GetUserAccount())
            { 
			    GameObject itemGO 								= GameObject.Instantiate (_prefab_NextVillagePopup_RequestFriendItem) as GameObject;

                itemGO.name = "A_" + i.ToString();
                Kakao_NextVillagePopup_RequestFriend_Item item 	= itemGO.GetComponent<Kakao_NextVillagePopup_RequestFriend_Item>();
			
			    // 리스트 윈도우에 친구 추가하기...
			    item.SetItemInfo (i, myKakaoFriends[i].profileImage, myKakaoFriends[i].nickname );			
			    window.Add_KakaoFriendItem(item);
			
			    bool isAlreadySendMessage = KakaoFriendsController.Instance.FindRequestFriend(myKakaoFriends[i].userid);	// 이미 조르기 메시지를 보낸 친구인지 확인...
			
			    if (isAlreadySendMessage)	// 조르기 메시지를 한 번이라도 보낸 유저라면 체크버튼 비활성화...
			    {
				    UIToggle toggle = item.Button;
				    toggle.value = true;									// 체크표시 나타내고...
				    toggle.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
			    }
			
			    // 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
			    if (myKakaoFriends[i].messageBlocked == true)
			    {
				    item.GetComponentInChildren<UILabel>().color = Color.red;
                }
            }
		}
		//////////////
		#endregion
				
		window.ScrollView_RepositionNow ();
		
		////KakaoNativeExtension.Instance.ShowAlertMessage("그리드 내 아이템 수량 :  " + inviteWindow.GetGridChild ().ToString());

        StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_LIST_HELP_REQ(myKakaoFriends, window));
		
	}


	/// <summary>
	/// 키펫 설치한 카카오 친구 윈도우 나타내고 "젬 선물"...
	/// </summary>
	public IEnumerator GiftKakaoKeypetFriendsForGem()
	{
		KakaoFriendsController.Instance.UsingKakaoFriends = true;
		
		Kakao_GiftFriend_Window window = GuiMgr.Instance.Show<Kakao_GiftFriend_Window>(GuiMgr.ELayerType.Front, true, null);
		
		// 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
		while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
			yield return null;
		
		List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;	// _myKakaoKeypetFriends 리스트 가져오기...
		int myKakaoFriendsCount = myKakaoFriends.Count;
		
//		//KakaoNativeExtension.Instance.ShowAlertMessage ("myKakaoKeypetFriendsCount : " + myKakaoFriendsCount.ToString());
		
		KakaoFriendsController.Instance.LoadRequestFriendInfo ();	// 이미 요청 메시지를 보낸 유저 리스트 가져오기...
		
		int count = KakaoFriendsController.Instance.GetRequestFriendCount ();
		window.ShowFriendCount ("선물한 친구 수 : " + count.ToString());							// 선물한 친구 수 보여주기...
		
		for (int i = 0; i < myKakaoFriendsCount; i++)
		{
			GameObject itemGO 				= GameObject.Instantiate (_prefab_GiftFriendItem) as GameObject;
			Kakao_GiftFriend_Item item	= itemGO.GetComponent<Kakao_GiftFriend_Item>();
			
			// 리스트 윈도우에 친구 추가하기...
			item.SetItemInfo (i, myKakaoFriends[i].profileImage, myKakaoFriends[i].nickname );			
			window.Add_KakaoFriendItem(item);
			
			bool isAlreadySendMessage = KakaoFriendsController.Instance.FindRequestFriend(myKakaoFriends[i].userid);	// 이미 조르기 메시지를 보낸 친구인지 확인...
			
			if (isAlreadySendMessage)	// 조르기 메시지를 한 번이라도 보낸 유저라면 체크버튼 비활성화...
			{
				UIToggle toggle = item.Button;
				toggle.value = true;									// 체크표시 나타내고...
				toggle.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
			}
			
			// 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
			if (myKakaoFriends[i].messageBlocked == true)
			{
				item.GetComponentInChildren<UILabel>().color = Color.red;
			}
		}
		
		window.ScrollView_RepositionNow ();
		
		////KakaoNativeExtension.Instance.ShowAlertMessage("그리드 내 아이템 수량 :  " + inviteWindow.GetGridChild ().ToString());
		
		// "나가기" 버튼 클릭시 처리...
		window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
		{
			KakaoFriendsController.Instance.UsingKakaoFriends = false;
			
			window.ClearItems();
			window.ExitBtn.onClick = null;
			window = null;
			
			GuiMgr.Instance.Show<Kakao_GiftFriend_Window>(GuiMgr.ELayerType.Front, false, null);	
		};
	}


	void test()
	{				
	}

    void LateUpdate()
    {
        if (_bSetup)
        {
            window.PanelInfo();
        }
    }
}
