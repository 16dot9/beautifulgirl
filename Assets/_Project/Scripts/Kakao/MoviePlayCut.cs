﻿using UnityEngine;
using System.Collections;

public class MoviePlayCut : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //StartCoroutine(PlayVideo("[Cut]1min.mp4"));

	}
	
	// Update is called once per frame
	void Update () {        
    }
    IEnumerator PlayVideo(string videoPath) 
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("Allowing all orientations");
        AllowAutoRotation(true);
        yield return new WaitForSeconds(.1f);
        Debug.Log("Playing video");
        yield return new WaitForEndOfFrame();
        Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.Full);
        Debug.Log("Video Finished");
        Debug.Log("Setting to Portrait");
        AllowAutoRotation(false); yield return new WaitForSeconds(.1f);
    }

    void AllowAutoRotation(bool status) 
    {
        if (status == true)
        {
            Screen.orientation = ScreenOrientation.LandscapeRight;
        }
        else
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
    } 
}
