﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoMain : MonoBehaviour
{

    public Texture2D DefaultProfileImage;	// 유저, 카카오친구들의 기본 프로필 이미지...

    private static KakaoMain _Instance;
    public static KakaoMain Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<KakaoMain>();
                if (_Instance == null)
                {
                    GameObject container = new GameObject();
                    container.name = "KakaoMain";
                    _Instance = container.AddComponent<KakaoMain>();
                    DontDestroyOnLoad(_Instance);
                }
            }

            return _Instance;
        }
    }

    public string _SenderId;

    /// <summary>
    /// 카카오 초기화...
    /// </summary>
    public void Init()
    {
        Debug.Log("Init ! ");
        // 초기화 성공시 onInitComplete() 호출함...
        // 토큰 변경시 onTokens() 호출함...
        KakaoNativeExtension.Instance.Init(onInitComplete, onTokens);
    }

    /// <summary>
    /// KakaoSDK 초기화 성공시 호출...
    /// </summary>
    private void onInitComplete()
    {
        Debug.Log("onInitComplete ! ");

        // 카카오 계정으로 로그인 인증 처리...
        KakaoNativeExtension.Instance.Authorized(onAuthorized);
    }

    /// <summary>
    /// <----------------------->
    /// _nCnt Dummy 삭제해도 무방
    /// <----------------------->
    /// </summary>
    private int _nCnt = 0;

    private void onLocalUserComplete()
    {
#if UNITY_EDITOR
        StartSend_PROTOCOL_LOGIN_REQ();
#elif UNITY_ANDROID || UNITY_IPHONE    
        StartKakaoloadGameInfo();
#endif

    }
    void StartSend_PROTOCOL_LOGIN_REQ()
    {
        StartCoroutine(this.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_LOGIN_REQ(
            KakaoLocalUser.Instance.nickName, KakaoLocalUser.Instance.userId,
            PlayerPrefs.GetString(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs)));//KakaoLocalUser.Instance.userId));
    }

    void StartKakaoloadGameInfo()
    {
        KakaoNativeExtension.Instance.loadGameInfo(this.onGameInfoComplete, this.onGameInfoError);
        // onGameInfoComplete --> KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);
        // onGameUserInfoComplete_StartType --> StartSend_PROTOCOL_LOGIN_REQ

    }

    private void onGameInfoComplete()
    {
        Debug.Log("onGameInfoComplete");
        KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete_StartType, this.onGameUserInfoError);
    }
    //Load Leaderboard
    private void onLoadLeaderboardComplete()
    {
        Debug.Log("onLoadLeaderboardComplete");
        // test
        //KakaoLeaderboards.Instance.printToConsole();
    }
    private void onLoadLeaderboardError(string status, string message)
    {
        Debug.Log("onLoadLeaderboardError");
        KakaoReConnectMessage(_DefaultMessage);
    }
    private void onGameUserInfoComplete_StartType()
    {
        // 유저 갱신 및 하트 갱신 확인.
        onGameUserInfoComplete();
        // 디폴트 리더보드 호출 체크.
        //KakaoNativeExtension.Instance.loadLeaderboard("DEFAULT", this.onLoadLeaderboardComplete, this.onLoadLeaderboardError);

        // 하트 이외의 유저 정보 로긴 처리.
        StartSend_PROTOCOL_LOGIN_REQ();
    }
    private void onGameInfoError(string status, string message)
    {
        Debug.Log("onGameInfoError");
        KakaoReConnectMessage(_DefaultMessage);
    }
    public string _DefaultMessage = "접속이 원활하지 않습니다.\n잠시후 재시도 해주십시오";
    public void KakaoReConnectMessage(string strMessage)
    {
        //Hwang Wark Message Popup Start
        JSONNode msgParam = new JSONClass();
        msgParam["message"] = strMessage;
        msgParam["show_type"] = "one";

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...
        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
                // onInitComplete();
                Application.Quit();
            }
        };
    }

    private void onLocalUserError(string status, string message)
    {
        showAlertErrorMessage(status, message);
    }
    // 중복 사용 체크 필요.
    private void onGameUserInfoComplete()
    {
        //NGUIDebug.Log("onGameUserInfoComplete");
        _dRegenStartAt = KakaoGameUserInfo.Instance.heart_regen_starts_at;
        string heart_regen_starts_at = KakaoGameUserInfo.Instance.heart_regen_starts_at.ToString();
        MyInfoMgr.Instance.m_Power = (uint)KakaoGameUserInfo.Instance.heart;

        if (MyInfoMgr.Instance.m_Power < 5)
        {
            StartCoroutine(IE_RechargeHeart());
        }
        //NGUIDebug.Log("heart:" + MyInfoMgr.Instance.m_Power);
    }
    private void onGameUserInfoError(string status, string message)
    {
        Debug.Log("onGameUserInfoError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    /// <summary>
    /// 카카오 계정 인증 처리를 위해 호출...
    /// </summary>
    private void onAuthorized(bool _authorized)
    {
        Debug.Log("authorized : " + _authorized);

        if (_authorized)	// 카카오 계정으로 이미 로그인 프로세스가 진행되었다면 게임으로 진입...
        {
            KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
        }
        else   // 카카오 계정으로 로그인을 하지 않은 것이라면 약관 동의(및 로그인) 유도 화면으로 진입...
        {
            GameStateMgr.Instance.ChangeState(0, typeof(GSLoginAgreement), null);
        }
    }

    /// <summary>
    /// token이 변경되는 시점에 호출되는 델리게이트 함수 (로그인, 로그아웃, 탈퇴, 토근만료 및 자동갱신 등)...
    /// </summary>
    private void onTokens(string accessToken, string refreshToken)
    {
        // 인증 토큰을 최신 상태로 유지...
        KakaoNativeExtension.Instance.updateTokenCache(accessToken, refreshToken);

        if (KakaoNativeExtension.Instance.hasValidTokenCache() == true)	// 인증 토큰이 유효하다면 게임으로 진입...
        {
            Debug.LogError("<---------------->");
            Debug.LogError("2. Token Local");
            Debug.LogError("<---------------->");
            KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
        }
        else   // 인증 토큰이 유효하지 않다면 약관 동의(및 로그인)을 유도시키는 화면으로 진입...
        {
            GameStateMgr.Instance.ChangeState(0, typeof(GSLoginAgreement), null);
        }
    }


    /// <summary>
    /// 게임 맵 화면으로 전환 및 카카오 유저정보/친구정보 로딩...
    /// </summary>
    public void GoToStageMap()
    {
        #region from Server

        bool BlockUser = false;	// 재가입이 금지되었는지 여부는 서버에서 받아야 하는 값임 (일단은 테스트로 세팅함).....

        #endregion

        // 재가입이 금지된 유저의 경우 다시 탈퇴 처리 시킴...
        if (BlockUser)
        {
            JSONNode msgParam = new JSONClass();

            msgParam["message"] = "재가입이 불가능한 회원입니다. \n 나중에 다시 시도해 주세요.";
            msgParam["show_type"] = "one";		// "ok" button...

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk)
                {
                    Unregister();
                }
            };
            return;
        }

        //KakaoLocalUserController.Instance.LoadUserProfile();		// 카카오 유저 정보 로드...	
        InvokeRepeating("RefreshKakaoFriends", 0f, 60f * 60f);		// 18분마다 카카오 서버에서 카카오 친구 목록 로드...

        GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);	// 맵 화면으로 전환...
    }


    /// <summary>
    /// 카카오 친구 목록을 카카오 캐싱주기(10분)를 참고해서 갱신함...
    /// </summary>
    private void RefreshKakaoFriends()
    {
        // 친구 목록을 사용하고 있지 않을 때  		---> 친구 목록을 사용 중일 때 데이터를 변경하면 안되므로... 
        if (KakaoFriendsController.Instance.UsingKakaoFriends == true)
        {
            ////KakaoNativeExtension.Instance.ShowAlertMessage("친구리스트를 사용 중이므로 업데이트 안 함...");
            return;
        }

        // 게임 플레이를 하고 있지 않을 때 			--> 플레이를 방해하지 않기 위해...
        GSPlay play = GameStateMgr.Instance.GetComponentInChildren<GSPlay>();
        if (play != null)
        {
            if (play.IsPlayState() == true)
            {
                ////KakaoNativeExtension.Instance.ShowAlertMessage("플레이 중이므로 친구리스트 업데이트 안 함...");
                return;
            }
        }

    }



    /// <summary>
    /// "카카오 로그인" 버튼 클릭시 호출...
    /// </summary>
    public void Login()
    {
        KakaoNativeExtension.Instance.Login(onLoginComplete, onLoginError);
    }


    /// <summary>
    /// "카카오 계정 등록" 버튼 클릭시 호출...
    /// </summary>
    public void WebLogin()
    {
        KakaoResponseHandler.Instance.loginComplete = this.onLoginComplete;
        KakaoResponseHandler.Instance.loginError = this.onLoginError;
        KakaoNativeExtension.Instance.LoginWithWebview(onLoginComplete, onLoginError);
    }


    /// <summary>
    /// 로그인 성공시 호출...
    /// </summary>
    private void onLoginComplete()
    {
        KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
        ////KakaoNativeExtension.Instance.ShowAlertMessage("6. 카카오 로그인 성공");
    }


    /// <summary>
    /// 로그인 에러시 호출...
    /// </summary>
    private void onLoginError(string status, string message)
    {
        KakaoMain.Instance.showAlertErrorMessage(status, message);

        // 로딩 화면 닫기.
        GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, false, null);
    }


    /// <summary>
    /// "로그아웃" 버튼 클릭시 호출...
    /// </summary>
    public void Logout()
    {
        KakaoNativeExtension.Instance.Logout(onLogoutComplete, onLogoutError);
    }

    /// <summary>
    /// 로그아웃 성공시 호출됨...
    /// </summary>
    private void onLogoutComplete()
    {
        // //KakaoNativeExtension.Instance.ShowAlertMessage ("2. 카카오 로그아웃 성공.");

        // 로그아웃했으므로 이후에 토큰의 유효성을 확인하기 위해 onTokens()가 자동 호출됨...
    }


    /// <summary>
    /// 로그아웃 에러시 호출됨...
    /// </summary>
    private void onLogoutError(string status, string message)
    {
        showAlertErrorMessage(status, message);
    }


    /// <summary>
    /// 탈퇴시 호출...
    /// </summary>
    public void Unregister()
    {
        KakaoNativeExtension.Instance.Unregister(onUnregisterComplete, onUnregisterError);
    }


    /// <summary>
    /// 탈퇴 성공시 호출...
    /// </summary>
    private void onUnregisterComplete()
    {
        ////KakaoNativeExtension.Instance.ShowAlertMessage ("회원 탈퇴 성공.");

        // 탈퇴했으므로 이후에 토큰의 유효성을 확인하기 위해 onTokens()가 자동 호출됨...
        StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_PUSH_UNREGISTER_REQ());
    }


    /// <summary>
    /// 탈퇴 실패시 호출...
    /// </summary>
    private void onUnregisterError(string status, string message)
    {
        showAlertErrorMessage(status, message);
    }


    /// <summary>
    /// Shows the alert error message.
    /// </summary>
    public void showAlertErrorMessage(string code, string message)
    {
        string alertMessage = "";
        if (code != null)
        {
            alertMessage += ("Error Code : " + code);
        }

        if (message != null)
        {
            alertMessage += "\n";
            alertMessage += ("Error Message : " + message);
        }

        ////KakaoNativeExtension.Instance.ShowAlertMessage(alertMessage);
    }


    /*	private void onSendMessageComplete() 
        {
            //KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendMessage");
        }

        private void onSendMessageError(string status, string message) 
        {
            //KakaoNativeExtension.Instance.ShowAlertMessage("Send Message Error test");
            showAlertErrorMessage(status,message);
        }
    */

    /// <summary>
    /// Shows the kakao message box.
    /// </summary>
    public void ShowKakaoMessageBox(string errorCode, string errorMessage, string from)
    {
        string msg = null;

        if (errorCode == "-31")
        {
            // test 용으로 호출해 본 것.....
            // SaveInvitedUserInfo (_invitedUserID, true);	

            msg = "동일한 친구에게는 31일 이후에 다시 초대 메시지를 보낼 수 있습니다.";
        }
        else if (errorCode == "-32")
        {
            msg = "하루 최대 30명 이상 초대 메시지를 보낼 수 없습니다.";
        }
        else if (errorCode == "-17")
        {
            msg = "수신자가 카카오톡 초대 메시지를 차단한 상태입니다.";
        }
        else if (errorCode == "-500") // 일단 네트워크 오류일 경우에 확인이 필요해서...
        {
            msg = from + " : 알 수 없는 에러입니다. 네트워크 연결 상태를 확인해 주세요.";
        }
        else if (errorCode == "카카오톡 메시지수신 차단 확인")
        {
            msg = "우선 \"카카오톡 > 더보기 > 설정 > 카카오계정 > 연결된 앱 관리 > 키펫브레이크\"의 메시지 수신을 체크해야 카카오톡 메시지를 받을 수 있습니다.";
        }
        else if (errorCode == "조르기 메시지 에러")
        {
            msg = from + " : 조르기 메시지 전송 실패. " + errorMessage;
        }
        else
            msg = from + " : 메시지를 보낼 수 없습니다. " + errorMessage;

        JSONNode msgParam = new JSONClass();
        msgParam["message"] = msg;
        msgParam["show_type"] = "one";		// "ok" button...

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
            }
        };
    }

    private double _dServerTime;
    private double _dRegenStartAt;
    int _MaxHeart = 0;
    float _HeartTime = 0.0f;
    private int _Heart_Interval;

    bool IE_RechargeHeart_Loop = false;
    IEnumerator IE_RechargeHeart()
    {
        if (IE_RechargeHeart_Loop == false)
        {
            IE_RechargeHeart_Loop = true;
            Debug.Log("RechargeHeart");

            _dServerTime = KakaoGameUserInfo.Instance.server_time;
            while (true)
            {
                if (MyInfoMgr.Instance.m_Power >= 5)
                {
                    Debug.Log("MaxHeart");
                    IE_RechargeHeart_Loop = false;
                    break;
                }
                _dServerTime += Time.fixedDeltaTime;

                double _dTime = _dServerTime - _dRegenStartAt;
                int _acheckTime = _Heart_Interval - (int)_dTime;

                string strtime = string.Format("{0:0#}:{1:0#}",
                    _acheckTime / 60, _acheckTime % 60);
                if (_acheckTime <= 0.0f)
                {
                    break;
                }
                

                yield return new WaitForFixedUpdate();
            }

            KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);

        }
    }    
}
