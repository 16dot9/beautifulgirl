﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public class GuiMgr : MonoBehaviour
{

    #region Singleton

    private static GuiMgr _instance = null;

    public static GuiMgr Instance
    {
        get
        {
            return _instance;
        }
    }

    #endregion Singleton


    public enum ELayerType
    {
        Back,
        Front,
		ScrollView
    }


    protected int								_limitOfPools = 5;
    protected List<GuiBase>					    _guiEntityPools = new List<GuiBase>();

    protected Dictionary<string, GuiBase>		_showGuiEntityList = new Dictionary<string,GuiBase>();
    protected Dictionary<string, GuiBase>       _hideGuiEntityList = new Dictionary<string, GuiBase>();


	public Camera GetBackCamera()
	{
		return GameObject.Find("Gui/Back/Camera").GetComponent<Camera>();
	}

	public UICamera GetBackUICamera()
	{
		return GameObject.Find("Gui/Back/Camera").GetComponent<UICamera>();
	}

	public Camera GetFrontCamera()
	{
		return GameObject.Find("Gui/Front/Camera").GetComponent<Camera>();
	}

	public UICamera GetFrontUICamera()
	{
		return GameObject.Find("Gui/Front/Camera").GetComponent<UICamera>();
	}


    public T Find<T>() where T : GuiBase
    {
        GuiBase guiBase;

        if (_showGuiEntityList.TryGetValue(typeof(T).ToString(), out guiBase))
        {
            return guiBase as T;
        }

        return null;
    }

    public T Show<T>(ELayerType pLayer, bool pShow, JSONNode pParams) where T : GuiBase
    {
        string guiTypeName = typeof(T).ToString();

        if (pShow)
        {
            GuiBase guiBase;

            if (_showGuiEntityList.TryGetValue(guiTypeName, out guiBase))
            {
                return guiBase as T;
            }

            if (_hideGuiEntityList.TryGetValue(guiTypeName, out guiBase))
            {
                guiBase.StopAllCoroutines();
                guiBase.gameObject.SetActive(false);

                _hideGuiEntityList.Remove(guiTypeName);

                _showGuiEntityList.Add(guiTypeName, guiBase);

                guiBase.SetParameter(pParams);

                guiBase.gameObject.SetActive(true);

                return guiBase as T;
            }

            for ( int i = 0; i < _guiEntityPools.Count; ++i )
            {
                guiBase = _guiEntityPools[i];

                if (guiTypeName == guiBase.GetType().ToString())
                {
                    _guiEntityPools.RemoveAt(i);

                    _showGuiEntityList.Add(guiTypeName, guiBase);

                    guiBase.SetParameter(pParams);

                    guiBase.gameObject.SetActive(true);

                    return guiBase as T;
                }
            }

            Transform parentTrans = null;

            switch (pLayer)
            {
                case ELayerType.Back:
                    {
                        parentTrans = GameObject.Find("Gui/Back/Camera").transform;
                    }
                    break;

                case ELayerType.Front:
                    {
						parentTrans = GameObject.Find("Gui/Front/Camera").transform;
                    }
                    break;

				case ELayerType.ScrollView:
					{
						parentTrans = GameObject.Find("Gui").transform;
					}
					break;
			}

            GameObject guiGO = (GameObject)GameObject.Instantiate(Resources.Load(string.Format("Gui/{0}", guiTypeName)));

            guiGO.name = guiTypeName;
            guiGO.transform.parent = parentTrans;
            guiGO.transform.localPosition = Vector3.zero;
            guiGO.transform.localRotation = Quaternion.identity;
            guiGO.transform.localScale = Vector3.one;

            guiBase = (GuiBase)guiGO.GetComponent(guiTypeName);

            _showGuiEntityList.Add(guiTypeName, guiBase);

            guiBase.SetParameter(pParams);

            return guiBase as T;
        }
        else
        {
            GuiBase guiBase = null;

            if (_showGuiEntityList.TryGetValue(guiTypeName, out guiBase))
            {
                _showGuiEntityList.Remove(guiTypeName);

                _hideGuiEntityList.Add(guiTypeName, guiBase);

                guiBase.OnFinish();
            }

            return guiBase as T;
        }
    }


    protected void Awake()
    {
        _instance = this;

        InvokeRepeating("RefreshPoolQueue", 0.0f, 1.0f);
    }

    protected void Update()
    {
        if (0 == _hideGuiEntityList.Count)
        {
            return;
        }

        List<GuiBase> goToPools = null;

        foreach (GuiBase guiBase in _hideGuiEntityList.Values)
        {
            if (guiBase.IsFinished)
            {
                if (null == goToPools)
                {
                    goToPools = new List<GuiBase>();
                }

                goToPools.Add(guiBase);
            }
        }

        if (null != goToPools)
        {
            foreach (GuiBase guiBase in goToPools)
            {
                guiBase.StopAllCoroutines();
                guiBase.gameObject.SetActive(false);

                _hideGuiEntityList.Remove(guiBase.GetType().ToString());

                _guiEntityPools.Add(guiBase);
            }
        }
    }

    protected void OnDestroy()
    {
        StopAllCoroutines();

        _instance = null;
    }


    protected void RefreshPoolQueue()
    {
        if (0 == _guiEntityPools.Count)
        {
            return;
        }

        int delCount = _guiEntityPools.Count - _limitOfPools;

        if (delCount <= 0)
        {
            return;
        }

        List<GuiBase> removeList = null;

        foreach ( GuiBase guiBase in _guiEntityPools )
        {
            if (delCount > 0)
            {
                delCount--;

                if (null == removeList)
                {
                    removeList = new List<GuiBase>();
                }

                removeList.Add(guiBase);
            }
            else
            {
                break;
            }
        }

        if (null != removeList)
        {
            foreach (GuiBase guiBase in removeList)
            {
                _guiEntityPools.Remove(guiBase);

                GameObject.DestroyImmediate(guiBase.gameObject);
            }
        }
    }

}
