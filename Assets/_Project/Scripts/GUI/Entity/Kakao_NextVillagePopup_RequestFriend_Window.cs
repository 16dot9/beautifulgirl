﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class Kakao_NextVillagePopup_RequestFriend_Window : GuiBase 
{
	public Dictionary<int, Kakao_NextVillagePopup_RequestFriend_Item>	_FriendItemList 		= new Dictionary<int, Kakao_NextVillagePopup_RequestFriend_Item> ();	// 키펫 설치한 친구들...
	public Dictionary<int, Kakao_InviteFriend_Item>						_InviteFriendItemList 	= new Dictionary<int, Kakao_InviteFriend_Item>();						// 키펫 설치 안 한 친구들...
	
	
	public UIEventListener ExitBtn
	{
		get { return UIEventListener.Get(_usedTransList["ExitBtn"].gameObject); }
	}
			
	/// <summary>
	/// 키펫 설치한 친구 아이템을 리스트에 추가...
	/// </summary>
	public void Add_KakaoFriendItem(Kakao_NextVillagePopup_RequestFriend_Item item)
	{
		item.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();
		
		item.transform.parent = _usedTransList["UIGrid"];
		item.transform.localPosition = Vector3.zero;
		item.transform.localRotation = Quaternion.identity;
		item.transform.localScale = Vector3.one;
		
		_FriendItemList.Add(item.SerialID, item);
	}


	/// <summary>
	/// 키펫 설치 안 한 친구 아이템을 리스트에 추가...
	/// </summary>
	public void Add_KakaoInviteFriendItem(Kakao_InviteFriend_Item item)
	{
		item.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();
		
		item.transform.parent = _usedTransList["UIGrid"];
		item.transform.localPosition = Vector3.zero;
		item.transform.localRotation = Quaternion.identity;
		item.transform.localScale = Vector3.one;
		
		_InviteFriendItemList.Add(item.SerialID, item);
	}
	
	
	/// <summary>
	/// 모든 Add_KakaoFriendItem() 호출이 끝난 후에 반드시 호출할 것...
	/// </summary>
	public void ScrollView_RepositionNow()
	{
		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
		//_usedTransList ["UIGrid"].GetComponent<UIGrid> ().Reposition ();
	}
	
	
	/// <summary>
	/// "초대/조르기"한 카카오톡 친구 수를 표시함...
	/// </summary>
	public void ShowFriendCount (string text)
	{
        if(_usedTransList ["Count"])
        {
            _usedTransList["Count"].GetComponent<UILabel>().text = text;
        }
	}
	
	// 테스트용...
	public void ResetPosition()
	{
		_usedTransList ["Items"].GetComponent<UIScrollView> ().ResetPosition ();
	}
	
	// 테스트용....
	public int GetGridChild()
	{
		return _usedTransList["UIGrid"].childCount;
	}

	
	public void ClearItems()
	{
		foreach (Kakao_NextVillagePopup_RequestFriend_Item item in _FriendItemList.Values)
		{
			GameObject.DestroyImmediate(item.gameObject);
			//GameObject.Destroy(guiKakaotalkReqItem.gameObject);
		}		
		_FriendItemList.Clear();

		foreach (Kakao_InviteFriend_Item item in _InviteFriendItemList.Values)
		{
			GameObject.DestroyImmediate(item.gameObject);
			//GameObject.Destroy(guiKakaotalkReqItem.gameObject);
		}		
		_InviteFriendItemList.Clear();
		
		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;	// 혹시나 하는 마음으로...
	}
	
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["ExitBtn"] = null;
		
		_usedTransList["Count"] = null;	// 조르기한 친구 수 표시...

        _usedTransList["Friend_01"] = null;
        _usedTransList["Friend_02"] = null;
        _usedTransList["Friend_03"] = null;

		FindTrans();
	}
	
	public override void OnEnter()
	{
	}
	
	public override void OnLeave()
	{
		ClearItems();
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}
    public void SetFriendImage(int _Conut, Texture _Texture, string _nickName)
    {
        Debug.Log("Friend_0" + (_Conut + 1).ToString());

        _usedTransList["Friend_0" + (_Conut + 1).ToString()].GetComponent<UITexture>().mainTexture = _Texture;
        _usedTransList["Friend_0" + (_Conut + 1).ToString()].GetComponentInChildren<UILabel>().text = _nickName;
    }
}
