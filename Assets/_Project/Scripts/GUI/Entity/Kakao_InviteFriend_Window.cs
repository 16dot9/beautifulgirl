﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;


public class Kakao_InviteFriend_Window : GuiBase
{
    public UIPanel _Panel;

	public Dictionary<int, Kakao_InviteFriend_Item>	_FriendItemList = new Dictionary<int, Kakao_InviteFriend_Item>();
	
	public UIEventListener ExitBtn
	{
		get { return UIEventListener.Get(_usedTransList["ExitBtn"].gameObject); }
	}
		
    //public UIEventListener OkBtn
    //{
    //    get { return UIEventListener.Get(_usedTransList["OkBtn"].gameObject); }
    //}
	
		
	public void Add_KakaoFriendItem(Kakao_InviteFriend_Item item, float offsetY)
	{
		item.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();
		
		item.transform.parent = _usedTransList["UIGrid"];
		item.transform.localScale = Vector3.one;
        item.transform.localPosition = new Vector3(0f, offsetY, 0f);  // Vector3.zero;
		item.transform.localRotation = Quaternion.identity;
		
		_FriendItemList.Add(item.SerialID, item);
	}
	
	
	/// <summary>
	/// 모든 Add_KakaoInviteFriendItem() 호출이 끝난 후에 반드시 호출할 것...
	/// </summary>
	public void ScrollView_RepositionNow()
	{
		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
		//_usedTransList ["UIGrid"].GetComponent<UIGrid> ().Reposition ();
	}
        
	/// <summary>
	/// "초대/조르기"한 카카오톡 친구 수를 표시함...
	/// </summary>
	public void ShowFriendCount (string text)
	{
		_usedTransList ["Count"].GetComponent<UILabel> ().text = text;
	}

	// 테스트용...
	public void ResetPosition()
	{
		_usedTransList ["Items"].GetComponent<UIScrollView> ().ResetPosition ();
	}

	// 테스트용....
	public int GetGridChild()
	{
		return _usedTransList["UIGrid"].childCount;
	}
		
	
	public void ClearItems()
	{
		foreach (Kakao_InviteFriend_Item item in _FriendItemList.Values)
		{
			GameObject.DestroyImmediate(item.gameObject);
			//GameObject.Destroy(guiKakaotalkReqItem.gameObject);
		}
		
		_FriendItemList.Clear();
    }
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;
		
//		_usedTransList["OkBtn"] = null;
		_usedTransList["ExitBtn"] = null;
		
		_usedTransList["Count"] = null;	// 초대한 친구 수 표시...
		
		FindTrans();
	}
	
	public override void OnEnter()
	{
	}
	
	public override void OnLeave()
	{
		ClearItems();
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

    float yoffset = 1000f;
    bool isPanelUpdate = false;
    //141022 판넬 안에 있는 친구 목록만 보여준다.
    public void PanelInfo()
    {
        if (isPanelUpdate == false)
        {
            isPanelUpdate = true;
            foreach (var pair in _FriendItemList.Values)
            {
                if (pair.transform.localPosition.y >= ( (_Panel.clipOffset.y - _Panel.GetViewSize().y) - yoffset ) &&
                    pair.transform.localPosition.y <= ( (_Panel.clipOffset.y + 38.0f) + yoffset )
                    )
                {
                    pair.gameObject.SetActive(true);
                }
                else
                {
                    pair.gameObject.SetActive(false);
                }
            }
            isPanelUpdate = false;
        }
    }

}
