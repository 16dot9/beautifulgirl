﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

/*

				for(int i = 0; i < _GUIStage._cGameFriendInfo.Count; i++)
				{
					if(_GUIStage._cGameFriendInfo[i]._UserId == serviceUserId)
					{
						//Season_Best_Score; 140911
						_GUIStage._cGameFriendInfo[i]._nLsLeaderboard.Add(score.season_score);
					}

			_cGameBase = new cGameFriendInfo();
			*/

public class GuiStageMapInfoScrollView : GuiBase
{
	/*
	static KakaoLeaderboards _instance;
	public static KakaoLeaderboards Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new KakaoLeaderboards();
			}
			
			return _instance;
		}
	}*/

	protected UIAnchor							_uiAnchor = null;
	protected UIViewport						_uiViewPort = null;
	protected UIDraggableCamera					_uiDraggableCamera = null;
	protected UITable							_uiTable = null;

	protected Dictionary<string,GameObject>		_mapItemList = new Dictionary<string, GameObject>();
	protected Dictionary<string,GameObject>		_stageItemList = new Dictionary<string, GameObject>();
	protected Dictionary<string,GameObject>		_nextStageItemList = new Dictionary<string, GameObject>();
	protected Dictionary<string,GameObject>		_mapObjectItemList = new Dictionary<string, GameObject>();

	int CountOfStageMapInfo;		// StageMapInfo의 수량을 저장... 현재 약 17개....
	int mapIndexOfScroll = 0;		// 맵 스크롤 시에 map의 인덱스...

    private float _MinLocalY = -260;
    private float _MaxLocalY = 338286; // 320판 이후 y좌표

	public void Setting(UICamera pViewTargetUICamera, Camera pViewTargetCamera, Transform pViewTargetTopLeft, Transform pViewTargetBottomRight)
	{
		_uiAnchor.uiCamera = pViewTargetCamera;

		_uiViewPort.sourceCamera = pViewTargetCamera;
		_uiViewPort.topLeft = pViewTargetTopLeft;
		_uiViewPort.bottomRight = pViewTargetBottomRight;
	}

    /*   // moon...
    public void AddMapItem(string pKey, int id, bool pIsCloud)
    {
        GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewMapItem"));

        go.name = pKey;
        go.transform.parent = _usedTransList["Maps"];   
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;

        go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;

        GuiStageMapInfoScrollViewMapItem mapItem = go.GetComponent<GuiStageMapInfoScrollViewMapItem>();
     
        string strsprite = string.Format("Map_{0}", ((id % 2) + 1).ToString() );
        // Map_0  는 Map_1로 대체
        if (strsprite == "Map_0")
        {
            strsprite = "Map_1";
        }
     
        mapItem.SetSpriteName(strsprite);
        mapItem.ShowCloud(pIsCloud);

        _mapItemList.Add(pKey, go);

        _uiTable.repositionNow = true;
    }
*/
    public void ClearMapItem()
	{
		foreach (GameObject go in _mapItemList.Values)
		{
			GameObject.DestroyImmediate(go);
		}
		_mapItemList.Clear();

		// GuiStageMapInfoScrollView 프리펩의 Items의 Maps 객체에서 테이블 컴포넌트를 사용하면, Map_001과 Map_002를 테이블의 셀에 배치하면서 사이가 벌어지게 된다...
		// 그래서 프리펩에서 테이블을 사용하지 않도록 하기 위해 비활성화 처리했다.. 
		// 그러므로 ClearMapItem () 에서  repositionNow를 호출하면 다시 사이가 벌어지므로 아래 코드 닫아버림.....
		//_uiTable.repositionNow = true;
	}


	public GameObject FindStageItem(string pKey)
	{
		GameObject go;

		if (_stageItemList.TryGetValue(pKey, out go))
		{
			return go;
		}

		return null;
	}

	/*   // moon...
	public GuiStageMapInfoScrollViewStageItemStage AddStageItem(uint pSI, uint pStageNum, Vector2 pPos, bool pIsLock, Texture2D[] pImages)
	{
		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemStage"));

		go.name = pSI.ToString();
		go.transform.parent = _usedTransList["Stages"];
		go.transform.localPosition = new Vector3(pPos.x, pPos.y, 0.0f);
		go.transform.localRotation = Quaternion.identity;
//      go.transform.localScale = Vector3.one;
        go.transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);

		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;

		GuiStageMapInfoScrollViewStageItemStage stage = go.GetComponent<GuiStageMapInfoScrollViewStageItemStage>();

		stage.SetSI(pSI);
		stage.SetStageNum(pStageNum);
		stage.LockStageBtn(pIsLock);
		stage.AddProfileInfo(pImages);

		_stageItemList.Add(pSI.ToString(), go);
		//Hwang Wark
		go.transform.FindChild ("StageBtn").GetComponent<ShopItemIndexNumber> ().thisIndex = int.Parse(go.name);
		//Hwang End
		return stage;
	}
	*/

	public void ClearStageItem()
	{
		foreach (GameObject go in _stageItemList.Values)
		{
			GameObject.DestroyImmediate(go);
		}
		_stageItemList.Clear();
	}


	public GameObject FindNextStageItem(string pKey)
	{
		GameObject go;

		if (_nextStageItemList.TryGetValue(pKey, out go))
		{
			return go;
		}

		return null;
	}

	/* // moon..
	public GameObject AddNextStageItem(string pKey, Vector2 pPos)
	{
		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemNextBtn"));

		go.name = pKey;
		go.transform.parent = _usedTransList["Stages"];
		go.transform.localPosition = new Vector3(pPos.x, pPos.y, 0.0f);
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;

		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;

		_nextStageItemList.Add(pKey, go);

		return go;
	}
	*/

	public void ClearNextStageItem()
	{
		foreach (GameObject go in _nextStageItemList.Values)
		{
			GameObject.DestroyImmediate(go);
		}
		_nextStageItemList.Clear();
	}


	public GameObject FindMapObjectItem(string pKey)
	{
		GameObject go;

		if (_mapObjectItemList.TryGetValue(pKey, out go))
		{
			return go;
		}

		return null;
	}


	/* // moon...
	// 표지판 객체 생성 SignObjectName..
	public GameObject AddMapObjectItem(string pKey, int pMapObjID, Vector2 pPos, int pWidth, int pHegiht, string villageName)
	{
		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemMapObj"));

		go.name = pKey;
		go.transform.parent = _usedTransList["Stages"];
		go.transform.localPosition = new Vector3(pPos.x, pPos.y, 0.0f);
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;

		// 표지판의 테스처를 바꾸지 않을 거라면 위에서 생성된 객체가 처음부터 기본 표지판 텍스처를 가지고 있으면 된다. 그럴 경우 아래 코드는 삭제 가능...
		UITexture uiTexture = go.GetComponent<UITexture>();
		Texture2D loadedTex = (Texture2D)Resources.Load(string.Format("Images/MapObject/MapObj_{0}", pMapObjID.ToString("000")));

		uiTexture.mainTexture = loadedTex;
		uiTexture.width = pWidth;
		uiTexture.height = pHegiht;

		uiTexture.depth = 1;	// 배경맵의 depth가 0이어서 오브젝트의 depth를 1로 세팅함..

		go.transform.GetComponentInChildren<UILabel> ().text = villageName;	// 표지판 이름..

		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;

		_mapObjectItemList.Add(pKey, go);

		return go;
	}
	*/

	public void ClearMapObjectItem()
	{
		foreach (GameObject go in _mapObjectItemList.Values)
		{
			GameObject.DestroyImmediate(go);
		}
		_mapObjectItemList.Clear();
	}


	/// <summary>
	/// 카메라를 목표까지 이동시키는 함수( Direct? or Scroll? )...
	/// </summary>
	/// isDirect == true 이면 곧바로 이동하고 , isDirect == false 이면 천천히 스크롤하면서 이동함...
	/// 현재 게임에서는 스크롤 이동이 필요없음....
	public void MoveCamera(Vector2 pTargetPos, bool isDirect, bool bStartZero)
	{
		StopCoroutine("MovingCamera");
		StartCoroutine(MovingCamera(pTargetPos, isDirect, bStartZero));
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["Maps"] = null;
		_usedTransList["Stages"] = null;
		_usedTransList["ScrollCamera"] = null;

		FindTrans();

        SaveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
		_uiAnchor = _usedTransList["Items"].GetComponent<UIAnchor>();
		_uiViewPort = _usedTransList["ScrollCamera"].GetComponent<UIViewport>();
		_uiDraggableCamera = _usedTransList["ScrollCamera"].GetComponent<UIDraggableCamera>();
		_uiTable = _usedTransList["Maps"].GetComponent<UITable>();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearMapItem();
		ClearStageItem();
		ClearNextStageItem();
		ClearMapObjectItem();
    }

    public override void OnDelete()
    {
		ClearMapItem();
		ClearStageItem();

		ClearNextStageItem();
		ClearMapObjectItem();

		_uiAnchor = null;
		_uiViewPort = null;
		_uiDraggableCamera = null;
		_uiTable = null;

		_usedTransList.Clear();
	}


	/// <summary>
	/// 카메라를 목표까지 이동시키는 함수( Direct? or Scroll? )...
	/// </summary>
	/// isDirect == true 이면 곧바로 이동하고 , isDirect == false 이면 천천히 스크롤하면서 이동함...
	/// 현재 게임에서는 스크롤 이동이 필요없음....
    protected IEnumerator MovingCamera(Vector2 pTargetPos, bool isDirect, bool bStartZero)
	{
		Transform camTrans = _usedTransList["ScrollCamera"];

        if (bStartZero == true)
        {
            camTrans.localPosition = Vector3.zero;
        }

		if (!isDirect)
		{
			Vector2 curPos = new Vector2(camTrans.localPosition.x, camTrans.localPosition.y);
			Vector2 movingDir = (pTargetPos - curPos).normalized;
			float movingDist = (pTargetPos - curPos).magnitude;
            float movingSpeed = 3f;// 10.0f;
			
			float curMoveDist = 0.0f;
			
			while (curMoveDist < movingDist - 0.5f)
			{
				curMoveDist = Mathf.Lerp(curMoveDist, movingDist, Time.deltaTime * movingSpeed);
				
				Vector2 nextPos = curPos + movingDir * curMoveDist;
				
				camTrans.localPosition = new Vector3(nextPos.x, nextPos.y, camTrans.localPosition.z);
				
				yield return null;
			}
		}

		camTrans.localPosition = new Vector3(pTargetPos.x, pTargetPos.y, camTrans.localPosition.z);

        //// moon
        //if (isDirect)
        //{
        //    DirectScrollMap (); // 스테이지 플레이 후에 다시 맵 화면으로 돌아올 때, 해당 스테이지가 위치한 맵으로 한 번에 돌아오기 위해 호출...
        //    yield return null;
        //}
        ////
	}


	/// <summary>
	/// 맵에 부착할 표지판 객체 생성 ..
	/// </summary>
	/// 처음부터 맵에 부착해도 된다...
	public GameObject AddMapSignBoard(string pKey, int pMapObjID, Vector2 pPos, int pWidth, int pHegiht, string villageName)
	{
		if (_mapObjectItemList.ContainsKey (pKey))
		{
			GameObject SignOG;
			if (_mapObjectItemList.TryGetValue (pKey, out SignOG))
			{
				return SignOG;
			}
			return null;
		}

		float mapHeight = 2048.0f;
		float mapWidth = 1024.0f;

		Vector3 pos = new Vector3 (pPos.x - mapWidth / 2, pPos.y - mapHeight / 2 - mapHeight, 0.0f);

		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemMapObj"));
		
		//go.name = pKey + "SignBoard";
		go.name = "SignBoard";
		go.transform.parent =  _usedTransList ["Maps"].Find ("Map_002");	// 기획 데이터 테이블에서 Map_001로 위치를 바꿀 경우에는 Map_001로 바꿔야 한다....
		go.transform.localPosition = new Vector3(pos.x, pos.y, 0.0f);
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;
		
		// 표지판의 테스처를 바꾸지 않을 거라면 위에서 생성된 객체가 처음부터 기본 표지판 텍스처를 가지고 있으면 된다. 그럴 경우 아래 코드는 삭제 가능...
		UITexture uiTexture = go.GetComponent<UITexture>();
		Texture2D loadedTex = (Texture2D)Resources.Load(string.Format("Images/MapObject/MapObj_{0}", pMapObjID.ToString("000")));
		
		uiTexture.mainTexture = loadedTex;
		uiTexture.width = pWidth;
		uiTexture.height = pHegiht;
		
		uiTexture.depth = 1;	// 배경맵의 depth가 0이어서 오브젝트의 depth를 1로 세팅함..
		
		go.transform.GetComponentInChildren<UILabel> ().text = villageName;	// 표지판 이름..
		
		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;
		
		_mapObjectItemList.Add(pKey, go);
		
		return go;
	}


	/// <summary>
	/// 맵에 부착된 표지판 위치 및 이름 바꾸기...
	/// </summary>
	private void ChangeSignBoard (bool isMap001, int stageMapInfo_Number)
	{
		if (isMap001 == false)
		{
			StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
			
			StageMapInfo stageMapInfo = stageMapInfoMgr.GetStageMapInfo ((uint)stageMapInfo_Number);	// 2개의 맵과 20개의 스테이지 정보를 가지고 있는 클래스...
			
			Transform signTransform = _usedTransList ["Maps"].Find("Map_002/SignBoard");
			
			if (signTransform == null)
				Debug.LogError ("not Find SignBoard");
			
			float mapHeight = 2048.0f;
			float mapWidth = 1024.0f;									
			
			float offsetY = (stageMapInfo_Number - 1) * (mapHeight * 2); // 데이터 파일 내의 좌표값이 월드좌표로 되어있으므로 이를 조정한다...
			Vector3 pos = stageMapInfo.SignPos;
			
			signTransform.localPosition = new Vector3 (pos.x - mapWidth / 2, pos.y - mapHeight / 2 - mapHeight - offsetY, 0.0f);	// Map_002에 붙기...
			signTransform.GetComponentInChildren<UILabel>().text = stageMapInfo.vill_name;
		}
	}



	/// <summary>
	/// Map_002 상단에 "Next Stage Button" 부착하기...
	/// </summary>
	public GameObject AddNextStageButton(string pKey, Vector2 pPos)
	{
		if (_nextStageItemList.ContainsKey (pKey))
		{
			GameObject stageButtonGO;
			if (_stageItemList.TryGetValue (pKey, out stageButtonGO))
			{
				return stageButtonGO;
			}
			return null;
		}

		float mapHeight = 2048.0f;
		float mapWidth = 1024.0f;

		Vector3 pos = new Vector3 (pPos.x - mapWidth / 2, pPos.y - mapHeight / 2 - mapHeight, 0.0f);

		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemNextBtn"));
		
		go.name = pKey + "NextButton";
		go.transform.parent = _usedTransList ["Maps"].Find ("Map_002");
		go.transform.localPosition = new Vector3(pos.x, pos.y, 0.0f);
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;
		
		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;
		
		_nextStageItemList.Add(pKey, go);
		
		return go;
	}



	/// <summary>
	/// Map_001, Map_002 객체에 기본이 되는 스테이지 20개 생성해서 부착하기...
	/// </summary>
	public GuiStageMapInfoScrollViewStageItemStage AddStage(uint pSI, uint pStageNum, Vector2 pPos, bool pIsLock, Texture2D[] pImages)
	{
		if (_stageItemList.ContainsKey (pSI.ToString()))
		{

			GameObject stageGO;
			if (_stageItemList.TryGetValue (pSI.ToString(), out stageGO))
			{
				GuiStageMapInfoScrollViewStageItemStage stageItem = stageGO.GetComponent<GuiStageMapInfoScrollViewStageItemStage>();
				return stageItem;
			}
			return null;
		}
		    

		///////// 스테이지 객체 생성...

		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewStageItemStage"));
		
		go.name = pSI.ToString();

		float mapHeight = 2048.0f;
		float mapWidth = 1024.0f;
		
		string parentName;
		Vector3 pos;

		// Map001 객체에는 11개의 스테이지, Map002 객체에는 9개의 스테이지 부착...
		if (pSI < 12) 
		{
			parentName = "Map_001";
			pos = new Vector3 (pPos.x - mapWidth / 2, pPos.y - mapHeight / 2, 0.0f);
		}
		else
		{
			parentName = "Map_002";
			pos = new Vector3 (pPos.x - mapWidth / 2, pPos.y - mapHeight / 2 - mapHeight, 0.0f);
		}
		
		go.transform.parent = _usedTransList ["Maps"].Find (parentName);
		
		go.transform.localPosition = pos;
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);		
		
		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;


		///// 스테이지 객체에 고유의 스테이지 정보 세팅...
		GuiStageMapInfoScrollViewStageItemStage stage;
		stage = ReSetStageItemInfo (go.transform, pSI, pStageNum, pIsLock, pImages);

		// 처음 한 번 스테이지를 생성하고 나면 리스트에 저장한다...
		_stageItemList.Add(pSI.ToString(), go);

		return stage;
	}

    private InfoSave SaveData;
	/// <summary>
	/// Stage 고유의 데이터 세팅...
	/// </summary>
	public GuiStageMapInfoScrollViewStageItemStage  ReSetStageItemInfo (Transform goTrans, uint pSI, uint pStageNum, bool pIsLock, Texture2D[] pImages)
	{   
		GuiStageMapInfoScrollViewStageItemStage stage = goTrans.GetComponent<GuiStageMapInfoScrollViewStageItemStage>();

        stage.ClearBackProfile();

        stage.ClearProfileTexture();

		stage.SetSI(pSI);

        //리볼 스테이지 변경
        if (SaveData.clearStageTopNumber == pStageNum && pStageNum > 40
            && pStageNum % 4 == 0)
        {
            if (SaveData.ReballClearStage() == false)
            {
                stage.SetStageNum(pStageNum);
                stage.SetStageStr("Bonus\nStage");
            }
            else
            {
                stage.SetStageNum(pStageNum);
            }
        }
        else
        {
            stage.SetStageNum(pStageNum);
        }

		stage.LockStageBtn(pIsLock);
		//stage.AddProfileInfo(pImages);

		UpdateLock (goTrans, pSI);

        
        return stage;
		// 프로파일 이미지의 메모리도 관리해야 하지 않을까?...
	}


    /// <summary>
    /// 리더보드 로드 1_ 를 붙인 이유는 Update 되기전이라 리더보드 이름을 1_ 라고 붙임
    /// </summary>
    /// <param name="_nStage"></par
    /// am>

    private int _nStage;
    public IEnumerator LeaderBoardLoad(int _nStage)
    {
        while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
            yield return null;

        KakaoNativeExtension.Instance.loadLeaderboard("1_" + _nStage, this.onLoadLeaderboardComplete, this.onLoadLeaderboardError);
    }

    //Load Leaderboard
    private void onLoadLeaderboardComplete()
    {
        Debug.Log("onLoadLeaderboardComplete");
        // test
        KakaoLeaderboards.Instance.printToConsole();
    }

    //Error Load Map
    private void onLoadLeaderboardError(string status, string message)
    {
        Debug.Log("onLoadLeaderboardError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

	/// <summary>
	/// 스테이지에 자물쇠를 채울지 말지 결정...
	/// </summary>
	void UpdateLock ( Transform stageBtnTrans, uint pSI)
	{
		stageBtnTrans.Find ("StageBtn").GetComponent<ShopItemIndexNumber> ().thisIndex = (int)pSI;
		stageBtnTrans.Find ("StageBtn").GetComponent<ShopItemIndexNumber> ().LockImage ();


	}


	/// <summary>
	/// map scroll에 사용할 기본 맵 2개를 생성한다...
	/// </summary>
	public void CreateBaseMap()
	{	
		for (int id = 0; id < 2; id++)	// 기본이 되는 Map_001, Map_002 두 개의 맵 생성하기..
		{
			string mapName = string.Format ("Map_{0}", (id + 1).ToString ("000"));	
			
			if (!_mapItemList.ContainsKey (mapName))
				AddMap (mapName, id, false);
		}

		// StageMapInfo의 수량을 저장... 현재 약 17개....
		StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
		CountOfStageMapInfo = stageMapInfoMgr.CountOfStageMapInfo;
	}


	/// <summary>
	/// 게임 오브젝트 Map_001, Map_002를 생성하고, _mapItemList에 추가한다...
	/// </summary>
	private void AddMap(string mapName, int id, bool pIsCloud)
	{
		float mapHeight = 2048.0f;
		float mapWidth = 1024.0f;
		Vector3 mapPos;
		
		if (id == 0)
			//mapPos = new Vector3(mapWidth/2, mapHeight/2, 0);	// Map_001
			mapPos = new Vector3(0, 0, 0);	// Map_001
		else
			//mapPos = new Vector3(mapWidth/2, mapHeight/2 + mapHeight, 0);	// Map_002
			mapPos = new Vector3(0, mapHeight, 0);	// Map_002

		GameObject go = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiStageMapInfoScrollViewMapItem"));
		
		go.name = mapName;
		go.transform.parent = _usedTransList["Maps"];
		go.transform.localPosition = mapPos;
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one;

		go.GetComponent<UIDragCamera>().draggableCamera = _uiDraggableCamera;
		
		GuiStageMapInfoScrollViewMapItem mapItem = go.GetComponent<GuiStageMapInfoScrollViewMapItem>();

		//mapItem.SetSpriteName(string.Format("Map_{0}", ((id % 2) + 1).ToString("00")));

        string strsprite = string.Format("Map_{0}", ((id % 2) + 1).ToString() );
        // Map_0  는 Map_1로 대체
        if (strsprite == "Map_0")
        {
            strsprite = "Map_1";
        }
        mapItem.SetSpriteName(strsprite);

		mapItem.ShowCloud(pIsCloud);
		
		_mapItemList.Add(mapName, go);
	}


	/// <summary>
	/// 스테이지 플레이 후에 다시 맵 화면으로 돌아올 때, 해당 스테이지가 위치한 맵으로 한 번에 돌아오기 위해 호출...
	/// </summary>
	public void DirectScrollMap ()
	{
		bool isLoop = true;
	
		while (isLoop)
		{
			isLoop = ScrollMap ();
		}
	}


	/// <summary>
	/// 2개의 기본맵을 사용해서 반복적으로 스크롤되는 맵 만들기...
	/// </summary>
	public bool ScrollMap ()
	{
        float hOffset = 10;
		float mapHeight = 2048.0f;
		int mapNumber = 1;
		GameObject mapGO;

		Transform camTrans = _usedTransList["ScrollCamera"];

		string mapName = string.Format ("Map_{0}", (mapIndexOfScroll + 1).ToString ("000"));	

		if (!_mapItemList.TryGetValue (mapName, out mapGO))
		{
//			Debug.LogError (mapName + " 이 _mapItemList 에 존재하지 않습니다.");
			return false;
		}

		// 위로 스크롤할 경우...
		if (camTrans.localPosition.y > mapGO.transform.localPosition.y + (mapHeight - hOffset) )	// 2개의 맵 중 아래에 있는 맵을 기준으로 mapHeight 보다 위로 스크롤될 때 맵을 바꿔치기 한다....
		{
// 업데이트에 대량 발생 할수 있음.			Debug.LogError("Scroll Map Change");
			/*
			float topHeight = CountOfStageMapInfo * (mapHeight * 2) - (mapHeight * 0.5f);

			if (camTrans.localPosition.y > topHeight-1024f)	// 최상단 맵 위로는 스크롤되지 않는다...
			{
				camTrans.localPosition = new Vector3( camTrans.localPosition.x, topHeight-1024f, camTrans.localPosition.z);
				return false;
			}
			*/

			float heightLimit = (CountOfStageMapInfo - 1) * (mapHeight * 2);

			if (mapGO.transform.localPosition.y >= heightLimit)	// 최상단 맵 위로는 스크롤되지 않는다...
			{
                //camTrans.localPosition = new Vector3( camTrans.localPosition.x, heightLimit + 2048f, camTrans.localPosition.z);
                //return false;
			}

			// 스크롤을 위해 위에 맵의 인덱스를 찾는다.
			if (mapIndexOfScroll == 0)
				mapIndexOfScroll = 1;
			else
				mapIndexOfScroll = 0;

			// 제일 위로 이동시켜야 할 맵을 찾는다.
			GameObject upMap;
			mapName = string.Format ("Map_{0}", (mapIndexOfScroll + 1).ToString ("000"));	// 2개의 맵 중 위에 있는 맵의 이름... 맵이름이 Map_001, Map_002와 같으므로  'mapIndex + 1' 한다...

			if (!_mapItemList.TryGetValue (mapName, out upMap))	// 2개의 맵 중 위에 있는 맵을 찾는다...
				Debug.LogError ("스크롤 업 시에 " + mapName + " 이 _mapItemList 에 존재하지 않습니다.");


			// 현재 카메라가 위치한 맵을 기준으로 이동해야 할 위쪽 좌표값을 찾는다...
			Vector3 newPos = new Vector3(
				upMap.transform.localPosition.x,
				upMap.transform.localPosition.y + mapHeight,
				upMap.transform.localPosition.z);

			mapGO.transform.localPosition = newPos;	// 아래에 있는 맵을 제일 위로 이동시킨다...

			// 위로 이동한 맵의 배경을 바꿔준다...
			ChageMapTexture (mapGO);


			////// StageItem 데이터도 맵과 같이 스크롤 시키기...

			// 한 개의 StageMapInfo는 '두 개의 맵'과 20개의 스테이지 정보를 가지고 있다. 그래서 mapHeight*2로 나누어야 해당하는 StageMapInfo를 찾을 수 있다...
			int stageMapInfo_Number = (int)(mapGO.transform.localPosition.y / (mapHeight*2));	
			++stageMapInfo_Number;	// 위에서 계산한 값에 +1 하기...

			bool isMap001;
			if (mapIndexOfScroll == 1)
				isMap001 = true;	// Map_001...
			else
				isMap001 = false;	// Map_002...

			ChangeStageItemInfo(isMap001, stageMapInfo_Number);	// Stage 데이터도 바꿔치기 하기...
			ChangeSignBoard(isMap001, stageMapInfo_Number);		// 표지판 텍스트 바꿔치기...

			return true;
		}

		// 아래로 스크롤할 경우...
		else if (camTrans.localPosition.y <= mapGO.transform.localPosition.y) // 아래에 있는 맵보다 밑으로 카메라가 이동할 때 바꿔치기 시작...
		{
			//if (camTrans.localPosition.y <= mapHeight)	// 최하단 맵 아래로는 스크롤되지 않는다...
            //if (camTrans.localPosition.y <= mapHeight * 0.5f)
            //{
            //    return false;
            //}

			// 스크롤을 위해 위에 맵의 인덱스를 찾는다.
			if (mapIndexOfScroll == 0)
				mapIndexOfScroll = 1;
			else
				mapIndexOfScroll = 0;

			// 제일 아래로 이동시켜야 할 맵을 찾는다.
			GameObject downMap;
			mapName = string.Format ("Map_{0}", (mapIndexOfScroll + 1).ToString ("000"));	

			if (!_mapItemList.TryGetValue (mapName, out downMap))
				Debug.LogError ("스크롤 다운 시에 " + mapName + " 이 _mapItemList 에 존재하지 않습니다.");

			// 현재 카메라가 위치한 맵을 기준으로 이동해야 할 아래쪽 좌표값을 찾는다...
			Vector3 newPos = new Vector3(
				mapGO.transform.localPosition.x,
				mapGO.transform.localPosition.y - mapHeight,
				mapGO.transform.localPosition.z);
			
			downMap.transform.localPosition = newPos;	// 위에 있는 맵을 제일 아래로 이동시킨다...

			// 아래로 이동한 맵의 배경을 바꿔준다...
			ChageMapTexture (downMap);


			int stageMapInfo_Number = (int)(downMap.transform.localPosition.y / (mapHeight*2));
			++stageMapInfo_Number;

			bool isMap001;
			if (mapIndexOfScroll == 1)
				isMap001 = false;
			else
				isMap001 = true;
			
			ChangeStageItemInfo(isMap001, stageMapInfo_Number);	// Stage 데이터도 바꿔치기 하기...
			ChangeSignBoard(isMap001, stageMapInfo_Number);		// 표지판 텍스트 바꿔치기...

			return true;
		}
        ///////////////////////////////// 최소 최대 스크롤 설정.
        if (camTrans.localPosition.y < _MinLocalY)
        {
            camTrans.localPosition = new Vector3(camTrans.localPosition.x, _MinLocalY, camTrans.localPosition.z);
        }

        if (camTrans.localPosition.y > _MaxLocalY)
        {
            camTrans.localPosition = new Vector3(camTrans.localPosition.x, _MaxLocalY, camTrans.localPosition.z);
        }

		return false;
	}

	/// <summary>
	/// 위 또는 아래로 이동한 맵의 배경 텍스쳐를 바꿔준다...
	/// </summary>
	private void ChageMapTexture (GameObject mapGO)
	{
		float mapHeight = 2048.0f;
		int maxMapCount = 8;

		// 바꿔줄 맵 파일의 이름을 구한다...
		int mapCount = (int)(mapGO.transform.localPosition.y / mapHeight);
		int nameNumber = (mapCount % maxMapCount) + 1;
		string newMapName = "Map_" + nameNumber.ToString();

		// 맵텍스쳐 교체...
		GuiStageMapInfoScrollViewMapItem mapItem = mapGO.GetComponent<GuiStageMapInfoScrollViewMapItem>();
        // Map_0  는 Map_1로 대체
        if (newMapName == "Map_0")
        {
            newMapName = "Map_1";
        }
		mapItem.SetSpriteName(newMapName);
	}



	/// <summary>
	/// 맵 스크롤 시에 맵에 달라붙은 20개의 stage에 관한 정보도 바꿔치기...
	/// </summary>
	public void ChangeStageItemInfo (bool isMap001, int stageMapInfo_Number)
	{
		StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
		StageInfoMgr stageInfoMgr = GameDataMgr.Instance.GetInfo<StageInfoMgr>();

		// 2개의 맵과 20개의 스테이지 정보를 가지고 있는 클래스...
		StageMapInfo stageMapInfo = stageMapInfoMgr.GetStageMapInfo ((uint)stageMapInfo_Number);

		if (stageMapInfo == null)
			Debug.LogError ("ChangeStageItemInfo() 내에서 " + stageMapInfo_Number.ToString() + " 에 해당하는 stageMapInfo 가 없습니다.");

		// Map 객체의 자식인 stage 객체의 이름이 '1,2,3,,,,20' 과 같은 숫자이기 때문에 이를 가리키는 변수...
		int NumberOfStageObj = 0;

		foreach (LinkedStageInfo linkedStageInfo in stageMapInfo.linked_stage_info) 	// 20개의 스테이지를 돌면서...
		{
			StageInfo stageInfo = stageInfoMgr.GetStageInfo (linkedStageInfo.stage_si);
			
			if (null == stageInfo) 
			{
				// stage 데이터 파일에서 330번 이후의 데이터가 없어서 에러 발생 중....
				Debug.LogWarning ("ChangeStageItemInfo() 내에서 " + linkedStageInfo.stage_si.ToString() + " 에 해당하는 stageInfo 가 없습니다.");
				continue;
			}


			#region dummy images setting
			
			Texture2D[] images = null;
			
			int random = Random.Range(0, 100);
			
			if (random < 10)
			{
				images = new Texture2D[] { (Texture2D)Resources.Load("Images/Friends/profile_0001"), (Texture2D)Resources.Load("Images/Friends/profile_0001"), (Texture2D)Resources.Load("Images/Friends/profile_0001"), (Texture2D)Resources.Load("Images/Friends/profile_0001") };
			}
			
			#endregion dummy images setting


			NumberOfStageObj++;

			//Debug.LogError("NumberofStage : " + NumberOfStageObj);

			if (isMap001)
			{
				if (NumberOfStageObj > 11)	// Map001은 1 ~ 11번까지의 스테이지만을 가지고 있으므로...
					return;

				GameObject go;

				if (_stageItemList.TryGetValue (NumberOfStageObj.ToString(), out go))	// 정보를 바꿔줘야 하는 stage 객체를 찾고...
				{
                	ResetStageItemPosition (NumberOfStageObj, linkedStageInfo, go, stageMapInfo_Number);
					ReSetStageItemInfo  (go.transform, stageInfo.si, linkedStageInfo.stage_si, false, images);

				}
				else
					Debug.LogError ("ChangeStageItemInfo()의 _stageItemList 내에 " + NumberOfStageObj.ToString() + " 에 해당하는 오브젝트가 없습니다.");
			}
			else
			{
				if (NumberOfStageObj < 12)	// Map002는 12 ~ 20번까지의 스테이지만을 가지고 있으므로...
				{
					continue;
				}

				GameObject go;

				if (_stageItemList.TryGetValue (NumberOfStageObj.ToString(), out go))
                {
					ResetStageItemPosition (NumberOfStageObj, linkedStageInfo, go, stageMapInfo_Number);
					ReSetStageItemInfo  (go.transform, stageInfo.si, linkedStageInfo.stage_si, false, images);

				}
				else
					Debug.LogError ("ChangeStageItemInfo()의 _stageItemList 내에 " + NumberOfStageObj.ToString() + " 에 해당하는 오브젝트가 없습니다.");
			}
		}
	}


	/// <summary>
	///화면이 스크롤될 때, 다양한 배경맵에 맞게 스테이지 객체의 좌표값을 재설정함 ( "stage_map_data" 파일에 정의된 대로 ) ...
	/// </summary>
	void ResetStageItemPosition (int NumberOfStageObj, LinkedStageInfo linkedStageInfo, GameObject go, int stageMapInfo_Number)
	{	
		float mapHeight = 2048.0f; 
		float mapWidth = 1024.0f;
		Vector3 pos;

		float offsetY = (stageMapInfo_Number - 1) * (mapHeight * 2);	// 데이터 파일 내의 좌표값이 월드좌표로 되어있으므로 이를 조정한다...
				
		// Map001 객체에는 11개의 스테이지, Map002 객체에는 9개의 스테이지 부착되어 있음...
		// linkedStageInfo 내의 좌표값은 월드 좌표값이고, 스테이지 객체는 Map의 자식으로 로컬 좌표값을 가지므로, 로컬 좌표로 변환해줘야 한다...
		if (NumberOfStageObj < 12) 
		{
			pos = new Vector3 (linkedStageInfo.pos_x - mapWidth / 2, linkedStageInfo.pos_y - mapHeight / 2 - offsetY, 0.0f);
		}
		else
		{
			pos = new Vector3 (linkedStageInfo.pos_x - mapWidth / 2, linkedStageInfo.pos_y - mapHeight / 2 - mapHeight - offsetY, 0.0f);
		}

		go.transform.localPosition = pos;
	}

	//LoadFriend 
	public void GameFriendLoad()
	{
		KakaoNativeExtension.Instance.loadGameFriends(this.onLoadGameFriendsComplete, this.onLoadGameFriendsError);
	}

	//Load Game Friends - 140904
	private void onLoadGameFriendsComplete() {
		Debug.Log("onLoadGameFriendsComplete");
		//GameFriendInstantiate ();
	}

	private void onLoadGameFriendsError(string status, string message) {
		Debug.Log("onLoadGameFriendsError");
		KakaoMain.Instance.showAlertErrorMessage(status,message);
	}

	private int _nFriendCount = 0;
	
	public void GameFriendInstantiate()
	{
		foreach( var pair in KakaoGameFriends.Instance.leaderboardFriends ) {
			KakaoGameFriends.LeaderboardFriend friend = pair.Value;
			if( friend==null )
				continue;

			KakaoLeaderboards.Instance._cGameFriendInfo[_nFriendCount].UserUrl = friend.profileImageUrl;

			Debug.Log("_nFriendCount : " + KakaoLeaderboards.Instance._cGameFriendInfo[_nFriendCount].UserUrl);

			_nFriendCount ++;
		}
	}
	
}
