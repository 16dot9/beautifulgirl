﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiStageMapInfo : GuiBase
{

	public Transform GetViewTargetTopLeft()
	{
		return _usedTransList["TopLeft"];
	}

	public Transform GetViewTargetBottomRight()
	{
		return _usedTransList["BottomRight"];
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["TopLeft"] = null;
		_usedTransList["BottomRight"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
