﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class GuiChargeWindow : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// 젬 구매창 나타내기...
	/// </summary>
	public void ShowGemWindow()
	{
		GuiChargeGem guiChargeGem = GuiMgr.Instance.Show<GuiChargeGem>(GuiMgr.ELayerType.Front, true, null);
		
		GameObject itemGO;
		GuiChargeGemItem guiChargeGemItem;
		
		// shop table 에서 Gem type 들만 추출
		ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
		PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();
		
		foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
		{
            if (shopInfo.type != ShopType.Gem)
            {
                Debug.Log("shopinfo Type:" + shopInfo.type);
                continue;
            }
            else 
            {
                Debug.Log("shopinfo !Gem:" + shopInfo.type);
            }

			// 리스트 항목을 생성 추가 한다.
			itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargeGemItem"));
			guiChargeGemItem = itemGO.GetComponent<GuiChargeGemItem>();
			guiChargeGemItem.SerialID = shopInfo.pack_idx;
            uint _SerialId = guiChargeGemItem.SerialID;
			uint powersu = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).item_1_su;
			float cost = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value;
			
			bool isFriendInvite = false;
			string priceText = "";
			
			isFriendInvite = false;

            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Korean:
            //        priceText = cost.ToString() + " 원" + "\n" + "구매하기";
            //       break;
            //    case SystemLanguage.English:
            //        priceText = cost.ToString() + " $" + "\n" + "Buy";
            //        break;
            //    case SystemLanguage.Portuguese:
            //        priceText = cost.ToString() + " $" + "\n" + "Buy";
            //        break;
            //}
            priceText = "R " + cost.ToString() + " $" + "\n" + "Comprar";
            
            guiChargeGemItem.SetItemInfo(shopInfo.pack_idx, priceText, "x " + powersu.ToString());
			
			guiChargeGem.AddChargeGemItem(guiChargeGemItem);
			
			
			guiChargeGemItem.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
			{
                Debug.Log("<-------------- Power Su :" + powersu + " _SerialId :" + _SerialId + "---------------->");

                JSONNode msgParam = new JSONClass();
                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.Portuguese:
                //        msgParam["message"] = "Você gostaria de comprar?";
                //        break;
                //    case SystemLanguage.English:
                //        msgParam["message"] = "Would you like to buy?";
                //        break;
                //    case SystemLanguage.Korean:
                //        msgParam["message"] = "구매 하시겠습니까?";
                //        break;
                //}
                msgParam["message"] = "Você gostaria de comprar?";
                        
				msgParam["show_type"] = "two";
					
				GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);
					
				guiMessageBox.OnButtonClick = (bool pIsOk) =>
				{
					if (pIsOk)
					{
                        Debug.Log("_SerialId : " + _SerialId);

                        //< 결제 정보
                        //OpenIABPay.Instance.PurchaseIndex((int)_SerialId);
                        Debug.Log("Test");
                        //MyInfoMgr.Instance.m_Gem += (uint)powersu;
                        //GameObject.Find("InfoMgr").GetComponent<InfoSave>().GemSuPlus((uint)powersu);
					}
				};
			};
		}
		
		guiChargeGem.CloseBtn.onClick = (GameObject pCloseGO) =>
		{
			guiChargeGem.CloseBtn.onClick = null;
			
			guiChargeGem = null;
			
			GuiMgr.Instance.Show<GuiChargeGem>(GuiMgr.ELayerType.Front, false, null);

			Destroy(gameObject);
		};
	}

}
