﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class GuiStageInfo : GuiBase
{
	//Hwang Wark
	protected GuiPlayerInfo					_guiPlayerInfo = null;
    protected GuiStageMapInfo _guiStaGemapInfo = null;
    protected GuiStageMapInfoScrollView _guiStaGemapInfoScrollView = null;
	protected GuiSettingMenu				_guiSettingMenu = null;

	//Hwang End

	protected GuiChargeWindow 				_guiChargeWindow	= null;

	public delegate void DELEGATEVOID();
	public delegate void DELEGATEGUIITEM(GuiItem pGuiItem);


	public DELEGATEGUIITEM                  OnItemClick = null;


    protected List<GuiItem>                 _itemList = new List<GuiItem>();


    public UIEventListener CloseBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
        }
    }

	public UIEventListener PlayBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["PlayBtn"].gameObject);
		}
	}

	public Transform PowerIcon	// 게임시작 버튼 왼쪽에 있는 빈 파워 이미지...
	{
		get
		{
			return _usedTransList["PowerFull"];
		}

	}


	public void SetStageNum(uint pStageNum)
    {
        _usedTransList["Stage"].GetComponent<UILabel>().text = string.Format("Fase {0}", pStageNum);
    }

    public void SetStageGoal(string pStageGoal)
    {
        _usedTransList["Goal"].GetComponent<UILabel>().text = pStageGoal;
    }

    public void SetItemName(string pItemName)
    {
        _usedTransList["ItemName"].GetComponent<UILabel>().text = pItemName;
    }

    public void SetItemDesc(string pItemDesc)
    {
        _usedTransList["ItemDesc"].GetComponent<UILabel>().text = pItemDesc;
    }

    public void AddItem(GuiItem pGuiItem)
    {
        pGuiItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

        pGuiItem.transform.parent = _usedTransList["UIGrid"];
        pGuiItem.transform.localPosition = Vector3.zero;
        pGuiItem.transform.localRotation = Quaternion.identity;
        pGuiItem.transform.localScale = Vector3.one;

        _usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;

        UIEventListener.Get(pGuiItem.gameObject).onClick = GuiItemClick;

        _itemList.Add(pGuiItem);
    }

    public void ClearItem()
    {
        foreach ( GuiItem item in _itemList )
        {
            UIEventListener.Get(item.gameObject).onClick = null;
            GameObject.DestroyImmediate(item.gameObject);
        }

        _itemList.Clear();
    }


	public void ShowPowerDir(Vector3 pSrcPos, Vector3 pDestPos, DELEGATEVOID pDirFinished)
	{
		StartCoroutine(PowerDir(pSrcPos, pDestPos, pDirFinished));
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

        _usedTransList["Items"] = null;
        _usedTransList["UIGrid"] = null;

        _usedTransList["Stage"] = null;
        _usedTransList["Goal"] = null;
        _usedTransList["ItemName"] = null;
        _usedTransList["ItemDesc"] = null;

        _usedTransList["ItemsNaviLeftBtn"] = null;
        _usedTransList["ItemsNaviRightBtn"] = null;

		_usedTransList["PowerDir"] = null;

        _usedTransList["CloseBtn"] = null;
        _usedTransList["PlayBtn"] = null;

		_usedTransList ["PowerFull"] = null;

		FindTrans();

		_usedTransList["PowerDir"].gameObject.SetActive(false);

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
                
        //_usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //_usedTransList["PlayBtn"].GetComponentInChildren<UILabel>().text = "Game Start";
        //        break;
        //    case SystemLanguage.Portuguese:
                
        //_usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //_usedTransList["PlayBtn"].GetComponentInChildren<UILabel>().text = "Game Start";
                
        //        break;
        //}


        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["PlayBtn"].GetComponentInChildren<UILabel>().text = "Iniciar Jogo";
	}

    //게임시작 할때 로딩 처리중 , 모든 키펫, 팝업창, 게임 메뉴, 룰렛에서 확인

    public override void OnEnter()
    {
        UIEventListener.Get(_usedTransList["ItemsNaviLeftBtn"].gameObject).onClick = GuiItemsNaviLeftBtnClick;
        UIEventListener.Get(_usedTransList["ItemsNaviRightBtn"].gameObject).onClick = GuiItemsNaviRightBtnClick;
    }

    public override void OnLeave()
    {
		ClearItem();

        UIEventListener.Get(_usedTransList["ItemsNaviLeftBtn"].gameObject).onClick = null;
        UIEventListener.Get(_usedTransList["ItemsNaviRightBtn"].gameObject).onClick = null;
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


    protected void GuiItemsNaviLeftBtnClick(GameObject pGO)
    {
        _usedTransList["Items"].GetComponent<UIScrollView>().currentMomentum = new Vector3(0.05f, 0.0f);
    }

    protected void GuiItemsNaviRightBtnClick(GameObject pGO)
    {
        _usedTransList["Items"].GetComponent<UIScrollView>().currentMomentum = new Vector3(-0.05f, 0.0f);
    }

    public void ShowShop()
    {
        GuiItemShop guiItemShop = GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, true, null);
        guiItemShop.ClearItemShopItem();

        // shop table 에서 Item type 들만 추출
        ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

        GuiItemShopItem item;
        //Hwang Wark
        GuiItemShopItem guiItemShopItem;
        //Hwang End
        foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
        {
            if (shopInfo.type != ShopType.Item)
                continue;

            // 리스트 항목을 생성 추가 한다.
            item = ((GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItemShopItem"))).GetComponent<GuiItemShopItem>();

            PackageInfo pkginfo = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx);

            int _funnysu = (int)pkginfo.price_value;
            PriceType valuetype = pkginfo.price_type;


            item.SerialID = shopInfo.pack_idx;

            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Korean:
            //        {
            //            if (valuetype == PriceType.Funny)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\n퍼니",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else if (valuetype == PriceType.Gem)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " 젬",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //        }
            //        break;
            //    case SystemLanguage.English:
            //        {
            //            if (valuetype == PriceType.Funny)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else if (valuetype == PriceType.Gem)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //        }
            //        break;
            //    case SystemLanguage.Portuguese:
            //        {
            //            if (valuetype == PriceType.Funny)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else if (valuetype == PriceType.Gem)
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //            else
            //            {
            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            //            }
            //        }
            //        break;


            //}
            if (valuetype == PriceType.Funny)
            {
                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            }
            else if (valuetype == PriceType.Gem)
            {
                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            }
            else
            {
                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
            }

            item.transform.Find("BG/BuyBtn").GetComponent<ShopItemIndexNumber>().thisIndex = (int)shopInfo.pack_idx;
            guiItemShop.AddItemShopItem(item);
            //Hwang Wark
            //Debug.Log(shopInfo.pack_idx);
            guiItemShopItem = item;

            guiItemShopItem.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
            {
                //Hwang Wark
                int isIndex = pBuyBtnGO.GetComponent<ShopItemIndexNumber>().thisIndex;
                //Hwang End

                if (MyInfoMgr.Instance.m_Funny >= _funnysu)
                {
                    JSONNode msgParam = new JSONClass();
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParam["message"] = "Você gostaria de comprar?";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParam["message"] = "Would you like to buy?";
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        msgParam["message"] = "구매 하시겠습니까?";
                    //        break;
                    //}

                    msgParam["message"] = "Você gostaria de comprar?";
                    msgParam["show_type"] = "two";

                    GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                    guiMessageBox.OnButtonClick = (bool pIsOk) =>
                    {
                        if (pIsOk)
                        {
                            GameObject.Find("InfoMgr").GetComponent<InfoSave>().FunnySuMinus(_funnysu);
                            GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate((int)isIndex, 1);
                            GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
                        }
                    };
                }
                //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 Start
                if (MyInfoMgr.Instance.m_Funny < _funnysu)
                {
                    JSONNode msgParamfs = new JSONClass();
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParamfs["message"] = "Ir para Engraçado Shop";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParamfs["message"] = "Go to Funny Shop";
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                    //        break;
                    //}
                    msgParamfs["message"] = "Ir para Engraçado Shop";
                    msgParamfs["show_type"] = "two";

                    GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                    guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                    {
                        if (pIsOkfs)
                        {
                            guiItemShop = null;
                            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                            GuiChargeFunny guiChargeFunny = GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, true, null);

                            GameObject fitemGO;
                            GuiChargeFunnyItem guiChargeFunnyItemf;

                            ShopInfoMgr shopInfoMgrf = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
                            PackageInfoMgr packageInfoMgrf = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

                            foreach (ShopInfo shopInfof in shopInfoMgr.GetShopInfo())
                            {
                                if (shopInfof.type != ShopType.Funny)
                                    continue;

                                if (guiChargeFunny.isFunnyItem(shopInfof.pack_idx))
                                    continue;

                                // 리스트 항목을 생성 추가 한다.								
                                fitemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargeFunnyItem"));
                                guiChargeFunnyItemf = fitemGO.GetComponent<GuiChargeFunnyItem>();

                                guiChargeFunnyItemf.SerialID = shopInfof.pack_idx;

                                uint powersu = packageInfoMgr.GetPackageInfo(shopInfof.pack_idx).item_1_su;
                                float cost = packageInfoMgr.GetPackageInfo(shopInfof.pack_idx).price_value;

                                //switch (Application.systemLanguage)
                                //{
                                //    case SystemLanguage.Korean:
                                //        guiChargeFunnyItemf.SetLabel("퍼니 충전");
                                //        guiChargeFunnyItemf.SetItemInfo(guiChargeFunnyItemf.SerialID, cost.ToString() + " 젬", "x " + powersu.ToString());
                                //        break;
                                //    case SystemLanguage.English:
                                //        guiChargeFunnyItemf.SetLabel("Funny Charge");
                                //        guiChargeFunnyItemf.SetItemInfo(guiChargeFunnyItemf.SerialID, cost.ToString() + " Jóias", "x " + powersu.ToString());
                                //        break;
                                //    case SystemLanguage.Portuguese:
                                //        guiChargeFunnyItemf.SetLabel("Funny Charge");
                                //        guiChargeFunnyItemf.SetItemInfo(guiChargeFunnyItemf.SerialID, cost.ToString() + " Jóias", "x" + powersu.ToString());
                                //        break;
                                //}

                                guiChargeFunnyItemf.SetLabel("Funny Charge");
                                guiChargeFunnyItemf.SetItemInfo(guiChargeFunnyItemf.SerialID, cost.ToString() + " Jóias", "x" + powersu.ToString());
                              
                                JSONNode msgParamf = new JSONClass();

                                msgParamf["show_type"] = "two";

                                guiChargeFunnyItemf.BuyBtn.onClick = (GameObject pBuyBtnGOf) =>
                                {
                                    //switch (Application.systemLanguage)
                                    //{
                                    //    case SystemLanguage.Portuguese:
                                    //        msgParamf["message"] = "Você gostaria de comprar?";
                                    //        break;
                                    //    case SystemLanguage.English:
                                    //        msgParamf["message"] = "Would you like to buy?";
                                    //        break;
                                    //    case SystemLanguage.Korean:
                                    //        msgParamf["message"] = "구매 하시겠습니까?";
                                    //        break;
                                    //}
                                    msgParamf["message"] = "Você gostaria de comprar?";
                                          
                                    GuiMessageBox guiMessageBoxf = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamf);
                                    guiMessageBoxf.OnButtonClick = (bool pIsOkf) =>
                                    {
                                        if (pIsOkf)
                                        {
                                            if (MyInfoMgr.Instance.m_Gem >= cost)
                                            {
                                                GameObject.Find("InfoMgr").GetComponent<InfoSave>().FunnySuPlus((int)powersu);
                                                GameObject.Find("InfoMgr").GetComponent<InfoSave>().GemSuMinus((int)cost);
                                                GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate(shopInfof.pack_idx, 1);
                                                return;
                                            }

                                            //Hwang Wark 퍼니상점에서 잼이 코스트보다 부족할때 잼상점 팝업 Start					
                                            if (MyInfoMgr.Instance.m_Gem < cost)
                                            {
                                                guiChargeFunny.CloseBtn.onClick = null;
                                                //guiChargeFunny.FunnyReqBtn.onClick = null;				

                                                guiChargeFunny = null;

                                                GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);

                                                GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
                                                _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
                                                if (_guiChargeWindow != null)
                                                    _guiChargeWindow.ShowGemWindow();		// 젬 윈도우 팝업....																	
                                            }
                                        }
                                    };
                                };

                                guiChargeFunny.AddChargeFunnyItem(guiChargeFunnyItemf);
                            }

                            //#endregion dummy funny item setting									

                            guiChargeFunny.CloseBtn.onClick = (GameObject pCloseGO) =>
                            {
                                guiChargeFunny.CloseBtn.onClick = null;
                                guiChargeFunny = null;
                                GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);
                            };
                        }
                    };
                }
                //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 End
            };
            //Hwang End
        }
        guiItemShop.BackBtn.onClick = (GameObject) =>
        {
            guiItemShop = null;
            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);
        };

        guiItemShop.GachaBtn.onClick = (GameObject) =>
        {
            Debug.Log("~~~~~~~~~~~~ GachaBtn ");
            if (MyInfoMgr.Instance.m_Funny >= 5000)
            {
                //AudioMgr.Instance.Play("Gacha");
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().FunnySuMinus((uint)MyInfoMgr._gachaFunnySu);
                guiItemShop.ShowGachaDlg(true);
            }
            else{
                guiItemShop = null;
                GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                GuiChargeFunny guiChargeFunny = GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, true, null);

                #region funny item setting

                GameObject itemGOf;
                GuiChargeFunnyItem guiChargeFunnyItemf;

                // shop table 에서 Funny type 들만 추출
                ShopInfoMgr shopInfoMgrf = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
                PackageInfoMgr packageInfoMgrf = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

                foreach (ShopInfo shopInfof in shopInfoMgrf.GetShopInfo())
                {
                    if (shopInfof.type != ShopType.Funny)
                        continue;

                    // 리스트 항목을 생성 추가 한다.
                    itemGOf = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargeFunnyItem"));
                    guiChargeFunnyItemf = itemGOf.GetComponent<GuiChargeFunnyItem>();
                    guiChargeFunnyItemf.SerialID = shopInfof.pack_idx;
                    uint powersuf = packageInfoMgrf.GetPackageInfo(shopInfof.pack_idx).item_1_su;
                    float costf = packageInfoMgrf.GetPackageInfo(shopInfof.pack_idx).price_value;
                    
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Korean:
                    //        guiChargeFunnyItemf.SetLabel("퍼니 충전");
                    //        guiChargeFunnyItemf.SetItemInfo(shopInfof.pack_idx, costf.ToString() + " 젬", "x " + powersuf.ToString());
                    //        break;
                    //    case SystemLanguage.English:
                    //        guiChargeFunnyItemf.SetLabel("Funny Charge");
                    //        guiChargeFunnyItemf.SetItemInfo(shopInfof.pack_idx, costf.ToString() + " Jóias", "x " + powersuf.ToString());
                    //        break;
                    //    case SystemLanguage.Portuguese:
                    //        guiChargeFunnyItemf.SetLabel("Funny Charge");
                    //         guiChargeFunnyItemf.SetItemInfo (shopInfof.pack_idx, costf.ToString () + " Jóias", "x" + powersuf.ToString ());
                    //        break;
                    //}
                    guiChargeFunnyItemf.SetLabel("Funny Charge");
                    guiChargeFunnyItemf.SetItemInfo(shopInfof.pack_idx, costf.ToString() + " Jóias", "x" + powersuf.ToString());
                     
                    int pack_idx = (int)shopInfof.pack_idx;
                    guiChargeFunnyItemf.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
                    {
                        JSONNode msgParam = new JSONClass();
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        msgParam["message"] = "Você gostaria de comprar?";
                        //        break;
                        //    case SystemLanguage.English:
                        //        msgParam["message"] = "Would you like to buy?";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        msgParam["message"] = "구매 하시겠습니까?";
                        //        break;
                        //}

                        msgParam["message"] = "Você gostaria de comprar?";
                        msgParam["show_type"] = "two";

                        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);
                        guiMessageBox.OnButtonClick = (bool pIsOk) =>
                        {
                            if (pIsOk)
                            {
                                if (MyInfoMgr.Instance.m_Gem >= costf)
                                {
                                    //MyInfoMgr.Instance.m_Funny += (uint)powersu;
                                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().FunnySuPlus((int)powersuf);
                                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().GemSuMinus((int)costf);
                                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate(pack_idx, 1);
                                    return;
                                }

                                //Hwang Wark 퍼니상점에서 잼이 코스트보다 부족할때 잼상점 팝업 Start
                                if (MyInfoMgr.Instance.m_Gem < costf)
                                {
                                    //Debug.Log("aaaa");

                                    guiChargeFunny.CloseBtn.onClick = null;
                                    //guiChargeFunny.FunnyReqBtn.onClick = null;

                                    guiChargeFunny = null;

                                    GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);

                                    // 젬 윈도우 팝업....
                                    GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
                                    _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
                                    if (_guiChargeWindow != null)
                                        _guiChargeWindow.ShowGemWindow();
                                }
                                //Hwang Wark 파워상점에서 잼이 코스트보다 부족할때 잼상점 팝업 End
                            }
                        };
                    };

                    guiChargeFunny.AddChargeFunnyItem(guiChargeFunnyItemf);
                }

                #endregion power item setting

                guiChargeFunny.CloseBtn.onClick = (GameObject pCloseGO) =>
                {
                    guiChargeFunny.CloseBtn.onClick = null;

                    guiChargeFunny = null;

                    GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);
                };
            }
        };

        guiItemShop.CloseBtn.onClick = (GameObject) =>
        {
            AudioMgr.Instance.Stop("Gacha");

            guiItemShop.ShowGachaDlg(false);
        };

        guiItemShop.TakingItemBtn.onClick = (GameObject) =>
        {
            AudioMgr.Instance.Stop("Gacha");

            //5000원 만큼 가격 깍기
            GameObject.Find("InfoMgr").GetComponent<InfoSave>().FunnySuMinus((int)5000);
            
            string packageName = "";
            string strDesc = "";
            string rewardIcon = "";

            uint pay = 0;
            GetGachaResult(packageInfoMgr, out packageName, out pay);	// 랜덤하게 가차뽑기..				

            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)pay);
            if (packageInfo != null)
            {
                packageName = packageInfo.name;
                strDesc = packageInfo.desc;
                rewardIcon = packageInfo.icon_image;
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().InGameItemHaveUpdate((int)pay, (int)packageInfo.item_1_su);
            }
            guiItemShop.ShowResult(true, packageName, strDesc, rewardIcon);

        };

        guiItemShop.OneMoreBtn.onClick = (GameObject) =>
        {
            guiItemShop.ShowResult(false, "", "", "");
            guiItemShop.GachaBtn.onClick(GameObject);
        };

        guiItemShop.ResultBackBtn.onClick = (GameObject) =>
        {
            guiItemShop.ShowGachaDlg(false);
            guiItemShop.ShowResult(false, "", "", "");
        };
    }

    // "가차뽑기"시 랜덤하게 패키지를 뽑는 함수...
    protected void GetGachaResult(PackageInfoMgr packageInfoMgr, out string packageName, out uint pay)
    {
        RandomboxInfoMgr randomboxInfoMgr = GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>();

        foreach (RandomboxInfo randomboxInfo in randomboxInfoMgr.GetRandomboxInfo())
        {
            if (randomboxInfo.randombox_type != RandomboxType.ShopGacha) // 샵에서 가차를 돌릴 경우만 이하 실행..
                continue;

            Debug.Log("Local");

            // 확률을 담는 배열 만들기..
            int[] temp = new int[1000];
            int inc = 0;

            for (int i = 0; i < 10; i++)
            {
                int dropPercent = (int)randomboxInfo.packagesetInfo[i].drop_percent; // 패키지당 드랍 확률...

                //Debug.Log(string.Format("PackagesetInfo {0}의 확률 : {1}", i, dropPercent));
                //
                for (int j = 0; j < dropPercent; j++)
                {
                    temp[inc] = i;	// 패키지 자신의 인덱스값 i를 담는다..
                    inc++;
                }
            }

            int randNum = UnityEngine.Random.Range(0, temp.Length);
            int index = temp[randNum];

            // 가차에서 뽑힌 패키지 데이터 정보 뽑기..
            uint package_SI = randomboxInfo.packagesetInfo[index].package_si;
            //Hwang Wark
            //Debug.Log(package_SI);
            //GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate((int)package_SI, 1);
            //GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
            //_guiSettingMenu.OnEnter();
            //Hwang End
            packageName = packageInfoMgr.GetPackageInfo(package_SI).name;
            pay = package_SI;
            return;
        }
        packageName = "GachaPackageName Error";
        pay = 0;

        return;
    }


	// 네트워크 연결 상태일때 사용하는 "가차뽑기"시 랜덤하게 패키지를 뽑는 함수...
    IEnumerator IE_GetGachaResult(GuiItemShop guiItemShop) // PackageInfoMgr packageInfoMgr, out string packageName, out uint pay)
    {
        string packageName = "";
        string strDesc = "", rewardIcon = "";
        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
        cmd._sOutputString = "";
        int resultPack_idx = 0;
        int resultPack_qty = 0;

        guiItemShop.ShowGachaDlg(false);
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
        yield return StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_GACHA_REQ());
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
        if (cmd._sOutputString == "")
        {
            Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
        }
        else
        {
            Debug.Log(cmd._sOutputString);
            JSONNode root = JSON.Parse(cmd._sOutputString);
            var resultCode = int.Parse(root["rc"]);

            if (resultCode == 0)
            {
                resultPack_idx = int.Parse(root["pack_idx"]);
                resultPack_qty = int.Parse(root["pack_qty"]);
                Debug.Log("~~~~~~~~~~~~~~~~~ resultPack_idx:" + resultPack_idx);

                // resultPack_idx = 11; // test 25
            }
        }
        if (resultPack_idx == 0)
        {
            // message box 필요.
            Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);
            cmd.PlayErrorMessage("가차뽑기 중 " + cmd._DefaultMessage,0);
        }
        else
        {
            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)resultPack_idx);
            if (packageInfo != null)
            {
                packageName = packageInfo.name;
                strDesc = packageInfo.desc;
                rewardIcon = packageInfo.icon_image;
                Debug.Log("~~~~~~~~~~~~~ Package Index : " + resultPack_idx.ToString() + " packageInfo.desc:" + packageInfo.desc + " icon:" + packageInfo.icon_image);

                cmd.PackItem_SetGemFunny(cmd._sOutputString);
                StartCoroutine(cmd.Send_PROTOCOL_LIST_ITEM_REQ());
            }
            guiItemShop.ShowResult(true, packageName, strDesc, rewardIcon);
        }
    }
    protected void GuiItemClick(GameObject pGO)
    {
        GuiItem guiItem = pGO.GetComponent<GuiItem>();

        SetItemName(guiItem.GetItemName() + " (" + guiItem.GetSu().ToString()+" 개)");
        SetItemDesc(guiItem.GetItemDesc());
		//Debug.Log (guiItem.GetSu());
        if (null != OnItemClick && guiItem.GetSu() != 0)
        {
            OnItemClick(guiItem);
        }

        ////Hwang Wark 아이템이 0개면 상점 팝업
        //if (guiItem.GetSu () == 0) 
        //{
        //    ShowShop();
        //}
        ////스테이지 인포 상점 End
    }

    IEnumerator Get_Gacha_Result() // PackageInfoMgr packageInfoMgr, out string packageName, out uint pay)
	{
        yield return StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_ROULETTE_REQ() );

// 로컬 손님 용
//        RandomboxInfoMgr randomboxInfoMgr = GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>();		
//        foreach(RandomboxInfo randomboxInfo in randomboxInfoMgr.GetRandomboxInfo())
//        {
//            if(randomboxInfo.randombox_type != RandomboxType.ShopGacha) // 샵에서 가차를 돌릴 경우만 이하 실행..
//                continue;
			
//            // 확률을 담는 배열 만들기..
//            int[] temp = new int[100];
//            int inc = 0;
			
//            for( int i = 0; i < 10; i++ )
//            {
//                int dropPercent = (int)randomboxInfo.packagesetInfo[i].drop_percent; // 패키지당 드랍 확률...
				
//                Debug.Log(string.Format("PackagesetInfo {0}의 확률 : {1}", i, dropPercent));
//                //
//                for( int j = 0; j < dropPercent; j++ )
//                {							
//                    temp[inc] = i;	// 패키지 자신의 인덱스값 i를 담는다..
//                    Debug.Log(string.Format("확률 인덱스 플래그 : {0}", temp[inc]));
//                    inc++;
//                }
//            }
			
//            int randNum = Random.Range(0,100);
//            int index = temp[randNum];
			
//            // 가차에서 뽑힌 패키지 데이터 정보 뽑기..
//            uint package_SI = randomboxInfo.packagesetInfo[index].package_si;
//            //Hwang Wark
//            //Debug.Log(package_SI);
////			GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate((int)package_SI,1);
//// 서버 연졀 체크.
//            GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
//            //Hwang End
//            packageName = packageInfoMgr.GetPackageInfo(package_SI).name;				
//            pay = randomboxInfo.attr_1;
			
        //    return;
        //}
        //packageName = "GachaPackageName Error";
        //pay = 0;
		
        //return;

	}

	protected IEnumerator PowerDir(Vector3 pSrcPos, Vector3 pDestPos, DELEGATEVOID pDirFinished)
	{
		GameObject powerDirGO =_usedTransList["PowerDir"].gameObject;

		powerDirGO.SetActive(true);
		powerDirGO.transform.position = pSrcPos;

		float time = 0.0f;

		while (time <= 1.0f)
		{
			time += Time.deltaTime * 2.0f;

			powerDirGO.transform.position = Vector3.Lerp(pSrcPos, pDestPos, time);

			yield return null;
		}

		powerDirGO.transform.position = pDestPos;
		powerDirGO.SetActive(false);

		if (null != pDirFinished)
		{
			pDirFinished();
		}
	}

}
