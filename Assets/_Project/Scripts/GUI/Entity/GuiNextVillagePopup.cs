﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiNextVillagePopup : GuiBase
{

	public delegate void DELEGATEGUINEXTVILLAGEPOPUPITEM(GuiNextVillagePopupRankItem pGuiNextVillagePopupRankItem);


	public DELEGATEGUINEXTVILLAGEPOPUPITEM			OnGuiNextVillagePopupRankItem = null;


	protected Dictionary<GameObject, GuiNextVillagePopupRankItem>	_rankItemList = new Dictionary<GameObject, GuiNextVillagePopupRankItem>();


	public UIEventListener GetCloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}
    
	public UIEventListener GetRequireGemBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RequireGemBtn"].gameObject);
		}
	}
    
	public UIEventListener GetGemBackBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GemBackBtn"].gameObject);
		}
	}

	public UIEventListener GetGemOkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GemOkBtn"].gameObject);
		}
	}

    public UIEventListener GetFreeOkBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["FreeNextViliage"].gameObject);
        }
    }

	public void ClearRankItem()
	{
		foreach (GuiNextVillagePopupRankItem rankItem in _rankItemList.Values)
		{
			rankItem.GetRequireFriendBtn.onClick = null;
			GameObject.DestroyImmediate(rankItem.gameObject);
		}

		_rankItemList.Clear();
	}


	public void ShowBaseDlg()
	{
		_usedTransList["Dialog_Base"].gameObject.SetActive(true);
		_usedTransList["Dialog_Gem"].gameObject.SetActive(false);
	}

	public void ShowFriendDlg()
	{
		_usedTransList["Dialog_Base"].gameObject.SetActive(false);
		_usedTransList["Dialog_Gem"].gameObject.SetActive(false);
	}

	public void ShowGemDlg()
	{
		_usedTransList["Dialog_Base"].gameObject.SetActive(false);
		_usedTransList["Dialog_Gem"].gameObject.SetActive(true);
	}

    public void ShowFreeVililage()
    {
        ES3.DeleteKey("NextViliageTime");

        _usedTransList["FreeNextViliage"].gameObject.SetActive(true);
        _usedTransList["RequireGemBtn"].gameObject.SetActive(false);

        if (_usedTransList["RequireGemBtn"].gameObject.activeSelf)
            _usedTransList["RequireGemBtn"].gameObject.SetActive(false);
    }

    public void ShowNextViliageBtn()
    {
        _usedTransList["FreeNextViliage"].gameObject.SetActive(false);
        _usedTransList["RequireGemBtn"].gameObject.SetActive(true);
    }

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Dialog_Base"] = null;
		_usedTransList["Dialog_Gem"] = null;

		_usedTransList["CloseBtn"] = null;
		_usedTransList["RequireGemBtn"] = null;

		_usedTransList["GemBackBtn"] = null;
        _usedTransList["GemOkBtn"] = null; //

        _usedTransList["Desc_Base"] = null; // 

        _usedTransList["FreeNextViliage"] = null;

        _usedTransList["Free_Desc"] = null;
        _usedTransList["Desc"] = null;
        _usedTransList["Title_Base"] = null;

		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.Korean:
                
        //         _usedTransList["GemBackBtn"].GetComponentInChildren<UILabel>().text = "취소";
        //        _usedTransList["GemOkBtn"].GetComponentInChildren<UILabel>().text = "확인"; //
        //        _usedTransList["Desc"].GetComponent<UILabel>().text =         "젬이 부족합니다. 구매하시겠습까?";
        //        break;
        //    case SystemLanguage.English:
        //        _usedTransList["GemBackBtn"].GetComponentInChildren<UILabel>().text   = "Sair";
        //        _usedTransList["GemOkBtn"]  .GetComponentInChildren<UILabel>().text   =  "OK"; //
        //        _usedTransList["Desc"].GetComponentInChildren<UILabel>().text =         "Not Enough Gem\nDo you want to purchase?";
        //        _usedTransList["Desc_Base"].GetComponent<UILabel>().text = "Please solving the locking device by using the gem in order to go to the next village.";
        //        _usedTransList["Title_Base"].GetComponent<UILabel>().text = "Next Village";
        //        _usedTransList["RequireGemBtn"].GetComponentInChildren<UILabel>().text = "Unlock Stage\t\t1";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["GemBackBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["GemOkBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["Desc"].GetComponentInChildren<UILabel>().text = "Not Enough Gem \n Você quer comprar?";
        //        _usedTransList["Desc_Base"].GetComponent<UILabel>().text = "Por favor, resolver o dispositivo de bloqueio usando a jóia, a fim de ir para a aldeia mais próxima.";
        //        _usedTransList["Title_Base"].GetComponent<UILabel>().text = "Próximo Village.";
        //        _usedTransList["RequireGemBtn"].GetComponentInChildren<UILabel>().text = "Unlock Stage 1";
        //        break;
        //}
        _usedTransList["GemBackBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["GemOkBtn"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["Desc"].GetComponentInChildren<UILabel>().text = "Not Enough Joias \n Você quer comprar?";
        _usedTransList["Desc_Base"].GetComponent<UILabel>().text = "Por favor, resolver o dispositivo de bloqueio usando a jóia, a fim de ir para a aldeia mais próxima.";
        _usedTransList["Title_Base"].GetComponent<UILabel>().text = "Próximo Aldeia";
        _usedTransList["RequireGemBtn"].GetComponentInChildren<UILabel>().text = "Desbloquear Stage 1";

        _usedTransList["FreeNextViliage"].GetComponentInChildren<UILabel>().text = "Mover para a direita";
        _usedTransList["FreeNextViliage"].gameObject.SetActive(false);
        _usedTransList["RequireGemBtn"].gameObject.SetActive(true);
	}

    public void EnoughTimeNextViliage(string str) 
    {
        _usedTransList["Free_Desc"].GetComponent<UILabel>().text = str;
    }

    public void CheckLastGame()
    {
        InfoSave info = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
        if (info)
        {
            if (info.clearStageTopNumber >= GameStateMgr._LastStage_LockPoint)
            {

                if (_usedTransList["Desc_Base"])
                {
                    UILabel desc = _usedTransList["Desc_Base"].GetComponent<UILabel>(); // gameObject.SetActive(false);
                    if (desc)
                    {
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        desc.text = "Eu gosto deste ... mau caçador tem bloqueando a estrada \n Este é o lugar onde você está cavando até a estrada \n Aguarde..";
                        //         break;
                        //    case SystemLanguage.English:
                        //        desc.text = "I like this ... bad hunter has blocking the road.\nThis is where you are digging up the road.\nPlease wait";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        desc.text = "이런... 못된 사냥꾼이 길을 막아 놓았네요.\n길을 파헤치고 있는 중이에요.\n기다려주세요.";
                        //        break;
                        //}

                        desc.text = "Ah não, o caçador malvado bloqueou a passagem! \n Aguarde um momento.";
//"Eu gosto deste ... mau caçador tem bloqueando a estrada \n Este é o lugar onde você está cavando até a estrada \n Aguarde..";
                             
                    }
                }

                _usedTransList["FreeNextViliage"].gameObject.SetActive(false);
                _usedTransList["RequireGemBtn"].gameObject.SetActive(false);
            }
            else
            {
                if (_usedTransList["Desc_Base"])
                {
                    UILabel desc = _usedTransList["Desc_Base"].GetComponent<UILabel>(); // gameObject.SetActive(false);
                    if (desc)
                    {
                        //switch (Application.systemLanguage) 
                        //{
                        //    case SystemLanguage.English:
                        //        desc.text = "By using the gem to go to the next village, please release the lock.";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        desc.text = "다음 마을로 가기 위해서는 젬을 이용하여 잠금 장치를 풀어 주세요.";
                        //        break;
                        //    case SystemLanguage.Portuguese:
                        //        desc.text = "Ao usar a jóia para ir para a aldeia mais próxima, por favor liberar o bloqueio.";
                        //        break;
                        //}
                        desc.text = "Você precisa abrir o cadeado se quiser ir para a próxima vila.";
//"Ao usar a jóia para ir para a aldeia mais próxima, por favor liberar o bloqueio.";
                               

                    }
                }
                _usedTransList["RequireGemBtn"].gameObject.SetActive(true);
            }
        }

    }

    public override void OnEnter()
    {
		ShowBaseDlg();
        CheckLastGame();
    }

    public override void OnLeave()
    {
		ClearRankItem();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}
}
