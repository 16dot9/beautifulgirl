﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiLogo : GuiBase
{

	public void ShowLogo1()
	{
		_usedTransList["Logo_01"].gameObject.SetActive(true);
		_usedTransList["Logo_02"].gameObject.SetActive(false);

		TweenAlpha alpha = TweenAlpha.Begin(_usedTransList["Logo_01"].gameObject, 0.2f, 1.0f);
		alpha.from = 0.0f;
		alpha.method = UITweener.Method.EaseIn;
	}

	public void ShowLogo2()
	{
		_usedTransList["Logo_01"].gameObject.SetActive(false);
		_usedTransList["Logo_02"].gameObject.SetActive(true);

		TweenAlpha alpha = TweenAlpha.Begin(_usedTransList["Logo_02"].gameObject, 0.2f, 1.0f);
		alpha.from = 0.0f;
		alpha.method = UITweener.Method.EaseIn;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Logo_01"] = null;
		_usedTransList["Logo_02"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
		_usedTransList["Logo_01"].gameObject.SetActive(false);
		_usedTransList["Logo_02"].gameObject.SetActive(false);
	}

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
