﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiBG : GuiBase
{

	public void Show(bool pIsShow)
	{
		gameObject.SetActive(pIsShow);
	}


    public override void OnCreate()
    {
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
    }

}
