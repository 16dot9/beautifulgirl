﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayMenu : GuiBase
{
    /*
	public UIEventListener ChangeSettingBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["ChangeSettingBtn"].gameObject);
		}
	}*/

	public UIEventListener ContinueBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["ContinueBtn"].gameObject);
		}
	}

	public UIEventListener GotoMainMenuBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GotoMainMenuBtn"].gameObject);
		}
	}

	public UIEventListener ReplayBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["ReplayBtn"].gameObject);
		}
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		//_usedTransList["ChangeSettingBtn"] = null;
		_usedTransList["ContinueBtn"] = null;
		_usedTransList["GotoMainMenuBtn"] = null;
		_usedTransList["ReplayBtn"] = null;
        _usedTransList["SetLabel"] = null;

		FindTrans();


        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        //_usedTransList["ChangeSettingBtn"].GetComponentInChildren<UILabel>().text = "Option";
        //        _usedTransList["ContinueBtn"].GetComponentInChildren<UILabel>().text = "Continue";
        //        _usedTransList["GotoMainMenuBtn"].GetComponentInChildren<UILabel>().text = "Main Lobby";
        //        _usedTransList["ReplayBtn"].GetComponentInChildren<UILabel>().text = "Replay";
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Game Menu";
        //        break;
        //    case SystemLanguage.Korean:
        //        //_usedTransList["ChangeSettingBtn"].GetComponentInChildren<UILabel>().text = "설정변경";
        //        _usedTransList["ContinueBtn"].GetComponentInChildren<UILabel>().text = "계속하기";
        //        _usedTransList["GotoMainMenuBtn"].GetComponentInChildren<UILabel>().text = "시작화면가기";
        //        _usedTransList["ReplayBtn"].GetComponentInChildren<UILabel>().text = "다시하기";
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "게임메뉴";
        //        break;

        //    case SystemLanguage.Portuguese:
        //        //_usedTransList["ChangeSettingBtn"].GetComponentInChildren<UILabel>().text = "설정변경";
        //        _usedTransList["ContinueBtn"].GetComponentInChildren<UILabel>().text = "Continue";
        //        _usedTransList["GotoMainMenuBtn"].GetComponentInChildren<UILabel>().text = "Main Lobby";
        //        _usedTransList["ReplayBtn"].GetComponentInChildren<UILabel>().text = "Replay";
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Game Menu";
        //        break;


        //}

        //_usedTransList["ChangeSettingBtn"].GetComponentInChildren<UILabel>().text = "설정변경";
        _usedTransList["ContinueBtn"].GetComponentInChildren<UILabel>().text = "Continuar";
        _usedTransList["GotoMainMenuBtn"].GetComponentInChildren<UILabel>().text = "Menu Principal";
        _usedTransList["ReplayBtn"].GetComponentInChildren<UILabel>().text = "Reiniciar";
        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Jogar";
                
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
