﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class Kakao_NextVillagePopup_RequestFriend_Item : GuiBase 
{
	public int SerialID
	{
		get; set;
	}
	
	public UIToggle Button
	{
		get { return _usedTransList ["Button"].GetComponent<UIToggle> (); }
	}
	
	public string ProfilImage
	{
		get { return _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture.name;	}
	}
	
	public string NickName
	{
		get { return _usedTransList["NickName"].GetComponent<UILabel>().text; }
	}
	
	
	public void SetItemInfo(int serialID, Texture2D profilImage, string nickName)
	{
		SerialID = serialID;
		
		_usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = profilImage;
		_usedTransList["NickName"].GetComponent<UILabel>().text = nickName;
		_usedTransList["Button"].GetComponent<UIToggle>().value = false;
	}
	
	
	/// <summary>
	/// 메시지를 보낼지 확인하는 창을 보여주고 OK 하면 메시지 보내기...  
	/// </summary>
	private void ShowMessageBox (MyFriend myKakaoFriend, string msg, KakaoMessage.MessageType messageType)
	{
		UIToggle button = _usedTransList ["Button"].GetComponent<UIToggle> ();
		
		JSONNode msgParam = new JSONClass();
		
		msgParam["message"] = myKakaoFriend.nickname + msg;
		msgParam["show_type"] = "two";		// "ok", "cancel" two button...
		
		GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...
		
		guiMessageBox.OnButtonClick = (bool pIsOk) =>
		{
			if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
			{
                button.value = true;									// 체크 표시...
				button.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
                StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_UNLOCK_STAGE_REQ(myKakaoFriend.userid, messageType, myKakaoFriend));
            }
			else
			{
				button.value = false;
			}
		};
	}
	

	/// <summary>
	/// 요청 버튼 클릭시 호출...
	/// </summary>
	public void Request_KakaoKeypetFriends()
	{
		// 게임 설치한 친구들 찾고...
        List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;

        ShowMessageBox(myKakaoFriends[SerialID], KakaoMessage.SendMsg.NextVillage_Msg, KakaoMessage.MessageType.NextVillage);	// "요청 메시지를 보낼까요?" 확인창 출력...
	}
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["ProfilImage"] = null;			// 프로필 사진...
		_usedTransList["NickName"] = null;				// 닉네임...
		_usedTransList["Button"] = null;				// 조르기 버튼...

        _usedTransList["Friend_01"] = null;
        _usedTransList["Friend_02"] = null;
        _usedTransList["Friend_03"] = null;

		FindTrans();
	}
	
	public override void OnEnter()
	{
	}
	
	public override void OnLeave()
	{
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

    public void SetFriendImage(int _Conut, Texture _Texture, string _nickName)
    {
        _usedTransList["Friend_0" + (_Conut + 1).ToString()].GetComponent<UITexture>().mainTexture = _Texture;
        _usedTransList["Friend_0" + (_Conut + 1).ToString()].GetComponentInChildren<UILabel>().text = _nickName;
    }
}
