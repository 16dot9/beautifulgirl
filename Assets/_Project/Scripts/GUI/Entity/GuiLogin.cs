﻿using UnityEngine;
using System.Collections;

public class GuiLogin : GuiBase
{
	public override void OnCreate()
	{		
		_usedTransList.Clear();
		
		_usedTransList["Kakao Login Button"] = null;
				
		FindTrans();
	}

	
	public override void OnEnter()
	{
		//_usedTransList["Kakao Login Button"].gameObject.SetActive(false);
	}


	public override void OnLeave()
	{
	}

	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

	/// <summary>
	/// "카카오 로그인" 버튼 클릭시 호출...
	/// </summary>
	public void KakaoLogin()
	{
		//KakaoMain.Instance.Login ();
        //FBMain.Instance.FBLogin();
        // wtkim: 로딩 화면 완료시 GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, false, null); 로 꺼준다. 
        // - onLoginComplete, onLoginError 에서 체크 - 게임서버 완료는 Send_PROTOCOL_LOGIN_REQ 에서 체크.
        GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, true, null);
	}

    public void SingleBtn() 
    {
        InfoSave.Instance.FbUserNew(true);

        GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
    }
	/// <summary>
	/// "카카오 계정 등록" 버튼 클릭시 호출...
	/// </summary>
	public void KakaoWebLogin()
	{
		KakaoMain.Instance.WebLogin ();
	}

}
