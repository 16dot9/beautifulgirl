﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiItem : GuiBase
{
    public uint item_index = 0;
    public uint item_su = 0;
    protected uint selected_count = 0;
    public string item_desc;
    public InfoSave _InfoSave;

    public UIEventListener BuyBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
        }
    }

    public uint GetSelectCount()
    {
        return selected_count;
 //       return int.Parse(_usedTransList["ItemCount"].GetComponent<UILabel>().text);
    }

    public uint GetSu()
    {
        return item_su;
    }

    public void ResetCount()
    {
        selected_count = 0;
        
        _usedTransList["ItemCountBG"].gameObject.SetActive(false);
    }

    public void AddCount()
    {
        selected_count += 1;
        if (selected_count > item_su)
            selected_count = item_su;

        if (selected_count > 0)
        {
            _usedTransList["ItemCountBG"].gameObject.SetActive(true);
        }
        else
        {
            _usedTransList["ItemCountBG"].gameObject.SetActive(false);
        }

        _usedTransList["ItemCount"].GetComponent<UILabel>().text = selected_count.ToString();
    }


    public string GetItemName()
    {
        return _usedTransList["ItemName"].GetComponent<UILabel>().text;
    }

    public void SetItemName(string pItemName)
    {
        _usedTransList["ItemName"].GetComponent<UILabel>().text = pItemName;

        GuiItemCount itemCount = transform.GetComponentInChildren<GuiItemCount>();
        if (itemCount)
        {
            itemCount._refGuiItem = this;
        }
    }


    public void SetItemDesc(string str)
    {
        _usedTransList["ItemDesc"].GetComponent<UILabel>().text = str;
    }

    public string GetItemDesc()
    {
        return _usedTransList["ItemDesc"].GetComponent<UILabel>().text;
    }


    public void SetItemImage(Texture2D pItemImage)
    {
        _usedTransList["ItemImage"].GetComponent<UITexture>().mainTexture = pItemImage;
    }


    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["ItemCountBG"] = null;
        _usedTransList["ItemCount"] = null;// 

        _usedTransList["ItemName"] = null;
        _usedTransList["ItemDesc"] = null;

        _usedTransList["ItemImage"] = null;// 
        _usedTransList["BuyBtn"] = null;

        _usedTransList["TotalCount"] = null;
        _usedTransList["BuyBtn"] = null;

        FindTrans();

        _InfoSave = GameObject.Find("InfoMgr").GetComponent<InfoSave>();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["BuyBtn"].GetComponentInChildren<UILabel>().text = "Purchase";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["BuyBtn"].GetComponentInChildren<UILabel>().text = "Purchase";
                
        //        break;
        //}
        _usedTransList["BuyBtn"].GetComponentInChildren<UILabel>().text = "Comprar";
                
    }

    public void SetItemCount(uint itemsu) 
    {
        string cnName = "";

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        cnName = " ea";
        //        break;
        //    case SystemLanguage.Korean:
        //        cnName = " 개";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        cnName = " ea";
        //        break;
        //}

        cnName = " ea";
        _usedTransList["TotalCount"].GetComponent<UILabel>().text = itemsu.ToString() + cnName;
    }

    public override void OnEnter()
    {
        _usedTransList["ItemCountBG"].gameObject.SetActive(false);
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }

}
