﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiGameSetting : GuiBase
{

	public UIEventListener TweeterBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["TweeterBtn"].gameObject);
		}
	}

	public UIEventListener FaceBookBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["FaceBookBtn"].gameObject);
		}
	}

	public UIEventListener NaverBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["NaverBtn"].gameObject);
		}
	}

	public UIEventListener OkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["OkBtn"].gameObject);
		}
	}

    //ShowMethe Money

	public UIEventListener TutorialBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["TutorialBtn"].gameObject);
		}
	}



    public UIEventListener ContactBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["ContactBtn"].gameObject);
        }
    }
    public UIEventListener BonusBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["BonusBtn"].gameObject);
        }
    }


    public UIEventListener ShowMeTheMoneyBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["ShowMeTheMoneyBtn"].gameObject);
        }
    }


    public UIEventListener FBSetBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["FBbtn"].gameObject);
        }
    }
	public void SetVersion(string pVersion)
	{
		_usedTransList["Version"].GetComponent<UILabel>().text = pVersion;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["SoundEffect"] = null;
		_usedTransList["BGEffect"] = null;

		//_usedTransList["TweeterBtn"] = null;
		_usedTransList["FaceBookBtn"] = null;
		_usedTransList["NaverBtn"] = null;

		_usedTransList["Version"] = null;

		_usedTransList["OkBtn"] = null;
		_usedTransList ["TutorialBtn"] = null;

		_usedTransList ["LogoutBtn"] = null;
		_usedTransList ["KakaoAccount"] = null;
		_usedTransList ["KakaoUnregister"] = null;
        _usedTransList["ContactBtn"] = null;
        _usedTransList["BonusBtn"] = null;
        //_usedTransList["FBbtn"] = null;

        _usedTransList["BgSound"]           = null;
        _usedTransList["EffectBG"]          = null;
        _usedTransList["SettingLabel"]      = null;
        _usedTransList["BoLabel"]           = null;
        _usedTransList["GoLabel"]           = null;
        _usedTransList["e mail address"]    = null;
        _usedTransList["TuLabel"] = null;
        _usedTransList["OkLabel"]           = null;

		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["BgSound"]           .GetComponent<UILabel>().text = "BGM";
        //        _usedTransList["EffectBG"]          .GetComponent<UILabel>().text = "SE";
        //        _usedTransList["SettingLabel"]      .GetComponent<UILabel>().text = "Option";
        //        _usedTransList["BoLabel"]           .GetComponent<UILabel>().text = "Bonus Tutorial";
        //        _usedTransList["GoLabel"]           .GetComponent<UILabel>().text = "CS";
        //        _usedTransList["e mail address"]    .GetComponent<UILabel>().text = "CS : valkyri007@gmail.com\n\nValkyri Entertainment";
        //        _usedTransList["TuLabel"]           .GetComponent<UILabel>().text = "Tutorial";
        //        _usedTransList["OkLabel"]           .GetComponent<UILabel>().text = "OK";
        //        break;
        //    case SystemLanguage.Portuguese:

        //        _usedTransList["BgSound"]           .GetComponent<UILabel>().text = "Música";
        //        _usedTransList["EffectBG"]          .GetComponent<UILabel>().text = "Sons";
        //        _usedTransList["SettingLabel"]      .GetComponent<UILabel>().text = "Opção";
        //        _usedTransList["BoLabel"]           .GetComponent<UILabel>().text = "Tutorial-bônus";
        //        _usedTransList["GoLabel"]           .GetComponent<UILabel>().text = "CS";
        //        _usedTransList["e mail address"]    .GetComponent<UILabel>().text = "CS : valkyri007@gmail.com\n\nValkyri Entertainment";
        //        _usedTransList["TuLabel"]           .GetComponent<UILabel>().text = "Tutorial";
        //        _usedTransList["OkLabel"]           .GetComponent<UILabel>().text = "OK";
        //        break;
        //}

        _usedTransList["BgSound"].GetComponent<UILabel>().text = "Música";
        _usedTransList["EffectBG"].GetComponent<UILabel>().text = "Sons";
        _usedTransList["SettingLabel"].GetComponent<UILabel>().text = "Opção";
        _usedTransList["BoLabel"].GetComponent<UILabel>().text = "Tutorial-bônus";
        _usedTransList["GoLabel"].GetComponent<UILabel>().text = "Contato";
        _usedTransList["e mail address"].GetComponent<UILabel>().text = "E-mail de Suporte: mobile@playspot.com.br\n\nPlayspot";
        _usedTransList["TutorialBtn"].GetComponentInChildren<UILabel>().text = "Tutorial";
        _usedTransList["OkLabel"].GetComponent<UILabel>().text = "Sim";

        //if (FBMain.Instance.isFB)
        //    _usedTransList["FBbtn"].GetComponentInChildren<UILabel>().text = "FB Logout";
        //else
        //    _usedTransList["FBbtn"].GetComponentInChildren<UILabel>().text = "FB Login";
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

    public void SetFacebookUserID(string name) 
    {
        Debug.Log(name);

        Debug.Log(_usedTransList["KakaoAccount"].GetComponent<UILabel>().text);

        _usedTransList["KakaoAccount"].GetComponent<UILabel>().text = name;
    }
	/// <summary>
	/// 게임 설정창 숨기기...
	/// </summary>
	public void HideGameSettingWindow()
	{
		GuiGameSetting guiGameSetting = GuiMgr.Instance.Find<GuiGameSetting> ();	// 현재 열려있는 게임 설정창 객체를 얻기 위해 한 번 더 호출함...
				
		// 열려있는 게임 설정창을 제거하고...
		if (guiGameSetting)
		{
			guiGameSetting = null;
			GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
		}
	}

    public void BonusTtuWindow(bool _bSetting) 
    {
        _usedTransList["BonusBtn"].gameObject.SetActive(_bSetting);
    }

    /*
    void OnGUI()
    {
        #region dummy Cheat

        if (GUI.Button(new Rect(50, 50, 150, 150), "Show Me the Money"))
        {
            StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_SHOW_ME_THE_MONEY_REQ());
        }
        #endregion
    }
    */

}
