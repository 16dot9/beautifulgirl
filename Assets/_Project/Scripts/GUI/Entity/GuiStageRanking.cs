﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;


public class GuiStageRanking : GuiBase
{

    public delegate void DELEGATEGUISTAGERANKINGITEM(GuiStageRankingItem pGuiStageRankingItem);


    public DELEGATEGUISTAGERANKINGITEM              OnGuiPowerBtnClick;


	List<GuiStageRankingItem>	_guiStageRankingItemList = new List<GuiStageRankingItem>();


    public void AddStageRankingItem(GuiStageRankingItem pGuiStageRankingItem)
    {
        pGuiStageRankingItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Rankings"].GetComponent<UIScrollView>();

        pGuiStageRankingItem.transform.parent = _usedTransList["UIGrid"];
        pGuiStageRankingItem.transform.localPosition = Vector3.zero;
        pGuiStageRankingItem.transform.localRotation = Quaternion.identity;
        pGuiStageRankingItem.transform.localScale = Vector3.one;

        _guiStageRankingItemList.Add( pGuiStageRankingItem);
        
        _usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
    }

    public void ClearStageRankingItem()
    {
        _usedTransList["Rankings"].transform.localPosition =
               new Vector3(0.0f, 0.0f, 0.0f);

        _usedTransList["Rankings"].GetComponent<UIPanel>().clipOffset = new Vector2(0.0f, 0.0f);
   
        foreach (var pair in _guiStageRankingItemList) 
        {
            DestroyObject(pair.gameObject);
        }

        _guiStageRankingItemList.Clear();
 }


    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["Rankings"] = null;
        _usedTransList["UIGrid"] = null;

        FindTrans();
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearStageRankingItem();
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }


    protected void GuiPowerBtnClick(GameObject pGO)
    {
        //GuiStageRankingItem rankItem;

        //if (!_guiStageRankingItemList.TryGetValue(pGO, out rankItem))
        //{
        //    return;
        //}

        //if (null != OnGuiPowerBtnClick)
        //{
        //    //OnGuiPowerBtnClick(rankItem);

        //    Debug.Log("OnguiPowerBtnClick"); 
        //}
    }

}
