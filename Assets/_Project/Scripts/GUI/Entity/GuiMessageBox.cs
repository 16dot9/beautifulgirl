﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiMessageBox : GuiBase
{

	public delegate void DELEGATEBOOL(bool pIsOk);


	public DELEGATEBOOL		OnButtonClick = null;


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Message"] = null;

		_usedTransList["OneButton"] = null;
		_usedTransList["OneOk"] = null;

		_usedTransList["TwoButton"] = null;
		_usedTransList["TwoOk"] = null;
		_usedTransList["TwoCancel"] = null;

		FindTrans();

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["OneOk"].GetComponentInChildren<UILabel>().text = "O K";
        //        _usedTransList["TwoOk"].GetComponentInChildren<UILabel>().text = "O K";
        //        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Cancel";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["OneOk"].GetComponentInChildren<UILabel>().text = "O K";
        //        _usedTransList["TwoOk"].GetComponentInChildren<UILabel>().text = "O K";
        //        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Cancel";
        //        break;
        //}

        _usedTransList["OneOk"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["TwoOk"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Não";
        

		_usedTransList["OneButton"].gameObject.SetActive(false);
		_usedTransList["TwoButton"].gameObject.SetActive(false);

		UIEventListener.Get(_usedTransList["OneOk"].gameObject).onClick = OnOk;
		UIEventListener.Get(_usedTransList["TwoOk"].gameObject).onClick = OnOk;
		UIEventListener.Get(_usedTransList["TwoCancel"].gameObject).onClick = OnCancel;

	}

    public override void OnEnter()
    {
		_usedTransList["Message"].GetComponent<UILabel>().text = _params["message"];

		if (string.Equals(_params["show_type"], "one"))		
		{
			_usedTransList["OneButton"].gameObject.SetActive(true);
			_usedTransList["TwoButton"].gameObject.SetActive(false);
		}
		else
		{
			_usedTransList["OneButton"].gameObject.SetActive(false);
			_usedTransList["TwoButton"].gameObject.SetActive(true);
		}
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


	protected void OnOk(GameObject pGO)
	{
		if (null != OnButtonClick)
		{
			OnButtonClick(true);
			OnButtonClick = null;
		}

		GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, false, null);
	}

	protected void OnCancel(GameObject pGO)
	{
		if (null != OnButtonClick)
		{
			OnButtonClick(false);
			OnButtonClick = null;
		}

		GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, false, null);
	}

}
