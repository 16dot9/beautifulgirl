﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;

public class GuiStageMapInfoScrollViewStageItemStage : GuiBase
{
    
	public delegate void DELEGATEHANDLER(GuiStageMapInfoScrollViewStageItemStage pComp);

	public DELEGATEHANDLER		OnStageMapClick = null;

    public delegate void DELEGATEBOOL(bool pIsOk);

    public DELEGATEBOOL OnButtonClick = null;

	protected class ProfileInfo
	{
		public GameObject		UIInfo = null;
		public UITexture		UITex = null;
	}


	protected uint				_si = 0;
	protected uint				_stageNum = 0;

	protected ProfileInfo[]		_profileInfo = null;
	protected bool				_isProfileExtend = false;

    //트윈역활 해주는 좌표 저장
    const   uint    Max_Profile = 4;
    private Vector3[]           _ProfileFromVector;
    private Vector3[]           _ProfileToVector;
    private bool                isTweenPostion = true;

    private int Count           = 0;
    protected void OnOk(GameObject pGO)
    {
        if (null != OnButtonClick)
        {
            OnButtonClick(true);
        }
    }

    public void TweenMoved() 
    {
        if (isTweenPostion == false)
        {
            isTweenPostion = true;
        }
        else
        {
            isTweenPostion = false;
        }

        TweenToMove(isTweenPostion);
    }

	public uint GetSI()
	{
		return _si;
	}

	public void SetSI(uint pSI)
	{
		_si = pSI;
	}

	public uint GetStageNum()
	{
		return _stageNum;
	}

	public void SetStageNum(uint pStageNum)
	{
		_stageNum = pStageNum;
		_usedTransList["Stage"].GetComponent<UILabel>().text = pStageNum.ToString();

        //if (InteractiveConsole.GetInstance().isFriendCall)
        //{
        //    int Count = 1;

        //    foreach (var pair in InteractiveConsole.GetInstance().gameFriends)
        //    {
        //        InteractiveConsole.GameFriends gamer = pair.Value;
        //        if (gamer == null)
        //            continue;

        //        if (gamer.score.ToString() == _usedTransList["Stage"].GetComponent<UILabel>().text)
        //        {
        //            SetStageProfile(gamer.url, Count);

        //            Count++;

        //            if (Count > 4)
        //            {
        //                Count = 0;
        //            }
        //        }
        //    }
        //}
    }

    public void SetStageProfile(string url = "",int Count = 1) 
    {
        if (Count < 5)
        {
            string _str = "Profile_0" + Count.ToString();

            StartCoroutine(FriendProfileImageLoad(url, _usedTransList[_str].GetComponent<UITexture>()));
	    }
    }

    public void SetStageStr(string str)
    {
        _usedTransList["Stage"].GetComponent<UILabel>().text = string.Format("{0}", str);
    }

	public void LockStageBtn(bool pIsLock)
	{
		//_usedTransList["StageBtn"].GetComponent<UIImageButton>().isEnabled = !pIsLock;
		_usedTransList["Stage"].gameObject.SetActive(!pIsLock);
	}

    GameObject thisChild;

    IEnumerator FriendProfileImageLoad(string url, UITexture _go)
	{
		while (true)
		{
			if (_go != null)
			{
				break;
			}
			yield return new WaitForFixedUpdate();
		}
		
		yield return new WaitForSeconds(0.1f);
		
		WWW www = new WWW(url);
		
		yield return www;
		
		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.Log(www.error);        
		}
		else
		{
			_go.mainTexture = www.texture;

            CreateBackProfile(url, _go.gameObject);
        }
    }
    private void CreateBackProfile(string url, GameObject _go) 
    {
        //if (InteractiveConsole.GetInstance().MyFbInfo.url == url
        //    && InteractiveConsole.GetInstance().MyFbInfo.score.ToString() == _stageNum.ToString()
        //    && thisChild == null)
        //{
        //    thisChild = Instantiate(_go) as GameObject;

        //    thisChild.transform.parent = _go.gameObject.transform;
        //    thisChild.transform.localScale = new Vector3(1.1f, 1.1f, 1.0f);
        //    thisChild.transform.localPosition = Vector3.zero;

        //    thisChild.GetComponent<UITexture>().color = Color.blue;

        //    thisChild.AddComponent<TweenAlpha>();

        //    thisChild.GetComponent<TweenAlpha>().from = 0.5f;
        //    thisChild.GetComponent<TweenAlpha>().to = 0.0f;

        //    thisChild.GetComponent<TweenAlpha>().style = UITweener.Style.PingPong;
        //}
    }
    public void ClearBackProfile() 
    {
        if (thisChild != null)
            DestroyImmediate(thisChild);
    }

    public void ClearProfileTexture() 
    {
        for (int i = 1; i < Max_Profile; i++) 
        {
            string str = "Profile_0" + i.ToString();

            _usedTransList[str].GetComponent<UITexture>().mainTexture = null;
        }
    }

	//profile Create - ahn 140903
	//<------------------------->
	// 이미지 요기다가 추가
    //<------------------------->
	public void AddProfileInfo(Texture2D[] pImageList)
	{
		ClearProfileInfo();

		if (null != pImageList)
		{
			_profileInfo = new ProfileInfo[pImageList.Length];

			bool isReverse = false;

			for (int i = 0; i < pImageList.Length; ++i)
			{
				_profileInfo[i] = new ProfileInfo();
				_profileInfo[i].UIInfo = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiStageMapInfoScrollViewStageItemStageProfileInfo"));
				_profileInfo[i].UITex = _profileInfo[i].UIInfo.GetComponentInChildren<UITexture>();

				_profileInfo[i].UIInfo.transform.parent = _usedTransList[string.Format("Profile_{0}", (i + 1).ToString("00"))];
				_profileInfo[i].UIInfo.transform.localPosition = Vector3.zero;
				_profileInfo[i].UIInfo.transform.localRotation = Quaternion.identity;
//				_profileInfo[i].UIInfo.transform.localScale = Vector3.one;
                _profileInfo[i].UIInfo.transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);

				UIEventListener.Get(_profileInfo[i].UIInfo.gameObject).onClick = OnProfileBtnClick;

				if (_profileInfo[i].UIInfo.transform.parent.transform.position.x > 1000.0f)
				{
					isReverse = true;
				}
			}

			if (isReverse)
			{
				for (int i = 0; i < pImageList.Length; ++i)
				{
					Vector3 pos = _profileInfo[i].UIInfo.transform.parent.transform.localPosition;
					_profileInfo[i].UIInfo.transform.parent.transform.localPosition = new Vector3(-pos.x, pos.y, pos.z);
				}
			}

			ExtendProfileInfo(false);
		}
	}

	public void ClearProfileInfo()
	{
		if (null == _profileInfo)
		{
			return;
		}

		foreach (ProfileInfo profileInfo in _profileInfo)
		{
			GameObject.DestroyImmediate(profileInfo.UIInfo);
		}

		_profileInfo = null;
	}

	public void ExtendProfileInfo(bool pIsExtend)
	{
		if (null == _profileInfo || 0 == _profileInfo.Length)
		{
			return;
		}

		_isProfileExtend = pIsExtend;

		if (pIsExtend)
		{
			for (int i = 0; i < _profileInfo.Length; ++i)
			{
				_profileInfo[i].UIInfo.gameObject.SetActive(true);
			}
		}
		else
		{
			for (int i = 0; i < _profileInfo.Length; ++i)
			{
				if (0 == i)
				{
					_profileInfo[i].UIInfo.gameObject.SetActive(true);
				}
				else
				{
					_profileInfo[i].UIInfo.gameObject.SetActive(false);
				}
			}
		}
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["StageBtn"] = null;
		_usedTransList["Stage"] = null;

		_usedTransList["ProfileBtn"] = null;
		_usedTransList["Profile_01"] = null;
		_usedTransList["Profile_02"] = null;
		_usedTransList["Profile_03"] = null;
		_usedTransList["Profile_04"] = null;

		FindTrans();

        _ProfileFromVector = new Vector3[4];
        _ProfileToVector = new Vector3[4];

        for (int i = 1; i < Max_Profile; i++) 
        {
            string str = "Profile_0" + (i + 1).ToString();

            _ProfileFromVector[i]   = _usedTransList[str].GetComponent<TweenPosition>().from;
            _ProfileToVector[i]     = _usedTransList[str].GetComponent<TweenPosition>().to;
        }

        UIEventListener.Get(_usedTransList["StageBtn"].gameObject).onClick = OnStageBtnClick;
        UIEventListener.Get(_usedTransList["ProfileBtn"].gameObject).onClick = OnOk;
        
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

	public void OnStageBtnClick(GameObject pGO)
	{
		Camera scrollCamera = GameObject.Find ("Gui/GuiStageMapInfoScrollView/ScrollCamera").GetComponent<Camera>();
		GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().clearStageNumber = transform.Find("StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex;
		GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().clearTopStageCameraPosition = transform.parent.position + transform.localPosition;
		GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().item12Index = GameObject.Find ("Gui/GuiStageMapInfoScrollView/Items/Maps/Map_002/12/StageBtn").GetComponent<ShopItemIndexNumber> ().thisIndex;
		if (null != OnStageMapClick && pGO.transform.GetComponent<ShopItemIndexNumber>().thisIndex <= GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber)
		{
			OnStageMapClick(this);
		}
	}

    public int GetStageIndex() 
    {
        return transform.Find("StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex;
    }
	protected void OnProfileBtnClick(GameObject pGO)
	{
		ExtendProfileInfo(!_isProfileExtend);
	}


    private void TweenToMove(bool isOrginal) 
    {
        if (isOrginal)
        {
            for (int i = 1; i < Max_Profile; i++)
            {
                string str = "Profile_0" + (i + 1).ToString();

                _usedTransList[str].GetComponent<TweenPosition>().from = _ProfileToVector[i];
                _usedTransList[str].GetComponent<TweenPosition>().to = _ProfileFromVector[i];
            }
        }
        else 
        {
            for (int i = 1; i < Max_Profile; i++)
            {
                string str = "Profile_0" + (i + 1).ToString();

                _usedTransList[str].GetComponent<TweenPosition>().from = _ProfileFromVector[i];
                _usedTransList[str].GetComponent<TweenPosition>().to = _ProfileToVector[i];
            }
        }

        for (int i = 1; i < Max_Profile; i++)
        {
            string str = "Profile_0" + (i + 1).ToString();

            _usedTransList[str].GetComponent<TweenPosition>().duration = 1.0f;//(float)(1.0f - (0.1 * i));
            _usedTransList[str].GetComponent<TweenPosition>().ignoreTimeScale = true;
            _usedTransList[str].GetComponent<TweenPosition>().style = TweenPosition.Style.Once;
            
            _usedTransList[str].GetComponent<TweenPosition>().ResetToBeginning();

            _usedTransList[str].GetComponent<TweenPosition>().enabled = true;
        }
    }
}
