﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GuiPlayFever : GuiBase
{

	public void Pause(bool pPause)
	{
		gameObject.GetComponentInChildren<FeverBGDirCtrl>().Pause(pPause);
	}


    public override void OnCreate()
    {
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
	}

}
