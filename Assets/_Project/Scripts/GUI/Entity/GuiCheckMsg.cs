﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiCheckMsg : GuiBase
{

	public UIEventListener OkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["OkBtn"].gameObject);
		}
	}


	public void SetMsg(string pMsg)
	{
		_usedTransList["Msg"].GetComponent<UILabel>().text = pMsg;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Msg"] = null;
		_usedTransList["OkBtn"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
