﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayerInfo : GuiBase
{

	public UIEventListener PowerBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["PowerBtn"].gameObject);
		}
	}

	public UIEventListener FunnyBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["FunnyBtn"].gameObject);
		}
	}

	public UIEventListener GemBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GemBtn"].gameObject);
		}
	}

    public string GetPowerRecharge()
    {
        return _usedTransList["PowerCount"].GetComponent<UILabel>().text;
    }


	public void SetPower(int pPower)
	{
		int powerIconIndex = pPower > 5 ? 5 : pPower;

		for (int i = 0; i <= 5; ++i)
		{
			_usedTransList[string.Format("Power_{0}", i.ToString("00"))].gameObject.SetActive(i == powerIconIndex ? true : false);
		}

		_usedTransList["PowerBtn"].GetComponentInChildren<UILabel>().text = string.Format("+{0}", pPower);
	}

	public void SetFunny(int pFunny)
	{
		string value = pFunny.ToString();
		Helper.ConvertMoneyType(ref value);

		_usedTransList["FunnyBtn"].GetComponentInChildren<UILabel>().text = value;
	}

	public void SetGem(int pGem)
	{
		string value = pGem.ToString();
		Helper.ConvertMoneyType(ref value);

		_usedTransList["GemBtn"].GetComponentInChildren<UILabel>().text = value;
	}

    public void SetPowerRechargeCount(string _str) 
    {
        _usedTransList["PowerCount"].GetComponent<UILabel>().text = _str;
    }


	public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["PowerBtn"] = null;
		_usedTransList["Power_00"] = null;
		_usedTransList["Power_01"] = null;
		_usedTransList["Power_02"] = null;
		_usedTransList["Power_03"] = null;
		_usedTransList["Power_04"] = null;
		_usedTransList["Power_05"] = null;

		_usedTransList["FunnyBtn"] = null;

        _usedTransList["PowerCount"] = null;

		_usedTransList["GemBtn"] = null;
        FindTrans();
	}

    public override void OnEnter()
    {
        KakaoMain.Instance._bLoding = true;

        //KakaoMain.Instance.CheckUpdateHeart();
    }

    public override void OnLeave()
    {
        KakaoMain.Instance._bLoding = false;
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
