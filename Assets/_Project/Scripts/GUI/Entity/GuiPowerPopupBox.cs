﻿using UnityEngine;
using System.Collections;

public class GuiPowerPopupBox : GuiBase
{
    public UIEventListener PowerCharge
    {
        get { return UIEventListener.Get(_usedTransList["TwoCancel"].gameObject); }
    }
    /*
    public UIEventListener LoadAD
    {
        get { return UIEventListener.Get(_usedTransList["TwoOk"].gameObject); }
    }*/
    public UIEventListener Exit
    {
        get { return UIEventListener.Get(_usedTransList["Exit"].gameObject); }
    }

    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["Count"] = null;
        _usedTransList["Message"] = null;

        _usedTransList["TwoButton"] = null;
        _usedTransList["TwoCancel"] = null;

        _usedTransList["Exit"] = null;

        FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
                
        //        _usedTransList["Message"].GetComponent<UILabel>().text = "Not enough Power!";

        //        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Power Charge";

        //        break;
        //    case SystemLanguage.Portuguese:
                
        //        _usedTransList["Message"].GetComponent<UILabel>().text = "Not enough Power!";

        //        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Power Charge";

        //        break;
        //}


        _usedTransList["Message"].GetComponent<UILabel>().text = "Sem Força suficiente";

        _usedTransList["TwoCancel"].GetComponentInChildren<UILabel>().text = "Carregue sua Força";

    }

    public void SettingMessage(int str) 
    {
        Debug.Log("str :" + str);

        _usedTransList["Count"].GetComponent<UILabel>().text = "오늘 남은 횟수 : " + str;

        //_usedTransList["MessageCount"].GetComponent<UILabel>().text = string.Format("오늘 남은 횟수 : {0}", str);
    }

    public override void OnEnter()
    {
    }


    public override void  OnLeave()
    {
    }
}
