﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargeGemItem : GuiBase
{

	public uint SerialID
	{
		get;
		set;
	}

	public UIEventListener BuyBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
		}
	}


	public void SetItemInfo(uint pSerialID, string pCost, string pGemCount)
	{
		SerialID = pSerialID;

		_usedTransList["Cost"].GetComponent<UILabel>().text = pCost;
		_usedTransList["GemCount"].GetComponent<UILabel>().text = pGemCount;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["GiftBtn"] = null;
		_usedTransList["BuyBtn"] = null;
		_usedTransList["Cost"] = null;
		_usedTransList["GemCount"] = null;

        _usedTransList["Lable"] = null;

		FindTrans();

        _usedTransList["Lable"].GetComponent<UILabel>().text = "Carga de Joias";
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
