﻿using UnityEngine;
using System.Collections;
using System;

public class TimeStamp
{

    public short year_;
    public short month_;
    public short day_;
    public short hour_;
    public short minute_;
    public short second_;
    public int fraction_;

    public void serialize(ByteBuffer payload)
    {
        payload.WriteInt16(year_);
        payload.WriteInt16(month_);
        payload.WriteInt16(day_);
        payload.WriteInt16(hour_);
        payload.WriteInt16(minute_);
        payload.WriteInt16(second_);
        payload.WriteInt32(fraction_);
    }

    public void deserialize(ByteBuffer payload)
    {
        year_ = payload.ReadInt16();
        month_ = payload.ReadInt16();
        day_ = payload.ReadInt16();
        hour_ = payload.ReadInt16();
        minute_ = payload.ReadInt16();
        second_ = payload.ReadInt16();
        fraction_ = payload.ReadInt32();
    }

    public override string ToString()
    {
        string ret = month_.ToString() + "월 " + day_.ToString() + "일 " + hour_.ToString() + "시";
        return ret;
    }

    public string ToString2()
    {
        string ret = string.Format("{0:D4}-{1:D2}-{2:D2} {3:D2}:{4:D2}", year_, month_, day_, hour_, minute_);
        return ret;
    }

    public string formatString()
    {
        return string.Format("{0:####}-{1:##}-{2:##} {3:00}:{4:00}:{5:00}", year_, month_, day_, hour_, minute_, second_);
    }

    public void SetTimeToNow()
    {
        DateTime now = DateTime.Now;
        SetTime(now);
    }

    public void SetTime(DateTime time)
    {
        year_ = (short)time.Year;
        month_ = (short)time.Month;
        day_ = (short)time.Day;
        hour_ = (short)time.Hour;
        minute_ = (short)time.Minute;
        second_ = (short)time.Second;
    }

    public void SetUnixTime(double time)
    {
        SetTime(UnixTimeStampToDateTime(time));
    }

    public int getTimeStampINT()
    {
        DateTime from = DateTime.Parse("1970-01-01 09:00:00");
        DateTime now = DateTime.Parse(formatString());

        TimeSpan delta = now - from;
        return Convert.ToInt32(delta.TotalSeconds);
    }

    public Double getTimeStampDou()
    {
        DateTime from = DateTime.Parse("1970-01-01 09:00:00");
        DateTime now = DateTime.Parse(formatString());

        TimeSpan delta = now - from;
        return delta.TotalSeconds;
    }

    static public int getTimeStampNow()
    {
        DateTime from = DateTime.Parse("1970-01-01 09:00:00");
        DateTime now = DateTime.Now;

        TimeSpan delta = now - from;
        return Convert.ToInt32(delta.TotalSeconds);
    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }

    public void SetTimeToMongoJsonMillisecond(double millisecond)
    {
        DateTime json_time = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(millisecond).ToLocalTime();
        year_ = (short)json_time.Year;
        month_ = (short)json_time.Month;
        day_ = (short)json_time.Day;
        hour_ = (short)json_time.Hour;
        minute_ = (short)json_time.Minute;
        second_ = (short)json_time.Second;
    }

    //public static Int32 operator -(TimeStamp lhs, TimeStamp rhs)
    //{
    //    return lhs.getTimeStamp() - rhs.getTimeStamp();
    //}
}
