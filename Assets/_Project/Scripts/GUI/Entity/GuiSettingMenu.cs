﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiSettingMenu : GuiBase
{

	public UIEventListener StoreBtn
	{
		get { return UIEventListener.Get(_usedTransList["StoreBtn"].gameObject); }
	}

    /*
	public UIEventListener MsgBtn
	{
		get { return UIEventListener.Get(_usedTransList["MsgBtn"].gameObject); }
	}*/

	public UIEventListener SettingBtn
	{
		get { return UIEventListener.Get(_usedTransList["SettingBtn"].gameObject); }
	}

	/*public UIEventListener FriendBtn
	{
		get { return UIEventListener.Get (_usedTransList ["FriendBtn"].gameObject); }
	}*/
	public UIEventListener MykeypetBtn
	{
		get { return UIEventListener.Get (_usedTransList ["MyKeypetBtn"].gameObject); }
	}

	public void SetMsgCount(int pCount)
	{
		if (pCount > 0)
		{
			_usedTransList["MsgCountBG"].gameObject.SetActive(true);
			_usedTransList["MsgCountBG"].GetComponentInChildren<UILabel>().text = pCount.ToString();
		}
		else
		{
			_usedTransList["MsgCountBG"].gameObject.SetActive(false);
		}
	}

	public void ShowRoullette(bool pIsShow)
	{
		_usedTransList["Roullette"].gameObject.SetActive(pIsShow);
	}

	public void ShowNewKeypet(bool pIsShow)
	{
		_usedTransList["NewKeypet"].gameObject.SetActive(pIsShow);
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["StoreBtn"] = null;
		//_usedTransList["MsgBtn"] = null;
		_usedTransList["SettingBtn"] = null;
		//_usedTransList["FriendBtn"] = null;
		_usedTransList["MyKeypetBtn"] = null;
       // _usedTransList["FriendBtn"] = null;

		//_usedTransList["MsgCountBG"] = null;
		_usedTransList["Roullette"] = null;
		_usedTransList["NewKeypet"] = null;
        
		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["StoreBtn"].GetComponent<UISprite>().spriteName = "ButtonShop_e";
        //        _usedTransList["SettingBtn"]    .GetComponent<UISprite>().spriteName = "ButtonOption_e";
        //        _usedTransList["MyKeypetBtn"]   .GetComponent<UISprite>().spriteName = "ButtonKeypet_e";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["StoreBtn"].GetComponent<UISprite>().spriteName = "ButtonShop_e";
        //        _usedTransList["SettingBtn"].GetComponent<UISprite>().spriteName = "ButtonOption_e";
        //        _usedTransList["MyKeypetBtn"].GetComponent<UISprite>().spriteName = "ButtonKeypet_e";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["StoreBtn"].GetComponent<UISprite>().spriteName = "ButtonShop";
        //        _usedTransList["SettingBtn"].GetComponent<UISprite>().spriteName = "setting_idle 1";
        //        _usedTransList["MyKeypetBtn"].GetComponent<UISprite>().spriteName = "ButtonKeypet";
        //        break;
        //}

        _usedTransList["StoreBtn"].GetComponent<UISprite>().spriteName = "ButtonShop_p";
        _usedTransList["SettingBtn"].GetComponent<UISprite>().spriteName = "ButtonOption_p";
        _usedTransList["MyKeypetBtn"].GetComponent<UISprite>().spriteName = "ButtonKeypet_e";
	}

    public override void OnEnter()
    {
        bool KeypetValue = false;

        ShowRoullette(GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteCountCheck());

        if (PlayerPrefs.GetInt("Keypet") == 1)
        {
            KeypetValue = true;
        }
        else 
        {
            KeypetValue = false;
        }

        ShowNewKeypet(KeypetValue);
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

    public int _nHeartMessageCount = 0;

    public void KeypetMessageLoadCount()
    {
        _nHeartMessageCount = 0;

//        #region Unity_Message Check Count
//#if UNITY_EDITOR

//        _nHeartMessageCount += KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().g_new_msg;

//        SetMsgCount(_nHeartMessageCount);
//#endif

//#if UNITY_ANDROID
//        KakaoNativeExtension.Instance.loadGameMessages(this.onLoadGameMessagesComplete, this.onLoadGameMessagesError);
//#endif

//        #endregion

    }

    private void onLoadGameMessagesComplete()
    {
        Debug.LogError("onLoadGameMessagesComplete");
       _nHeartMessageCount = KakaoGameMessages.Instance.gameMessages.Count;

       _nHeartMessageCount += KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().g_new_msg;

       SetMsgCount(_nHeartMessageCount);

       Debug.LogError("_nHeartMessageCount : " + _nHeartMessageCount);
	}

    private void onLoadGameMessagesError(string status, string message)
    {
        Debug.Log("onLoadGameMessagesError");
        Debug.Log("### Error Mesage:" + status + message);
    }
}
