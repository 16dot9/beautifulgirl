﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiItemShopItem : GuiBase
{
	public UIEventListener BuyBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
		}
	}

	public uint SerialID
	{
		get;
		set;
	}


	public void SetItemInfo(uint pSerialID, string pItemName, string pItemDesc, string pBuyInfo, string pIcon)
	{
		SerialID = pSerialID;
         
		_usedTransList["ItemName"].GetComponent<UILabel>().text = pItemName;
		_usedTransList["ItemDesc"].GetComponent<UILabel>().text = pItemDesc;
		_usedTransList["BuyInfo"].GetComponent<UILabel>().text = pBuyInfo;
        _usedTransList["Icon"].GetComponent<UITexture>().mainTexture = (Texture)Resources.Load(pIcon);
    }


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["BuyBtn"] = null;
		_usedTransList["BuyInfo"] = null;
		_usedTransList["ItemName"] = null;
		_usedTransList["ItemDesc"] = null;
        _usedTransList["Icon"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
