﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargePowerItem : GuiBase
{
    protected uint mSerialID = 0;

	public uint SerialID
	{
        get
        {
            return mSerialID;
        }
        set
        {
            mSerialID = value;
        }
	}


	public UIEventListener BuyBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
		}
	}


	public void SetItemInfo(uint pSerialID, string pCostGem, string pPowerCount)
	{
		SerialID = pSerialID;

		_usedTransList["CostGem"].GetComponent<UILabel>().text = pCostGem;
		_usedTransList["PowerCount"].GetComponent<UILabel>().text = pPowerCount;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["BuyBtn"] = null;
		_usedTransList["CostGem"] = null;
		_usedTransList["PowerCount"] = null;
        _usedTransList["Lable"] = null;

		FindTrans();
	    
        //switch(Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["Lable"].GetComponent<UILabel>().text = "Power Charge";
        //    break;
        //    case SystemLanguage.Korean:
        //    _usedTransList["Lable"].GetComponent<UILabel>().text = "파워 충전";
        //    break;
        //    case SystemLanguage.Portuguese:
        //    _usedTransList["Lable"].GetComponent<UILabel>().text = "Carga de força";
        //    break;
        //}
        _usedTransList["Lable"].GetComponent<UILabel>().text = "Carga de Força";
            
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
