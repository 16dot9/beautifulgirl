﻿using UnityEngine;
using System.Collections;

public class GuiScenario : GuiBase {

	public GameObject _skipButton;
	public GameObject _catTalkText;
	public GameObject _dogTalkText;
	public GameObject _catImage;
	public GameObject _dogImage;

	ScenarioGroup _senarioGroup;	// 시나리오 데이터..

	public float _sceneSpeed;		// 장면 전환 시간.. (프리팹에서 조정)..
	float _currentTime = 0.0f;
	int _currentIndex = 0;
	public bool _quitScenario = false;	// 시나리오를 끝내고 넘어 가는지 여부..


	void Awake ()
	{
	}


	void Start ()
	{				
		ShowScene ();	// 시나리오 중 첫 장면을 우선 보여준다..
	}
	

	void Update () 
	{
		_currentTime += Time.deltaTime;

		if (_currentTime >= _sceneSpeed)	// _sceneSpeed 시간마다 다음 장면으로 전환하기..
		{
			ShowScene();	// 현재 장면 보여주기..
		}
	}

	// 현재 선책한 마을의 시나리오 리스트 정보를 받아온다..
	public void SetSenarioGroup(ScenarioGroup senarioGroup)
	{
		_senarioGroup = senarioGroup;
	}


	// 현재 선택한 마을의 시나리오리스트 중에서 현재 보여줘야 하는 장면을 보여준다..
	// _sceneSpeed의 시간이 흐를때마다 또는 화면을 터치하면 호출됨...
	public void ShowScene()
	{
		int count = _senarioGroup.scenarioInfoList.Count;	//현재 마을에서 보여줘야 하는 시나리오 장면의 수량..
		
		if (_currentIndex >= count) 	// 시나리오 장면의 끝에 다다르면 시나리오 종료처리..
		{
			_quitScenario = true;
			return;
		}

		ScenarioInfo scenarioInfo = _senarioGroup.scenarioInfoList[_currentIndex]; //현재 보여줘야 하는 장면에 관한 정보를 찾고..

		if (scenarioInfo.keypetDirect == KeypetDirect.KoNya) 	// 고양이..
		{
			_catImage.GetComponent<UISprite>().spriteName = scenarioInfo.keypetFace;
			_catTalkText.GetComponent<UILabel>().text = scenarioInfo.keypetScript01;
			_dogTalkText.GetComponent<UILabel>().text = scenarioInfo.keypetScript02;
		}
		else 													// 강아지..
		{
			_dogImage.GetComponent<UISprite>().spriteName = scenarioInfo.keypetFace;
			_dogTalkText.GetComponent<UILabel>().text = scenarioInfo.keypetScript01;
			_catTalkText.GetComponent<UILabel>().text = scenarioInfo.keypetScript02;
		}

		++_currentIndex;	// 다음 장면을 보여주기 위해..
		_currentTime = 0.0f;
	}


	// Skip 버튼 클릭시 종료처리...
	public void Click_SkipButton ()
	{
		_quitScenario = true;
	}
}
