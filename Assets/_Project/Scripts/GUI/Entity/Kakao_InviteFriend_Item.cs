﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class Kakao_InviteFriend_Item : GuiBase
{
    public bool _bSupported { get; set; }

    public bool _bMessageblock { get; set; }

    public bool _bLoad
    {
        get;
        set;
    }
	public int SerialID
	{
		get; set;
	}

    public string ProfileImageURL
    {
        get;

        set;
    }
	
	public UIToggle Button
	{
		get { return _usedTransList ["Button"].GetComponent<UIToggle> (); }
	}

	public string ProfilImage
	{
		get { return _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture.name;	}
	}

    public Texture ProfileTextureImage
    {
        get { return _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture; }
        set { _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = ProfileTextureImage; }
    }

	public string NickName
	{
		get { return _usedTransList["NickName"].GetComponent<UILabel>().text; }
	}

    public void SetProfileImage(Texture _Texture) 
    {
        _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = _Texture;
    }

    MyFriend _myKakaoFriend = null;
    public void SetItemInfo(int serialID,bool _bSu , bool _bMe, Texture _Texture, string nickName, MyFriend myKakaoFriend)
	{
        _bSupported = _bSu;
        _bMessageblock = _bMe;

		SerialID = serialID;
        
		_usedTransList["NickName"].GetComponent<UILabel>().text = nickName;
		_usedTransList["Button"].GetComponent<UIToggle>().value = false;

        _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = _Texture;
        _myKakaoFriend = myKakaoFriend;
    }
    
	/// <summary>
	/// 메시지를 보낼지 확인하는 창을 보여주고 OK 하면 메시지 보내기...  
	/// </summary>
	private void ShowMessageBox (MyFriend myKakaoFriend, string msg, KakaoMessage.MessageType messageType)
	{
		UIToggle button = _usedTransList ["Button"].GetComponent<UIToggle> ();
		
		JSONNode msgParam = new JSONClass();

        Debug.Log("_bSupported :" + _bSupported + "_bMessageblock :" + _bMessageblock);

        if (_bSupported)
        {
            if (_bMessageblock)
            {

                msgParam["message"] = "초대메시지 수신을 차단한 유저입니다";
                msgParam["show_type"] = "two";		// "ok", "cancel" two button...

                GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

                guiMessageBox.OnButtonClick = (bool pIsOk) =>
                {
                    if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
                    {
                        Dictionary<string, string> metaInfo = new Dictionary<string, string>();
                        metaInfo.Add(myKakaoFriend.nickname, "A Good Friend");

                        KakaoFriendsController.Instance.SendMessageToMyKakaoFriend(myKakaoFriend, messageType, metaInfo);	// 카카오 친구에게 메시지 보내기...
                        button.value = true;									// 체크 표시...
                        button.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
                    }
                    else
                    {
                        button.value = false;
                    }
                };
            }
            else 
            {

                msgParam["message"] = myKakaoFriend.nickname + msg;
                msgParam["show_type"] = "two";		// "ok", "cancel" two button...

                GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

                guiMessageBox.OnButtonClick = (bool pIsOk) =>
                {
                    if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
                    {
                        Dictionary<string, string> metaInfo = new Dictionary<string, string>();
                        metaInfo.Add(myKakaoFriend.nickname, "A Good Friend");

                        KakaoFriendsController.Instance.SendMessageToMyKakaoFriend(myKakaoFriend, messageType, metaInfo);	// 카카오 친구에게 메시지 보내기...
                        button.value = true;									// 체크 표시...
                        button.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 
                    }
                    else
                    {
                        button.value = false;
                    }
                };

            }
        }
        else 
        {
            msgParam["message"] = myKakaoFriend.nickname + "님은 지원하지 않는 OS를 사용하는 유저입니다.";
            msgParam["show_type"] = "two";		// "ok", "cancel" two button...

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
                {
                }
                else
                {
                    button.value = false;
                }
            };
        }
	}


	/// <summary>
	/// 초대하기 버튼 클릭시 호출...
	/// </summary>
	public void Invite_KakaoNoKeypetFriends()
	{
		// 게임 설치하지 않은 친구들 찾고...
        List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoNoKeypetFriends;

        Debug.Log("myKakaoFriend Count :" + myKakaoFriends.Count); //< PC에서 테스트하기 위해서
        Debug.Log("SerialID :" + SerialID);
        Debug.Log("KakaoMessage.SendMsg.InviteFriend_Msg : " + KakaoMessage.SendMsg.InviteFriend_Msg);
        Debug.Log("KakaoMessage.MessageType.InviteFriend :" + KakaoMessage.MessageType.InviteFriend);

		ShowMessageBox (myKakaoFriends[SerialID], KakaoMessage.SendMsg.InviteFriend_Msg, KakaoMessage.MessageType.InviteFriend);	// "초대 메시지를 보낼까요?" 확인 메시지 박스 출력...
    }
	
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["ProfilImage"] = null;			// 프로필 사진...
		_usedTransList["NickName"] = null;				// 닉네임...
		_usedTransList["Button"] = null;				// 초대 버튼...
		
		FindTrans();
	}
	
	public override void OnEnter()
	{

	}
	
	public override void OnLeave()
	{
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

    void LateUpdate()
    {
        if (_myKakaoFriend != null)
        {
            if (_myKakaoFriend._isUpdateTexture == true)
            {
                try
                {
                    _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = _myKakaoFriend.profileImage;
                    _myKakaoFriend._isUpdateTexture = false;
                }
                catch(Exception e)
                {
                    Debug.Log("e:" + e.ToString());
                }
            }
        }
    }
}
	