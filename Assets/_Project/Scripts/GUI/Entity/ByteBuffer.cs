﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ByteBuffer
{
    private static int MAX_LENGTH = 1024 * 8;
    private int _Position;
    private int _Capacity;
    private int _Start;

    private byte[] _Buffer;


    private ByteBuffer(int capacity, int position, int start, byte[] buffer)
    {
        _Capacity = capacity;
        _Position = position;
        _Start = start;
        _Buffer = buffer;
    }

    public ByteBuffer(int capacity)
        : this(capacity, 0, 0, new byte[capacity])
    {
    }

    public ByteBuffer(byte[] buffer)
        : this(buffer.Length, 0, 0, buffer)
    {
    }

    public ByteBuffer()
        : this(MAX_LENGTH, 0, 0, new byte[MAX_LENGTH])
    {
    }

    public void Flip()
    {
        _Capacity = _Position;
        _Position = _Start;
    }

    public ByteBuffer Slice()
    {
        return new ByteBuffer(_Capacity - _Position, 0, _Position, _Buffer);
    }

    public int Position()
    {
        return _Position;
    }

    public void Position(int pos)
    {
        this._Position = pos;
    }

    public int Limit()
    {
        return _Capacity;
    }

    public void Limit(int lim)
    {
        this._Capacity = lim;
    }

    #region "Read Buffer"
    public bool ReadBool()
    {
        if (_Capacity >= _Position + 1)
        {
            bool temp = _Buffer[_Position] == 1;
            _Position += 1;
            return temp;
        }
        return false;
    }
    public byte ReadByte()
    {
        if (_Capacity >= _Position + 1)
        {
            byte temp = _Buffer[_Position];
            _Position += 1;
            return temp;
        }
        return 0;
    }

    public byte[] ReadBytes(int length)
    {
        if (_Capacity >= _Position + length)
        {
            byte[] temp = new byte[length];
            for (int i = 0; i < length; i++)
            {
                temp[i] = _Buffer[_Position];
                _Position += 1;
            }
            return temp;
        }
        return null;
    }

    public Int16 ReadInt16()
    {
        if (_Capacity >= _Position + 2)
        {
            Int16 temp = BitConverter.ToInt16(_Buffer, _Position);
            _Position += 2;
            return temp;
        }
        return 0;
    }

    public UInt16 ReadUint16()
    {
        if (_Capacity >= _Position + 2)
        {
            UInt16 temp = BitConverter.ToUInt16(_Buffer, _Position);
            _Position += 2;
            return temp;
        }
        return 0;
    }

    public Int32 ReadInt32()
    {
        if (_Capacity >= _Position + 4)
        {
            Int32 temp = BitConverter.ToInt32(_Buffer, _Position);
            _Position += 4;
            return temp;
        }

        Console.WriteLine(String.Format("ReadD return zero {0} {0}", _Capacity, _Position));
        return 0;
    }

    public float ReadFloat()
    {
        if (_Capacity >= _Position + 4)
        {
            float temp = (float)BitConverter.ToSingle(_Buffer, _Position);
            _Position += 4;
            return temp;
        }
        return 0;
    }

    public Int64 ReadInt64()
    {
        if (_Capacity >= _Position + 8)
        {
            Int64 temp = BitConverter.ToInt64(_Buffer, _Position);
            _Position += 8;
            return temp;
        }
        return 0;
    }

    public double ReadDouble()
    {
        if (_Capacity >= _Position + 8)
        {
            double temp = BitConverter.ToDouble(_Buffer, _Position);
            _Position += 8;
            return temp;
        }
        return 0;
    }

    public string ReadString()
    {
        Int32 len = ReadInt32();
        byte[] data = ReadBytes(len);
        return Encoding.Default.GetString(data);
    }

    public string ReadAnsiString()
    {
        string read_str = this.ReadString();

        byte[] nickNameBytes = Convert.FromBase64String(read_str);
        read_str = Encoding.UTF8.GetString(nickNameBytes);

        return read_str;
    }

    #endregion

    #region "Write Buffer"
    public void WriteBool(bool val)
    {
        if (_Capacity >= _Position + 1)
        {
            byte[] bit_bytes = BitConverter.GetBytes(val);
            _Buffer[_Position] = bit_bytes[0];
            _Position += 1;
        }
    }

    public void WriteByte(byte val)
    {
        if (_Capacity >= _Position + 1)
        {
            _Buffer[_Position] = val;
            _Position += 1;
        }
    }

    public void WriteBytes(byte[] val)
    {
        if (_Capacity >= _Position + val.Length)
        {
            for (int i = 0; i < val.Length; i++)
            {
                WriteByte(val[i]);
            }
        }
    }

    public void WriteBytes(byte[] val, int len)
    {
        if (_Capacity >= _Position + len)
        {
            for (int i = 0; i < len; i++)
            {
                WriteByte(val[i]);
            }
        }
    }

    public void WriteInt16(Int16 val)
    {
        if (_Capacity >= _Position + 2)
        {
            byte[] tmp = new byte[2];
            tmp = BitConverter.GetBytes(val);

            _Buffer[_Position] = tmp[0];
            _Buffer[_Position + 1] = tmp[1];

            _Position += 2;
        }
    }

    public void WriteInt16(int index, Int16 val)
    {
        byte[] tmp = new byte[2];
        tmp = BitConverter.GetBytes(val);

        _Buffer[index] = tmp[0];
        _Buffer[index + 1] = tmp[1];
    }

    public void WriteInt32(Int32 val)
    {
        if (_Capacity >= _Position + 4)
        {
            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes(val);

            _Buffer[_Position] = tmp[0];
            _Buffer[_Position + 1] = tmp[1];
            _Buffer[_Position + 2] = tmp[2];
            _Buffer[_Position + 3] = tmp[3];

            _Position += 4;
        }
    }

    public void WriteInt64(Int64 val)
    {
        if (_Capacity >= _Position + 8)
        {
            byte[] tmp = new byte[8];
            tmp = BitConverter.GetBytes(val);

            _Buffer[_Position] = tmp[0];
            _Buffer[_Position + 1] = tmp[1];
            _Buffer[_Position + 2] = tmp[2];
            _Buffer[_Position + 3] = tmp[3];
            _Buffer[_Position + 4] = tmp[4];
            _Buffer[_Position + 5] = tmp[5];
            _Buffer[_Position + 6] = tmp[6];
            _Buffer[_Position + 7] = tmp[7];

            _Position += 8;
        }
    }

    public void WriteFloat(float val)
    {
        if (_Capacity >= _Position + 4)
        {
            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes(val);

            _Buffer[_Position] = tmp[0];
            _Buffer[_Position + 1] = tmp[1];
            _Buffer[_Position + 2] = tmp[2];
            _Buffer[_Position + 3] = tmp[3];

            _Position += 4;
        }
    }

    public void WriteDouble(double val)
    {
        if (_Capacity >= _Position + 8)
        {
            byte[] tmp = new byte[8];
            tmp = BitConverter.GetBytes(val);

            _Buffer[_Position] = tmp[0];
            _Buffer[_Position + 1] = tmp[1];
            _Buffer[_Position + 2] = tmp[2];
            _Buffer[_Position + 3] = tmp[3];
            _Buffer[_Position + 4] = tmp[4];
            _Buffer[_Position + 5] = tmp[5];
            _Buffer[_Position + 6] = tmp[6];
            _Buffer[_Position + 7] = tmp[7];

            _Position += 8;
        }
    }

    public void WriteString(string text)
    {
        if (_Capacity >= _Position + (text.Length * 2) + 2)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);
            WriteInt32(data.Length);
            for (int i = 0; i < data.Length; i++)
            {
                WriteByte(data[i]);
            }
        }
    }

    public void WriteAnsiString(string text)
    {
        byte[] tmpTextBytes = Encoding.UTF8.GetBytes(text);
        string convert_text = Convert.ToBase64String(tmpTextBytes);
        this.WriteString(convert_text);
    }

    #endregion

    public byte[] Array()
    {
        byte[] tmp = new byte[_Capacity];
        for (int i = 0; i < _Capacity; i++)
        {
            tmp[i] = _Buffer[i];
        }
        return tmp;
    }
    public byte[] GetBytes()
    {
        byte[] tmp = new byte[_Position];
        for (int i = 0; i < _Position; i++)
        {
            tmp[i] = _Buffer[i];
        }
        return tmp;
    }

    public byte[] RemainArray()
    {
        byte[] tmp = new byte[_Capacity - _Position];
        for (int i = 0; i < _Capacity - _Position; i++)
        {
            tmp[i] = _Buffer[_Position + i];
        }
        return tmp;
    }

    public int Remaining()
    {
        return _Capacity - _Position;
    }

    public int arrayOffset()
    {
        return 0;
    }
}


