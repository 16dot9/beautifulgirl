﻿using UnityEngine;
using System.Collections;

public class GuiItemCount : MonoBehaviour {
    public GuiItem _refGuiItem;
    public UILabel _lbCount = null;
    

	// Use this for initialization
	void Start () {

        _lbCount = GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void LateUpdate()
    {
        string cnName = "";

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        cnName = " ea";
        //        break;
        //    case SystemLanguage.Korean:
        //        cnName = " 개";
        //        break;
        //    case SystemLanguage.Portuguese:

        //        cnName = " ea";
        //        break;
        //}

        cnName = " ea";

        if (_refGuiItem != null)
        {
            if (_lbCount == null)
            {
                _lbCount = GetComponent<UILabel>();
            }

            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(_refGuiItem.item_index);
            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
            int item_su = GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[(int)getItemIndex];

            int calcValue = item_su - (int)_refGuiItem.GetSelectCount();

            _lbCount.text = "" + calcValue.ToString() + cnName;

            if (_refGuiItem.item_su != (uint)item_su)
            {
                _refGuiItem.item_su = (uint)item_su;
            }
        }
    }
}
