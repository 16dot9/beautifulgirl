﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiMessage : GuiBase
{
	protected Dictionary<uint, GuiMessageItem>		_messageItemList = new Dictionary<uint, GuiMessageItem>();

	public UIEventListener OkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["OkBtn"].gameObject);
		}
	}

	public UIEventListener AllOkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["AllOkBtn"].gameObject);
		}
	}


	public void AddMessageItem(GuiMessageItem pGuiMessageItem)
	{
		pGuiMessageItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiMessageItem.transform.parent = _usedTransList["UIGrid"];
		pGuiMessageItem.transform.localPosition = Vector3.zero;
		pGuiMessageItem.transform.localRotation = Quaternion.identity;
		pGuiMessageItem.transform.localScale = Vector3.one;

		_messageItemList.Add(pGuiMessageItem.SerialID, pGuiMessageItem);

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}

    public void Reposition() 
    {
        _usedTransList["UIGrid"].GetComponent<UIGrid>().onReposition();
    }


    public void Clearitem(uint _Serial, bool _bGameMessage = false) 
    {
        if (_messageItemList != null)
        {
            Debug.Log(_messageItemList.Count);
            
            GuiMessageItem guiMes;

            _messageItemList.TryGetValue(_Serial,out guiMes);

            Debug.Log("guiMes :" + guiMes.gameObject.name);

            DestroyImmediate(guiMes.gameObject);
            
            _messageItemList.Remove(_Serial);

            Reposition();
        }

        if(_bGameMessage)
            ClearItems();

    }

	public void ClearItems()
	{
        if (_messageItemList != null)
        {
            foreach (GuiMessageItem guiMessageItem in _messageItemList.Values) 
            {
                Destroy(guiMessageItem.gameObject);
            }
        }

        _messageItemList.Clear();
    }


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["OkBtn"] = null;
		_usedTransList["AllOkBtn"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
        ClearItems();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
