﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiItemShop : GuiBase
{

	public delegate void DELEGATEGUIUITEMSHOPITEM(GuiItemShopItem pGuiItemShopItem);


	public DELEGATEGUIUITEMSHOPITEM				OnGuiItemShopItem = null;


	protected Dictionary<uint, GuiItemShopItem>	_itemShopItemList = new Dictionary<uint, GuiItemShopItem>();


	public UIEventListener BackBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BackBtn"].gameObject);
		}
	}

	public UIEventListener GachaBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GachaBtn"].gameObject);
		}
	}

	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

	public UIEventListener TakingItemBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["TakingItemBtn"].gameObject);
		}
	}

	public UIEventListener OneMoreBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["OneMoreBtn"].gameObject);
		}
	}

	public UIEventListener ResultBackBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["ResultBackBtn"].gameObject);
		}
	}


	public void AddItemShopItem(GuiItemShopItem pGuiItemShopItem)
	{
		pGuiItemShopItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiItemShopItem.transform.parent = _usedTransList["UIGrid"];
		pGuiItemShopItem.transform.localPosition = Vector3.zero;
		pGuiItemShopItem.transform.localRotation = Quaternion.identity;
		pGuiItemShopItem.transform.localScale = Vector3.one;

		_itemShopItemList.Add(pGuiItemShopItem.SerialID, pGuiItemShopItem);

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}

	public void ClearItemShopItem()
	{
		foreach (GuiItemShopItem guiItemShopItem in _itemShopItemList.Values)
		{
			GameObject.DestroyImmediate(guiItemShopItem.gameObject);
		}

		_itemShopItemList.Clear();
	}


	public void ShowGachaDlg(bool pIsShow)
	{
        if (pIsShow)
        {
            _usedTransList["Dialog_Base"].gameObject.SetActive(false);
        }
        else
        {
            _usedTransList["Dialog_Base"].gameObject.SetActive(true);
        }
		_usedTransList["Dialog_Gacha"].gameObject.SetActive(pIsShow);
	}

    public void ShowResult(bool pIsShow, string pItemInfo, string pOneMoreInfo, string rewardIcon)
	{
		if (pIsShow)
		{
            _usedTransList["Dialog_Base"].gameObject.SetActive(false);
            _usedTransList["Dialog_Gacha"].gameObject.SetActive(false);
			_usedTransList["GetItemInfo"].GetComponent<UILabel>().text = pItemInfo;
			//_usedTransList["OneMoreInfo"].GetComponent<UILabel>().text = pOneMoreInfo;
			_usedTransList["gachaMachine"].gameObject.SetActive(true);

            // 보상 아이템 아이콘 나타내기..
            if (string.IsNullOrEmpty(rewardIcon) == false) // power..
            {
                _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = rewardIcon;
                _usedTransList["MyRewardItem"].GetComponent<UISprite>().MakePixelPerfect();
            }
		}
		else
		{
            _usedTransList["Dialog_Base"].gameObject.SetActive(true);
		}

		_usedTransList["Dialog_Result"].gameObject.SetActive(pIsShow);
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["Dialog_Base"] = null;
		_usedTransList["Dialog_Gacha"] = null;
		_usedTransList["Dialog_Result"] = null;

		_usedTransList["BackBtn"] = null;
		_usedTransList["GachaBtn"] = null;

		_usedTransList["CloseBtn"] = null;
		_usedTransList["gachaMachine"] = null;
		_usedTransList["TakingItemBtn"] = null;

		_usedTransList["GetItemInfo"] = null;
		_usedTransList["OneMoreBtn"] = null;
		_usedTransList["OneMoreInfo"] = null;
		_usedTransList["ResultBackBtn"] = null;

        _usedTransList["MyRewardItem"] = null;

        _usedTransList["ResultBackBtn"]     = null;
        _usedTransList["OneMoreBtn"]        = null;
        _usedTransList["TakingItemBtn"]     = null;
        _usedTransList["BackBtn"]           = null;
        _usedTransList["GachaBtn"]          = null;

        _usedTransList["OneMoreBtn"]        = null;
        _usedTransList["SetLabel1"] = null;
        _usedTransList["SetLabel2"] = null;
        _usedTransList["SetLabel3"] = null;
        _usedTransList["GetItemInfoDesc"] = null;

		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["BackBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["GachaBtn"].FindChild("LineLabel").GetComponent<UILabel>().text = "Gacha";
        //        _usedTransList["OneMoreBtn"].FindChild("Label").GetComponent<UILabel>().text = "One More";
        //        _usedTransList["SetLabel1"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["SetLabel2"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["SetLabel3"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["TakingItemBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["ResultBackBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["GetItemInfoDesc"].GetComponent<UILabel>().text = "Get Item!";
        //        break;
        //    case SystemLanguage.Portuguese:
                
        //        _usedTransList["BackBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["GachaBtn"].FindChild("LineLabel").GetComponent<UILabel>().text = "Gacha";
        //        _usedTransList["OneMoreBtn"].FindChild("Label").GetComponent<UILabel>().text = "One More";
        //        _usedTransList["SetLabel1"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["SetLabel2"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["SetLabel3"].GetComponent<UILabel>().text = "Item Shop";
        //        _usedTransList["TakingItemBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["ResultBackBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["GetItemInfoDesc"].GetComponent<UILabel>().text = "Get Item!";
        //        break;
        //}

        _usedTransList["BackBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["GachaBtn"].Find("LineLabel").GetComponent<UILabel>().text = "Gacha";
        _usedTransList["OneMoreBtn"].Find("Label").GetComponent<UILabel>().text = "Mais Um";
        _usedTransList["SetLabel1"].GetComponent<UILabel>().text = "Loja de itens";
        _usedTransList["SetLabel2"].GetComponent<UILabel>().text = "Loja de itens";
        _usedTransList["SetLabel3"].GetComponent<UILabel>().text = "Loja de itens";
        _usedTransList["TakingItemBtn"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["ResultBackBtn"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["GetItemInfoDesc"].GetComponent<UILabel>().text = "Obter o item!";
               
	}

    public override void OnEnter()
    {
		_usedTransList["Dialog_Gacha"].gameObject.SetActive(false);
		_usedTransList["Dialog_Result"].gameObject.SetActive(false);
        _usedTransList["Dialog_Base"].gameObject.SetActive(true);
	}

    public override void OnLeave()
    {
		ClearItemShopItem();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
