﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiProcessing : GuiBase
{

    public override void OnCreate()
    {
        _usedTransList["Label"] = null;

        FindTrans();

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["Label"].GetComponent<UILabel>().text = "Processing...";
                
        //        break;
        //    case SystemLanguage.English:
        //        _usedTransList["Label"].GetComponent<UILabel>().text = "Processing...";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["Label"].GetComponent<UILabel>().text = "처리중...";
        //        break;
        //}

        _usedTransList["Label"].GetComponent<UILabel>().text = "Carregando...";
    }

    public override void OnEnter()
    {
	}

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
    }

}
