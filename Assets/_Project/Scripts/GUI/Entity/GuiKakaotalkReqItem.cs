﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiKakaotalkReqItem : GuiBase
{

	public uint SerialID
	{
		get;
		set;
	}

	public string GetValue()
	{
		return _usedTransList ["Selected"].GetComponent<UIToggle> ().value.ToString ();
	}
	public string GetTextureName()
	{
		return _usedTransList["Face"].GetComponent<UITexture>().mainTexture.name;
	}
	public string GetName()
	{
		return _usedTransList["Name"].GetComponent<UILabel>().text;
	}


	public void SetItemInfo(uint pSerialID, Texture2D pFaceImage, string pName)
	{
		SerialID = pSerialID;

		_usedTransList["Face"].GetComponent<UITexture>().mainTexture = pFaceImage;
		_usedTransList["Name"].GetComponent<UILabel>().text = pName;
		_usedTransList["Selected"].GetComponent<UIToggle>().value = false;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Face"] = null;
		_usedTransList["Name"] = null;
		_usedTransList["Selected"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
