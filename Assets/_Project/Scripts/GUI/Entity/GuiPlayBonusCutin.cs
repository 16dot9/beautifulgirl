﻿using UnityEngine;
using System.Collections;

public class GuiPlayBonusCutin : GuiBase {


    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["BonusTime"] = null;

        FindTrans();
    }

    public override void OnEnter()
    {
        _usedTransList["BonusTime"].gameObject.SetActive(false);

        StartCoroutine(ShowBonusCutin());
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }


    protected IEnumerator ShowBonusCutin()
    {
        GameObject go = _usedTransList["BonusTime"].gameObject;

        go.SetActive(true);

        TweenPosition tweenPos = TweenPosition.Begin(go, 0.7f, Vector3.zero);
        tweenPos.from = new Vector3(10000, 0.0f, 0.0f);
        tweenPos.method = UITweener.Method.EaseIn;

        yield return new WaitForSeconds(1.2f);

        tweenPos = TweenPosition.Begin(go, 0.5f, new Vector3(-10000, 0.0f, 0.0f));
        tweenPos.from = Vector3.zero;
        tweenPos.method = UITweener.Method.EaseOut;

        yield return new WaitForSeconds(0.6f);

        go.SetActive(false);
    }
}
