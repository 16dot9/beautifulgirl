﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class GuiItemBuyPopupOK : GuiBase
{

	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Dialog_Base"] = null;

		_usedTransList["CloseBtn"] = null;

        _usedTransList["MyRewardItem"] = null; // GetItemInfo, GetItemInfoDesc
        _usedTransList["GetItemInfo"] = null; // GetItemInfo, GetItemInfoDesc
        _usedTransList["GetItemInfoDesc"] = null; // GetItemInfo, GetItemInfoDesc
        _usedTransList["SetLabel"] = null;
		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.Korean:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "아이템 구매";
        //        break;
        //    case SystemLanguage.English:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Buy Item";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Buy Item";
        //        break;
        //}

        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Buy Item";
             
	}

    public override void OnEnter()
    {
        //string strIconName = _params["icon_name"];
        //string strPackName = _params["icon_name"];
        //string strIconName = _params["icon_name"];
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

    public IEnumerator ShowPack(PackageInfo pack_info)
    {
        yield return new WaitForFixedUpdate();
        if (pack_info != null)
        {
            _usedTransList["GetItemInfo"].GetComponent<UILabel>().text = pack_info.name;
            _usedTransList["GetItemInfoDesc"].GetComponent<UILabel>().text = pack_info.desc;
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = pack_info.icon_image;
        }
    }
}
