﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayMiddle : GuiBase
{

	protected static int			_puzzleItemOriginalSize = 136;

	protected int					_puzzleWidth = 0;
	protected int					_puzzleHeight = 0;

	protected float					_puzzleItemScale = 1.0f;

    public List<GameObject>         _puzzleItemBGList = new List<GameObject>();
    public List<GameObject>		    _puzzleItemList = new List<GameObject>();
    public List<GameObject>         _puzzleitemActive = new List<GameObject>();

    public Transform GetMiddleBG() 
    {
        return _usedTransList["BG"].transform;
    }

	public Vector3 GetTopRight()
	{
		return _usedTransList["TopRight"].position;
	}

	public Vector3 GetBottomLeft()
	{
		return _usedTransList["BottomLeft"].position;
	}

    public int GetPuzzleActiveCount()
    {
        return _puzzleitemActive.Count;
    }

	public int SlotToIndex(int x, int y)
	{
		int index = x + y * _puzzleWidth;

		if (index < 0 || index >= _puzzleItemList.Count)
		{
			return -1;
		}

		return index;
	}

	public int GetPuzzleItemBGCount()
	{
		return _puzzleItemBGList.Count;
	}

    public IEnumerable GetPuzzleItemBG()
    {
        foreach (GameObject puzzleItemBG in _puzzleItemBGList)
        {
            yield return puzzleItemBG;
        }
    }

    public GameObject GetPuzzleItemBG(int pIndex)
    {
        if (pIndex < 0 || pIndex >= _puzzleItemBGList.Count)
        {
            return null;
        }

        return _puzzleItemBGList[pIndex];
    }

	public int GetPuzzeleItemCount()
	{
		return _puzzleItemList.Count;
	}

	public IEnumerable GetPuzzleItem()
	{
		foreach (GameObject puzzleItem in _puzzleItemList)
		{
			yield return puzzleItem;
		}
	}

	public GameObject GetPuzzleItem(int pIndex)
	{
		if (pIndex < 0 || pIndex >= _puzzleItemList.Count)
		{
			return null;
		}

		return _puzzleItemList[pIndex];
	}

    public IEnumerable GetPuzzleReball() 
    {
        Transform transGameItem = _usedTransList["GameItems"].transform;

        int cnt = transGameItem.childCount;

        List<GameObject> isKeyPet = new List<GameObject>();

        isKeyPet.Clear();

        for (int i = 0; i < cnt; i++) 
        {
            KeypetCtrl keypetcur = transGameItem.GetChild(i).GetComponentInChildren<KeypetCtrl>();

            if (keypetcur != null) 
            {
                if (keypetcur.IsBlank) 
                {
                    continue;
                }

                yield return keypetcur.gameObject;
            }
        }
    }
    
	public void Setting(int pPuzzleWidth, int pPuzzleHeight, KeypetResInfo[] pKeyPetList)
	{
        if (_puzzleitemActive.Count > 0)
        {
            _puzzleitemActive.Clear();
        }

        if (_usedTransList["Hand"] != null)
        {
            _usedTransList["Hand"].gameObject.SetActive(false);
        }
        else 
        {
            Debug.Log("왜 널일까...");
        }

		_puzzleWidth = pPuzzleWidth;
		_puzzleHeight = pPuzzleHeight;

		Transform topRight = _usedTransList["TopRight"];
		Transform bottomLeft = _usedTransList["BottomLeft"];

		Vector3 centerPos = (topRight.localPosition + bottomLeft.localPosition) * 0.5f;
		centerPos.z = topRight.localPosition.z;

		int splitWidth = (int)(topRight.localPosition.x - bottomLeft.localPosition.x + 0.5f) / _puzzleWidth;
		int splitHeight = (int)(topRight.localPosition.y - bottomLeft.localPosition.y + 0.5f) / _puzzleHeight;

		int puzzleSize = Mathf.Min(splitWidth, splitHeight);
		int puzzleHalfSize = puzzleSize / 2;

		int puzzleWidthHalfTotalSize = puzzleSize * _puzzleWidth / 2;
		int puzzleHeightHalfTotalSize = puzzleSize * _puzzleHeight / 2;

		_puzzleItemScale = (float)puzzleSize / (float)_puzzleItemOriginalSize;

        Transform itemParent = _usedTransList["GameItems"];

		int index = 0;

		for (int y = puzzleHeightHalfTotalSize; y > -puzzleHeightHalfTotalSize; y -= puzzleSize)
		{
			for (int x = -puzzleWidthHalfTotalSize; x < puzzleWidthHalfTotalSize; x += puzzleSize)
			{
				GameObject itemBG = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiPlayMiddlePuzzleItemBG"));
				itemBG.name = string.Format("PuzzleItemBG_{0}", index.ToString("000"));
				itemBG.transform.parent = itemParent;
				itemBG.transform.localPosition = centerPos + new Vector3(x, y, 0.0f) + new Vector3(puzzleHalfSize, -puzzleHalfSize);
				itemBG.transform.localRotation = Quaternion.identity;
				itemBG.transform.localScale = new Vector3(_puzzleItemScale, _puzzleItemScale, 1.0f);
				_puzzleItemBGList.Add(itemBG);

				GameObject item = (GameObject)GameObject.Instantiate(Resources.Load<GameObject>("Gui/GuiPlayMiddlePuzzleItem"));
				item.name = string.Format("PuzzleItem_{0}", index.ToString("000"));
				item.transform.parent = itemParent;
				item.transform.localPosition = centerPos + new Vector3(x, y, 0.0f) + new Vector3(puzzleHalfSize, -puzzleHalfSize);
				item.transform.localRotation = Quaternion.identity;
				item.transform.localScale = new Vector3(_puzzleItemScale, _puzzleItemScale, 1.0f);

				GameObject keypet = (GameObject)GameObject.Instantiate(pKeyPetList[index].KeypetResObj);
				Vector3 curScale = keypet.transform.localScale;
				keypet.name = "Keypet";
				keypet.transform.parent = item.transform;
				keypet.transform.localPosition = Vector3.zero;
				keypet.transform.localRotation = Quaternion.identity;
				keypet.transform.localScale = curScale;
				KeypetCtrl keypetCtrl = keypet.GetComponent<KeypetCtrl>();
				keypetCtrl.CurSlotIndex = index;
				keypetCtrl.CurKeypetType = pKeyPetList[index].KeypetResType;
				keypetCtrl.IsBlank = pKeyPetList[index].KeypetResType == KeypetType.blank ? true : false;
				
                _puzzleItemList.Add(item);

                if (pKeyPetList[index].KeypetResType != KeypetType.blank)
                {
                    _puzzleitemActive.Add(item);
                }

				++index;
			}
		}
	}

    public void TutorialActive(bool IsActive) 
    {
        if (_usedTransList["Hand"] != null)
        {
            if (_usedTransList["Hand"].gameObject.activeSelf == true)
                _usedTransList["Hand"].gameObject.SetActive(IsActive);
        }
    }

    public void SetHandPosition(Transform _Trans) 
    {
        _usedTransList["Hand"].transform.parent = _Trans;

        _usedTransList["Hand"].transform.localPosition = new Vector3(0.0f, -60.0f, 0.0f);
    }

    public void SetHandPosition(Vector3 _Pos) 
    {
        if (_usedTransList["Hand"] != null)
        {
            if (_usedTransList["Hand"].gameObject.activeSelf == false)
                _usedTransList["Hand"].gameObject.SetActive(true);

            _Pos = new Vector3(_Pos.x, _Pos.y - 60.0f, _Pos.z);

            _usedTransList["Hand"].transform.localPosition = _Pos;
        }
        else
        {
            Debug.Log("왜 널일까...");
        }
        
        //< 아이트윈
    }
    public void SetTutorialText(string _str)
    {
        _usedTransList["Label"].GetComponent<UILabel>().text = _str;
    //    iTween.ShakePosition(_usedTransList["Hand"].gameObject, iTween.Hash("y", 0.01f));
    //    iTween.ShakeScale(_usedTransList["Hand"].gameObject, iTween.Hash("y", 0.01f));
    }

    public void SetHandLoopCancle() 
    {
        iTween.ShakePosition(_usedTransList["Hand"].gameObject, iTween.Hash("y", 0.05f, "looptype", iTween.LoopType.none));
    }

    //< 두개를 요기서 관리한다...
    public void SetTalkBoxPostion() 
    {
        _usedTransList["TalkBox"].transform.localPosition = new Vector3(1.5f,
            _usedTransList["TalkBox"].transform.localPosition.y, _usedTransList["TalkBox"].transform.localPosition.z);
    }
    public void SetTalkBoxPostion2()
    {
        _usedTransList["TalkBox"].transform.localPosition = new Vector3(-2.6f,
            _usedTransList["TalkBox"].transform.localPosition.y, _usedTransList["TalkBox"].transform.localPosition.z);
    }
    public void SetTalkBoxPostion3()
    {
        _usedTransList["TalkBox"].transform.localPosition = new Vector3(3.0f,
            _usedTransList["TalkBox"].transform.localPosition.y, _usedTransList["TalkBox"].transform.localPosition.z);
    }

    public void SetTalkBoxZeroX()
    {
        _usedTransList["TalkBox"].transform.localPosition = new Vector3(0.0f,
            _usedTransList["TalkBox"].transform.localPosition.y, _usedTransList["TalkBox"].transform.localPosition.z);
    }

	public void ClearSetting()
	{
		_puzzleWidth = 0;
		_puzzleHeight = 0;

		foreach (GameObject go in _puzzleItemList)
		{
			GameObject.DestroyImmediate(go);
		}
		_puzzleItemList.Clear();

		foreach (GameObject go in _puzzleItemBGList)
		{
			GameObject.DestroyImmediate(go);
		}
		_puzzleItemBGList.Clear();
	}


	public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["BG"] = null;
		_usedTransList["TopRight"] = null;
		_usedTransList["BottomLeft"] = null;
        _usedTransList["GameItems"] = null;
        _usedTransList["Hand"] = null;
        _usedTransList["Label"] = null;
        _usedTransList["TalkBox"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearSetting();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
