﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiKakaotalkReq : GuiBase
{

	public Dictionary<uint, GuiKakaotalkReqItem>		_kakaotalkReqItemList = new Dictionary<uint, GuiKakaotalkReqItem>();


	public UIEventListener BackBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BackBtn"].gameObject);
		}
	}

	public UIEventListener GiftBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["GiftBtn"].gameObject);
		}
	}

	public UIEventListener PesterBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["PesterBtn"].gameObject);
		}
	}

	public void ShowGiftWindow()
	{
		_usedTransList["GiftWindow"].gameObject.SetActive(true);
		_usedTransList["PesterWindow"].gameObject.SetActive(false);
		_usedTransList["PowerReqWindow"].gameObject.SetActive(false);
	}

	public void ShowPesterWindow()
	{
		_usedTransList["GiftWindow"].gameObject.SetActive(false);
		_usedTransList["PesterWindow"].gameObject.SetActive(true);
		_usedTransList["PowerReqWindow"].gameObject.SetActive(false);
	}

	public void ShowPowerReqWindow()
	{
		_usedTransList["GiftWindow"].gameObject.SetActive(false);
		_usedTransList["PesterWindow"].gameObject.SetActive(false);
		_usedTransList["PowerReqWindow"].gameObject.SetActive(true);
	}


	public void AddKakaotalkReqItem(GuiKakaotalkReqItem pGuiKakaotalkReqItem)
	{
		pGuiKakaotalkReqItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiKakaotalkReqItem.transform.parent = _usedTransList["UIGrid"];
		pGuiKakaotalkReqItem.transform.localPosition = Vector3.zero;
		pGuiKakaotalkReqItem.transform.localRotation = Quaternion.identity;
		pGuiKakaotalkReqItem.transform.localScale = Vector3.one;

		_kakaotalkReqItemList.Add(pGuiKakaotalkReqItem.SerialID, pGuiKakaotalkReqItem);

		//_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}


	/// <summary>
	/// 모든 AddKakaotalkReqItem() 호출이 끝난 후에 반드시 호출할 것...
	/// </summary>
	public void ScrollView_RepositionNow()
	{
		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
		//_usedTransList ["UIGrid"].GetComponent<UIGrid> ().Reposition ();
	}


	// 테스트용....
	public int GetGridChild()
	{
		return _usedTransList["UIGrid"].childCount;
	}

	/// <summary>
	/// 초대/조르기한 카카오톡 친구 수를 표시함...
	/// </summary>
	public void ShowInvitedFriendCount (string text)
	{
		_usedTransList ["InvitedNumber"].GetComponent<UILabel> ().text = text;
	}

	public void ResetPosition()
	{
		_usedTransList ["Items"].GetComponent<UIScrollView> ().ResetPosition ();
	}


	public void ClearItems()
	{
		foreach (GuiKakaotalkReqItem guiKakaotalkReqItem in _kakaotalkReqItemList.Values)
		{
			GameObject.DestroyImmediate(guiKakaotalkReqItem.gameObject);
			//GameObject.Destroy(guiKakaotalkReqItem.gameObject);
		}

		_kakaotalkReqItemList.Clear();

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;	// 혹시나 하는 마음으로...
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["BackBtn"] = null;

		_usedTransList["GiftWindow"] = null;
		_usedTransList["GiftBtn"] = null;

		_usedTransList["PesterWindow"] = null;
		_usedTransList["PesterBtn"] = null;

		_usedTransList["InvitedNumber"] = null;	// 초대한 친구 수 표시...

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearItems();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
