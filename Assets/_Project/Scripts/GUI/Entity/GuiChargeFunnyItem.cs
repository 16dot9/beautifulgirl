﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargeFunnyItem : GuiBase
{

	public uint SerialID
	{
		get;
		set;
	}


	public UIEventListener BuyBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
		}
	}


	public void SetItemInfo(uint pSerialID, string pCostGem, string pFunnyCount)
	{
		SerialID = pSerialID;

		_usedTransList["CostGem"].GetComponent<UILabel>().text = pCostGem;
		_usedTransList["FunnyCount"].GetComponent<UILabel>().text = pFunnyCount;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["BuyBtn"] = null;
		_usedTransList["CostGem"] = null;
		_usedTransList["FunnyCount"] = null;
        _usedTransList["Lable"] = null;

		FindTrans();
	}

    public void SetLabel(string str)
    {
        _usedTransList["Lable"].GetComponent<UILabel>().text = str;

    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
