﻿using UnityEngine;
using System.Collections;

public class GuiLoginAgreement : GuiBase 
{
    public Color _buttonColor;	// 확인 버튼 색...

    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["InformationToggle"] = null;	// 정보이용 동의 토글 버튼...
        _usedTransList["ServiceToggle"] = null;		// 서비스이용 동의 토글 버튼...
        _usedTransList["OkButton"] = null;			// 확인 버튼...

        FindTrans();
    }


    public override void OnEnter()
    {
        // 체크표시 안보이게...
        _usedTransList["InformationToggle"].GetComponent<UIToggle>().value = false;
        _usedTransList["ServiceToggle"].GetComponent<UIToggle>().value = false;

        // 확인버튼 클릭되지 않도록... 
        _usedTransList["OkButton"].GetComponent<BoxCollider>().enabled = false;
    }


    public override void OnLeave()
    {
    }


    public override void OnDelete()
    {
        _usedTransList.Clear();
    }

    void Update()
    {
        //bool checkInformation = _usedTransList["InformationToggle"].GetComponent<UIToggle>().value;
        bool checkService = _usedTransList["ServiceToggle"].GetComponent<UIToggle>().value;

        if (checkService)	// checkInformation && 약관에 모두 동의하면 "확인" 버튼 활성화 표시...
        {
            // 확인버튼 클릭할 수 있게... 
            _usedTransList["OkButton"].GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            // 확인버튼 클릭되지 않도록... 
            _usedTransList["OkButton"].GetComponent<BoxCollider>().enabled = false;
        }
    }

    /// <summary>
    /// "확인" 버튼 누르면 카카오 로그인 화면으로 전환...
    /// </summary>
    public void GoLogin()
    {
        GameStateMgr.Instance.ChangeState<GSLogin>(0, null);
    }

}
