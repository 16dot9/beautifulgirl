﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiStageMapInfoScrollViewMapItem : GuiBase
{

	public void SetSpriteName(string pSpriteName)
	{
		gameObject.GetComponent<UISprite>().spriteName = pSpriteName;
	}

	public void ShowCloud(bool pIsShow)
	{
		_usedTransList["Cloud"].gameObject.SetActive(pIsShow);
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Cloud"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
