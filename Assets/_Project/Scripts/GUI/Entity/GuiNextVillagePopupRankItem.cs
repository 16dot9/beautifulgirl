﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiNextVillagePopupRankItem : GuiBase
{

	public UIEventListener GetRequireFriendBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RequireFriend"].gameObject);
		}
	}


	public void SetFriendImage(Texture2D pImage)
	{
		_usedTransList["FriendImage"].GetComponent<UITexture>().mainTexture = pImage;
	}

	public void SetFriendName(string pName)
	{
		_usedTransList["Name"].GetComponent<UILabel>().text = pName;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["FriendImage"] = null;
		_usedTransList["Name"] = null;
		_usedTransList["RequireFriend"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
