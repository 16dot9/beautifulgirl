﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class Kakao_BoastFriend_Item : GuiBase 
{
    public float _fTime = 0.0f;

    public bool _bFail = false;

    public string UserID
    {
        get;
        set;
    }

    public string ProfileImageURL
    {
        get;
        set;
    }

	public int SerialID
	{
		get; set;
	}
	
	public UIToggle Button
	{
		get { return _usedTransList ["Button"].GetComponent<UIToggle> (); }
	}
	
	public string ProfilImage
	{
		get { return _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture.name;	}
	}
	
	public string NickName
	{
		get { return _usedTransList["NickName"].GetComponent<UILabel>().text; }
	}


    public void SetItemInfo(int serialID, Texture2D _profileImage ,string userid, string nickName, float _fTimeCheckMessage)
	{
		SerialID = serialID;
		
		_usedTransList["NickName"].GetComponent<UILabel>().text = nickName;
		_usedTransList["Button"].GetComponent<UIToggle>().value = false;

        _fTime = _fTimeCheckMessage;

        UserID = userid;

        _usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = _profileImage;
	}
	
    //_usedTransList["ProfilImage"].GetComponent<UITexture>().mainTexture = profilImage;
	
	/// <summary>
	/// 메시지를 보낼지 확인하는 창을 보여주고 OK 하면 메시지 보내기...  
	/// </summary>
	private void ShowMessageBox (MyFriend myKakaoFriend, string msg, KakaoMessage.MessageType messageType, Dictionary<string,string> metaInfo)
	{
		UIToggle button = _usedTransList ["Button"].GetComponent<UIToggle> ();
		
		JSONNode msgParam = new JSONClass();

        msgParam["message"] = myKakaoFriend.nickname + msg;
        msgParam["show_type"] = "two";		// "ok", "cancel" two button...

        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

        guiMessageBox.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
            {
                KakaoFriendsController.Instance.SendMessageToMyKakaoFriend(myKakaoFriend, messageType, metaInfo);	// 카카오 친구에게 메시지 보내기...
                button.value = true;									// 체크 표시...
                button.GetComponent<BoxCollider>().enabled = false;		// 버튼이 클릭되지 않도록... 

            }
            else
            {
                button.value = false;
            }
        };
	}

	/// <summary>
	/// 요청 버튼 클릭시 호출...
	/// </summary>
	public void Request_KakaoKeypetFriends()
	{
		double score = GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().nowScore;		// 점수...
		int stage = GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageNumber;	// 스테이지 번호...

		// 전달할 메시지의 파라미터...
		Dictionary<string,string> metaInfo = new Dictionary<string, string>();
        metaInfo.Add("sender_nick", KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._nickname);
        metaInfo.Add("stage", KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._sStage);
        metaInfo.Add("score", string.Format("{0:0,0}", KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._nScoreCurrent));

        //metaInfo.Add("stage", stage.ToString() + " stage");
        //metaInfo.Add("score", string.Format ("{0:0,0}", score));

        // 게임 설치한 친구들 찾고...
        List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;
        try
        {
            if (myKakaoFriends[SerialID].userid != KakaoLocalUserController.Instance.GetUserAccount())
            {
                if (_bFail)
                {
                    ShowMessageBox(myKakaoFriends[SerialID], KakaoMessage.SendMsg.SendFriend_Power_Msg, KakaoMessage.MessageType.SendFriend_Power, metaInfo);
                }
                else
                {
                    if (_fTime == 0.0f)
                    {
                        ShowMessageBox(myKakaoFriends[SerialID], KakaoMessage.SendMsg.BoastFriend_Msg, KakaoMessage.MessageType.BoastFriend, metaInfo);	// "요청 메시지를 보낼까요?" 확인창 출력...
                    }
                    else
                    {
                        UIToggle button = _usedTransList["Button"].GetComponent<UIToggle>();
                        JSONNode msgParam = new JSONClass();
       
                        msgParam["message"] = "1분 이내에 다시 자랑 하실수 없어요.";
                        msgParam["show_type"] = "two";		// "ok", "cancel" two button...

                        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

                        guiMessageBox.OnButtonClick = (bool pIsOk) =>
                        {
                            if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
                            {
                            }
                            else
                            {
                                button.value = false;
                            }
                        };
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Boast : " + e.ToString());
        }
    }
	
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["ProfilImage"] = null;			// 프로필 사진...
		_usedTransList["NickName"] = null;				// 닉네임...
		_usedTransList["Button"] = null;				// 요청 버튼...
		
		FindTrans();
	}
	
	public override void OnEnter()
	{
	}
	
	public override void OnLeave()
	{
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

}
