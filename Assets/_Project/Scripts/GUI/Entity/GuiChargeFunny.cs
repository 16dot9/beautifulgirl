﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargeFunny : GuiBase
{

	protected Dictionary<uint, GuiChargeFunnyItem>	_chargeFunnyItemList = new Dictionary<uint, GuiChargeFunnyItem>();


	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

    public bool isFunnyItem(uint serid) 
    {
        if (_chargeFunnyItemList.ContainsKey(serid)) 
        {
            return true;
        }

        return false;
    }

	public void AddChargeFunnyItem(GuiChargeFunnyItem pGuiChargeFunnyItem)
    {
		pGuiChargeFunnyItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiChargeFunnyItem.transform.parent = _usedTransList["UIGrid"];
		pGuiChargeFunnyItem.transform.localPosition = Vector3.zero;
		pGuiChargeFunnyItem.transform.localRotation = Quaternion.identity;
		pGuiChargeFunnyItem.transform.localScale = Vector3.one;

        //if (_chargeFunnyItemList.ContainsKey(pGuiChargeFunnyItem.SerialID) != true)
		
        _chargeFunnyItemList.Add(pGuiChargeFunnyItem.SerialID, pGuiChargeFunnyItem);

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}

	public void ClearItems()
	{
        foreach (GuiChargeFunnyItem guiChargeFunnyItem in _chargeFunnyItemList.Values)
        {
            GameObject.DestroyImmediate(guiChargeFunnyItem.gameObject);
        }

		_chargeFunnyItemList.Clear();
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["CloseBtn"] = null;
        _usedTransList["SetLabel"] = null;

        FindTrans();

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Funny Shop";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "퍼니 충전";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "확인";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de Funny";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //}

        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de Funny";
        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
               
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearItems();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
