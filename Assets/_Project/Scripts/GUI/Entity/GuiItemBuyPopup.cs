﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

// 보류
public class GuiItemBuyPopup : GuiBase
{

    public UIEventListener BuyBtn
	{
		get
		{
            return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
		}
	}

	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Dialog_Base"] = null;

		_usedTransList["BuyBtn"] = null;
		_usedTransList["CloseBtn"] = null;

        _usedTransList["MyRewardItem"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
        //string strIconName = _params["icon_name"];
        //string strPackName = _params["icon_name"];
        //string strIconName = _params["icon_name"];
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
