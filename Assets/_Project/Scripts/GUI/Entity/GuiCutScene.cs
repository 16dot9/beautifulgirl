﻿using UnityEngine;
using System.Collections;

public class GuiCutScene : MonoBehaviour {
	
	public GameObject _textBox;
	public GameObject _nextButton;
	public GameObject _skipButton;
	
	int _currentIndex = 0;
	public bool _quitCutScene = false;	// 튜토리얼을 끝내고 넘어 가는지 여부..
	
	
	void Awake ()
	{
		foreach (Transform child in _textBox.transform) 
		{
			child.gameObject.SetActive(false);
		}
		
		// 처음 텍스트박스만 활성화 시키고...
		_textBox.transform.Find ("TextBox_00").gameObject.SetActive (true);		
	}
	
	
	void Start () 
	{

	}
	
	
	void Update () 
	{
		// 다른 곳에서 파괴해야 할지도...
		if (_quitCutScene)
			Destroy (gameObject);
	}
	
	
	// Next 버튼 클릭시 호출...
	public void Click_NextButton ()
	{
		// 지금 보여지는 텍스트박스는 비활성화 시키고...
		string textBoxName = string.Format ("TextBox_{0}", _currentIndex.ToString ("00"));
		Transform child = _textBox.transform.Find (textBoxName);		
		child.gameObject.SetActive (false);
		
		// 마지막까지 다 읽었으면 튜토리얼 끝..
		if (_currentIndex == _textBox.transform.childCount - 1) 
		{
			_quitCutScene = true;
			return;
		}
		
		// 다음 텍스트박스를 활성화 시킴..
		_currentIndex++;
		textBoxName = string.Format ("TextBox_{0}", _currentIndex.ToString ("00"));
		child = _textBox.transform.Find (textBoxName);
		child.gameObject.SetActive (true);
	}
	
	
	// Skip 버튼 클릭시 호출...
	public void Click_SkipButton ()
	{
		_quitCutScene = true;
	}
	
}
