﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class GuiStageRankingItem : GuiBase
{
    public string UserID;

    public string NickName;

    public int Score { get; set; }

    public void SetUserId(string _userid) 
    {
        this.UserID = _userid;
    }

    public void SetFriendImage(string pImage)
    {
        StartCoroutine(FriendProfileImageLoad(pImage));
    }
    IEnumerator FriendProfileImageLoad(string url)
    {
        WWW www = new WWW(url);

        yield return www;

        if (!String.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
        }
        else
        {
            _usedTransList["FriendImage"].GetComponent<UITexture>().mainTexture = www.texture;
        }
    }

    public void SetFriendName(string pName)
    {
        Debug.Log("nickName :" + pName);

        _usedTransList["FriendName"].GetComponent<UILabel>().text = pName;
        NickName = pName;
    }


    public void SetFriendSocre(int pSocre)
    {
        Score = pSocre;
        _usedTransList["FriendScore"].GetComponent<UILabel>().text = pSocre.ToString();
    }


    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["FriendImage"] = null;
        _usedTransList["FriendName"] = null;
        _usedTransList["FriendScore"] = null;

        _usedTransList["PowerBtn"] = null;

        FindTrans();
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }

}
