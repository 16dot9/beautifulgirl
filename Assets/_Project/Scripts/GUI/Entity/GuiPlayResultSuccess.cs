﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayResultSuccess : GuiBase
{

	public delegate void DELEGATEVOID();


	public DELEGATEVOID				OnDirectionFinished;


	public UIEventListener RewardBtn_01
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RewardBtn_01"].gameObject);
		}
	}

	public UIEventListener RewardBtn_02
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RewardBtn_02"].gameObject);
		}
	}

	public UIEventListener RewardBtn_03
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RewardBtn_03"].gameObject);
		}
	}

	public UIEventListener OkBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["OkBtn"].gameObject);
		}
	}

	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

    public UIEventListener BoastBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["BoastBtn"].gameObject);
        }
    }

    public void ActiveNormal()
    {
        ShowBestRecord(false);
    }
    public void ActiveBest()
    {
        ShowBestRecord(true);

    }

	public void SetStage(int pStage)
	{
		_usedTransList["Stage"].GetComponent<UILabel>().text = string.Format("Fase {0}", pStage);
	}

	public void ShowBestRecord(bool pIsShow)
	{
		_usedTransList["BestRecordBG"].gameObject.SetActive(pIsShow);
	}

	public void SetMyScore(double pScore)
	{
		_usedTransList ["MyScore"].GetComponent<UILabel> ().text = string.Format ("{0:0,0}", pScore);
	}

    public void DirectionReward(int pRewardItemSI, int pRewardMuch)
    {
        StartCoroutine(DirectionRewardTaking(pRewardItemSI, pRewardMuch));
    }

	/// <summary>
	/// 자랑하기 버튼 클릭시 호출 : 친구 목록 윈도우 출력...
	/// </summary>
	public void ShowKakaotalkBoast(bool pIsShow)
	{
		StartCoroutine (BoastKakaoKeypetFriends (""));
	}


	public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Stage"] = null;

		_usedTransList["BestRecordBG"] = null;
		_usedTransList["MyScore"] = null;
		_usedTransList["RewardBtn_01"] = null;
		_usedTransList["RewardBtn_02"] = null;
		_usedTransList["RewardBtn_03"] = null;
		_usedTransList["OkBtn"] = null;
		_usedTransList["CloseBtn"] = null;

		_usedTransList["RewardGetDirectionBG"] = null;
		_usedTransList["MyRewardItem"] = null;
		_usedTransList["MyRewardNum"] = null;
        _usedTransList["BoxSprite"] = null;
        _usedTransList["ResultBG"] = null;
        _usedTransList["BoastBtn"] = null;

		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["BoxSprite"].GetComponentInChildren<UILabel>().text = "Pick a box of compensation, please.";
        //        _usedTransList["OkBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["ResultBG"].GetComponent<UISprite>().spriteName = "successMsg_e";
        //        break;
        //        case SystemLanguage.Portuguese:
        //        _usedTransList ["BoxSprite"].GetComponentInChildren <UILabel> ().text = "Escolha uma caixa de compensação, por favor.";
        //        _usedTransList["OkBtn"].GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["ResultBG"].GetComponent<UISprite>().spriteName = "successMsg_e";
        //         break;
        //}
        
        _usedTransList["BoxSprite"].GetComponentInChildren<UILabel>().text = "Escolha uma caixa de compensação, por favor.";
        _usedTransList["OkBtn"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["ResultBG"].GetComponent<UISprite>().spriteName = "successMsg_q";
               
	}

    public override void OnEnter()
    {
		_usedTransList["RewardGetDirectionBG"].gameObject.SetActive(false);
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


    protected IEnumerator DirectionRewardTaking(int pRewardItemSI, int pRewardMuch)
    {
		_usedTransList["MyRewardItem"].gameObject.SetActive(false);
		_usedTransList["MyRewardNum"].gameObject.SetActive(false);

		TweenPosition tweenPos;
		TweenAlpha tweenAlpha;
		TweenScale tweenScale;

		_usedTransList["RewardGetDirectionBG"].gameObject.SetActive(true);

        //tweenPos = TweenPosition.Begin(_usedTransList["RewardGetDirectionBG"].gameObject, 0.7f, Vector3.zero);
        //tweenPos.from = new Vector3(1000.0f, 0.0f, 0.0f);
        //tweenPos.method = UITweener.Method.BounceIn;
        tweenScale = TweenScale.Begin(_usedTransList["RewardGetDirectionBG"].gameObject, 0.5f, Vector3.one);
        tweenScale.from = new Vector3(0.5f, 0.5f, 1.0f);
        tweenScale.method = UITweener.Method.BounceIn;

		tweenAlpha = TweenAlpha.Begin(_usedTransList["RewardGetDirectionBG"].gameObject, 0.5f, 1.0f);
		tweenAlpha.from = 0.5f;
		tweenAlpha.method = UITweener.Method.BounceIn;

		yield return new WaitForSeconds(0.7f);

		_usedTransList["MyRewardItem"].gameObject.SetActive(true);

        string rewardIcon = "";
        string strName = "";

        PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)pRewardItemSI);
        if (packageInfo != null)
        {
            rewardIcon = packageInfo.icon_image;
            strName = packageInfo.name;
            Debug.Log("~~~~~~~~~~~~~ Package Index : " + pRewardItemSI.ToString() + " name:" + strName + " icon:" + rewardIcon);
        }

        // 보상 아이템 아이콘 나타내기..
        if (string.IsNullOrEmpty(rewardIcon) == false) // power..
        {
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = rewardIcon;
        }
        //else if (pReward_Pack_type == 8) //gem..
        //    _usedTransList ["MyRewardItem"].GetComponent<UISprite>().spriteName = "itemIcon_Jóias";
        //else if (pReward_Pack_type == 7) //punny..
        //    _usedTransList ["MyRewardItem"].GetComponent<UISprite>().spriteName = "itemIcon_Funny";
        //else if (pReward_Pack_type == 6) // item..
        //    _usedTransList ["MyRewardItem"].GetComponent<UISprite>().spriteName = "item_001";
        else // error 
        {
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "rewardBox_press 1"; // UI 변경시 확인.
        }

        //_usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = packageInfo.icon_image;
		
        /*
        // 보상 아이템 아이콘 나타내기..
        if (pRewardItemSI == 1) // power..
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "itemIcon_power";
        else if (pRewardItemSI == 2) //gem..
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "itemIcon_Jóias";
        else if (pRewardItemSI == 3) //punny..
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "itemIcon_Funny";
        else if (pRewardItemSI == 4) // item..
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "item_001";
        else // error 
        {
            _usedTransList["MyRewardItem"].GetComponent<UISprite>().spriteName = "rewardBox_press 1"; // UI 변경시 확인.
        }*/

        _usedTransList["MyRewardItem"].GetComponent<UISprite>().MakePixelPerfect();

		Camera frontCam = GuiMgr.Instance.GetFrontCamera();
		Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

		Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(frontCam.WorldToScreenPoint(_usedTransList["MyRewardItem"].position));
		effectTargetPos.z = 0.0f;

		GameObject.Instantiate(Resources.Load("Effects/itemCharge"), effectTargetPos, Quaternion.identity);

		tweenScale = TweenScale.Begin(_usedTransList["MyRewardItem"].gameObject, 0.5f, Vector3.one);
		tweenScale.from = new Vector3(1.5f, 1.5f, 1.0f);
		tweenScale.method = UITweener.Method.BounceIn;

		yield return new WaitForSeconds(0.6f);

		// 보상 아이템의 수량 나타내기..
        _usedTransList["MyRewardNum"].GetComponent<UILabel>().text = string.Format("{0}", strName);
		_usedTransList["MyRewardNum"].gameObject.SetActive(true);

		tweenPos = TweenPosition.Begin(_usedTransList["MyRewardNum"].gameObject, 0.3f, Vector3.zero);
		tweenPos.from = new Vector3(1000.0f, 0.0f, 0.0f);
		tweenPos.method = UITweener.Method.Linear;

		yield return new WaitForSeconds(1.5f);

		if (null != OnDirectionFinished)
		{
			OnDirectionFinished();
		}
	}

	/// <summary>
	/// 키펫 설치한 카카오 친구 윈도우 나타내고 "자랑하기"...
	/// </summary>
	private IEnumerator BoastKakaoKeypetFriends(string parentType)
	{
        KakaoFriendsController.Instance.UsingKakaoFriends = true;
		
		// 테스트로....
		//if (parentType == "GuiChargePower")
		//	GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, false, null);
		
		
		Kakao_BoastFriend_Window window = GuiMgr.Instance.Show<Kakao_BoastFriend_Window>(GuiMgr.ELayerType.Front, true, null);
		
		// 카카오 친구 목록을 모두 로드하지 않은 상태라면 완료될 때까지 기다리기... 
        while (KakaoFriendsController.Instance.LoadingKakaoFriends == true)
			yield return null;

        List<MyFriend> myKakaoFriends = KakaoFriendsController.Instance._myKakaoKeypetFriends;	// _myKakaoKeypetFriends 리스트 가져오기...
		int myKakaoFriendsCount = myKakaoFriends.Count;
		int _nCount = 0;

        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();

        foreach (var pair in myKakaoFriends) 
        {
            if (pair.userid != KakaoLocalUserController.Instance.GetUserAccount())
            {
                GameObject itemGO;
                Kakao_BoastFriend_Item item;

                itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/Kakao_BoastFriend_Item"));
                item = itemGO.GetComponent<Kakao_BoastFriend_Item>();

                // 리스트 윈도우에 친구 추가하기...
                item.SetItemInfo(_nCount, pair.profileImage , pair.userid, pair.nickname, pair._fBoastTime);
                window.Add_KakaoFriendItem(item);

                // 키펫 카카오 메시지 차단한 유저라면 붉은색으로 일단 변경....
                if (pair.messageBlocked == true)
                {
                    item.GetComponentInChildren<UILabel>().color = Color.red;
                }

                _nCount++;
            }
        }
		
		window.ScrollView_RepositionNow ();
				
		// "나가기" 버튼 클릭시 처리...
		window.ExitBtn.onClick = (GameObject pBackBtnGO) =>
		{
            KakaoFriendsController.Instance.UsingKakaoFriends = false;
			
			window.ClearItems();
			window.ExitBtn.onClick = null;
			window = null;
			
			GuiMgr.Instance.Show<Kakao_BoastFriend_Window>(GuiMgr.ELayerType.Front, false, null);
		};
	}


}
