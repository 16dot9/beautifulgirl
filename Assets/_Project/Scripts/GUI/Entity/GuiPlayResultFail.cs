﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayResultFail : GuiBase
{

	public delegate void DELEGATEGUIRANKITEM(GuiPlayResultFailRankItem pRankItem);


	public DELEGATEGUIRANKITEM						OnRankItemClick = null;


    protected Dictionary<int, Kakao_BoastFriend_Item> _rankItemList = new Dictionary<int, Kakao_BoastFriend_Item>();

	
	public UIEventListener RePlayBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RePlayBtn"].gameObject);
		}
	}

	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

    public void SetStageNumber(string _Stage) 
    {
        _usedTransList["Stage"].GetComponent<UILabel>().text =  "Fase " + _Stage;
    }
    
	public void ClearRankingItem()
	{
        foreach (Kakao_BoastFriend_Item rankItem in _rankItemList.Values)
        {
            GameObject.DestroyImmediate(rankItem.gameObject);
        }

        _rankItemList.Clear();
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Stage"] = null;

		_usedTransList["RePlayBtn"] = null;
		_usedTransList["CloseBtn"] = null;

        _usedTransList["Sprite_1"] = null;
        _usedTransList["Sprite_2"] = null;
		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English: 
        //        _usedTransList["RePlayBtn"].GetComponentInChildren<UILabel>().text = "RePlay";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["Sprite_1"].GetComponent<UISprite>().spriteName = "MissionFailure02_e";
        //        _usedTransList["Sprite_2"].GetComponent<UISprite>().spriteName = "MissionFailure01_e";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList ["RePlayBtn"].GetComponentInChildren <UILabel> ().text = "Replay";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["Sprite_1"].GetComponent<UISprite>().spriteName = "MissionFailure02_e";
        //        _usedTransList["Sprite_2"].GetComponent<UISprite>().spriteName = "MissionFailure01_e";
        //        break;
        //}

        _usedTransList["RePlayBtn"].GetComponentInChildren<UILabel>().text = "Reiniciar";
        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["Sprite_1"].GetComponent<UISprite>().spriteName = "MissionFailure02_Q";
        _usedTransList["Sprite_2"].GetComponent<UISprite>().spriteName = "MissionFailure01_q";
               
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearRankingItem();
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


	protected void OnGuiPlayResultFailRankItem(int pGO)
	{
        if (null != OnRankItemClick)
        {
            Kakao_BoastFriend_Item rankItem;

            if (!_rankItemList.TryGetValue(pGO, out rankItem))
            {
                return;
            }
            //OnRankItemClick(rankItem);
        }
	}

}
