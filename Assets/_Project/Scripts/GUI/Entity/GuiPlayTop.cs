﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayTop : GuiBase
{

	public Vector3 GetTimePos()
	{
		return _usedTransList["Time"].position;
	}

    public Vector3 GetMoveInfoPos()
    {
        return _usedTransList["MoveInfo"].position;
    }

	public GameObject GetPauseBtn()
	{
		return _usedTransList["PauseBtn"].gameObject;
	}


	public Vector3 GetScorePos()
	{
		return _usedTransList["Score"].position;
	}

	public void SetSocre(double pScore)
	{
		string score = pScore.ToString();
		////Helper.ConvertMoneyType(ref score);
		/// 
		/// 
		score = string.Format ("{0:0,0}", double.Parse(score));
		_usedTransList["Score"].GetComponent<UILabel>().text = score;
	}

	public void EnableTime(bool pIsEnable)
	{
		_usedTransList["Time"].gameObject.SetActive(pIsEnable);
		_usedTransList["CoolTime"].gameObject.SetActive(pIsEnable);
		_usedTransList["ClockBG"].GetComponent<Animation>().Stop();
	}

	public void SetTime(float pCurTime, float pMaxTime)
	{
		_usedTransList["Time"].gameObject.GetComponent<UILabel>().text = ((int)(pMaxTime - pCurTime)).ToString();
		_usedTransList["CoolTime"].gameObject.GetComponent<UISprite>().fillAmount = pCurTime / pMaxTime;

		if (pMaxTime - pCurTime <= 10.0f)
		{
			Animation anim = _usedTransList["ClockBG"].GetComponent<Animation>();

			if (!anim.isPlaying)
			{
				anim.Play("TimerAlarm");
			}
		}
	}

	public void EnableCount(bool pIsEnable)
	{
		StopCoroutine("CountDir");
		StopCoroutine("CountAlarm");

		_usedTransList["BGActive"].gameObject.SetActive(!pIsEnable);
		_usedTransList["BGDeactive"].gameObject.SetActive(pIsEnable);
		_usedTransList["MoveCount"].gameObject.SetActive(pIsEnable);
	}

	public void SetCount(int pCurCount, int pMaxCount, bool pIsDir)
	{
		_usedTransList["MoveCount"].gameObject.GetComponent<UILabel>().text = (pMaxCount - pCurCount).ToString();

		if (pIsDir)
		{
			if (pMaxCount - pCurCount > 5)
			{
				StartCoroutine("CountDir");
			}
			else if (pMaxCount - pCurCount == 5)
			{
				StartCoroutine("CountAlarm");
			}
		}
	}

    public void SetStageGoal(string pStageGoal)
    {
        _usedTransList["Goal"].GetComponent<UILabel>().text = pStageGoal;
    }


    public override void OnCreate()
    {
        _usedTransList.Clear();

		_usedTransList["Score"] = null;
		_usedTransList["MoveInfo"] = null;
		_usedTransList["BGActive"] = null;
		_usedTransList["BGDeactive"] = null;
		_usedTransList["MoveCount"] = null;
		_usedTransList["ClockBG"] = null;
		_usedTransList["Time"] = null;
		_usedTransList["CoolTime"] = null;
        _usedTransList["PauseBtn"] = null;
        _usedTransList["Goal"] = null;

        FindTrans();
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }


	protected IEnumerator CountDir()
	{
		_usedTransList["BGActive"].gameObject.SetActive(true);

		yield return new WaitForSeconds(0.1f);

		_usedTransList["BGActive"].gameObject.SetActive(false);

		yield return new WaitForSeconds(0.1f);

		_usedTransList["BGActive"].gameObject.SetActive(true);

		yield return new WaitForSeconds(0.1f);

		_usedTransList["BGActive"].gameObject.SetActive(false);
	}

	protected IEnumerator CountAlarm()
	{
		bool isLoop = true;

		while (isLoop)
		{
			_usedTransList["BGActive"].gameObject.SetActive(true);

			yield return new WaitForSeconds(0.1f);

			_usedTransList["BGActive"].gameObject.SetActive(false);

			yield return new WaitForSeconds(0.1f);

			_usedTransList["BGActive"].gameObject.SetActive(true);

			yield return new WaitForSeconds(0.1f);

			_usedTransList["BGActive"].gameObject.SetActive(false);

			yield return new WaitForSeconds(0.1f);
		}
	}


}
