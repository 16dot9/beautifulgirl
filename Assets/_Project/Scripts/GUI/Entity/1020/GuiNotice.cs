﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleJSON;

public class GuiNotice : GuiBase
{
    private string imageurl;

    public UITexture _UITexture;

    private int _nindex = 0;

    public bool _bCheckToday { get; set; }

    public void SettingItem(string Url, int idx) 
    {
        StartCoroutine(IE_DownloadUrl(Url, idx));
    }

    public IEnumerator IE_DownloadUrl(string _url, int _idx) 
    {
        Debug.Log("_url :" + _url);
        Debug.Log("_idx :" + _idx);

        if(_url.Length > 0)
        {
            _bCheckToday = false;

            _nindex = _idx;

            imageurl = _url.Substring(1, _url.Length -2);

            Debug.Log("url :" + imageurl);

            WWW www = new WWW(imageurl);

            yield return www;

            if (!String.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
            }
            else
            {
                _UITexture.mainTexture = www.texture;
            }
        }
    }

    public void Check()
    {
        if (_bCheckToday)
        {
            _bCheckToday = false;
        }
        else
        {
            _bCheckToday = true;
        }
    }
    
    //닫기
    //다시 보지 않기
    public void Exit() 
    {
        Debug.Log(" _nindex :" + _nindex);

        string _Last;

        Debug.Log(" strNow : " + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        Debug.Log("lastLoginDate : " + GameObject.Find("InfoMgr").GetComponent<InfoSave>()._snotice.TryGetValue(_nindex, out _Last));
        
        if (_bCheckToday) 
        {
            string strNow = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string lastLoginDate = GameObject.Find("InfoMgr").GetComponent<InfoSave>()._snotice[_nindex];

            GameObject.Find("InfoMgr").GetComponent<InfoSave>().SetSaveNotice(_nindex, strNow);
        }

        Destroy(this.gameObject);
    }

    public void OpenURL() 
    {
        Application.OpenURL("http://cafe.naver.com/keypetbreak");
    }
}
