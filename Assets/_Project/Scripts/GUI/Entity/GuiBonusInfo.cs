﻿using UnityEngine;
using System.Collections;

public class GuiBonusInfo : GuiBase
{
    public enum BonusStauts
    {
        Fail,
        Item,
        Reball
    }

    public delegate void DELEGATEBOOL(bool pIsOk);

    public DELEGATEBOOL OnButtonClick = null;
    public DELEGATEBOOL OnButtonReClick = null;
    public DELEGATEBOOL OnButtonExitClick = null;
    
    public DELEGATEBOOL OnButtonStClick = null;
    public DELEGATEBOOL OnButtonBAitClick = null;

    protected bool _bEndItem;

    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["BG"] = null;
        _usedTransList["Fail"] = null;
        _usedTransList["Item"] = null;
        _usedTransList["View"] = null;

        _usedTransList["Item_01"] = null;
        _usedTransList["Item_01_BG"] = null;
        _usedTransList["Item_Label"] = null;

        _usedTransList["Explation"] = null;

        _usedTransList["Enter"] = null;
        _usedTransList["Exit"] = null;
        _usedTransList["Return"] = null;

        _usedTransList["BtnBack"] = null;
        _usedTransList["BtnStart"] = null;

        FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["Enter"]     .GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["Exit"]      .GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["Return"]    .GetComponentInChildren<UILabel>().text = "RePlay";
        //        _usedTransList["BtnBack"]   .GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["BtnStart"]  .GetComponentInChildren<UILabel>().text = "Start";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["Enter"]     .GetComponentInChildren<UILabel>().text = "확인";
        //        _usedTransList["Exit"]      .GetComponentInChildren<UILabel>().text = "나가기";
        //        _usedTransList["Return"]    .GetComponentInChildren<UILabel>().text = "다시 하기";
        //        _usedTransList["BtnBack"]   .GetComponentInChildren<UILabel>().text = "나가기";
        //        _usedTransList["BtnStart"]  .GetComponentInChildren<UILabel>().text = "시작 하기";
        //        break;
        //    case SystemLanguage.English:
        //        _usedTransList["Enter"]     .GetComponentInChildren<UILabel>().text = "OK";
        //        _usedTransList["Exit"]      .GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["Return"]    .GetComponentInChildren<UILabel>().text = "RePlay";
        //        _usedTransList["BtnBack"]   .GetComponentInChildren<UILabel>().text = "Sair";
        //        _usedTransList["BtnStart"]  .GetComponentInChildren<UILabel>().text = "Start";
        //        break;
        //}

        _usedTransList["Enter"].GetComponentInChildren<UILabel>().text = "Sim";
        _usedTransList["Exit"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["Return"].GetComponentInChildren<UILabel>().text = "Reiniciar";
        _usedTransList["BtnBack"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["BtnStart"].GetComponentInChildren<UILabel>().text = "Jogar";
              

        UIEventListener.Get(_usedTransList["Enter"].gameObject).onClick = OnOk;
        UIEventListener.Get(_usedTransList["Exit"].gameObject).onClick = OnExitOk;
        UIEventListener.Get(_usedTransList["Return"].gameObject).onClick = OnRetryOk;

        UIEventListener.Get(_usedTransList["BtnBack"].gameObject).onClick = OnBackOk;
        UIEventListener.Get(_usedTransList["BtnStart"].gameObject).onClick = OnStartOk;

        System.GC.Collect();

    }
    public override void OnEnter()
    {
        _bEndItem = true;
    }

    public override void OnLeave()
    {
        _bEndItem = false;
    }

    protected void OnOk(GameObject pGO)
    {
        if (null != OnButtonClick)
        {
            OnButtonClick(true);
           // OnButtonClick = null;
        }
    }

    protected void OnExitOk(GameObject pGO)
    {
        if (null != OnButtonExitClick)
        {
            OnButtonExitClick(true);
            OnButtonExitClick = null;
        }
    }

    protected void OnRetryOk(GameObject pGO)
    {
        if (null != OnButtonReClick)
        {
            OnButtonReClick(true);
            OnButtonReClick = null;
        }
    }

    protected void OnStartOk(GameObject pGO)
    {
        if (null != OnButtonStClick)
        {
            OnButtonStClick(true);
            OnButtonStClick = null;
        }
    }

    protected void OnBackOk(GameObject pGO)
    {
        if (null != OnButtonBAitClick)
        {
            OnButtonBAitClick(true);
            OnButtonBAitClick = null;
        }
    }
    public void StatusAlarm(BonusStauts Stauts)
    {
        switch (Stauts)
        {
            case BonusStauts.Fail:
            {
                _usedTransList["Fail"].gameObject.SetActive(true);
                _usedTransList["Item"].gameObject.SetActive(false);
                _usedTransList["View"].gameObject.SetActive(false);

                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.English:
                //        SetLabel("Escape Fail");
                //        break;
                //    case SystemLanguage.Korean:
                //        SetLabel("탈출 실패");
                //        break;
                //    case SystemLanguage.Portuguese:
                //        SetLabel("Escape Fail");
                //        break;
                //}

                SetLabel("Escape Fail");
            }
            break;

            case BonusStauts.Item:
            {
                _usedTransList["Fail"].gameObject.SetActive(false);
                _usedTransList["Item"].gameObject.SetActive(true);
                _usedTransList["View"].gameObject.SetActive(false);

                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.English:
                //        SetLabel("Escape Success");
                //        break;
                //    case SystemLanguage.Korean:
                //        SetLabel("탈출 성공");
                //        break;
                //    case SystemLanguage.Portuguese:
                        
                //        SetLabel ("Escape sucesso");
                //        break;
                //}


                SetLabel("Escape sucesso");
            } 
            break;

            case BonusStauts.Reball:
            { 
                _usedTransList["Fail"].gameObject.SetActive(false);
                _usedTransList["Item"].gameObject.SetActive(false);
                _usedTransList["View"].gameObject.SetActive(true);


                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.English:
                //        SetLabel("Find new Hidden Stage");
                //        break;
                //    case SystemLanguage.Korean:
                //        SetLabel("사냥꾼에게서 탈출 할 수 있는\n새로운 방법 발견했어요 !!!");
                //        break;
                //    case SystemLanguage.Portuguese:
                //        SetLabel ("Encontrar novos Invisível Stage");
                //        break;
                //}

                SetLabel("Encontrar novos Invisível Stage");
            }
            break;
        }
    }

    private void SetLabel(string str) 
    {
        _usedTransList["Explation"].GetComponent<UILabel>().text = str;
    }


    //< 우리나라는 고스톱 방향
    public void SetTextureItem(Texture2D _Item01, string _str, float _fSpeed)
    {
        _usedTransList["Item_01"].GetComponent<UITexture>().mainTexture = _Item01;

        _usedTransList["Item_Label"].GetComponent<UILabel>().text = _str;
        StartCoroutine(IE_RotationITEM(_fSpeed));
        // _usedTransList["Item_02"].GetComponent<UITexture>().mainTexture = _item02;
    }

    //UI 셋팅
    protected IEnumerator IE_RotationITEM(float speed)
    {
        while (_bEndItem)
        {
            _usedTransList["Item_01_BG"].transform.Rotate(Vector3.forward * Time.deltaTime * speed);

            yield return new WaitForFixedUpdate();
        }
    }
}
