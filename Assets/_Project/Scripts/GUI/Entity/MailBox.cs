﻿using UnityEngine;
using System.Collections;

public class MailBox
{
    public int seial;
    public int msg_type;
    public string receiver;
    public int sender;
    public int pack_idx;
    public int pack_qty;
    public string send_ts;
    public int confirmed;

    public void Set_Mail(int seial, int msg_type, string receiver,
        int sender, int pack_idx, int pack_qty, string send_ts, int confirmed) 
    {
        this.seial = seial;
        this.msg_type = msg_type;
        this.receiver = receiver;
        this.sender = sender;
        this.pack_idx = pack_idx;
        this.pack_qty = pack_qty;
        this.send_ts = send_ts;
        this.confirmed = confirmed;

        Debug.Log("Setting Mail");
    }

}
