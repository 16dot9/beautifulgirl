﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SimpleJSON;


public class GuiAttendance : GuiBase
{

	public delegate void DELEGATEGUIATTENDANCEITEM(GuiAttendanceItem pRankItem);


	public DELEGATEGUIATTENDANCEITEM				OnRankItemClick = null;


    protected Dictionary<GameObject, GuiAttendanceItem>		_attendanceItemList = new Dictionary<GameObject, GuiAttendanceItem>();
	protected GameObject									_effect = null;


    public UIEventListener OkBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["OkBtn"].gameObject);
        }
    }


    public void SetDayClear(int pDay, bool pIsClear, bool pIsDir)
    {
        if ( pDay < 1 ) // || pDay > 10 )
        {
            return;
        }

		if (!pIsDir)
		{
			_usedTransList[string.Format("Day_{0}", pDay.ToString("00"))].GetComponent<UIToggle>().value = pIsClear;
		}
		else
		{
			StartCoroutine(ShowDir(pDay, pIsClear));
		}
    }

    public void AddRakingItem(GuiAttendanceItem pAttendanceItem)
    {
        //pAttendanceItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["RankingScrollView"].GetComponent<UIScrollView>();

        //pAttendanceItem.transform.parent = _usedTransList["UIGrid"];
        //pAttendanceItem.transform.localPosition = Vector3.zero;
        //pAttendanceItem.transform.localRotation = Quaternion.identity;
        //pAttendanceItem.transform.localScale = Vector3.one;

        //pAttendanceItem.PowerBtn.onClick = OnGuiAttendanceItem;

        //_attendanceItemList.Add(pAttendanceItem.PowerBtn.gameObject, pAttendanceItem);

        //_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
    }

    public void ClearRankingItem()
    {
        //foreach (GuiAttendanceItem attendanceItem in _attendanceItemList.Values)
        //{
        //    attendanceItem.PowerBtn.onClick = OnGuiAttendanceItem;

        //    GameObject.DestroyImmediate(attendanceItem.gameObject);
        //}

        //_attendanceItemList.Clear();
    }


    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["Day_01"] = null;
        _usedTransList["Day_02"] = null;
        _usedTransList["Day_03"] = null;
        _usedTransList["Day_04"] = null;
        _usedTransList["Day_05"] = null;
        _usedTransList["Day_06"] = null;
        _usedTransList["Day_07"] = null;
        _usedTransList["Day_08"] = null;
        _usedTransList["Day_09"] = null;
        _usedTransList["Day_10"] = null;
        _usedTransList["Day_11"] = null;
        _usedTransList["Day_12"] = null;

        _usedTransList["RankingScrollView"] = null;
        _usedTransList["UIGrid"] = null;

        _usedTransList["OkBtn"] = null;

        FindTrans();

        _usedTransList["Day_01"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_02"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_03"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_04"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_05"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_06"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_07"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_08"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_09"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_10"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_11"].GetComponent<UIToggle>().value = false;
        _usedTransList["Day_12"].GetComponent<UIToggle>().value = false;

    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearRankingItem();

		if (null != _effect)
		{
			GameObject.DestroyImmediate(_effect);
			_effect = null;
		}
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }


	protected void OnGuiAttendanceItem(GameObject pGO)
	{
		if (null != OnRankItemClick)
		{
			GuiAttendanceItem rankItem;

			if (!_attendanceItemList.TryGetValue(pGO, out rankItem))
			{
				return;
			}

			OnRankItemClick(rankItem);
		}
	}


	protected IEnumerator ShowDir(int pDay, bool pIsClear)
	{
        Debug.Log("~~~~~~~~~~~~~~~~~~~~ string.Format('Day_{0}', pDay.ToString('00')" + string.Format("Day_{0}", pDay.ToString("00")));

		Transform targetTrans = _usedTransList[string.Format("Day_{0}", pDay.ToString("00"))];

		yield return new WaitForSeconds(1.0f);

		targetTrans.GetComponent<UIToggle>().value = !pIsClear;

		//AudioMgr.Instance.Play("ButtonOn");

		Camera backCam = GuiMgr.Instance.GetBackCamera();
		Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

		Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(targetTrans.position));
		effectTargetPos.z = 0.0f;

		_effect = (GameObject)GameObject.Instantiate(Resources.Load("Effects/keypetTwinkle"), effectTargetPos, Quaternion.identity);

		yield return new WaitForSeconds(2.0f);

		targetTrans.GetComponent<UIToggle>().value = pIsClear;

        Debug.Log("~~~~~~~~~~~~~~~~~~~~ targetTrans: " + targetTrans.name+ "targetTrans.GetComponent<UIToggle>().value : " + targetTrans.GetComponent<UIToggle>().value);
    }

}
