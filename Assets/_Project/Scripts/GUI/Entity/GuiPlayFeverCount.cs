﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GuiPlayFeverCount : GuiBase
{

	public void ShowOn(bool pIsOn)
	{
		if (pIsOn)
		{
			_usedTransList["FeverCountBGOn"].gameObject.SetActive(true);
			_usedTransList["FeverCountBGOff"].gameObject.SetActive(false);

			_usedTransList["CountOn"].gameObject.SetActive(true);
			_usedTransList["CountOff"].gameObject.SetActive(false);
		}
		else
		{
			_usedTransList["FeverCountBGOn"].gameObject.SetActive(false);
			_usedTransList["FeverCountBGOff"].gameObject.SetActive(true);

			_usedTransList["CountOn"].gameObject.SetActive(false);
			_usedTransList["CountOff"].gameObject.SetActive(true);
		}
	}

	// 피버카운트 객체 자체를 안보이게 처리..
	// 피버타임이 진행중인 상태에서, 게임이 종료되면서 보너스 타임이 시작되면 피버타임 객체가 화면에서 보여지지 않도록 만든다.. 
	public void HideFiverCount()
	{
		gameObject.SetActive (false);
	}
    public void ShowFiverCount()
    {
        gameObject.SetActive(true);
    }

	public void SetCount(float pCountTime)
	{
		if (pCountTime <= 0.0f)
		{
			gameObject.SetActive(false);
		}
		else
		{
			if (!gameObject.activeSelf)
			{
				gameObject.SetActive(true);
			}

			if(_usedTransList["CountOff"].gameObject.activeSelf)
				_usedTransList["CountOff"].GetComponent<UILabel>().text = ((int)pCountTime).ToString();
			else
				_usedTransList["CountOn"].GetComponent<UILabel>().text = ((int)pCountTime).ToString();
		}
	}

	public void Move(Vector3 pPos)
	{
		transform.position = pPos;
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["FeverCountBGOn"] = null;
		_usedTransList["FeverCountBGOff"] = null;
		_usedTransList["CountOff"] = null;
		_usedTransList["CountOn"] = null;

		FindTrans();

		ShowOn(false);
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
