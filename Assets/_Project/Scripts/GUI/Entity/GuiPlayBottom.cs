﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiPlayBottom : GuiBase
{
    public delegate void DELEGATE_USEITEM(uint item_idx, GameObject go);
    public DELEGATE_USEITEM OnClickItem = null;

    protected uint[] _arUseItemIdx = new uint[3]{0, 0, 0};

    public void resetUseItems()
    {
        for (int i = 0; i < 3; i++)
        {
            _arUseItemIdx[i] = 0;
            string itemName = string.Format("Item_{0}_Image", (i + 1).ToString("00"));
            Transform imageTrm = _usedTransList[itemName];
            imageTrm.GetComponent<UITexture>().mainTexture = null;
            UIEventListener.Get(imageTrm.gameObject).onClick = null;
        }
    }

	public bool isEmptyItem()
	{
		if (null != UIEventListener.Get(_usedTransList["Item_01"].gameObject).onClick)
		{
			return false;
		}
		if (null != UIEventListener.Get(_usedTransList["Item_02"].gameObject).onClick)
		{
			return false;
		}
		if (null != UIEventListener.Get(_usedTransList["Item_03"].gameObject).onClick)
		{
			return false;
		}

		return true;
	}

	public bool setUseItem(int num, uint item_idx, string image , string name)
    {
		ItemInfo itemInfo;
		ItemInfoMgr itemInfoMgr = GameDataMgr.Instance.GetInfo<ItemInfoMgr>();
		itemInfo = itemInfoMgr.GetItemInfo(item_idx);


        if (num < 0 || num > 2)
            return false;

        _arUseItemIdx[num] = item_idx;  

        Transform imageTrm = _usedTransList[string.Format("Item_{0}_Image", (num+1).ToString("00"))];
		imageTrm.transform.parent.parent.Find("ItemName").GetComponent<UILabel>().text = itemInfo.name;
        imageTrm.GetComponent<UITexture>().mainTexture = (Texture2D)Resources.Load("Images/Item/"+image);

        UIEventListener.Get(_usedTransList[string.Format("Item_{0}", (num+1).ToString("00"))].gameObject).onClick = (GameObject go) =>
        {
			if(GameObject.Find("GameState/GSPlay").GetComponent<GSPlay>().itemUseWark == false){
		
	            // 이미지와 OnClick 을 초기화 한다.
	            imageTrm.GetComponent<UITexture>().mainTexture = null;
				//imageTrm.transform.parent.parent.FindChild("BG").GetComponent<UISprite>().spriteName = "itemAction_progress 1";
				imageTrm.transform.parent.parent.Find("ItemName").GetComponent<UILabel>().text = "";

	            UIEventListener.Get(go).onClick = null;
	            
	            if (OnClickItem != null)
                    OnClickItem(_arUseItemIdx[num], imageTrm.gameObject); // go);
			}
        };
        return true;
    }



/*
	public UIEventListener ItemBtn_01
	{
		get
		{
			return UIEventListener.Get(_usedTransList["Item_01"].gameObject);
		}
	}

	public UIEventListener ItemBtn_02
	{
		get
		{
			return UIEventListener.Get(_usedTransList["Item_02"].gameObject);
		}
	}

	public UIEventListener ItemBtn_03
	{
		get
		{
			return UIEventListener.Get(_usedTransList["Item_03"].gameObject);
		}
	}

*/
	public void SetItemCount_01(int pCount)
	{
		if (pCount > 0)
		{
			_usedTransList["Item_01_CountBG"].gameObject.SetActive(true);
		}
		else
		{
			_usedTransList["Item_01_CountBG"].gameObject.SetActive(false);
		}

		_usedTransList["Item_01_Count"].GetComponent<UILabel>().text = pCount.ToString();
	}

	public void SetItemCount_02(int pCount)
	{
		if (pCount > 0)
		{
			_usedTransList["Item_02_CountBG"].gameObject.SetActive(true);
		}
		else
		{
			_usedTransList["Item_02_CountBG"].gameObject.SetActive(false);
		}

		_usedTransList["Item_02_Count"].GetComponent<UILabel>().text = pCount.ToString();
	}

	public void SetItemCount_03(int pCount)
	{
		if (pCount > 0)
		{
			_usedTransList["Item_03_CountBG"].gameObject.SetActive(true);
		}
		else
		{
			_usedTransList["Item_03_CountBG"].gameObject.SetActive(false);
		}

		_usedTransList["Item_03_Count"].GetComponent<UILabel>().text = pCount.ToString();
	}

/*
	public void SetItemImage_01(Texture2D pImage)
	{
		_usedTransList["Item_01_Image"].GetComponent<UITexture>().mainTexture = pImage;
	}

	public void SetItemImage_02(Texture2D pImage)
	{
		_usedTransList["Item_02_Image"].GetComponent<UITexture>().mainTexture = pImage;
	}

	public void SetItemImage_03(Texture2D pImage)
	{
		_usedTransList["Item_03_Image"].GetComponent<UITexture>().mainTexture = pImage;
	}
*/

    public Transform GetItemObjectTransForm()
    {
        return _usedTransList["Item_01"].transform;
    }

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Item_01"] = null;
		_usedTransList["Item_01_CountBG"] = null;
		_usedTransList["Item_01_Count"] = null;
		_usedTransList["Item_01_Image"] = null;

		_usedTransList["Item_02"] = null;
		_usedTransList["Item_02_CountBG"] = null;
		_usedTransList["Item_02_Count"] = null;
		_usedTransList["Item_02_Image"] = null;

		_usedTransList["Item_03"] = null;
		_usedTransList["Item_03_CountBG"] = null;
		_usedTransList["Item_03_Count"] = null;
		_usedTransList["Item_03_Image"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
		SetItemCount_01(0);
		SetItemCount_02(0);
		SetItemCount_03(0);
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

    public GameObject _pfBubbleMessage;
    public void PlayBubbleMessage(Transform pos)
    {
        if (_pfBubbleMessage != null && pos != null)
        {
            GameObject obj = (GameObject) Instantiate(_pfBubbleMessage); // , pos.position, pos.rotation);
            obj.transform.parent = pos;
            obj.transform.localScale = new Vector3(1f,1f,1f);
            obj.transform.localPosition = Vector3.zero;

            Destroy(obj, 5f);
        }
    }
}
