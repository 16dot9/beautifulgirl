﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SimpleJSON;


public class GuiRoulette : GuiBase
{

	public delegate void DELEGATENOPARAM();


	public DELEGATENOPARAM	OnAlarmFinished;
	public DELEGATENOPARAM	OnStoped;
	public DELEGATENOPARAM	OnResultFinished;


	protected float			_rotSpeed = 200.0f;

	protected float			_curRotValue = 0.0f;
	protected float			_targetRotValue = 360.0f;

	protected float[]		_rouletteSoundList = new float[] { 16.57f, 52.57f, 88.6f, 124.5f, 159.9f, 196.3f, 232.9f, 268.1f, 304.5f, 340.2f };
	protected bool			_isStop = false;


	public UIEventListener StopBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["StopBtn"].gameObject);
		}
	}


	public void ShowAlarm()
	{
		StartCoroutine(ShowAlarmDir());
	}

	public void ShowRoultte(bool pIsShow)
	{
		_usedTransList["RouletteDlg"].gameObject.SetActive(pIsShow);
	}

	public void RouletteStop(float pStopDec)
	{
		_targetRotValue = 360.0f + pStopDec;

		_isStop = true;
	}

	public void ShowResult(Texture2D pImage, string pResult)
	{
		StartCoroutine(ShowResultDir(pImage, pResult));
	}


	public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["AlarmDlg"] = null;
		_usedTransList["RouletteDlg"] = null;
		_usedTransList["ResultDlg"] = null;

		_usedTransList["RoulettePlane"] = null;
		_usedTransList["StopBtn"] = null;

		FindTrans();

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:

        //        _usedTransList["StopBtn"].GetComponentInChildren<UILabel>().text = "Stop";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["StopBtn"].GetComponentInChildren<UILabel>().text = "Stop";
        //        break;
        //}

        _usedTransList["StopBtn"].GetComponentInChildren<UILabel>().text = "Stop";

		_usedTransList["AlarmDlg"].gameObject.SetActive(false);
		_usedTransList["ResultDlg"].gameObject.SetActive(false);
		_usedTransList["RouletteDlg"].gameObject.SetActive(false);
	}

    public override void OnEnter()
    {
    }

	public void FixedUpdate()
	{
		if (!_usedTransList["RouletteDlg"].gameObject.activeSelf)
		{
			return;
		}

		float oldRotValue = _curRotValue;

		if (_isStop)
		{
			float curRotValue = Mathf.Lerp(_curRotValue, _targetRotValue, Time.fixedDeltaTime);

			curRotValue = Mathf.Clamp(curRotValue, _curRotValue, _curRotValue + Time.fixedDeltaTime * _rotSpeed);

			_curRotValue = curRotValue;
			 
			if (_targetRotValue - _curRotValue < 1.0f)
			{
				if (null != OnStoped)
				{
					OnStoped();
				}
			}
		}
		else
		{
			_curRotValue += Time.fixedDeltaTime * _rotSpeed;

			if (_curRotValue >= 360.0f)
			{
				_curRotValue -= 360.0f;
			}
		}

		float nextRotValue = _curRotValue;

		foreach (float rouletteSoundTiming in _rouletteSoundList)
		{
			if (oldRotValue >= 360.0f)
			{
				oldRotValue -= 360.0f;
			}

			if (nextRotValue >= 360.0f)
			{
				nextRotValue -= 360.0f;
			}

			if (oldRotValue <= rouletteSoundTiming && rouletteSoundTiming <= nextRotValue)
			{
				if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().effectSoundOnOff == true){
					AudioMgr.Instance.Play("Roulette");
				}
			}
		}

		_usedTransList["RoulettePlane"].transform.localRotation = Quaternion.Euler(0.0f, 0.0f, _curRotValue);
	}

    public override void OnLeave()
    {
		// "RouletteDlg"에서 계속 룰렛돌리기를 할 경우를 위해 데이터 초기화.. 
		_curRotValue = 0.0f;
		_targetRotValue = 360.0f;
		_isStop = false;

		// "RouletteDlg"에서 계속 룰렛돌리기를 할 경우를 위해 "RouletteDlg"를 안보이게 만듬..
		_usedTransList["RouletteDlg"].gameObject.SetActive(false);
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


	protected IEnumerator ShowAlarmDir()
	{
		_usedTransList["AlarmDlg"].gameObject.SetActive(true);

		TweenPosition tweenPos = TweenPosition.Begin(_usedTransList["AlarmDlg"].gameObject, 0.5f, Vector3.zero);
		tweenPos.from = new Vector3(10000, 0.0f, 0.0f);
		tweenPos.method = UITweener.Method.EaseIn;

		yield return new WaitForSeconds(2.0f);

		_usedTransList["AlarmDlg"].gameObject.SetActive(false);

		if (null != OnAlarmFinished)
		{
			OnAlarmFinished();
		}
	}

	protected IEnumerator ShowResultDir(Texture2D pImage, string pResult)
	{
		Transform targetTrans = _usedTransList["ResultDlg"];

		targetTrans.gameObject.SetActive(true);

		targetTrans.GetComponentInChildren<UITexture>().mainTexture = pImage;
		targetTrans.GetComponentInChildren<UILabel>().text = pResult;

        //TweenPosition tweenPos = TweenPosition.Begin(targetTrans.gameObject, 0.5f, Vector3.zero);
        //tweenPos.from = new Vector3(10000, 0.0f, 0.0f);
        //tweenPos.method = UITweener.Method.EaseIn;

        TweenScale tweenScale = TweenScale.Begin(targetTrans.gameObject, 0.5f, Vector3.one);
        tweenScale.from = new Vector3(0.5f, 0.5f, 1.0f);
        tweenScale.method = UITweener.Method.BounceIn;


		yield return new WaitForSeconds(2.0f);

        //tweenPos = TweenPosition.Begin(targetTrans.gameObject, 0.5f, new Vector3(10000, 0.0f, 0.0f));
        //tweenPos.from = Vector3.zero;
        //tweenPos.method = UITweener.Method.EaseOut;

		targetTrans.gameObject.SetActive(false);

		if (null != OnResultFinished)
		{
			OnResultFinished();
		}
	}

}
