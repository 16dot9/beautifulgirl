﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SimpleJSON;


public class GuiKeypetCollection : GuiBase
{

	public UIEventListener ExitBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["ExitBtn"].gameObject);
		}
	}

	public UIEventListener RouletteBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["RouletteBtn"].gameObject);
		}
	}


	public void ShowKeypet(string pAlpahbet, int pCount)
	{
		string emptyName = string.Format("Empty_{0}", pAlpahbet);
		string keyName = string.Format("Item_{0}", pAlpahbet);

		if (pCount > 0)
		{
			_usedTransList[emptyName].gameObject.SetActive(false);

			_usedTransList[keyName].gameObject.SetActive(true);
			_usedTransList[keyName].GetComponentInChildren<UILabel>().text = string.Format("x {0}", pCount);
		}
		else
		{
			_usedTransList[keyName].gameObject.SetActive(false);
			_usedTransList[emptyName].gameObject.SetActive(true);
		}
	}

	public void SlotCount(int pCount, bool pIsDir)
	{
		_usedTransList["KeypetCount"].GetComponent<UILabel>().text = string.Format("{0} / {1}", pCount, 10);

		for (int i = 0; i < 10; ++i)
		{
			Transform trans = _usedTransList[string.Format("Slot_{0}", (i + 1).ToString("00"))];

			if (i < pCount)
			{
				trans.GetComponent<UIToggle>().value = true;

				if (pIsDir && i == pCount - 1)
				{
					Camera backCam = GuiMgr.Instance.GetBackCamera();
					Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

					Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(trans.position));
					effectTargetPos.z = 0.0f;

					GameObject.Instantiate(Resources.Load("Effects/keypetTwinkle"), effectTargetPos, Quaternion.identity);

					//AudioMgr.Instance.Play("ButtonOn");
				}
			}
			else
			{
				trans.GetComponent<UIToggle>().value = false;
			}
		}

		_usedTransList["Items"].position = _usedTransList["Items"].position;
	}


	public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Slot_01"] = null;
		_usedTransList["Slot_02"] = null;
		_usedTransList["Slot_03"] = null;
		_usedTransList["Slot_04"] = null;
		_usedTransList["Slot_05"] = null;
		_usedTransList["Slot_06"] = null;
		_usedTransList["Slot_07"] = null;
		_usedTransList["Slot_08"] = null;
		_usedTransList["Slot_09"] = null;
		_usedTransList["Slot_10"] = null;

		_usedTransList["Empty_A"] = null;
		_usedTransList["Empty_B"] = null;
		_usedTransList["Empty_C"] = null;
		_usedTransList["Empty_D"] = null;
		_usedTransList["Empty_E"] = null;
		_usedTransList["Empty_F"] = null;
		_usedTransList["Empty_G"] = null;
		_usedTransList["Empty_H"] = null;
		_usedTransList["Empty_I"] = null;
		_usedTransList["Empty_J"] = null;
		_usedTransList["Empty_K"] = null;
		_usedTransList["Empty_L"] = null;
		_usedTransList["Empty_M"] = null;
		_usedTransList["Empty_N"] = null;
		_usedTransList["Empty_O"] = null;
		_usedTransList["Empty_P"] = null;
		_usedTransList["Empty_Q"] = null;
		_usedTransList["Empty_R"] = null;
		_usedTransList["Empty_S"] = null;
		_usedTransList["Empty_T"] = null;
		_usedTransList["Empty_U"] = null;
		_usedTransList["Empty_V"] = null;
		_usedTransList["Empty_W"] = null;
		_usedTransList["Empty_X"] = null;
		_usedTransList["Empty_Y"] = null;
		_usedTransList["Empty_Z"] = null;

		_usedTransList["Item_A"] = null;
		_usedTransList["Item_B"] = null;
		_usedTransList["Item_C"] = null;
		_usedTransList["Item_D"] = null;
		_usedTransList["Item_E"] = null;
		_usedTransList["Item_F"] = null;
		_usedTransList["Item_G"] = null;
		_usedTransList["Item_H"] = null;
		_usedTransList["Item_I"] = null;
		_usedTransList["Item_J"] = null;
		_usedTransList["Item_K"] = null;
		_usedTransList["Item_L"] = null;
		_usedTransList["Item_M"] = null;
		_usedTransList["Item_N"] = null;
		_usedTransList["Item_O"] = null;
		_usedTransList["Item_P"] = null;
		_usedTransList["Item_Q"] = null;
		_usedTransList["Item_R"] = null;
		_usedTransList["Item_S"] = null;
		_usedTransList["Item_T"] = null;
		_usedTransList["Item_U"] = null;
		_usedTransList["Item_V"] = null;
		_usedTransList["Item_W"] = null;
		_usedTransList["Item_X"] = null;
		_usedTransList["Item_Y"] = null;
		_usedTransList["Item_Z"] = null;

		_usedTransList["Items"] = null;

		_usedTransList["ScrollGuard"] = null;

		_usedTransList["KeypetCount"] = null;
		_usedTransList["RouletteBtn"] = null;
		_usedTransList["ExitBtn"] = null;

		FindTrans();

		SlotCount(0, false);

		ShowKeypet("A", 0);
		ShowKeypet("B", 0);
		ShowKeypet("C", 2);
		ShowKeypet("D", 0);
		ShowKeypet("E", 0);
		ShowKeypet("F", 0);
		ShowKeypet("G", 2);
		ShowKeypet("H", 0);
		ShowKeypet("I", 0);
		ShowKeypet("J", 0);
		ShowKeypet("K", 1);
		ShowKeypet("L", 0);
		ShowKeypet("M", 4);
		ShowKeypet("N", 0);
		ShowKeypet("O", 0);
		ShowKeypet("P", 0);
		ShowKeypet("Q", 0);
		ShowKeypet("R", 0);
		ShowKeypet("S", 0);
		ShowKeypet("T", 1);
		ShowKeypet("U", 0);
		ShowKeypet("V", 0);
		ShowKeypet("W", 0);
		ShowKeypet("X", 0);
		ShowKeypet("Y", 0);
		ShowKeypet("Z", 1);
        
        
		StartCoroutine(ShowGuard());
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


	public IEnumerator ShowGuard()
	{
		Transform scrollGuard = _usedTransList["ScrollGuard"];

		TweenAlpha alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 1.0f);
		alpha.from = 0.0f;
		alpha.method = UITweener.Method.EaseIn;

		yield return new WaitForSeconds(0.3f);

		alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 0.0f);
		alpha.from = 1.0f;
		alpha.method = UITweener.Method.EaseOut;

		yield return new WaitForSeconds(0.3f);

		alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 1.0f);
		alpha.from = 0.0f;
		alpha.method = UITweener.Method.EaseIn;

		yield return new WaitForSeconds(0.3f);

		alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 0.0f);
		alpha.from = 1.0f;
		alpha.method = UITweener.Method.EaseOut;
	}

}
