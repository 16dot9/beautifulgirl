﻿using UnityEngine;
using SimpleJSON;


public class GuiTitle : GuiBase
{

	protected UISprite			_loadingProgressBar = null;
	protected UILabel			_Tips = null;


	public UISprite LoadingProgressBar
	{
		get
		{
			if (null == _loadingProgressBar)
			{
				_loadingProgressBar = _usedTransList["LoadingTop"].gameObject.GetComponent<UISprite>();
			}

			return _loadingProgressBar;
		}
	}

	public UILabel Tips
	{
		get
		{
			if(null == _Tips)
			{
				_Tips = _usedTransList["Tips"].gameObject.GetComponent<UILabel>();
			}

			return _Tips;
		}
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["LoadingTop"] = null;
		_usedTransList["Tips"] = null;
        _usedTransList["Title"] = null;

		FindTrans();

        if (Application.systemLanguage != SystemLanguage.Korean)
        {
            _usedTransList["Title"].GetComponent<UISprite>().spriteName = "keypetLogo 1";
        }
    }

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
		_loadingProgressBar = null;
		_Tips = null;

		_usedTransList.Clear();
	}

}
