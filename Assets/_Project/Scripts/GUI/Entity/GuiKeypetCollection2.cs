﻿using UnityEngine;
using System.Collections;

public class GuiKeypetCollection2 : GuiBase
{

    public UIEventListener ExitBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["ExitBtn"].gameObject);
        }
    }

    public UIEventListener RouletteBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["RouletteBtn"].gameObject);
        }
    }


    public void ShowKeypet(string pAlpahbet, int pCount)
    {
        string SlotName = string.Format("Slot_{0}", pAlpahbet);
        
        Transform trans = _usedTransList[SlotName];
        if (trans)
        {
            if (pCount > 0)
            {
                trans.GetComponent<UIToggle>().value = true;
                trans.GetComponentInChildren<UILabel>().text = "x" + pCount.ToString();
            }
            else
            {
                trans.GetComponent<UIToggle>().value = false;
                trans.GetComponentInChildren<UILabel>().text = "";
            }
        }


/*
        string keyName = string.Format("Item_{0}", pAlpahbet);

        if (pCount > 0)
        {
            _usedTransList[emptyName].gameObject.SetActive(false);

            _usedTransList[keyName].gameObject.SetActive(true);
            _usedTransList[keyName].GetComponentInChildren<UILabel>().text = string.Format("x {0}", pCount);
        }
        else
        {
            _usedTransList[keyName].gameObject.SetActive(false);
            _usedTransList[emptyName].gameObject.SetActive(true);
        }
 */
    }
    public bool RouletteKeypetCountCheck()
    {
        int maxSlot = 6;
        int checkOnCount = 0;

        for (int i = 1; i <= maxSlot; i++)
        {
            string cntName = string.Format("Slot_{0}", i.ToString("00"));
            Transform trans = _usedTransList[cntName];
            if (trans)
            {
                CollectionKeypetSu keypetSu = trans.GetComponentInChildren<CollectionKeypetSu>();

                if (keypetSu)
                {
                    if (keypetSu.GetCountIndex() > 0)
                    {
                        checkOnCount++;
                    }
                }
            }
        }

        if (maxSlot <= checkOnCount)
        {
            return true;
        }
        else
        {
            return false;
        }

        return false;
    }
    public void RouletteKeypetCountMinus()
    {
        int maxSlot = 6;
        for (int i = 1; i <= maxSlot; i++)
        {
            string cntName = string.Format("Slot_{0}", i.ToString("00"));
            Transform trans = _usedTransList[cntName];
            if (trans)
            {
                CollectionKeypetSu keypetSu = trans.GetComponentInChildren<CollectionKeypetSu>();
                if (keypetSu)
                {
                    if (keypetSu._inv != null && keypetSu._inv._qty > 0)
                    {
                        keypetSu._inv._qty--;
                        keypetSu.DisplayCount();
                    }
                }
            }
        }

    }

    public void RoulletCount(int cnt)
    {
        for (int i = 0; i < 11; i++)
        {
            string cntName = string.Format("pan_{0}", i.ToString("00"));
            Transform trans = _usedTransList[cntName];
            if (trans)
            {
				// Hwang Wark
				/*
                if (i == cnt)
                    trans.gameObject.SetActive(true);
                else
                    trans.gameObject.SetActive(false);
               */
				// Hwang End
            }
        }
    }

/*
    public void SlotCount(int pCount, bool pIsDir)
    {
        _usedTransList["KeypetCount"].GetComponent<UILabel>().text = string.Format("{0} / {1}", pCount, 10);

        for (int i = 0; i < 10; ++i)
        {
            Transform trans = _usedTransList[string.Format("Slot_{0}", (i + 1).ToString("00"))];

            if (i < pCount)
            {
                trans.GetComponent<UIToggle>().value = true;

                if (pIsDir && i == pCount - 1)
                {
                    Camera backCam = GuiMgr.Instance.GetBackCamera();
                    Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

                    Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(trans.position));
                    effectTargetPos.z = 0.0f;

                    GameObject.Instantiate(Resources.Load("Effects/keypetTwinkle"), effectTargetPos, Quaternion.identity);

                   // AudioMgr.Instance.Play("ButtonOn");
                }
            }
            else
            {
                trans.GetComponent<UIToggle>().value = false;
            }
        }

        _usedTransList["Items"].position = _usedTransList["Items"].position;
    }
*/

    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["Collection"] = null;
        _usedTransList["Slot_01"] = null;
        _usedTransList["Slot_02"] = null;
        _usedTransList["Slot_03"] = null;
        _usedTransList["Slot_04"] = null;
        _usedTransList["Slot_05"] = null;
        _usedTransList["Slot_06"] = null;
        _usedTransList["Slot_07"] = null;
        _usedTransList["Slot_08"] = null;
        _usedTransList["Slot_09"] = null;
        _usedTransList["Slot_10"] = null;
        _usedTransList["Slot_11"] = null;
        _usedTransList["Slot_12"] = null;

        _usedTransList["RoulletteSu"] = null;
        _usedTransList["pan_00"] = null;
        _usedTransList["pan_01"] = null;
        _usedTransList["pan_02"] = null;
        _usedTransList["pan_03"] = null;
        _usedTransList["pan_04"] = null;
        _usedTransList["pan_05"] = null;
        _usedTransList["pan_06"] = null;
        _usedTransList["pan_07"] = null;
        _usedTransList["pan_08"] = null;
        _usedTransList["pan_09"] = null;
        _usedTransList["pan_10"] = null;

        _usedTransList["RouletteBtn"] = null;
        _usedTransList["ExitBtn"] = null;
        _usedTransList["Desc"] = null;
        _usedTransList["TextBox"] = null;

        FindTrans();
		
        RoulletCount(3);
        

        ShowKeypet("01", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[8]);
		ShowKeypet("02", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[9]);
		ShowKeypet("03", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[10]);
		ShowKeypet("04", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[11]);
		ShowKeypet("05", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[12]);
		ShowKeypet("06", GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[13]);
        ShowKeypet("07", 0);
        ShowKeypet("08", 0);
        ShowKeypet("09", 0);
        ShowKeypet("10", 0);
        ShowKeypet("11", 0);
        ShowKeypet("12", 0);


        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["RouletteBtn"].GetComponentInChildren<UILabel>().text                        = "Roulette";
        //         _usedTransList["ExitBtn"].GetComponentInChildren<UILabel>().text                           = "Sair";
        //        _usedTransList["Desc"]         .GetComponent<UILabel>().text                                = "Gather all keypet, and Get items! (It consumes each keypet from all kinds when you play roulette)";
        //        _usedTransList["TextBox"].GetComponentInChildren<UILabel>().text                            = "If you collect all the keypets You can be the roulette.";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["RouletteBtn"].GetComponentInChildren<UILabel>().text                        = "Roleta";
        //         _usedTransList["ExitBtn"].GetComponentInChildren<UILabel>().text                           = "Sair";
        //        _usedTransList["Desc"]         .GetComponent<UILabel>().text                                = "Pegue todos os keypets e obtenha itens! (elimina cada keypet de todos os tipos quando você joga a roleta)";
        //        _usedTransList["TextBox"].GetComponentInChildren<UILabel>().text                            = "Se você coletar todos os keypets Você pode ser a roleta.";
        //        break;
        //}

        _usedTransList["RouletteBtn"].GetComponentInChildren<UILabel>().text = "Roleta";
        _usedTransList["ExitBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        _usedTransList["Desc"].GetComponent<UILabel>().text = "Pegue todos os keypets e obtenha itens! (elimina cada keypet de todos os tipos quando você joga a roleta)";
        _usedTransList["TextBox"].GetComponentInChildren<UILabel>().text = "Se você coletar todos os keypets Você pode ser a roleta.";
                
        //StartCoroutine(ShowGuard());
    }

    public override void OnEnter()
    {
        if (PlayerPrefs.GetInt("Keypet") == 1)
        {
            PlayerPrefs.SetInt("Keypet", 0);

            PlayerPrefs.Save();
        }
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }

/*
    public IEnumerator ShowGuard()
    {
        Transform scrollGuard = _usedTransList["ScrollGuard"];

        TweenAlpha alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 1.0f);
        alpha.from = 0.0f;
        alpha.method = UITweener.Method.EaseIn;

        yield return new WaitForSeconds(0.3f);

        alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 0.0f);
        alpha.from = 1.0f;
        alpha.method = UITweener.Method.EaseOut;

        yield return new WaitForSeconds(0.3f);

        alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 1.0f);
        alpha.from = 0.0f;
        alpha.method = UITweener.Method.EaseIn;

        yield return new WaitForSeconds(0.3f);

        alpha = TweenAlpha.Begin(scrollGuard.gameObject, 0.2f, 0.0f);
        alpha.from = 1.0f;
        alpha.method = UITweener.Method.EaseOut;
    }
*/

}
