﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GuiPlayFeverFront : GuiBase
{

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["FeverTime"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
		_usedTransList["FeverTime"].gameObject.SetActive(false);

		StartCoroutine(ShowFeverTimeDesc());
    }

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}


	protected IEnumerator ShowFeverTimeDesc()
	{
		GameObject go = _usedTransList["FeverTime"].gameObject;

		go.SetActive(true);

		TweenPosition tweenPos = TweenPosition.Begin(go, 0.7f, Vector3.zero);
		tweenPos.from = new Vector3(10000, 0.0f, 0.0f);
		tweenPos.method = UITweener.Method.EaseIn;

		yield return new WaitForSeconds(1.2f);

		tweenPos = TweenPosition.Begin(go, 0.5f, new Vector3(-10000, 0.0f, 0.0f));
		tweenPos.from = Vector3.zero;
		tweenPos.method = UITweener.Method.EaseOut;

		yield return new WaitForSeconds(0.6f);

		go.SetActive(false);
	}

}
