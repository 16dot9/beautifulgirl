﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class Kakao_RequestFriend_Window : GuiBase 
{
	public Dictionary<int, Kakao_RequestFriend_Item>	_FriendItemList = new Dictionary<int, Kakao_RequestFriend_Item> ();
	
	
	public UIEventListener ExitBtn
	{
		get { return UIEventListener.Get(_usedTransList["ExitBtn"].gameObject); }
	}
	
	public UIEventListener OkBtn
	{
		get { return UIEventListener.Get(_usedTransList["OkBtn"].gameObject); }
	}
	
	
	public void Add_KakaoFriendItem(Kakao_RequestFriend_Item item)
	{
		item.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();
		
		item.transform.parent = _usedTransList["UIGrid"];
		item.transform.localPosition = Vector3.zero;
		item.transform.localRotation = Quaternion.identity;
		item.transform.localScale = Vector3.one;
		
		_FriendItemList.Add(item.SerialID, item);
	}
	
	
	/// <summary>
	/// 모든 Add_KakaoFriendItem() 호출이 끝난 후에 반드시 호출할 것...
	/// </summary>
	public void ScrollView_RepositionNow()
	{
		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
		//_usedTransList ["UIGrid"].GetComponent<UIGrid> ().Reposition ();
	}
	
	
	/// <summary>
	/// "초대/조르기"한 카카오톡 친구 수를 표시함...
	/// </summary>
	public void ShowFriendCount (string text)
	{
		//_usedTransList ["Count"].GetComponent<UILabel> ().text = text;
	}
	
	// 테스트용...
	public void ResetPosition()
	{
		_usedTransList ["Items"].GetComponent<UIScrollView> ().ResetPosition ();
	}
	
	// 테스트용....
	public int GetGridChild()
	{
		return _usedTransList["UIGrid"].childCount;
	}
	
	
	public void ClearItems()
	{
		foreach (Kakao_RequestFriend_Item item in _FriendItemList.Values)
		{
			GameObject.DestroyImmediate(item.gameObject);
			//GameObject.Destroy(guiKakaotalkReqItem.gameObject);
		}
		
		_FriendItemList.Clear();
		
		//_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;	// 혹시나 하는 마음으로...
	}
	
	
	public override void OnCreate()
	{
		_usedTransList.Clear();
		
		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;
		
		_usedTransList["OkBtn"] = null;
		_usedTransList["ExitBtn"] = null;
		
		//_usedTransList["Count"] = null;	// 조르기한 친구 수 표시...
		
		FindTrans();
	}
	
	public override void OnEnter()
	{
	}
	
	public override void OnLeave()
	{
		ClearItems();
	}
	
	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

}
