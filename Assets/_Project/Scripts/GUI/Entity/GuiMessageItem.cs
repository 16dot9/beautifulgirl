﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class GuiMessageItem : GuiBase
{
    public int PackIdx
    {
        get;
        set;
    }

    public string UserID
    {
        get;
        set;
    }

	public uint SerialID
	{
		get;
		set;
	}

    public UIEventListener BuyBtn
    {
        get
        {
            return UIEventListener.Get(_usedTransList["BuyBtn"].gameObject);
        }
    }

	public void SetItemInfo(uint pSerialID, int packidx , Texture2D pFace, 
        string _GetMessage,string NickName, string pMessage)
	{
		SerialID = pSerialID;
        this.PackIdx = packidx;
        UserID = "";
        
        _usedTransList["GetMessage"].GetComponent<UILabel>().text = _GetMessage;
        _usedTransList["NickMessage"].GetComponent<UILabel>().text = NickName;

		_usedTransList["Face"].GetComponent<UITexture>().mainTexture = pFace;
		_usedTransList["Message"].GetComponent<UILabel>().text = pMessage;
    }
    public void SetItemInfo(string UserId, string Url,
        string _GetMessage, string NickName, string pMessage)
    {
        UserID = UserId;

        _usedTransList["GetMessage"].GetComponent<UILabel>().text = _GetMessage;
        _usedTransList["NickMessage"].GetComponent<UILabel>().text = NickName;

        _usedTransList["Message"].GetComponent<UILabel>().text = pMessage;

        StartCoroutine(FriendProfileImageLoad(Url));
    }
    IEnumerator FriendProfileImageLoad(string url)
    {
        WWW www = new WWW(url);

        yield return www;

        if (!String.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
        }
        else
        {
            _usedTransList["Face"].GetComponent<UITexture>().mainTexture = www.texture;
        }
    }

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Face"] = null;
		_usedTransList["Message"] = null;
        _usedTransList["GetMessage"] = null;
        _usedTransList["NickMessage"] = null;

		FindTrans();
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
    }

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

    public void Comfirm() 
    {
        Debug.Log("Comfirm! :" + UserID);

        if (UserID == "")
        {
            Debug.Log("PackIdx :" + PackIdx);

            byte[] _publicData =
                KakaoGameUserInfo.Instance.publicData == null ? null : (KakaoGameUserInfo.Instance.publicData);
            byte[] _privateData = KakaoGameUserInfo.Instance.privateData == null ? null : (KakaoGameUserInfo.Instance.privateData);

            switch(PackIdx)
            {
                case 29:
                    KakaoNativeExtension.Instance.updateUser(
                        1, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                    break;
                case 79:
                    KakaoNativeExtension.Instance.updateUser(
                        1, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 80:
                    KakaoNativeExtension.Instance.updateUser(
                        2, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 81:
                    KakaoNativeExtension.Instance.updateUser(
                        3, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 82:
                    KakaoNativeExtension.Instance.updateUser(
                        4, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 83:
                    KakaoNativeExtension.Instance.updateUser(
                        5, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 84:
                    KakaoNativeExtension.Instance.updateUser(
                        6, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 85:
                    KakaoNativeExtension.Instance.updateUser(
                        7, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
                case 120:
                    KakaoNativeExtension.Instance.updateUser(
                        1, _publicData, _privateData,
                        this.onUpdateUserComplete,
                        this.onUpdateUserError);
                break;
            }

            StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_READ_MSG_REQ((int)SerialID, this.gameObject));
        }
        else 
        { 
            KakaoNativeExtension.Instance.acceptGameMessage(UserID, onAcceptGameMessageComplete, onAcceptGameMessageError);
        }
    }

    private void onAcceptGameMessageComplete()
    {
//        ////KakaoNativeExtension.Instance.ShowAlertMessage("Completed accept message!");

//        KakaoMain.Instance.CheckUpdateHeart();
        
        GameObject.Find("GuiMessage").GetComponent<GuiMessage>().ClearItems();

        GSStageMap _gsstagemap = GameObject.Find("GSStageMap").GetComponent<GSStageMap>();

        _gsstagemap.KeypetMessageLoadCount();
        
       // KakaoNativeExtension.Instance.loadGameUserInfo(this.onGameUserInfoComplete, this.onGameUserInfoError);
    }
    private void onGameUserInfoComplete()
    {
        Debug.Log("onGameUserInfoComplete");
        string alertMessage = "";

        string user_id = KakaoGameUserInfo.Instance.user_id;
        string nickname = KakaoGameUserInfo.Instance.nickname;
        string profile_image_url = KakaoGameUserInfo.Instance.profile_image_url;
        string message_blocked = KakaoGameUserInfo.Instance.message_blocked == true ? "true" : "false";
        string exp = KakaoGameUserInfo.Instance.exp.ToString();
        string heart = KakaoGameUserInfo.Instance.heart.ToString();
        string heart_regen_starts_at = KakaoGameUserInfo.Instance.heart_regen_starts_at.ToString();

        MyInfoMgr.Instance.m_Power = KakaoGameUserInfo.Instance.heart;

        StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_LIST_MSG_REQ());
    }

    private void onGameUserInfoError(string status, string message)
    {
        Debug.Log("onGameUserInfoError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    private void onAcceptGameMessageError(string status, string message)
    {
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    private void onUpdateUserComplete()
    {
        Debug.Log("onUpdateUserComplete");

//        KakaoMain.Instance.CheckUpdateHeart();
    }

    private void onUpdateUserError(string status, string message)
    {
        Debug.Log("onUpdateUserError");
        KakaoMain.Instance.ShowKakaoMessageBox(status, message, "파워 충전");
    }
}
