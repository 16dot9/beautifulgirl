﻿using UnityEngine;
using System.Collections;

public class GuiPlayTutorialCutin : GuiBase {

    public delegate void DELEGATEBOOL(bool pIsOk);

    public DELEGATEBOOL OnButtonClick = null;

    public DELEGATEBOOL OnButtonSkipClick = null;

    protected bool _bEndItem;

    public enum Tutorial 
    {
        Tutorial_Start,
        Tutorial_End,
        Tutorial_Fish,
    }

    public override void OnCreate()
    {
        _usedTransList.Clear();

        _usedTransList["TutorialEnd"] = null;
        _usedTransList["TutorialStart"] = null;
        _usedTransList["TutorialFinish"] = null;

        _usedTransList["Item_01"] = null;
        _usedTransList["Item_01_BG"] = null;

        _usedTransList["Exit"] = null;
        _usedTransList["BG"] = null;

        _usedTransList["EndLabel"] = null;

        _usedTransList["Item_Label"] = null;

        _usedTransList["EndLabel"] = null;
        _usedTransList["FinishLabel"] = null;
        _usedTransList["StartLabel"] = null;

        FindTrans();

        UIEventListener.Get(_usedTransList["Exit"].gameObject).onClick = OnOk;
        UIEventListener.Get(_usedTransList["BG"].gameObject).onClick = OnSkipOk;

        //switch (Application.systemLanguage) 
        //{
        //    case SystemLanguage.English:

        //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Cheers for good work\n\nHow is it? It is really simple.\n\nYou have prepared \nthe game items as gift \nin return you have well come me a tutorial.\n\nSo, Now let's go to rescue Keypet.!\nGO GO GO";
        //        _usedTransList["FinishLabel"].GetComponent<UILabel>().text = "Get Item\n";
        //        _usedTransList["StartLabel"].GetComponent<UILabel>().text = "Welcome To come in. Keypet Break\n\nFrom now on, game the rules of the keypetbreak\nI'm going to briefly introduce you.\n\nAs it can learn it easy\nLook slowly along";
        //        _usedTransList["Exit"].GetComponentInChildren<UILabel>().text = "Ok";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Cheers for bom trabalho \n\n How é? É muito simples. \n\n prepararam itens de jogo nNão como presente \nin retornar você tem bem vir me um tutorial \n \nso, Agora vamos para resgatar Keypet \n GO GO GO ..!";
        //        _usedTransList ["FinishLabel"].GetComponent <UILabel> ().text = "Obter o item \n.";
        //         _usedTransList ["StartLabel"].GetComponent <UILabel> ().text = "Bem-vindo a entrar. Keypet Ruptura \n\n no de agora, jogo as regras do keypetbreak \nI estou indo introduzi-lo brevemente. \n \nComo ele pode aprender mais fácil \nlook devagar ";
        //         _usedTransList ["Exit"].GetComponentInChildren <UILabel> ().text = "Ok";
        //         break;
        //}
        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Cheers for bom trabalho \n\n How é? É muito simples. \n\n prepararam itens de jogo nNão como presente \nin retornar você tem bem vir me um tutorial \n \nso, Agora vamos para resgatar Keypet \n GO GO GO ..!";
        _usedTransList["FinishLabel"].GetComponent<UILabel>().text = "Obter o item \n.";
        _usedTransList["StartLabel"].GetComponent<UILabel>().text = "Bem-vindo(a) ao Pet Break. \n\n Antes de iniciar sua jornada para salvar seus amigos das mãos do malvado caçador, há algumas coisas que você precisa saber.";
//"Bem-vindo a entrar. Keypet Ruptura \n\n no de agora, jogo as regras do keypetbreak \nI estou indo introduzi-lo brevemente. \n \nComo ele pode aprender mais fácil \nlook devagar ";
        _usedTransList["Exit"].GetComponentInChildren<UILabel>().text = "Sim";
        
    }

    protected void OnOk(GameObject pGO)
    {
        if (null != OnButtonClick)
        {
            OnButtonClick(true);
            //OnButtonClick = null;
        }

        //GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, false, null);
    }

    protected void OnSkipOk(GameObject pGO)
    {
        if (null != OnButtonSkipClick)
        {
            OnButtonSkipClick(true);
            OnButtonSkipClick = null;
        }
    }

    public override void OnEnter()
    {
        _bEndItem = true;
    }

    public override void OnLeave()
    {
        _bEndItem = false;
    }

    public override void OnDelete()
    {
        _usedTransList.Clear();
    }

    public void TutorialAlarm(Tutorial _Tutorial) 
    {
        switch(_Tutorial)
        { 
            case Tutorial.Tutorial_Start:
            {
                _usedTransList["TutorialEnd"].gameObject.SetActive(false);
                _usedTransList["TutorialStart"].gameObject.SetActive(true);
                _usedTransList["TutorialFinish"].gameObject.SetActive(false);
                
            }break;
            case Tutorial.Tutorial_End:
            {
                _usedTransList["TutorialEnd"].gameObject.SetActive(true);
                _usedTransList["TutorialStart"].gameObject.SetActive(false);
                    _usedTransList["TutorialFinish"].gameObject.SetActive(false);
            }break;
            case Tutorial.Tutorial_Fish:
            {
                _usedTransList["TutorialEnd"].gameObject.SetActive(false);
                _usedTransList["TutorialStart"].gameObject.SetActive(false);
                _usedTransList["TutorialFinish"].gameObject.SetActive(true);
            }break;
        }
    }

    //< 우리나라는 고스톱 방향
    public void SetTextureItem(Texture2D _Item01, string _str , float _fSpeed) 
    {
        _usedTransList["Item_01"].GetComponent<UITexture>().mainTexture = _Item01;

        _usedTransList["Item_Label"].GetComponent<UILabel>().text = _str;
        StartCoroutine(IE_RotationITEM(_fSpeed));
        // _usedTransList["Item_02"].GetComponent<UITexture>().mainTexture = _item02;
    }

    //UI 셋팅
    protected IEnumerator IE_RotationITEM(float speed) 
    {
        while (_bEndItem) 
        {
            _usedTransList["Item_01_BG"].transform.Rotate(Vector3.forward * Time.deltaTime * speed); 

            yield return new WaitForFixedUpdate();
        }
    }

    public void SetEndLabel(bool isEnd) 
    {
        if (isEnd)
        {
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.English:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "[FFFFFF]Well done. How do you do? It was easy, wasn’t it? \n\n A well incorporated in this tutorial, I prepared it as a game [FFC800]items [FFFFFF]in return..\n\n And in key pet, so I am going to the rescue \nGO GO GO";
            //        break;
            //    case SystemLanguage.Korean:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "[FFFFFF]수고 하셨어요\n\n어때요? 정말 쉽죠?\n\n튜토리얼을 잘 따라와 준 보답으로\n\n게임 [FFC800]아이템[FFFFFF]을\n\n선물로 준비 했어요.\n\n그럼 이제 키펫을 구출하러\n\nGO GO GO";
            //        break;
            //    case SystemLanguage.Portuguese:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "[FFFFFF] Bem feito. Como vai você? Foi fácil, não era? \n\n A bem incorporado neste tutorial, eu preparei -lo como um jogo [FFC800] itens [FFFFFF] em troca .. \n \n E na chave animal de estimação, por isso estou indo para o resgate \n GO GO GO ";
            //        break;
            //}

            _usedTransList["EndLabel"].GetComponent<UILabel>().text = "[FFFFFF] Bem feito. Como vai você? Foi fácil, não era? \n\n A bem incorporado neste tutorial, eu preparei -lo como um jogo [FFC800] itens [FFFFFF] em troca .. \n \n E na chave animal de estimação, por isso estou indo para o resgate \n GO GO GO ";
                    
        }
        else
        {
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.English:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Cheers for good work\n\nHow is it? It is really simple.\n\nYou have prepared \nthe game items as gift \nin return you have well come me a tutorial.\n\nSo, Now let's go to rescue Keypet.!\nGO GO GO";
            //        break;
            //    case SystemLanguage.Korean:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "수고 하셨어요\n\n어때요? 정말 쉽죠?\n\n 그럼 이제 키펫을 구출하러\n\nGO GO GO";
            //        break;
            //    case SystemLanguage.Portuguese:
            //        _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Cheers for bom trabalho \n\n How é? É muito simples. \n\n prepararam itens de jogo nNão como presente \n in retornar você tem bem vir me um tutorial \n\n so, Agora vamos para resgatar Keypet \n GO GO GO ..!";
            //        break;
            //}
            _usedTransList["EndLabel"].GetComponent<UILabel>().text = "Bem Feito. Bom trabalho. \n\n Como é? É realmente simples. \n\n Você preparou os itens de jogo como presente, \n em troca de ter muito bem vir me um Tutorial. \n\n Então, agora vamos para resgatar Keypet! \n VA! VA! VA!";//"Cheers for bom trabalho \n\n How é? É muito simples. \n\n prepararam itens de jogo nNão como presente \n in retornar você tem bem vir me um tutorial \n\n so, Agora vamos para resgatar Keypet \n GO GO GO ..!";
                  
        }
    }
}

//손가락 황금색
