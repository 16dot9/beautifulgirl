﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiAttendanceItem : GuiBase
{

	public UIEventListener PowerBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["PowerBtn"].gameObject);
		}
	}


	public void SetName(string pName)
	{
		_usedTransList["Name"].GetComponent<UILabel>().text = pName;
	}

	public void SetRanking(int pRanking)
	{
		_usedTransList["Ranking"].GetComponent<UILabel>().text = pRanking.ToString();
	}

	public void SetStage(int pStage)
	{
		_usedTransList["Stage"].GetComponent<UILabel>().text = pStage.ToString();
	}


	public override void OnCreate()
	{
		_usedTransList.Clear();

		_usedTransList["Name"] = null;
		_usedTransList["Ranking"] = null;
		_usedTransList["Stage"] = null;
		_usedTransList["PowerBtn"] = null;

		FindTrans();
	}

	public override void OnEnter()
	{
	}

	public override void OnLeave()
	{
	}

	public override void OnDelete()
	{
		_usedTransList.Clear();
	}

}
