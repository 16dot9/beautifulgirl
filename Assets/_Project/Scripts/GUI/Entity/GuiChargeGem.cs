﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargeGem : GuiBase
{

	protected Dictionary<uint, GuiChargeGemItem>	_chargeGemItemList = new Dictionary<uint, GuiChargeGemItem>();


	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}
    
	public void AddChargeGemItem(GuiChargeGemItem pGuiChargeGemItem)
	{
		pGuiChargeGemItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiChargeGemItem.transform.parent = _usedTransList["UIGrid"];
		pGuiChargeGemItem.transform.localPosition = Vector3.zero;
		pGuiChargeGemItem.transform.localRotation = Quaternion.identity;
		pGuiChargeGemItem.transform.localScale = Vector3.one;

		_chargeGemItemList.Add(pGuiChargeGemItem.SerialID, pGuiChargeGemItem);

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}

	public void ClearItems()
	{
		foreach (GuiChargeGemItem guiChargeGemItem in _chargeGemItemList.Values)
		{
			GameObject.DestroyImmediate(guiChargeGemItem.gameObject);
		}

		_chargeGemItemList.Clear();
	}

    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["CloseBtn"] = null;
		_usedTransList["PesterBtn"] = null;

        _usedTransList["SetLabel"] = null;

        FindTrans();

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Gem Shop";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "파워 충전";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "확인";
        //        break;
        //    case SystemLanguage.Portuguese:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de Jóias";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //}

        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de Jóias";
        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
               
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearItems();
	}

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
