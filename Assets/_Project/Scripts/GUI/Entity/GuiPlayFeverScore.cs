﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GuiPlayFeverScore : GuiBase
{

    public override void OnCreate()
    {
	}

    public override void OnEnter()
    {
		StartCoroutine(Dir());
    }

    public override void OnLeave()
    {
	}

    public override void OnDelete()
    {
	}


	protected IEnumerator Dir()
	{
		yield return new WaitForSeconds(0.5f);

		TweenAlpha alpha = TweenAlpha.Begin(gameObject, 0.5f, 0.0f);
		alpha.from = 1.0f;
		alpha.method = UITweener.Method.Linear;

		yield return new WaitForSeconds(0.5f);

		GameObject.DestroyObject(gameObject);
	}

}
