﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;


public class GuiChargePower : GuiBase
{

	protected Dictionary<uint, GuiChargePowerItem>	_chargePowerItemList = new Dictionary<uint, GuiChargePowerItem>();


	public UIEventListener CloseBtn
	{
		get
		{
			return UIEventListener.Get(_usedTransList["CloseBtn"].gameObject);
		}
	}

	public void AddChargePowerItem(GuiChargePowerItem pGuiChargePowerItem)
	{
		pGuiChargePowerItem.GetComponent<UIDragScrollView>().scrollView = _usedTransList["Items"].GetComponent<UIScrollView>();

		pGuiChargePowerItem.transform.parent = _usedTransList["UIGrid"];
		pGuiChargePowerItem.transform.localPosition = Vector3.zero;
		pGuiChargePowerItem.transform.localRotation = Quaternion.identity;
		pGuiChargePowerItem.transform.localScale = Vector3.one;

		_chargePowerItemList.Add(pGuiChargePowerItem.SerialID, pGuiChargePowerItem);

		_usedTransList["UIGrid"].GetComponent<UIGrid>().repositionNow = true;
	}

	public void ClearItems()
	{
		foreach (GuiChargePowerItem guiChargePowerItem in _chargePowerItemList.Values)
		{
			GameObject.DestroyImmediate(guiChargePowerItem.gameObject);
		}

		_chargePowerItemList.Clear();
	}


    public override void OnCreate()
    {
		_usedTransList.Clear();

		_usedTransList["Items"] = null;
		_usedTransList["UIGrid"] = null;

		_usedTransList["CloseBtn"] = null;

        _usedTransList["SetLabel"] = null;

        FindTrans();

        //switch (Application.systemLanguage)
        //{
        //    case SystemLanguage.English:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Power Shop";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //    case SystemLanguage.Korean:
        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "파워 충전";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "확인";
        //        break;
        //    case SystemLanguage.Portuguese:

        //        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de força";
        //        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
        //        break;
        //}


        _usedTransList["SetLabel"].GetComponent<UILabel>().text = "Loja de força";
        _usedTransList["CloseBtn"].GetComponentInChildren<UILabel>().text = "Sair";
             
	}

    public override void OnEnter()
    {
    }

    public override void OnLeave()
    {
		ClearItems();
	}

    public override void OnDelete()
    {
		_usedTransList.Clear();
	}

}
