﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class InfoSave : MonoBehaviour {

    private static  InfoSave _Instance = null;

    public static InfoSave Instance 
    {
        get 
        {
            return _Instance;
        }
    }

    public bool isGameStage = false;
    public bool bClosedAD = false;

    public int powerSu = 5;
    public int funnySu = 500;
    public int gemSu = 0;
    public uint rouletteSu = 0;
    public bool rouletteFull = false;
    public bool rouletteTrueFalse = false;
    public bool rouletteButtonClick = false;
    public string _LoginDateTime = "";
	
    public int messageSu;

    public bool bgmOnOff = true;
    bool bgmActive = false;
    public bool effectSoundOnOff = true;
    bool effActive = false;
    public bool pushOnOff = true;
    bool pushActive = false;
    public bool kakaoNoticeOnOff = true;
    bool kakaoActive = false;

    public bool infoReset = false;

    static public bool mapScene = false;
    static public bool playScene = false;
    static public bool feverScene = false;

    public bool keypetMoveFinished = true;

    public int getDay;

    public int clearStageNumber;
    public int clearStageTopNumber;
    public double clearStageReball;
    public bool ReballClear = false;
    public bool FbUserNewe = false;
    public int item12Index = 12;
    public Vector3 clearTopStageCameraPosition = new Vector3(0, -260, 0);
    public bool stageClear = false;
    public bool gemLockClear = false;

    public double[] stageScores;
    public double nowScore;

    public bool rePlay = false;

    public int[] useitem;
    public int[] useitemMinus;

    public bool randomRescue;

    static public bool appStart = true;

    public int _maxAttendanceDay = 12;

    bool _cutScene = true;

    public Dictionary<int, string> _snotice = new Dictionary<int, string>();

    public int HeartReganTime;

    public int NextViliageTime = 0;

    public int _GetDayFreeHeart;

    private bool _OkAgreement;
    public bool OkAgreement 
    {
        get
        {
            if (ES3.KeyExists("OkAgreement"))
            {
                _OkAgreement = ES3.Load<bool>("OkAgreement");
            }
            return _OkAgreement;
        }
        set
        {
            _OkAgreement = value;
            ES3.Save("OkAgreement", _OkAgreement);
        }
    }

    private string _AccessFb;
    public string AccessToken 
    {
        get 
        {
            if (ES3.KeyExists("AccessToken")) 
            {
                _AccessFb = ES3.Load<string>("AccessToken");
            }
            return _AccessFb;
        }
        set 
        {
            _AccessFb = value;
            ES3.Save(_AccessFb, "AccessToken");
        }
    }
	
    public bool cutScene
    {
        get
        {
            if (ES3.KeyExists("cutScene"))
            {
                _cutScene = ES3.Load<bool>("cutScene");
            }
            return _cutScene;
        }
        set
        {
            _cutScene = value;
            ES3.Save("cutScene", _cutScene);
        }
    }

    bool _startTutorialIntro = true;	// 튜토리얼을 진행할지 여부..
    bool _startScnario = true;
    bool _startReballIntro = true;

    public void StartReballIntro(bool _intro) 
    {
        _startReballIntro = _intro;
        ES3.Save("startReballIntro", _startReballIntro);
    }

    public bool ReturnMyReballPlay() 
    {
        int StageNumber = clearStageNumber + 1;
        int StageTopNumber = clearStageTopNumber + 1;

        Debug.Log(StageNumber + " : " + StageTopNumber);

        Debug.Log((StageNumber == StageTopNumber).ToString());

        Debug.Log((StageNumber > 40).ToString());

        Debug.Log((StageNumber % 4 == 0).ToString());

        bool isReball = (StageNumber == StageTopNumber && StageNumber > 40
                && StageNumber % 4 == 0); //&& ReballClearStage() == false;

        return isReball;
    }

    public bool StartReballIntro()
    {        
        return _startReballIntro;
    }

    public void StartTutorialIntro(bool _intro) 
    {
        _startTutorialIntro = _intro;
        ES3.Save("startTutorialIntro", _startTutorialIntro);
    }

    public bool StartTutorialIntro() 
    {
        return _startTutorialIntro;
    }
    
    public bool startScnario
    {
        get
        {
            if (ES3.KeyExists("startScnario"))
            {
                _startScnario = ES3.Load<bool>("startScnario"); // 아래 set에서 _startTutorialIntro가 세팅되기 때문에 굳이 여기서 다시 load할 필요는 없지만, 안전을 위해....
            }
            return _startScnario;
        }
        set
        {
            _startScnario = value;
            ES3.Save("startScnario", _startScnario);
        }
    }

    bool _play_Intro = true;

    public bool playIntro
    {
        get
        {
            if (ES3.KeyExists("playIntro"))
            {
                _play_Intro = ES3.Load<bool>("playIntro");
            }
            return _play_Intro;
        }
        set
        {
            _play_Intro = value;
            ES3.Save("playIntro", _play_Intro);
        }
    }

    void Awake()
    {
        _Instance = this;

        if (ES3.KeyExists("cutScene"))
        {
            _cutScene = ES3.Load<bool>("cutScene");
        }
        if (ES3.KeyExists("startTutorialIntro"))
        {
            _startTutorialIntro = ES3.Load<bool>("startTutorialIntro");
        }
        if (ES3.KeyExists("playIntro"))
        {
            _cutScene = ES3.Load<bool>("playIntro");
        }
    }
    // Use this for initialization
    void Start()
    {
        clearTopStageCameraPosition = new Vector3(0, -260, 0);

        foreach (string ItemName in Enum.GetNames(typeof(MyInfoMgr.ITEM)))
        {
            Console.WriteLine("{0} = {1:D}", ItemName,
                              Enum.Parse(typeof(MyInfoMgr.ITEM), ItemName));
        }

        gemLockClear = false;

        //		string[] akeyWord = {"PowerSu" , "FunnySu", "GemSu"};
        //
        foreach (string ItemName in Enum.GetNames(typeof(MyInfoMgr.ITEM)))
        {
            if (ES3.KeyExists(ItemName))
            {
                MyInfoMgr.m_Item[(int)Enum.Parse(typeof(MyInfoMgr.ITEM), ItemName)] = ES3.Load<uint>(ItemName);
            }
        }
        //
        //
        //
        //		if (ES3.KeyExists("ScrollCameraLastPositionSave") != null) {
        //			if (ES3.KeyExists ("ScrollCameraLastPositionSave")) {
        //				clearStageTopNumber = ES3.Load<int> ("ClearTopStageSave");
        //				clearTopStageCameraPosition = ES3.Load<Vector3> ("ScrollCameraLastPositionSave");
        //			}
        //			if (ES3.KeyExists ("UseItem")) {
        //				useitem = ES3.LoadArray<int> ("UseItem");
        //			}
        //		}

        if(ES3.KeyExists("startTutorialIntro"))

        if (ES3.KeyExists("PowerSu"))
        {
            if (ES3.KeyExists("PowerSu"))
            {
                MyInfoMgr.Instance.m_Power =    ES3.Load<int>("PowerSu");
            }
        }

        if (ES3.KeyExists("FunnySu"))
        {
            if (ES3.KeyExists("FunnySu"))
            {
                MyInfoMgr.Instance.m_Funny =    ES3.Load<int>("FunnySu");
            }
        }

        if (ES3.KeyExists("GemSu"))
        {
            if (ES3.KeyExists("GemSu"))
            {
                MyInfoMgr.Instance.m_Gem =      ES3.Load<int>("GemSu");
            }
        }

        if (ES3.KeyExists("RouletteSu"))
        {
            if (ES3.KeyExists("RouletteSu"))
            {
                MyInfoMgr.Instance.m_Roulette = ES3.Load<uint>("RouletteSu");
            }
        }

        if (ES3.KeyExists("bgmOnOff"))
        {
            if (ES3.KeyExists("bgmOnOff"))
            {
                bgmOnOff = ES3.Load<bool>("bgmOnOff");
            }
        }

        if (ES3.KeyExists("effectSoundOnOff") != null)
        {
            if (ES3.KeyExists("effectSoundOnOff"))
            {
                effectSoundOnOff = ES3.Load<bool>("effectSoundOnOff");
            }
        }

        if (ES3.KeyExists("pushOnOff") != null)
        {
            if (ES3.KeyExists("pushOnOff"))
            {
                pushOnOff = ES3.Load<bool>("pushOnOff");
            }
        }

        if (ES3.KeyExists("kakaoNoticeOnOff") != null)
        {
            if (ES3.KeyExists("kakaoNoticeOnOff"))
            {
                kakaoNoticeOnOff = ES3.Load<bool>("kakaoNoticeOnOff");
            }
        }

        if (ES3.KeyExists("ClearTopStageSave") != null)
        {
            if (ES3.KeyExists("ClearTopStageSave"))
            {
                //< 최고 스테이지 갱신
                clearStageTopNumber = ES3.Load<int>("ClearTopStageSave");
            }
        }

        if (ES3.KeyExists("clearStageNumber") != null) 
        {
            if (ES3.KeyExists("clearStageNumber")) 
            {
                clearStageNumber = ES3.Load<int>("clearStageNumber");
            }
        }

        if (ES3.KeyExists("Item12Index") != null)
        {
            if (ES3.KeyExists("Item12Index"))
            {
                item12Index = ES3.Load<int>("Item12Index");
            }
        }

        if (ES3.KeyExists("HeartReganTime") != null) 
        {
            if (ES3.KeyExists("HeartReganTime")) 
            {
                HeartReganTime = ES3.Load<int>("HeartReganTime");
            }
        }

        if (ES3.KeyExists("NextViliageTime") != null) 
        {
            if (ES3.KeyExists("NextViliageTime"))
            {
                NextViliageTime = ES3.Load<int>("NextViliageTime");
            }
        }

        if (ES3.KeyExists("_LoginDateTime") != null)
        {
            if (ES3.KeyExists("_LoginDateTime"))
            {
                _LoginDateTime = ES3.Load<string>("_LoginDateTime");
            }
        }
        
        if (ES3.KeyExists("GetDayFreeHeart") != null) 
        {
            if (ES3.KeyExists("GetDayFreeHeart")) 
            {
                _GetDayFreeHeart = ES3.Load<int>("GetDayFreeHeart");
            }
        }
                     
        if (ES3.KeyExists("StageScores") != null)
        {
            if (ES3.KeyExists("StageScores"))
            {
                stageScores = ES3.Load<double[]>("StageScores");
            }
            else
            {
                stageScores = new double[1300];
                
                for (int i = 0; i < stageScores.Length; i++) 
                {
                    stageScores[i] = (double)0;
                }

                ES3.Save("StageScores", stageScores);
            }
        }

        if (ES3.KeyExists("ScrollCameraLastPositionSave") != null)
        {
            if (ES3.KeyExists("ScrollCameraLastPositionSave"))
            {
                clearStageTopNumber = ES3.Load<int>("ClearTopStageSave");
                clearTopStageCameraPosition = ES3.Load<Vector3>("ScrollCameraLastPositionSave");
            }
            if (ES3.KeyExists("UseItem"))
            {
                useitem = ES3.Load<int[]>("UseItem");
            }
        }

        if (ES3.KeyExists("clearStageReball") != null)
        {
            if (ES3.KeyExists("clearStageReball"))
            {
                clearStageReball = ES3.Load<double>("clearStageReball");
            }
            else
            {
                clearStageReball = 0;

                ES3.Save("clearStageReball", clearStageReball);
            }
        }

        if (ES3.KeyExists("ReballClear") != null) 
        {
            if (ES3.KeyExists("ReballClear"))
            {
                ReballClear = ES3.Load<bool>("ReballClear");
            }
            else 
            {
                ReballClear = false;
                ES3.Save("ReballClear", ReballClear);
            }
        }

        if (ES3.KeyExists("FbUserNewe") != null)
        {
            if (ES3.KeyExists("FbUserNewe"))
            {
                FbUserNewe = ES3.Load<bool>("FbUserNewe");
            }
            else
            {
                FbUserNewe = false;
                ES3.Save("FbUserNewe", FbUserNewe);
            }
        }

        if (ES3.KeyExists("startTutorialIntro") != null)
        {
            if (ES3.KeyExists("startTutorialIntro")) 
            {
                _startTutorialIntro = ES3.Load<bool>("startTutorialIntro"); // 아래 set에서 _startTutorialIntro가 세팅되기 때문에 굳이 여기서 다시 load할 필요는 없지만, 안전을 위해....
            }
            else {
                _startTutorialIntro = true;
               ES3.Save("startTutorialIntro", _startTutorialIntro);
            }
        }

        if (ES3.KeyExists("startReballIntro") != null)
        {
            if (ES3.KeyExists("startReballIntro")) 
            {
                _startReballIntro = ES3.Load<bool>("startReballIntro");
            }
            else
            {
                _startReballIntro = true;
                ES3.Save("startReballIntro", _startReballIntro);
            }
        
        }

        if (ES3.KeyExists("AccessToken") != null) 
        {
            
        }

        RouletteKeypetCountCheck();
    }

    // Update is called once per frame..
    void Update()
    {

        if (infoReset == true)
        {
            InfoReset();
            infoReset = false;
        }
    }
    public void SetLoginDateTime(string str)
    {
        _LoginDateTime = str;
        ES3.Save(_LoginDateTime, "_LoginDateTime");
    }

    public void SetSaveNotice(int idx, string str)
    {
        Debug.Log("idx :" + idx);

        _snotice[idx] = str;

        ES3.Save("Notice", _snotice);
    }

    public void SetHeartTime(int str) 
    {
        HeartReganTime = str;

        ES3.Save("HeartReganTime", HeartReganTime);
    }

    public void SetNextVillageTime(int str) 
    {
        NextViliageTime = str;

        ES3.Save("NextViliageTime", NextViliageTime);
    }

    public void SetGetDayFreeHeart(int getDayFree) 
    {
        _GetDayFreeHeart = getDayFree;

        ES3.Save("GetDayFreeHeart", _GetDayFreeHeart);
    }

    public void PowerSuPlusTime(int plus)
    {
        if (MyInfoMgr.Instance.m_Power <= 10)
        {
            MyInfoMgr.Instance.m_Power += plus;
            powerSu++;
        }
        else 
        {
            Debug.Log("Time Power");
                
            MyInfoMgr.Instance.m_Power = 10;
            powerSu++;
        }
        
        ES3.Save("PowerSu", MyInfoMgr.Instance.m_Power);
    }

    public void PowerSuPlus(int plus)
    {
        MyInfoMgr.Instance.m_Power += plus;
        powerSu++;
        ES3.Save("PowerSu", MyInfoMgr.Instance.m_Power);
    }

    public void PowerSuMinus()
    {
        MyInfoMgr.Instance.m_Power--;
        powerSu--;
        ES3.Save("PowerSu", MyInfoMgr.Instance.m_Power);

        MyInfoMgr.Instance.m_Power = ES3.Load<int>("PowerSu");
    }

    public void FunnySuPlus(int plus)
    {
        MyInfoMgr.Instance.m_Funny += plus;
        funnySu++;
        ES3.Save("FunnySu", MyInfoMgr.Instance.m_Funny);
    }

    public void FunnySuMinus(int plus)
    {
        MyInfoMgr.Instance.m_Funny -= (int)plus;
        funnySu--;
        ES3.Save("FunnySu", MyInfoMgr.Instance.m_Funny);
    }

    public void GemSuPlus(int plus)
    {
        MyInfoMgr.Instance.m_Gem += plus;
        gemSu++;
        ES3.Save("GemSu", MyInfoMgr.Instance.m_Gem);
    }

    public void GemSuMinus(int plus)
    {
        MyInfoMgr.Instance.m_Gem -= (int)plus;
        gemSu--;
        ES3.Save("GemSu", MyInfoMgr.Instance.m_Gem);
    }

    public void RouletteSuPlus(uint plus)
    {
        MyInfoMgr.Instance.m_Roulette += plus;
        rouletteSu++;
        //ES3.Save (MyInfoMgr.Instance.m_Roulette,"RouletteSu");
        ES3.Save("RouletteSu", rouletteSu);
        if (rouletteSu == 10)
        {
            rouletteFull = true;
        }
    }

    public void clearTutorialReball(double stage) 
    {
        clearStageReball = stage;
        ES3.Save("clearStageReball", clearStageReball);
    }

    public void ClearTopStage(double _nStage) 
    {
        clearStageReball = _nStage;

        if (clearStageNumber >= clearStageTopNumber)
        {
            if (clearStageReball < 0)
            {
                clearStageReball *= -1;

                clearStageReball++;
                SetReballClearStage(true);
                ES3.Save("clearStageReball", clearStageReball);
            }
            else
            {
                if (clearStageReball < 100)
                {
                    clearStageReball++;
                }
                    
                SetReballClearStage(true);
                ES3.Save("clearStageReball", clearStageReball);
            }
        }
        
    }

    public void SetReballClearStage(bool isOk)
    {
     
        ReballClear = isOk;
        ES3.Save("ReballClear", ReballClear);
     
    }

    public void FbUserNew(bool isNew)
    {
        FbUserNewe = isNew;
        ES3.Save("FbUserNewe", FbUserNewe);
    }

    public bool FBNEWUSER() 
    {
        return FbUserNewe;
    }

    public bool ReballClearStage() 
    {
        return ReballClear;
    }

    public void RouletteFail()
    {
        GameObject parentObject = GameObject.Find("Gui/Front/Camera").transform.gameObject;
        GameObject childPrefab = Resources.Load<GameObject>("Gui/GuiRouletteFail");
        NGUITools.AddChild(parentObject, childPrefab);
    }

    public void RouletteSuFull()
    {
        GameObject parentObject = GameObject.Find("Gui/Front/Camera").transform.gameObject;
        GameObject childPrefab = Resources.Load<GameObject>("Gui/GuiRouletteSuFull");
        NGUITools.AddChild(parentObject, childPrefab);
    }

    void InfoReset()
    {
        MyInfoMgr.Instance.m_Power = powerSu;
        MyInfoMgr.Instance.m_Funny = funnySu;
        MyInfoMgr.Instance.m_Gem = gemSu;
        MyInfoMgr.Instance.m_Roulette = rouletteSu;

        clearStageTopNumber = 1;
        clearTopStageCameraPosition = new Vector3(0, -260, 0);

        item12Index = 12;

        for (int i = 0; i <= useitem.Length - 1; i++)
        {
            useitem[i] = 0;
        }

        ES3.DeleteKey("PowerSu");
        ES3.DeleteKey("FunnySu");
        ES3.DeleteKey("GemSu");
        ES3.DeleteKey("RouletteSu");
        ES3.DeleteKey("bgmOnOff");
        ES3.DeleteKey("effectSoundOnOff");
        ES3.DeleteKey("pushOnOff");
        ES3.DeleteKey("kakaoNoticeOnOff");
        ES3.DeleteKey("ClearTopStageSave");
        ES3.DeleteKey("Item12Index");
        ES3.DeleteKey("StageScores");
        ES3.DeleteKey("ScrollCameraLastPositionSave");
        ES3.DeleteKey("cutScene");
        ES3.DeleteKey("startTutorialIntro");
        ES3.DeleteKey("playIntro");
        ES3.DeleteKey("UseItem");

        //ES3.DeleteKey ("InviteInfo");
    }

    public void BgmOnOff()
    {
        if (bgmOnOff == true && bgmActive == false)
        {
            bgmOnOff = false;
            bgmActive = true;
        }

        if (bgmOnOff == false && bgmActive == false)
        {
            bgmOnOff = true;
            bgmActive = true;
        }

        bgmActive = false;

        ES3.Save("bgmOnOff", bgmOnOff);
    }

    public void EffectSoundOnOff()
    {

        if (effectSoundOnOff == true && effActive == false)
        {
            effectSoundOnOff = false;
            effActive = true;
        }

        if (effectSoundOnOff == false && effActive == false)
        {
            effectSoundOnOff = true;
            effActive = true;
        }

        effActive = false;

        ES3.Save("effectSoundOnOff", effectSoundOnOff);
    }

    public void PushOnOff()
    {

        if (pushOnOff == true && pushActive == false)
        {
            pushOnOff = false;
            pushActive = true;
        }

        if (pushOnOff == false && pushActive == false)
        {
            pushOnOff = true;
            pushActive = true;
        }

        pushActive = false;

        ES3.Save("pushOnOff", pushOnOff);
    }

    public void KakaoNoticeOnOff()
    {

        if (kakaoNoticeOnOff == true && kakaoActive == false)
        {
            kakaoNoticeOnOff = false;
            kakaoActive = true;
        }

        if (kakaoNoticeOnOff == false && kakaoActive == false)
        {
            kakaoNoticeOnOff = true;
            kakaoActive = true;
        }

        kakaoActive = false;

        ES3.Save("kakaoNoticeOnOff", kakaoNoticeOnOff);
    }

    public void MapScene()
    {
        mapScene = true;
        playScene = false;
    }

    public void PlayScene()
    {
        mapScene = false;
        playScene = true;
    }

    public bool ReturnLinkedStageInfo() 
    {
        return (clearStageNumber % (int)MaxLinkedStageInfo.count == 0);
    }

    public bool FirstStageClear(int stage) 
    {
        if (stageScores[stage - 1] == 0)
            return true;

        return false;
    }

    public void ClearStage(int stage) 
    {
        StageScoreSave();

        if ((clearStageTopNumber % (int)MaxLinkedStageInfo.count == 0) == false
            && clearStageReball < 100)
        {
            if (ReballClear)
            {
                SetReballClearStage(false);
            }
        }

        clearStageNumber = stage;

        ES3.Save("clearStageNumber", clearStageNumber);

        if( (clearStageTopNumber % (int)MaxLinkedStageInfo.count != 0) && clearStageTopNumber <= stage )
        {
            clearStageTopNumber = clearStageTopNumber + 1;
            clearStageNumber = stage + 1;
        }


        ES3.Save("ClearTopStageSave", clearStageTopNumber);
        ES3.Save("Item12Index", item12Index);
        if (clearStageNumber > 5)
        {
            ES3.Save("ScrollCameraLastPositionSave", clearTopStageCameraPosition);
        }
    }

    public void ClearStageNumberSave()
    {
        if (gemLockClear == false)
        {
            StageScoreSave();
        }
    
        if((clearStageTopNumber % (int)MaxLinkedStageInfo.count == 0) == false
            && clearStageReball < 100)
        {
            if (ReballClear)
            {
                SetReballClearStage(false);
            }
        }

        if ((clearStageNumber % (int)MaxLinkedStageInfo.count == 0))
        {
            KakaoMain.Instance.UseMapHelpNextViliage();
        }

        if ((clearStageTopNumber % (int)MaxLinkedStageInfo.count != 0) || gemLockClear == true)
        {
            if (gemLockClear)
            {
                SetNextVillageTime(0);
            }

            clearStageTopNumber = clearStageTopNumber + 1;

            clearStageNumber = clearStageTopNumber;
        }

        ES3.Save("ClearTopStageSave", clearStageTopNumber);
        ES3.Save("Item12Index", item12Index);
        if (clearStageNumber > 5)
        {
            ES3.Save("ScrollCameraLastPositionSave", clearTopStageCameraPosition);
        }
        

        gemLockClear = false;
    }

    public bool Unlock_Precondition() 
    {
        return  (stageScores[clearStageTopNumber-1] > 0 && ((clearStageTopNumber % (int)MaxLinkedStageInfo.count) == 0));
    }

    public void ClearStageLockViliage() 
    {
        if (gemLockClear)
            clearStageTopNumber = clearStageTopNumber + 1;

        ES3.Save("ClearTopStageSave", clearStageTopNumber);
        ES3.Save("Item12Index", item12Index);
        if (clearStageNumber > 5)
        {
            ES3.Save("ScrollCameraLastPositionSave", clearTopStageCameraPosition);
        }

        ES3.DeleteKey("NextViliageTime");

        gemLockClear = false;
    }

    public void StageScoreSave()
    {
        if (clearStageNumber == clearStageTopNumber)
        {
            stageScores[clearStageNumber - 1] = (nowScore);
            //			Debug.Log (stageScores[clearStageNumber-1]);
        }

        if (clearStageNumber < clearStageTopNumber)
        {
            if ((int)stageScores.Length == clearStageNumber - 1)
            {
                if (nowScore > (double)stageScores[clearStageNumber - 1])
                {
                    stageScores[clearStageNumber - 1] = (nowScore);
                }
            }
        }
        ES3.Save("StageScores", stageScores);
    }


    public bool HighClearStage(int _nStage, double _nNowScore)
    {
        if (stageScores[_nStage - 1] <= _nNowScore)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    public void InGameItemHaveUpdate(int setNumber, int addCount)
    {
        if (setNumber >= 1 && setNumber <= 18)
        {
            //GemSuPlus((int)addCount);
            useitem[setNumber] += addCount;
        }

        if (setNumber == 6)
        {
            useitem[2]++;
            useitem[3]++;
            //useitem[6]--;
        }

        if (setNumber == 7)
        {
            useitem[4]++;
            useitem[5]++;
            //useitem[7]--;
        }

        if (setNumber == 17)
        {
            useitem[14]++;
            useitem[15]++;
            //useitem[17]--;
        }

        if (setNumber == 29 || (setNumber >= 49 && setNumber <= 53) || (setNumber >= 79 && setNumber <= 85))
        {
            PowerSuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }

        if ((setNumber >= 19 && setNumber <= 21) || (setNumber >= 25 && setNumber <= 27) || (setNumber >= 59 && setNumber <= 62) || (setNumber >= 70 && setNumber <= 78))
        {
            FunnySuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }

        if ((setNumber >= 22 && setNumber <= 23) || (setNumber == 28) || (setNumber >= 63 && setNumber <= 69) || (setNumber == 107) || (setNumber >= 109 && setNumber <= 118))
        {
            GemSuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }

        ES3.Save("UseItem", useitem);
    }

    //아이템 선택 후 사용
    public void ItemUseHave(uint setNumber)
    {
        for (int i = 0; i < useitem.Length; i++)
        {
            if (i == setNumber) 
            {
                useitem[i]--;
            }
        }

        ES3.Save("UseItem", useitem);
    }

    public void ItemGetRewardHave(uint setNumber, int rewardCount)
    {
        for (int i = 0; i < useitem.Length; i++)
        {
            if (i == setNumber)
            {
                useitem[i] += rewardCount;
            }
        }

        ES3.Save("UseItem", useitem);
    }

    //< 함수 복사
    public void ItemHaveUpdate(uint setNumber, int addCount)  
    {
        if (setNumber >= 1 && setNumber <= 18)
        {
            useitem[setNumber] += addCount;
        }

        if (setNumber == 6)
        {
            useitem[2]++;
            useitem[3]++;
            //useitem[6]--;
        }

        if (setNumber == 7)
        {
            useitem[4]++;
            useitem[5]++;
            //useitem[7]--;
        }

        if (setNumber == 17)
        {
            useitem[14]++;
            useitem[15]++;
            //useitem[17]--;
        }

        ES3.Save("UseItem", useitem);
    }

    public void ItemHaveUpdate(int setNumber, int addCount)
    {
        StartCoroutine(IE_ItemHaveUpdate(setNumber, addCount));

        if (setNumber >= 1 && setNumber <= 18)
        {
            //GemSuPlus((int)addCount);
            useitem[setNumber] += addCount;
        }

        if (setNumber == 6)
        {
            useitem[2]++;
            useitem[3]++;
            //useitem[6]--;
        }

        if (setNumber == 7)
        {
            useitem[4]++;
            useitem[5]++;
            //useitem[7]--;
        }

        if (setNumber == 17)
        {
            useitem[14]++;
            useitem[15]++;
            //useitem[17]--;
        }

        if (setNumber == 29 || (setNumber >= 49 && setNumber <= 53) || (setNumber >= 79 && setNumber <= 85))
        {
            Debug.Log("SetNumber PowerSuPlus!");
            PowerSuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }

        if ((setNumber >= 19 && setNumber <= 21) || (setNumber >= 25 && setNumber <= 27) || (setNumber >= 59 && setNumber <= 62) || (setNumber >= 70 && setNumber <= 78))
        {
            Debug.Log("SetNumber Funny!");
            FunnySuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }

        if ((setNumber >= 22 && setNumber <= 23) || (setNumber == 28) || (setNumber >= 63 && setNumber <= 69) || (setNumber == 107) || (setNumber >= 109 && setNumber <= 118))
        {
            Debug.Log("SetNumber GemSuPlus!");
            GemSuPlus(addCount);
            //useitem [setNumber] -=addCount;
        }
        ES3.Save("UseItem", useitem);
    }

    IEnumerator IE_ItemHaveUpdate(int setNumber, int addCount)
    {
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

        //구 입 페키지 에 따라 메시지 출력
        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();
        PackageInfo pack_info = packageInfoMgr.GetPackageInfo((uint)setNumber);

        GuiItemBuyPopupOK popup = GuiMgr.Instance.Show<GuiItemBuyPopupOK>(GuiMgr.ELayerType.Front, true, null);
        popup.CloseBtn.onClick = (GameObject pGO) =>
        {
            GuiMgr.Instance.Show<GuiItemBuyPopupOK>(GuiMgr.ELayerType.Front, false, null);
        };

        yield return StartCoroutine(popup.ShowPack(pack_info));
    }

    public void ItemHaveMinus()
    {
        for (int i = 0; i < useitem.Length; i++)
        {
            useitem[i] -= useitemMinus[i];
        }
        ES3.Save("UseItem", useitem);
    }

    public void RePlay()
    {
        rePlay = false;
        GameObject stageButton = null;
        GameObject obj_maps = GameObject.Find("Gui/GuiStageMapInfoScrollView/Items/Maps");
        ShopItemIndexNumber[] arr_stagebtn = null;
        if (obj_maps)
        {
            arr_stagebtn = obj_maps.GetComponentsInChildren<ShopItemIndexNumber>();
            foreach (ShopItemIndexNumber itemNumber in arr_stagebtn)
            {
                if (itemNumber.thisIndex == clearStageNumber)
                {
                    stageButton = itemNumber.gameObject;
                    break;
                }
            }
        }
        if (stageButton)
        {
            GuiStageMapInfoScrollViewStageItemStage stageitem = stageButton.transform.parent.GetComponent<GuiStageMapInfoScrollViewStageItemStage>();
            stageitem.OnStageBtnClick(stageButton);
        }
    }


    public bool RouletteCountCheck()
    {
        for (int i = 8; i < 14; i++)
        {
            if (useitem[i] != 0)
            {
                rouletteTrueFalse = true;
            }
            else {
                rouletteTrueFalse = false;
                break;
            }
        }

        return rouletteTrueFalse;
        //KeypetCountMinus ();
    }

    public bool RouletteNewCheck()
    {
        bool RouletteNewCheck = false;

        for (int i = 8; i < 14; i++)
        {
            if (useitem[i] != 0)
            {
                RouletteNewCheck = true;
                break;
            }
            else
            {
                RouletteNewCheck = false;
            }
        }

        return RouletteNewCheck;
        //KeypetCountMinus ();
    }

    public void RouletteKeypetCountCheck()
    {
        for (int i = 8; i < 14; i++)
        {
            if (useitem[i] == 0)
            {
                rouletteTrueFalse = false;
                return;
            }
            rouletteTrueFalse = true;
        }
        //KeypetCountMinus ();
    }

    public void KeypetCountMinus()
    {
        for (int i = 8; i < 14; i++)
        {
            useitem[i]--;
        }
        ES3.Save("UseItem", useitem);
    }
}