﻿using UnityEngine;
using System.Collections;

public class IndexShopToItem : MonoBehaviour {

	public uint returnItemIndex;

	public void GetIndexShopToItem(uint shopIndexNumver){
		switch (shopIndexNumver)
		{
		case 4:
			//긴급구출
			returnItemIndex = 3;
			break;
		case 5:
			//한번물리기
			returnItemIndex = 1;
			break;
		case 6:
			//턴증가
			returnItemIndex = 4;
			break;
		case 7:
			//시간증가
			returnItemIndex = 5;
			break;
		case 8:
			//렌덤구출
			returnItemIndex = 2;
			break;
		case 16:
			//피버시간증가
			returnItemIndex = 14;
			break;
		}
	}
}
