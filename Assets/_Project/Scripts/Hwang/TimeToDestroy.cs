﻿using UnityEngine;
using System.Collections;

public class TimeToDestroy : MonoBehaviour {

	public float destroyTime;
	public bool thisDestroy;

	void Start () {
	
		Destroy (gameObject, destroyTime);
	}
}
