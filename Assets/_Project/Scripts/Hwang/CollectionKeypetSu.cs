﻿using UnityEngine;
using System.Collections;

public class CollectionKeypetSu : MonoBehaviour {

	public int thisKeypetIndex;
    public Inventory_Item _inv = null;
    private int index;
	// Use this for initialization
	void Start () {
//        _inv = Cmd_Handler_CS.Instance.Get_Inventory_Item(thisKeypetIndex);
	}

    public void OnEnable()
    {
        if (thisKeypetIndex > 0)
        {
            if (GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex] > 0)
            {
                transform.GetComponent<UILabel>().text = GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex].ToString();
                index = GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex];
            }
            else 
            {
                transform.GetComponent<UILabel>().text = "0";
            }
        }
        else
        {
            transform.GetComponent<UILabel>().text = "0";
        }
    }
    public void DisplayCount()
    {
        if (_inv != null)
        {
            transform.GetComponent<UILabel>().text = GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex - 1].ToString(); //_inv._qty.ToString();
        }
    }

	// Update is called once per frame
	void LateUpdate () {

        if (GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex] > 0)
        {
            transform.GetComponent<UILabel>().text = "" + GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex];
            index = GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex];
        }

        if (GameObject.Find("InfoMgr").GetComponent<InfoSave>().useitem[thisKeypetIndex] <= 0)
        {
            transform.GetComponent<UILabel>().text = "0";
            index = 0;
        }
        
	}

    public int GetCountIndex() 
    {
        return index;
    }
}
