using UnityEngine;
using System.Collections;

public class HUDFPS : MonoBehaviour
{

	public bool isUnityGui;
	public bool isNgui;
	
		public  float updateInterval = 0.05F;
		private float accum = 0; // FPS accumulated over the interval
		private int   frames = 0; // Frames drawn over the interval
		private float timeleft; // Left time for current interval
 
		void Start ()
		{
				timeleft = updateInterval;  
		}
 
		void Update ()
		{
				timeleft -= Time.deltaTime;
				accum += Time.timeScale / Time.deltaTime;
				++frames;
 
				// Interval ended - update GUI text and start new interval
				if (timeleft <= 0.0) {
						// display two fractional digits (f2 format)
						float fps = accum / frames;
						string format = System.String.Format ("{0:F2} FPS", fps);
						
						//if(isUnityGui == true){
						//	GetComponent<GUIText>().text = format;
	 
						//	if (fps < 30)
						//			GetComponent<GUIText>().material.color = Color.yellow;
						//	else 
						//	if (fps < 10)
						//			GetComponent<GUIText>().material.color = Color.red;
						//	else
						//			GetComponent<GUIText>().material.color = Color.green;
						//	//	DebugConsole.Log(format,level);
						//	timeleft = updateInterval;
						//	accum = 0.0F;
						//	frames = 0;
						//}

						if(isNgui == true){
							
							transform.GetComponent<UILabel>().text = "" + format;
							timeleft = updateInterval;
							accum = 0.0F;
							frames = 0;
						}
				}
		}
}
