﻿using UnityEngine;
using System.Collections;

public class MapCloudControl : MonoBehaviour {

	bool isMap001 = false;
	bool isMap002 = false;
	int mapNumber;
	int nameNumber;
	GameObject thisObject;

	// Use this for initialization
	void Start () {
	
		if (transform.name == "Map_001") {
			isMap001 = true;
		}
		if (transform.name == "Map_002") {
			isMap002 = true;
		}
	}
	
	// Update is called once per frame
	void Update () {

		mapNumber = (int)(transform.localPosition.y / 2048f);
		nameNumber = (mapNumber % 8) + 1;
		thisObject = transform.gameObject;
        if (nameNumber == 0)
        {
            thisObject.GetComponent<UISprite>().spriteName = "Map_2";
        }
        else
        {
            thisObject.GetComponent<UISprite>().spriteName = "Map_" + nameNumber;
        }
	
		if(isMap001 == true){
			if(transform.Find("1/StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex <= GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber){
				transform.Find("Cloud").gameObject.SetActive(false);
			}
			if(transform.Find("1/StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex > GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber){
				transform.Find("Cloud").gameObject.SetActive(true);
			}
		}
	
		if(isMap002 == true){
			if(transform.Find("12/StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex <= GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber + 11){
				transform.Find("Cloud").gameObject.SetActive(false);
			}
			if(transform.Find("12/StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex > GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber + 11){
				transform.Find("Cloud").gameObject.SetActive(true);
			}
		}
	}
}
