﻿using UnityEngine;
using System.Collections;

public class NowPowerInfo : MonoBehaviour {

	public bool isPowersu;
	public bool isFunnysu;
	public bool isGemsu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void LateUpdate()
    {
        if (isPowersu == true)
        {
            transform.GetComponent<UILabel>().text = "+" + MyInfoMgr.Instance.m_Power;
        }

        if (isFunnysu == true)
        {
            transform.GetComponent<UILabel>().text = "+" + MyInfoMgr.Instance.m_Funny;
        }

        if (isGemsu == true)
        {
            transform.GetComponent<UILabel>().text = "+" + MyInfoMgr.Instance.m_Gem;
        }
    }
}
