﻿using UnityEngine;
using System.Collections;

public class LinkUrl : MonoBehaviour {

	public bool isTwitter;
	public bool isFacebook;
	public bool isNaver;

	void OnClick(){
		if(isTwitter == true){
            Application.OpenURL("https://twitter.com/Keypebreak");
		}

		if(isFacebook == true){
			Application.OpenURL ("https://www.facebook.com/playspotonline");
		}

		if(isNaver == true){
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.Playspot.PetBreak");//"http://cafe.naver.com/keypetbreak");
		}
	}
}
