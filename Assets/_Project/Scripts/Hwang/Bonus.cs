﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {

	public RuntimeAnimatorController bonus;
	public RuntimeAnimatorController[] dones;

	public float changeTime = 0.1f;
	// Use this for initialization
	void Start () {
	
		//Animator _this = transform.GetComponent<Animator>;
		transform.GetComponent<Animator>().runtimeAnimatorController = dones [Random.Range (0, dones.Length)];
		transform.GetComponent<Animator> ().SetTrigger("Done");
		StartCoroutine(WaitAndPrint(changeTime));

	}
	
	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		transform.GetComponent<Animator>().runtimeAnimatorController = bonus;
		transform.GetComponent<Animator> ().SetTrigger("bonus");
	}
}
