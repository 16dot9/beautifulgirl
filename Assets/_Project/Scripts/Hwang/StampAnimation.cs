﻿using UnityEngine;
using System.Collections;

public class StampAnimation : MonoBehaviour {

	public int number;

	// Use this for initialization
	void Start () {

        StartCoroutine(WaitAndPrint(1.0F));
		
	}
	
	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
        if (number == GameObject.Find("InfoMgr").GetComponent<InfoSave>().getDay)
        {
            transform.GetComponent<TweenScale>().enabled = true;
            transform.GetComponent<TweenAlpha>().enabled = true;
        }
		
	}
}
