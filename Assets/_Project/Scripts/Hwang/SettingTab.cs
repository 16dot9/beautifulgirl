﻿using UnityEngine;
using System.Collections;

public class SettingTab : MonoBehaviour {

	public bool isSetup;
	public bool isKakaoSetup;

	GameObject bgObj;


	void Start () {
	
		bgObj = GameObject.Find ("Gui/Front/Camera/GuiGameSetting/AnchorCenter/BG");
	}
	
	void OnClick(){

		if(isSetup == true){
			transform.parent.Find("NoticeBtn").gameObject.SetActive(true);

			bgObj.transform.Find("GameSetting").gameObject.SetActive(true);
			bgObj.transform.Find("kakaoSetting").gameObject.SetActive(false);

			transform.gameObject.SetActive(false);
		}

		if(isKakaoSetup == true){
			transform.parent.Find("SetupBtn").gameObject.SetActive(true);

			bgObj.transform.Find("kakaoSetting").gameObject.SetActive(true);
			bgObj.transform.Find("GameSetting").gameObject.SetActive(false);

			transform.gameObject.SetActive(false);
		}
	}
}
