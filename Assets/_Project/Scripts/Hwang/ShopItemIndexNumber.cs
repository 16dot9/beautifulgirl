﻿using UnityEngine;
using System.Collections;

public class ShopItemIndexNumber : MonoBehaviour {

	public int thisIndex;

	public bool isStageButton;
	public bool isShopButton;

	// Use this for initialization
	void Start () {
	
		LockImage ();
		//Debug.Log ("PopUP " + thisIndex);
	}

	public void LockImage ()
	{
		if (isStageButton == true)
		{
			if(thisIndex > GameObject.Find("InfoMgr").GetComponent<InfoSave>().clearStageTopNumber){
				transform.Find("BG").GetComponent<UISprite>().spriteName = "stagePoint_lock 1";
				transform.Find("Stage").gameObject.SetActive(false);
			}
			else{
				transform.Find("BG").GetComponent<UISprite>().spriteName = "stagePoint_idle 1";
				transform.Find("Stage").gameObject.SetActive(true);
			}
		}
	}
}
