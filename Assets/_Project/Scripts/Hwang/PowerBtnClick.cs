﻿using UnityEngine;
using System.Collections;
using System.Text;
using SimpleJSON;


public class PowerBtnClick : MonoBehaviour {

    public GuiStageRankingItem _Ranking;

	public bool isRankingButton;
	public int getPower;

	int test;

	void OnClick(){
        if (_Ranking.UserID != KakaoLocalUserController.Instance.GetUserAccount())
        { 
            JSONNode msgParam = new JSONClass();

            msgParam["message"] = _Ranking.NickName + "님에게 카카오톡으로 파워를 보내시겠습니까?";
            msgParam["show_type"] = "two";		// "ok", "cancel" two button...

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk)
                {
                    Debug.Log("UserId :" + _Ranking.UserID);

                    string publicData = "itemId|0000101";
                    KakaoNativeExtension.Instance.sendGameMessage(
                        _Ranking.UserID,
                        KakaoLocalUser.Instance.nickName + " 님이 파워를 보내셨어요! 다시 힘내서 키펫 세계로 떠나볼까요?",
                        "This is game message. You can check this message in game.",
                        1,
                        null,
                        Encoding.UTF8.GetBytes(publicData),
                        onSendGameMessageComplete,
                        onSendGameMessageError);
                }
            };
        }
	}

    private void onSendGameMessageComplete()
    {
        Debug.Log("Success");
        
//        //KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendGameMessage");
    }
    private void onSendGameMessageError(string status, string message)
    {
        KakaoMain.Instance.ShowKakaoMessageBox(status, message, "하트 보내기");
    }
}
