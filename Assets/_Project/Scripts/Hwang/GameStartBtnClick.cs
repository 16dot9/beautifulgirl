﻿using UnityEngine;
using System.Collections;

public class GameStartBtnClick : MonoBehaviour {

	public float waitTime;

	// Use this for initialization
	void Start () {
	
	}
	
	void OnClick(){
		Debug.LogError ("GameButton");

		StartCoroutine(WaitAndPrint(waitTime));
	}

	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		transform.Find ("PowerFull").GetComponent<UISprite> ().spriteName = "GameStartPowerIcon";		
	}
}
