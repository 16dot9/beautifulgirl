﻿using UnityEngine;
using System.Collections;

public class CreatedButton : MonoBehaviour {

	public GameObject parentObject;
	public GameObject createdPopup;

	void Start(){
		parentObject = GameObject.Find ("Gui/Front/Camera").gameObject;
	}

	void OnClick () {
	
		NGUITools.AddChild(parentObject, createdPopup);
	}
}
