﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class SettingOnOff : MonoBehaviour {

	public bool isBgmOnOff;
	public bool isEffectSoundOnOff;
	public bool isPushOnOff;
	public bool isKakaoNoticeOnOff;

	void Start(){
		StartCoroutine(OnOffImageChange(0.2F));
	}

	void OnClick(){
		StartCoroutine(OnOffImageChange(0.1F));

        Debug.Log(GameObject.Find("InfoMgr").GetComponent<InfoSave>().bgmOnOff);

		if(isBgmOnOff == true){
			GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().BgmOnOff();
			if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().bgmOnOff == true){
                if (InfoSave.mapScene == true)
                {
                    AudioMgr.Instance.Play("StageMap");
				}

				if(InfoSave.playScene == true){
					AudioMgr.Instance.Play("InGameBG");
				}

				if(InfoSave.feverScene == true){
					AudioMgr.Instance.Play("FeverBG");
				}
			}
			if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().bgmOnOff == false){
				if(InfoSave.mapScene == true){
                    AudioMgr.Instance.Stop("StageMap");
				}

				if(InfoSave.playScene == true){
					AudioMgr.Instance.Stop("InGameBG");
				}

				if(InfoSave.feverScene == true){
					AudioMgr.Instance.Stop("FeverBG");
				}
			}
		}
	
		if(isEffectSoundOnOff == true){
			GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().EffectSoundOnOff();
		}

		if(isPushOnOff == true){
			GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().PushOnOff();
		}



		if(isKakaoNoticeOnOff == true)		// 버튼 클릭 시...
		{
			if (KakaoLocalUserController.Instance.MessageBlocked())	// 카톡 서버에서 유저가 카톡 메시지 수신 차단 상태라면...
			{
				KakaoMain.Instance.ShowKakaoMessageBox ("카카오톡 메시지수신 차단 확인", null, null);	// "카카오톡 앱 설정에서 메시지 수신을 체크하라"는 메시지 출력... 버튼은 off 상태를 계속 유지힘...
			}
			else
			{
				if (GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().kakaoNoticeOnOff == true)	// 버튼이 on 상태일 때...
				{
					JSONNode msgParam = new JSONClass();
					msgParam["message"] = "더이상 카카오톡 메시지를 받을 수 없게 됩니다. 정말로 차단하시겠습니까?";
					msgParam["show_type"] = "two";		
					
					GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...
					
					guiMessageBox.OnButtonClick = (bool pIsOk) =>
					{
						if (pIsOk) 	
						{
							// 차단 코드 작성 필요...
                            MessageOnOff();
//							////KakaoNativeExtension.Instance.ShowAlertMessage("옵션 내 카톡메시지 허용 상태 변경 : " + GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().kakaoNoticeOnOff.ToString());
						}
					};
				}
				else                                                                                // 버튼이 off 상태일 때...
				{
                    MessageOnOff();
//					////KakaoNativeExtension.Instance.ShowAlertMessage("옵션 내 카톡메시지 허용 상태 변경 : " + GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().kakaoNoticeOnOff.ToString());
				}
			}
		}
	}

      public void MessageOnOff()
    {
       KakaoNativeExtension.Instance.blockMessage(!KakaoGameUserInfo.Instance.message_blocked,
           this.onBlockMessageComplete, this.onBlockMessageError);
    }

    private void onBlockMessageComplete()
    {
        Debug.Log("onBlockMessageComplete" + KakaoGameUserInfo.Instance.message_blocked);

        if (KakaoGameUserInfo.Instance.message_blocked)
        {
            GameObject.Find("InfoMgr").GetComponent<InfoSave>().KakaoNoticeOnOff();	// 버튼을 off로 바꾸고 저장...
            transform.GetComponent<UISprite>().spriteName = "swich_off 1";
        }
        else
        {
            GameObject.Find("InfoMgr").GetComponent<InfoSave>().KakaoNoticeOnOff();		// 버튼을 on 으로 바꾸고 저장...
            transform.GetComponent<UISprite>().spriteName = "swich_on 1";

        }
    }

    private void onBlockMessageError(string status, string message)
    {
        Debug.Log("onBlockMessageError");
    }


	IEnumerator OnOffImageChange(float waitTime){

		yield return new WaitForSeconds(waitTime);

		if (isBgmOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().bgmOnOff == true) {
			transform.GetComponent<UISprite>().spriteName = "swich_on 1";
		}
		
		if (isBgmOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().bgmOnOff == false) {
			transform.GetComponent<UISprite>().spriteName = "swich_off 1";
		}
		
		if (isEffectSoundOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().effectSoundOnOff == true) {
			transform.GetComponent<UISprite>().spriteName = "swich_on 1";
		}
		
		if (isEffectSoundOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().effectSoundOnOff == false) {
			transform.GetComponent<UISprite>().spriteName = "swich_off 1";
		}
		
		if (isPushOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().pushOnOff == true) {
			transform.GetComponent<UISprite>().spriteName = "swich_on 1";
      
         }
		
		if (isPushOnOff == true && GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().pushOnOff == false) {
			transform.GetComponent<UISprite>().spriteName = "swich_off 1";
        
		}

		if (isKakaoNoticeOnOff == true)		
		{
			if (KakaoLocalUserController.Instance.MessageBlocked())	// 카톡 서버에서 유저가 카톡 메시지 수신 차단 상태라면...
			{
				transform.GetComponent<UISprite>().spriteName = "swich_off 1";
			}
			else                                                    // 카톡 서버에서 유저가 카톡 메시지 수신 허용 상태라면...
			{
				if (GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().kakaoNoticeOnOff == true) // 유저가 옵션에서 on 으로 저장했다면...
					transform.GetComponent<UISprite>().spriteName = "swich_on 1";
				else
					transform.GetComponent<UISprite>().spriteName = "swich_off 1";
			}
		}
	}
}
