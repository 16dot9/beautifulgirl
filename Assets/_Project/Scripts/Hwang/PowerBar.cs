﻿using UnityEngine;
using System.Collections;

public class PowerBar : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(MyInfoMgr.Instance.m_Power <= 5){
			transform.GetComponent<UIProgressBar> ().value = (float)MyInfoMgr.Instance.m_Power / 5;
		}
	}
}
