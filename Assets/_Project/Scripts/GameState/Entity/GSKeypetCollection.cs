﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


// 룰렛 돌리기에서 당첨된 패키지의 정보를 담는다..
public struct RouletteResult
{
	public string		packageName;
    public string packageDesc;
    public uint packageSu;
	public string		iconImageName;	
	public float 		stopAngle;			// 룰렛판에서 당첨된 패키지까지 돌아가야 하는 회전(각도)값..
}


public class GSKeypetCollection : GameStateBase
{
	protected GuiKeypetCollection2		_guiKeypetCollection = null;
	protected GuiRoulette				_guiRoulette = null;
	protected RouletteResult			_rouletResult;

    // delay method coroutine
    IEnumerator DelayAction(float dTime, System.Action callback)
    {
        yield return new WaitForSeconds(dTime);
        callback();
    }

    void Exec_GSStageMap()
    {
        /////////////////// 해당 오브젝트가 소멸 한후 실행.
        StartCoroutine(IE_Exec_GSStageMap());

        /////////////////// 1초 딜레이후 실행 인 경우.
        //GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
        //StartCoroutine(DelayAction(1f, () =>
        //            {
        //                GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

        //                GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);

        //            }
        //        ));
    }

    IEnumerator IE_Exec_GSStageMap()
    {
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
        // 3초간 찾아서 없다면 다음 실행.
        for (int i = 0; i < 50; i++)
        {
            int cnt = GameStateMgr.Instance.GetCount_gameStateEntityPools();
            if (cnt <= 0)
                break;
//            Debug.Log("~~~~~~~~~~~~~~~ GameStateMgr.Instance.GetCount_gameStateEntityPools = " + cnt);
            yield return new WaitForSeconds(0.2f);
        }
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
        GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
    }

    private bool rouletteTrueFalse;

    public override void OnEnter()
    {
		int slotCount = 2;
		int maxSlotCount = 10;

		_guiKeypetCollection = GuiMgr.Instance.Show<GuiKeypetCollection2>(GuiMgr.ELayerType.Back, true, null);

		_guiKeypetCollection.RoulletCount(slotCount);

		_guiKeypetCollection.ExitBtn.onClick = (GameObject pGO) =>
		{
            if (null != GuiMgr.Instance.Find<GuiRoulette>())
            {
                return;
            }

            Exec_GSStageMap();
		};

        rouletteTrueFalse = this._guiKeypetCollection.RouletteKeypetCountCheck();
        if (rouletteTrueFalse == false)
        {
            UIButton btn = _guiKeypetCollection.RouletteBtn.GetComponent<UIButton>();
            if (btn)
            {
                btn.isEnabled = false;
            }
        }
        else
        {
            UIButton btn = _guiKeypetCollection.RouletteBtn.GetComponent<UIButton>();
            if (btn)
            {
                btn.isEnabled = true;
            }
        }

		_guiKeypetCollection.RouletteBtn.onClick = (GameObject pGO) =>
		{
			//Debug.Log(GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu);
			// GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
            // 인밴 키펫 체크 rouletteTrueFalse

            //if(rouletteTrueFalse == false && GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu < 10){
            //    GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteFail();
            //}
            //if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteTrueFalse == true && GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu == 11){
            //    GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteSuFull();
            //}
            //if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteTrueFalse == true && GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu < 10 
            //    && GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteFull == false && GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteButtonClick == false)
            if(rouletteTrueFalse == true)
            {
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteButtonClick = true;
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteSuPlus((uint)1);
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().KeypetCountMinus();
                //Debug.Log("룰렛수 : " + GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu);
                //if (null != GuiMgr.Instance.Find<GuiRoulette>())
                //{
                //    return;
                //}

	            _guiRoulette = GuiMgr.Instance.Show<GuiRoulette>(GuiMgr.ELayerType.Front, true, null);

				_guiRoulette.OnAlarmFinished = () =>
				{
					_guiRoulette.OnAlarmFinished = null;

					_guiRoulette.ShowRoultte(true);
				};

				_guiRoulette.StopBtn.onClick = (GameObject pStopGO) =>
				{
					GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteButtonClick = false;
					_guiRoulette.StopBtn.onClick = null;
                    // 키펫 화면에서 삭감.
                    this._guiKeypetCollection.RouletteKeypetCountMinus();

                    GameObject.Find("InfoMgr").GetComponent<InfoSave>().KeypetCountMinus();

                    GetRouletteResult();
// 룰렛 끝난후로 옮김.					_guiRoulette.RouletteStop(_rouletResult.stopAngle);

                    // 버튼 비활성 체크.
                    rouletteTrueFalse = this._guiKeypetCollection.RouletteKeypetCountCheck();
                    
                    Debug.Log("rouletteTrueFalse :" + this._guiKeypetCollection.RouletteKeypetCountCheck());

                    if (rouletteTrueFalse == false)
                    {
                        UIButton btn = _guiKeypetCollection.RouletteBtn.GetComponent<UIButton>();
                        if (btn)
                        {
                            btn.isEnabled = false;
                        }
                    }
				};

				_guiRoulette.OnStoped = () =>
				{
					_guiRoulette.OnStoped = null;

                    string resultDesc = "";

                    /*
                    switch (Application.systemLanguage) 
                    {
                        case SystemLanguage.Portuguese:
                        break;
                        case SystemLanguage.Korean:
                             resultDesc=  _rouletResult.packageName + " 획득"; // " : " + _rouletResult.packageDesc; // +" 획득";
				            break;
                        case SystemLanguage.English:
                             resultDesc=  _rouletResult.packageName + " Get Item"; // " : " + _rouletResult.packageDesc; // +" 획득";
				            break;
                    }*/
                    
                    resultDesc = _rouletResult.packageName + " Consiga o item"; // ":" + _rouletResult.packageDesc; // + "획득";
                    
                    string iconFileName = "Images/Item/" + _rouletResult.iconImageName;

					_guiRoulette.ShowResult((Texture2D)Resources.Load(iconFileName), resultDesc);
				};

				_guiRoulette.OnResultFinished = () =>
				{
					_guiRoulette.OnResultFinished = null;

					_guiRoulette = null;
					GuiMgr.Instance.Show<GuiRoulette>(GuiMgr.ELayerType.Front, false, null);

					slotCount = (slotCount + 1) % maxSlotCount;
	                if (null != _guiKeypetCollection)
	                {
	                    _guiKeypetCollection.RoulletCount(slotCount);
                        rouletteTrueFalse = _guiKeypetCollection.RouletteKeypetCountCheck();
	                }
				};

				// 알람 표시 않 함.  _guiRoulette.ShowAlarm();
                _guiRoulette.OnAlarmFinished();
			}
		};
	}

    public override void OnLeave()
    {
		if (null != GuiMgr.Instance)
		{
			_guiKeypetCollection.ExitBtn.onClick = null;

			_guiKeypetCollection = null;

			GuiMgr.Instance.Show<GuiKeypetCollection2>(GuiMgr.ELayerType.Back, false, null);
		}
	}

    public int index = 0;

	// "룰렛 돌리기"시 랜덤하게 패키지를 뽑는 함수...
    void GetRouletteResult()
	{
        /*
        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
        cmd._sOutputString = "";
        int resultPack_idx = 0;
        int resultPack_qty = 0;

        yield return StartCoroutine(cmd.Send_PROTOCOL_ROULETTE_REQ());

        if (cmd._sOutputString == "")
        {
            // Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
        }
        else
        {
            Debug.Log(cmd._sOutputString);
            JSONNode root = JSON.Parse(cmd._sOutputString);
            var resultCode = int.Parse(root["rc"]);

            if (resultCode == 0)
            {
                resultPack_idx = int.Parse(root["pack_idx"]);
                resultPack_qty = int.Parse(root["pack_qty"]);
                Debug.Log("~~~~~~~~~~~~~~~~~ resultPack_idx:" + resultPack_idx);

                // 룰렛 수 설정.
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().rouletteSu = uint.Parse(root["roulette_cnt"]);

                // resultPack_idx = 11; // test 25
            }
        }
        
        _guiRoulette.RouletteStop(resultPack_idx *3.6f); // * 36.0f);

        if (resultPack_idx == 0)
        {
            cmd.PlayErrorMessage("룰렛 돌리기 중 " + cmd._DefaultMessage,0);
        }
        else
        {
            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)resultPack_idx);
            if (packageInfo != null)
            {
                _rouletResult.packageName = packageInfo.name;
                _rouletResult.iconImageName = packageInfo.icon_image;
                _rouletResult.packageSu = (uint) resultPack_qty;
                _rouletResult.packageDesc = packageInfo.desc;

                //_rouletResult.stopAngle = resultPack_idx * 36.0f;
                //rewardIcon = packageInfo.icon_image;
                //strDesc = packageInfo.desc;
                Debug.Log("~~~~~~~~~~~~~ Package Index : " + resultPack_idx.ToString() + " packageInfo.desc:" + packageInfo.desc + " icon:" + packageInfo.icon_image);

            }
        }
        */

        // 로컬손님용.
        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();
        RandomboxInfoMgr randomboxInfoMgr = GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>();
        foreach (RandomboxInfo randomboxInfo in randomboxInfoMgr.GetRandomboxInfo())
        {
            if (randomboxInfo.randombox_type != RandomboxType.KeypetRoulette) // 룰렛을 돌릴 경우만 이하 실행..
                continue;

            int[] temp = new int[1250];	// 확률을 담는 배열 만들기..

            for (int i = 0, inc =0; i < 10; i++)
            {
                int dropPercent = (int)randomboxInfo.packagesetInfo[i].drop_percent; // 패키지당 드랍 확률...	

                //Debug.Log(string.Format("PackagesetInfo {0}의 확률 : {1}", i, dropPercent));

                for (int j = 0; j < dropPercent; j++)
                {
                    temp[inc] = i;	// 패키지 자신의 인덱스값 i를 담는다..
                    inc++;
                }
            }

            int randNum = Random.Range(0, temp.Length);
            int index = temp[randNum];

            // 룰렛에서 뽑힌 패키지 데이터 정보 뽑기..
            uint package_SI = randomboxInfo.packagesetInfo[index].package_si;

            //Debug.Log("package_SI :" + package_SI + "index :" + index);

            _guiRoulette.RouletteStop(package_SI * 36.0f); // * 36.0f);

            _rouletResult.packageName = packageInfoMgr.GetPackageInfo(package_SI).name;
            _rouletResult.iconImageName = packageInfoMgr.GetPackageInfo(package_SI).icon_image;
            _rouletResult.packageSu = randomboxInfo.packagesetInfo[index].item_su;
            _rouletResult.stopAngle = index * 36.0f;

            if (7 < (int)package_SI && (int)package_SI < 15)
            {
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate((int)package_SI,(int)_rouletResult.packageSu);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
            }

            if (8 > (int)package_SI || (int)package_SI > 13)
            {
                //GameObject.Find("InfoMgr").GetComponent<InfoSave>().ItemHaveUpdate((int)package_SI,(int)packageInfoMgr.GetPackageInfo(package_SI).item_1_su);
                GameObject.Find("InfoMgr").GetComponent<InfoSave>().RouletteKeypetCountCheck();
                //Debug.Log((int)packageInfoMgr.GetPackageInfo(package_SI).item_1_su);
            }

            GameObject.Find("InfoMgr").GetComponent<InfoSave>().InGameItemHaveUpdate((int)package_SI, (int)packageInfoMgr.GetPackageInfo(package_SI).item_1_su);
        }
	}

}
