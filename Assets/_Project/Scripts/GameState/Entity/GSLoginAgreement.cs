﻿using UnityEngine;
using System.Collections;

public class GSLoginAgreement : GameStateBase 
{
	public override void OnEnter()
	{
		GuiMgr.Instance.Show<GuiLoginAgreement>(GuiMgr.ELayerType.Back, true, null);
	}
	
	public override void OnLeave()
	{
		
		if (null != GuiMgr.Instance)
		{
			GuiMgr.Instance.Show<GuiLoginAgreement>(GuiMgr.ELayerType.Back, false, null);
		}		
	}
}
