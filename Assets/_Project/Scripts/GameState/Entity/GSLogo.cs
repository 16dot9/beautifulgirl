﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GSLogo : GameStateBase
{

	public override void OnEnter()
    {
		GuiMgr.Instance.Show<GuiLogo>(GuiMgr.ELayerType.Back, true, null);

		StartCoroutine(Sequence());
	}

    public override void OnLeave()
    {
		if (null != GuiMgr.Instance)
		{
			GuiMgr.Instance.Show<GuiLogo>(GuiMgr.ELayerType.Back, false, null);
			
			//Hwang Wark
			//Destroy(GameObject.Find("Gui/Back/Camera/GuiLogo").gameObject);
			//Hwang End
		}
	}

	protected IEnumerator Sequence()
	{
		yield return null;
			
		GuiMgr.Instance.Find<GuiLogo>().ShowLogo1();

		yield return new WaitForSeconds(2.0f);

		GuiMgr.Instance.Find<GuiLogo>().ShowLogo2();

		yield return new WaitForSeconds(1.2f);

		GameStateMgr.Instance.ChangeState<GSTitle>(0, null);
	}

}
