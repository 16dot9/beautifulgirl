﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GSTitle : GameStateBase
{

	protected GuiTitle					_guiTitle = null;


    public override void OnEnter()
    {
		_guiTitle = GuiMgr.Instance.Show<GuiTitle>(GuiMgr.ELayerType.Back, true, null);

		StartCoroutine(SoundSequence());
		StartCoroutine(Sequence());

	}

    public override void OnLeave()
    {
        if (null != AudioMgr.Instance && AudioMgr.Instance.IsPlay("StageMap")
            && GameObject.Find("InfoMgr") != null)
		{
			if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().bgmOnOff == true){
				//AudioMgr.Instance.Play("StageMap");
			}
		}

		_guiTitle = null;

		if (null != GuiMgr.Instance)
		{
			GuiMgr.Instance.Show<GuiTitle>(GuiMgr.ELayerType.Back, false, null);
			
			//Hwang Wark
			//GameObject.Find("InfoMgr").GetComponent<InfoSave>().MapScene();
			//Destroy(GameObject.Find("Gui/Back/Camera/GuiTitle").gameObject);
			//Hwang End
		}
	}


	protected IEnumerator SoundSequence()
	{
		if(GameObject.Find("InfoMgr").GetComponent<InfoSave>().bgmOnOff == true){
			AudioMgr.Instance.Play("keypetbreak");
		}
		while (AudioMgr.Instance.IsPlay("TitleVoice"))
		{
			yield return null;
		}
		if (GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().bgmOnOff == true) {
			AudioMgr.Instance.Play ("StageMap");
		}
	}


	/// <summary>
	/// streamingAssetsPath 에서 데이터 파일 읽기...
	/// </summary>
	protected IEnumerator LoadDataFromWww()
    {
        Debug.Log("streamingAssetsPath 폴더에서 데이터 파일 로딩 중");

        string rootPath;

        if (Application.platform == RuntimePlatform.Android)
        {
            //rootPath = "jar:file://" + Application.dataPath + "!/assets";
            rootPath = Application.streamingAssetsPath;
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            rootPath = Application.streamingAssetsPath;
        }
        else
        {
            rootPath = "file://" + Application.streamingAssetsPath;
        }


        WWW www;

        {
            www = new WWW(rootPath + "/Data/puzzle_data.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<PlacementInfoMgr>().Import(www.text);
            www.Dispose();
        }
        
        {
            www = new WWW(rootPath + "/Data/ReballPuzzleData_2.txt");
            yield return www;
            GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/stage_data.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<StageInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/stage_map_data2.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<StageMapInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/item_data.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<ItemInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/package_data.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<PackageInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/shop_data.csv");
            yield return www;
            GameDataMgr.Instance.GetInfo<ShopInfoMgr>().Import(www.text);
            www.Dispose();
        }

        {
            www = new WWW(rootPath + "/Data/randombox_data.csv");
            yield return www;

            GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>().Import(www.text);
            www.Dispose();
        }


        {
            www = new WWW(rootPath + "/Data/tip_data.csv");
            yield return www;

            GameDataMgr.Instance.GetInfo<TipInfoMgr>().Import(www.text);
            www.Dispose();
        }


        {
            www = new WWW(rootPath + "/Data/scenario_data.csv");
            yield return www;

            GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().Import(www.text);
            www.Dispose();
        }
    }


	/// <summary>
	/// Resources 폴더에서 데이터 파일 읽기...
	/// </summary>
	protected string LoadDataFromResources()
	{
        TextAsset data = null;

        //switch( Application.systemLanguage)
        //{ 
        //    case SystemLanguage.Korean:
        //        {
        //            {
        //                data = Resources.Load("Data/puzzle_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/puzzle_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<PlacementInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/ReballPuzzleData_2", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/puzzle_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/stage_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/stage_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<StageInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {

        //                data = Resources.Load("Data/stage_map_data2", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/stage_map_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<StageMapInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/item_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/item_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<ItemInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/package_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/package_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<PackageInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/shop_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/shop_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<ShopInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/random_box", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/random_box 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/tip_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/tip_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<TipInfoMgr>().Import(data.text);
        //                data = null;
        //            }

        //            {
        //                data = Resources.Load("Data/scenario_data", typeof(TextAsset)) as TextAsset;

        //                if (data == null)
        //                    return "Resources/Data/scenario_data 파일 로드 실패";

        //                GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().Import(data.text);
        //                data = null;
        //            }
        //        }
                
        //break;
        //    case SystemLanguage.English:
        //{
        //    {
        //        data = Resources.Load("Data/puzzle_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/puzzle_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<PlacementInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/ReballPuzzleData_2", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/puzzle_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/stage_data_en", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/stage_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<StageInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {

        //        data = Resources.Load("Data/stage_map_data2", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/stage_map_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<StageMapInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/item_data_en", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/item_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ItemInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/package_data_en", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/package_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<PackageInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/shop_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/shop_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ShopInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/random_box", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/random_box 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/tip_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/tip_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<TipInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/scenario_data_en", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/scenario_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().Import(data.text);
        //        data = null;
        //    }
        //}
        //break;
        //    case SystemLanguage.Portuguese:
        //{
        //    {
        //        data = Resources.Load("Data/puzzle_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/puzzle_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<PlacementInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/ReballPuzzleData_2", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/puzzle_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/stage_data_po", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/stage_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<StageInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {

        //        data = Resources.Load("Data/stage_map_data2", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/stage_map_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<StageMapInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/item_data_po", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/item_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ItemInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/package_data_po", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/package_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<PackageInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/shop_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/shop_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ShopInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/random_box", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/random_box 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/tip_data", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/tip_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<TipInfoMgr>().Import(data.text);
        //        data = null;
        //    }

        //    {
        //        data = Resources.Load("Data/scenario_data_po", typeof(TextAsset)) as TextAsset;

        //        if (data == null)
        //            return "Resources/Data/scenario_data 파일 로드 실패";

        //        GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().Import(data.text);
        //        data = null;
        //    }
        //}
        //    break;
        //}   
        
            {
                data = Resources.Load("Data/puzzle_data", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/puzzle_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<PlacementInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/ReballPuzzleData_2", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/puzzle_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/stage_data_po", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/stage_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<StageInfoMgr>().Import(data.text);
                data = null;
            }

            {

                data = Resources.Load("Data/stage_map_data2", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/stage_map_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<StageMapInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/item_data_po", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/item_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<ItemInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/package_data_po", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/package_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<PackageInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/shop_data", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/shop_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<ShopInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/random_box", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/random_box 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/tip_data", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/tip_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<TipInfoMgr>().Import(data.text);
                data = null;
            }

            {
                data = Resources.Load("Data/scenario_data_po_2", typeof(TextAsset)) as TextAsset;

                if (data == null)
                    return "Resources/Data/scenario_data 파일 로드 실패";

                GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().Import(data.text);
                data = null;
            }
		return "Success";
	}



	protected IEnumerator Sequence()
	{
		bool useResources = true;

		if (useResources)	// Resources 폴더에서 데이터 파일 로딩...
		{
			string loadResult = LoadDataFromResources ();

            if (loadResult != "Success")
            {
                _guiTitle.Tips.text = loadResult;	// 데이터 파일 로딩 실패시 임시로 툴팁에 나타내기...
            }


		}
		else     // streamingAssetsPath 에서 데이터 파일 로딩...
		{
			yield return StartCoroutine (LoadDataFromWww());
		}

		// 로딩 화면에 나오는 툴팁 보여주기.. 
		TipInfoMgr tipInfoMgr = GameDataMgr.Instance.GetInfo<TipInfoMgr> ();
        int tip_SI = 0;

        tip_SI = Random.Range(8, 14);

		_guiTitle.Tips.text = tipInfoMgr.GetTipInfo ((uint)tip_SI).desc;

		// Debug.Log(string.Format("{0} 번 팀 : {1}", tip_SI, _guiTitle.Tips.text));

		// 로딩 화면에 나오는 로딩 프로그래스바 진행 사항 보여주기..
		UISprite loadingProgressBar = _guiTitle.LoadingProgressBar;

		float loadingSpeed = 0.5f;
		float loadingRatio = 0.0f;

		while (loadingRatio <= 1.0f)
		{
			loadingRatio += Time.deltaTime * loadingSpeed;
			loadingProgressBar.fillAmount = loadingRatio;

			yield return null;
		}
		InfoSave.mapScene = true;
		
        //< 카카오톡 제거하면서 붙이는 작업
        //GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);	// 맵 화면으로 전환...
    
		// 카카오 초기화 및 필요시 로그인 화면 유도...
		//KakaoMain.Instance.Init ();
        
        GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);	// 맵 화면으로 전환...

        //if (InfoSave.Instance.FBNEWUSER() != true)
        //{
        //    FBMain.Instance.Init();
        //}
        //else 
        //{
        //    FBMain.Instance.isFB = false;

        //    GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);	// 맵 화면으로 전환...
        //}
	}
}
