﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Text;

//< 현재 GSPlay 와 Tutorial이 같이 있습니다.
//< 스클립트 수정이 필요합니다.
//< 14.11.25 안이삭

public class GSPlay : GameStateBase
{
    public bool itemUseWark = false;
    public float keypetMoveStartDely = 0.1f;

    public enum State
    {
        BeginPlay,
        TuTorial,
        Play,
        EndPlay,
        Result,
        FinishWait
    }

    public enum ResultType
    {
        Invalid,
        Success,
        CanNotMove,
        TimeOut,
        CountOut
    }

    protected enum PlacementDir
    {
        Begin,

        DU = Begin,			// up
        DUR,				// up-right
        DR,					// right
        DDR,				// down-right
        DD,					// down
        DDL,				// down-left
        DL,					// left
        DUL,				// up-left

        Max
    }

    protected enum MenuType
    {
        Invalid,

        ChangeSetting,
        Countine,
        GotoMainMenu,
        Replay
    }

    protected class KeypetCombinationInfo
    {
        public List<GameObject> KeypetList = new List<GameObject>();
    }

    protected class KeypetDirInfo
    {
        public GameObject KeypetGO = null;
        public KeypetCtrl Keypet = null;
        public KeypetBGCtrl KeypetBG = null;
    }

    protected class UndoInfo
    {
        public List<KeypetDirInfo> undoList = new List<KeypetDirInfo>();
        public int score = 0;
    }


    protected static int[] _dirLeanX = new int[] { 0, 1, 1, 1, 0, -1, -1, -1, 0 };
    protected static int[] _dirLeanY = new int[] { 1, 1, 0, -1, -1, -1, 0, 1, 0 };


    protected GuiBG _guiBG = null;
    protected GuiPlayTop _guiPlayTop = null;
    protected GuiPlayMiddle _guiPlayMiddle = null;
    protected GuiPlayBottom _guiPlayBottom = null;
    protected GuiPlayFeverCount _guiPlayFeverCount = null;
    protected GuiPlayFever _guiPlayFever = null;
    protected GuiPlayFeverFront _guiPlayFeverFront = null;
    protected GuiPlayResultSuccess _guiPlayResultSuccess = null;
    protected GuiPlayResultFail _guiPlayResultFail = null;
    protected GuiPlayMenu _guiPlayMenu = null;
    protected GuiGameSetting _guiGameSetting = null;
    protected GuiPlayTutorialCutin _guiPlayTutorialCutin = null;
                            

    protected KeypetResInfo[] _keypetResInfoList = null;

    protected State _state = State.BeginPlay;

    protected int _puzzleWidth = 0;
    protected int _puzzleHeight = 0;

    //첫번째 선택 키펫과 두번째 선택 키펫
    protected GameObject _KeypetBox1 = null;
    protected GameObject _KeypetBox2 = null;

    protected GameObject _selectedKeypet1 = null;
    protected GameObject _selectedKeypet2 = null;
    protected List<KeypetBGCtrl> _selectedKeypetList = new List<KeypetBGCtrl>();

    protected bool _isRescueMode = false;
    protected GameObject _rescueKeypte = null;

    protected bool _isBonusMode = false;

    protected double _curScore = 0;

    protected bool _isTime = false;
    protected float _maxTime = 0.0f;
    protected float _curTime = 0.0f;

    protected bool _isCount = false;
    protected int _maxCount = 0;
    protected int _curCount = 0;

    protected GameObject _feverTargetGO = null;
    protected float _feverWaitTime = 0.0f;

    protected bool _isFeverTime = false;
    protected float _feverReaminTime = 0.0f;
    protected float _feverMaxTime = 0.0f;
    protected GameObject _feverEffect1 = null;
    protected GameObject _feverEffect2 = null;

    protected ResultType _resultType = ResultType.Invalid;

    protected int _dirCount = 0;

    protected List<UndoInfo> _undoList = new List<UndoInfo>();

    protected bool _isPause = false;
    protected bool _isCombine = false;

    GameObject parentObject;
    GameObject tutorialPlay;

    private KeypetResInfo[] keypetList;

    //< 튜토리얼 변수
    private bool _bTutorial = false;

    private bool _bEndTurtorial = false;
    //< 리볼 플레이 일때 조건을 주기 위한 변수
    private bool isReballPlay = false;
    private bool isReballReward = false;

    private int _StageNumber = 0;
    
    uint stageInfoSI = 0;

    double ReballStage = 0;

    private int puzzleActiveCount = 0;

    private InfoSave SaveData;
    public override void OnEnter()
    {
        SaveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
        //리볼 플레이
        isReballPlay = KakaoMain.Instance.isReballPlay;

        isReballReward = isReballPlay;

        ReballStage = (int)SaveData.clearStageReball;

        if (SaveData.bgmOnOff == true)
        {
            AudioMgr.Instance.Stop("StageMap");
            AudioMgr.Instance.Play("InGameBG");
        }
        /*
        int user_status = MyInfoMgr.Instance.m_user_status & (1 << Cmd_Handler_CS.USER_STATE_NEW_USER);

        if (user_status > 0)
        {
            _bTutorial = true;
        }
        else 
        {
        }*/

        if(!isReballPlay)
        { 
            if (SaveData.StartTutorialIntro())   
            {
                _bTutorial = true;
            }
            else { 
                _bTutorial = false;
            }
        }

        if (KakaoMain.Instance._bTutorailStart)
            _bTutorial = true;

        if ((int)SaveData.clearStageReball == 0 && isReballPlay && SaveData.StartReballIntro())
        {
            _bTutorial = true;
        }
        //

        _guiBG = GuiMgr.Instance.Show<GuiBG>(GuiMgr.ELayerType.Back, true, null);
        _guiPlayTop = GuiMgr.Instance.Show<GuiPlayTop>(GuiMgr.ELayerType.Back, true, null);
        _guiPlayMiddle = GuiMgr.Instance.Show<GuiPlayMiddle>(GuiMgr.ELayerType.Back, true, null);
        _guiPlayBottom = GuiMgr.Instance.Show<GuiPlayBottom>(GuiMgr.ELayerType.Back, true, null);
        _guiPlayFeverCount = GuiMgr.Instance.Show<GuiPlayFeverCount>(GuiMgr.ELayerType.Front, true, null);
        _guiPlayFeverCount.ShowOn(false);

        _keypetResInfoList = new KeypetResInfo[(int)KeypetType.Max];

        for (int i = 0; i < (int)KeypetType.Max; ++i)
        {
            _keypetResInfoList[i] = new KeypetResInfo();

            _keypetResInfoList[i].KeypetResType = (KeypetType)i;
            _keypetResInfoList[i].KeypetResObj = Resources.Load(string.Format("Keypet/keypet_{0}", ((KeypetType)i).ToString().ToLower()));
        }

        UIEventListener.Get(_guiPlayTop.GetPauseBtn()).onClick = (GameObject pGO) =>
        {
            if (_isBonusMode)
                return;

            StartCoroutine(GuiPlayMenuBtnClick());
        };

        // 사용 아이템 세팅
        _guiPlayBottom.resetUseItems();

        if (!isReballPlay)
        {
            if (!_bTutorial)
            {
                int cnt = 0;

                foreach (uint idx in MyInfoMgr.Instance.m_UseItems)
                {
                    _guiPlayBottom.setUseItem(cnt, idx, GameDataMgr.Instance.GetInfo<ItemInfoMgr>().GetItemInfo(idx).icon_image, _guiPlayBottom.name);
                    cnt++;
                }
            }
            ItemSettingClick(_bTutorial);
        }
        /*
         * 클라이언트 DB저장
            InfoSave saveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
         */


        parentObject = GameObject.Find("Gui/Front/Camera").gameObject;
        tutorialPlay = (GameObject)Resources.Load("Gui/Tutorial_Play");

        _isPause = false;
        _bEndTutorial = false;

        StartCoroutine(Sequence());
        StartCoroutine(SequenceTime());
    }

    public override void OnLeave()
    {
        HideFeverTime(true);

        if (null != AudioMgr.Instance && GameObject.Find("InfoMgr") != null)
        {
            if (SaveData.bgmOnOff == true)
            {
                AudioMgr.Instance.Stop("InGameBG");
                AudioMgr.Instance.Play("StageMap");
            }
        }

        for (int i = 0; i < (int)KeypetType.Max; ++i)
        {
            _keypetResInfoList[i] = null;
        }

        _guiPlayMiddle.ClearSetting();

        if (null != GuiMgr.Instance)
        {
            UIEventListener.Get(_guiPlayTop.GetPauseBtn()).onClick = null;

            _guiBG = null;
            _guiPlayTop = null;
            _guiPlayMiddle = null;
            _guiPlayBottom = null;
            _guiPlayFeverCount = null;
            _guiPlayResultSuccess = null;
            _guiPlayResultFail = null;
            _guiPlayMenu = null;
            _guiGameSetting = null;
            _guiPlayTutorialCutin = null;

            GuiMgr.Instance.Show<GuiBG>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiPlayTop>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiPlayMiddle>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiPlayBottom>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiPlayFeverCount>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiPlayResultSuccess>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiPlayResultFail>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiPlayMenu>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, false, null);
        }
    }

    private bool isRun = false;

    protected IEnumerator SequenceTime() 
    {
        while (isRun)
        {
            if (_isPause)
            {
                yield return null;

                continue;
            }

            if (_isFeverTime)
            {
                _feverReaminTime -= Time.deltaTime;

                if (_feverReaminTime <= 0.0f)
                {
                    _feverReaminTime = 0.0f;

                    HideFeverTime(false);
                }

                _guiPlayFeverCount.SetCount(_feverReaminTime);
            }

            switch (_state)
            {
                case State.Play:
                    {
                        if (null != _feverTargetGO)
                        {
                            _feverWaitTime -= Time.deltaTime;

                            if (_feverWaitTime <= 0.0f)
                            {
                                _feverWaitTime = 0.0f;
                                _feverTargetGO = null;
                            }

                            _guiPlayFeverCount.SetCount(_feverWaitTime);
                        }


                        if (_isTime && !_bTutorial)
                        {
                            _curTime += Time.deltaTime;

                            if (_curTime > _maxTime)
                            {
                                _resultType = ResultType.TimeOut;
                                _state = State.EndPlay;
                                continue;
                            }

                            _guiPlayTop.SetTime(_curTime, _maxTime);
                        }
                    }
                    break;
            }

            yield return null;
        }
    }

    //    bool _IsSucess = false;
    protected IEnumerator Sequence()
    {

        //튜토리얼 인지 아닌지 체크하기
        //아마 하나 가져오는게 나을듯 합니다.
        //InfoSave saveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();

        /*
		if(_bTutorial == true){
			GameObject play_Intro = Instantiate (Resources.Load ("Gui/Tutorial_Play")) as GameObject;
			play_Intro.transform.parent = GuiMgr.Instance.GetFrontCamera ().transform;
			play_Intro.transform.name = "Play_Intro";
			play_Intro.transform.localPosition = Vector3.zero; 
			play_Intro.transform.localRotation = Quaternion.identity;
			play_Intro.transform.localScale = Vector3.one;

            //hello

            while (play_Intro.GetComponent<GuiTutorial>()._quitTutorial != true)	// 컷신을 끝낼 때까지 기다리기.. 
				yield return null;
			
			SaveData.playIntro = false;
		}
        */
    
        if(_bTutorial)
            StartCoroutine(IE_Tutorial());

        _state = State.BeginPlay;

        isRun = true;

        while (isRun)
        {
            if (_isPause)
            {
                yield return null;

                continue;
            }

            switch (_state)
            {
                /*
                 * 플레이 시작 준비 상태.
                 * 
                 */
                case State.BeginPlay:
                    {
                        #region BeginPlay

                        if (null == _params)
                        {
                            yield break;
                        }

                        HideFeverTime(false);

                        uint stageInfoSI = 0;

                        if (_bTutorial)
                        {
                            stageInfoSI = 1;
                        }
                        else
                        {
                            if (isReballPlay)
                            {
                                //등차수열로 맵 스테이지 정하기로 되었는데 다시 원상 복기

                                if ((int)SaveData.clearStageReball < 0)
                                {
                                    SaveData.clearStageReball *= -1;

                                    double n = (int)SaveData.clearStageReball;

                                    stageInfoSI = (uint)n;//(uint)((4 * n) + 40);
                                }
                                else
                                {
                                    double n = (int)SaveData.clearStageReball;

                                    stageInfoSI = (uint)n; //(uint)((4 * n) + 40);
                                }
                            }
                            else
                            {
                                stageInfoSI = uint.Parse(_params["stage_si"]);
                            }
                        }

                        ES3.Save("clearStageNumber", SaveData.clearStageNumber);

                        StageInfoMgr stageInfoMgr = GameDataMgr.Instance.GetInfo<StageInfoMgr>();
                        StageInfo stageInfo = stageInfoMgr.GetStageInfo(stageInfoSI);

                        if (_bTutorial)
                        {
                            stageInfo.game_mode = GameMode.Count;
                        }
                        //< 이 부분을 한번 체크를 해봐야 알것 같다.
                        //else
                        //{ 
                        //    if (stageInfoSI == 1) 
                        //    {
                        //         stageInfo.game_mode = GameMode.TimeAndCount;
                        //    }
                        //}

                        _curScore = 0;

                        _guiPlayTop.SetSocre(_curScore);

                        _isTime = false;
                        _maxTime = 0.0f;
                        _curTime = 0.0f;

                        _isCount = false;
                        _maxCount = 0;
                        _curCount = 0;

                        switch (stageInfo.game_mode)
                        {
                            case GameMode.Time:
                                {
                                    _isTime = true;
                                    _maxTime = stageInfo.attr_1;
                                    _curTime = 0.0f;

                                    _guiPlayTop.EnableTime(true);
                                    _guiPlayTop.EnableCount(false);
                                }
                                break;

                            case GameMode.Count:
                                {
                                    _isCount = true;
                                    _maxCount = stageInfo.attr_2;
                                    _curCount = 0;

                                    _guiPlayTop.EnableTime(false);
                                    _guiPlayTop.EnableCount(true);
                                    _guiPlayTop.SetCount(_curCount, _maxCount, false);
                                }
                                break;

                            case GameMode.TimeAndCount:
                                {
                                    _isTime = true;
                                    _maxTime = stageInfo.attr_1;
                                    _curTime = 0.0f;

                                    _isCount = true;
                                    _maxCount = stageInfo.attr_2;
                                    _curCount = 0;

                                    _guiPlayTop.EnableTime(true);
                                    _guiPlayTop.EnableCount(true);
                                    _guiPlayTop.SetCount(_curCount, _maxCount, false);
                                }
                                break;
                        }

                        int[] ixarr_slot = new int[3] { 0, 0, 0 };
                        for (int i = 0; i < MyInfoMgr.Instance.m_UseItems.Count; i++)
                        {
                            ixarr_slot[i] = (int)MyInfoMgr.Instance.m_UseItems[i];
                        }


                        //if(!_bTutorial && !KakaoMain.Instance._bTutorailStart)
                        //{ 
                        //    StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_START_GAME_REQ(int.Parse(_params["stage_si"]), 0,0,0));// ixarr_slot[0], ixarr_slot[1], ixarr_slot[2]));
                        //}
                        
                        PlacementInfoMgr placementInfoMgr = null;
                        PlacementInfo placementInfo = null;

                        ReballPlacementInfoMgr ReballplacementInfoMgr = null;
                        ReballPlacementInfo ReballplacementInfo = null;

                        if (!isReballPlay)
                        {
                            placementInfoMgr = GameDataMgr.Instance.GetInfo<PlacementInfoMgr>();
                         //   stageInfo.pi_si = (uint)Random.Range(0, 5);
                            placementInfo = placementInfoMgr.GetPlacementInfo(stageInfo.pi_si);

                            _puzzleWidth = placementInfo.plane_width;
                            _puzzleHeight = placementInfo.plane_height;
                        }
                        else
                        {
                            ReballplacementInfoMgr = GameDataMgr.Instance.GetInfo<ReballPlacementInfoMgr>();

                            int _Idx = Random.Range(0, 5);

                            ReballplacementInfo = ReballplacementInfoMgr.GetPlacementInfo(stageInfoSI);

                            _puzzleWidth = ReballplacementInfo.plane_width;
                            _puzzleHeight = ReballplacementInfo.plane_height;
                        }

                        _guiPlayTop.SetStageGoal(stageInfo.mode_desc);

                        //< 키펫 셋팅
                        keypetList = new KeypetResInfo[_puzzleWidth * _puzzleHeight];

                        for (int i = 0; i < keypetList.Length; ++i)
                        {/*
                            if (placementInfo.placed_button_info_list[i].placementType == AlgorithmOfPlacingButtons.PlacementType.Button)
                            {
                                Debug.LogError("placementInfo.placed_button_info_list[i].iD :" + placementInfo.placed_button_info_list[i].iD);
                                keypetList[i] = _keypetResInfoList[placementInfo.placed_button_info_list[i].iD - 1];
                            }
                            else if (placementInfo.placed_button_info_list[i].placementType == AlgorithmOfPlacingButtons.PlacementType.Block)
                            {
                                keypetList[i] = new KeypetResInfo();

                                keypetList[i].KeypetResType = KeypetType.blank;
                                keypetList[i].KeypetResObj = Resources.Load("Keypet/keypet_blank");
                            }*/
                            if (!isReballPlay)
                            {
                                if (placementInfo.placed_button_info_list[i].iD == -1)
                                {
                                    keypetList[i] = new KeypetResInfo();

                                    keypetList[i].KeypetResType = KeypetType.blank;
                                    keypetList[i].KeypetResObj = Resources.Load("Keypet/keypet_blank");
                                }
                                else //if (placementInfo.placed_button_info_list[i].placementType == AlgorithmOfPlacingButtons.PlacementType.Block)
                                {
                                    keypetList[i] = _keypetResInfoList[placementInfo.placed_button_info_list[i].iD - 1];
                                }
                            }
                            else
                            {

                                if (ReballplacementInfo.placed_button_info_list[i].iD == -1)
                                {
                                    keypetList[i] = new KeypetResInfo();

                                    keypetList[i].KeypetResType = KeypetType.blank;
                                    keypetList[i].KeypetResObj = Resources.Load("Keypet/keypet_blank");
                                }
                                else //if (placementInfo.placed_button_info_list[i].placementType == AlgorithmOfPlacingButtons.PlacementType.Block)
                                {
                                    keypetList[i] = _keypetResInfoList[ReballplacementInfo.placed_button_info_list[i].iD - 1];
                                }
                            }
                        }

                        _guiPlayMiddle.ClearSetting();
                        _guiPlayMiddle.Setting(_puzzleWidth, _puzzleHeight, keypetList);

                        List<GameObject> puzzleItemList = new List<GameObject>();

                        foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
                        {
                            KeypetCtrl keypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                            if (keypetCtrl.IsBlank)
                            {
                                continue;
                            }

                            puzzleItemList.Add(puzzleItem);

                            UIEventListener.Get(puzzleItem).onClick = (GameObject pGO) =>
                            {
                                if (_isRescueMode)
                                {
                                    if (null == _rescueKeypte)
                                    {
                                        _rescueKeypte = pGO;
                                    }
                                }
                                else
                                {
                                    if (null == _selectedKeypet1)
                                    {
                                        _selectedKeypet1 = pGO;
                                        _selectedKeypet2 = null;

                                        SelectSourceKeypet(_selectedKeypet1);
                                    }
                                    else if (null == _selectedKeypet2)
                                    {
                                        _selectedKeypet2 = pGO;
                                    }
                                }
                            };

                        }

                        _feverTargetGO = puzzleItemList[Random.Range(0, puzzleItemList.Count)];
                        _feverWaitTime = stageInfo.fever_time;

                        if(!_bTutorial)
                        { 
                            _guiPlayFeverCount.SetCount(_feverWaitTime);
                        }
                        yield return null;

                        Vector3 curPos = _feverTargetGO.transform.position;
                        curPos = GuiMgr.Instance.GetFrontCamera().ScreenToWorldPoint(GuiMgr.Instance.GetBackCamera().WorldToScreenPoint(curPos));
                        _guiPlayFeverCount.Move(curPos);

                        _resultType = ResultType.Invalid;

                        _undoList.Clear();
                        
                        //< 튜토리얼
                        if (!_bTutorial)
                        {
                            // Display Cutin description
                            GuiMgr.Instance.Show<GuiPlayCutin>(GuiMgr.ELayerType.Front, true, null);
                            yield return new WaitForSeconds(1.5f);
                            GuiMgr.Instance.Show<GuiPlayCutin>(GuiMgr.ELayerType.Front, false, null);
                        }
                        else 
                        {
                            _guiPlayTutorialCutin = GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, true, null);
                            _guiPlayTutorialCutin.TutorialAlarm(GuiPlayTutorialCutin.Tutorial.Tutorial_Start);

                            bool isMoveFinished = false;

                            _guiPlayTutorialCutin.OnButtonSkipClick = (bool pIsOk) =>
                            {
                                if (pIsOk)
                                {
                                    isMoveFinished = true;
                                }
                            };

                            while (!isMoveFinished)
                            {
                                yield return null;
                            }
                            
                            GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, false, null);
                        }

                        _state = State.Play;

                        #endregion BeginPlay

                        if (_bTutorial)
                        {
                            if (KakaoMain.Instance.isReballPlay)
                            {
                                SettingPuzzleItemName("PuzzleItem_009");
                            }
                            else
                            {
                                SettingPuzzleItemName("PuzzleItem_021");
                            }
                        }
                    }
                    break;
                /*
                 * 플레이 상태.
                 * 
                 */
                case State.Play:
                    {
                        #region Play

                        if (null != _selectedKeypet1 && null != _selectedKeypet2)
                        {
                            KeypetCombinationInfo KeypetCombinationInfo = new KeypetCombinationInfo();

                            if (CombineKeypets(_selectedKeypet1, _selectedKeypet2, ref KeypetCombinationInfo))
                            {
                                if(isReballPlay && _bTutorial == false)
                                { 
                                    UIEventListener.Get(SelectSource).onClick = null;

                                    foreach (GameObject KeypetSelect in keypetListKey)
                                    {
                                        UIEventListener.Get(KeypetSelect).onClick = null;
                                    }
                                }

                                SelectTargetKeypet(_selectedKeypet2);

                                if (isReballPlay)
                                {
                                    if (_bTutorial && _selectedKeypet2.name == "PuzzleItem_016")
                                    {
                                        _bEndTurtorial = true;
                                    }
                                }
                                else
                                {
                                    if (_bTutorial && _selectedKeypet2.name == "PuzzleItem_015")
                                    {
                                        _bEndTurtorial = true;
                                    }
                                }

                                _KeypetBox1 = _selectedKeypet1;
                                _KeypetBox2 = _selectedKeypet2;

                                _selectedKeypet1 = null;
                                _selectedKeypet2 = null;
                                
                                if (null != _feverTargetGO)
                                {
                                    foreach (GameObject keypetGO in KeypetCombinationInfo.KeypetList)
                                    {
                                        if (_feverTargetGO == keypetGO)
                                        {
                                            ShowFeverTime(_feverWaitTime);

                                            _feverWaitTime = 0.0f;
                                            _feverTargetGO = null;

                                            _guiPlayFeverCount.SetCount(_feverWaitTime);

                                            break;
                                        }
                                    }
                                }

                                puzzleActiveCount += KeypetCombinationInfo.KeypetList.Count;

                                StartCoroutine(DirectKeypetsCombination(KeypetCombinationInfo));

                            }
                            else
                            {
                                if (isReballPlay && _bTutorial == false)
                                {
                                    foreach (GameObject KeypetSelect in keypetListKey)
                                    {
                                        UIEventListener.Get(KeypetSelect).onClick = null;
                                    }
                                }

                                if (!isReballPlay && _bTutorial && !_bPuzzleHudle && _selectedKeypet1 != _selectedKeypet2)
                                {
                                    //튜토리얼 아이템 셋팅
                                    _guiPlayBottom.setUseItem(0, 4,
                                        GameDataMgr.Instance.GetInfo<ItemInfoMgr>().GetItemInfo(4).icon_image, _guiPlayBottom.name);

                                    ItemSettingClick(_bTutorial);

                                    _guiPlayMiddle.SetHandPosition(_guiPlayBottom.GetItemObjectTransForm());

                                    _guiPlayMiddle.SetTalkBoxPostion();

                                    /*switch (Application.systemLanguage)
                                    { 
                                        case SystemLanguage.Portuguese:
                                            break;
                                        case SystemLanguage.English:
                                            _guiPlayMiddle.SetTutorialText("[773326]If other [003260]Keypet [773326]is blocking the road, and have not been able to [00AFEF]rescue.[773326]Please press the Lesser by clicking on the [FF0000]item.");
                                            break;
                                        case SystemLanguage.Korean:
                                            _guiPlayMiddle.SetTutorialText("[773326]길을 다른 [003260]키펫[773326]이 막고 있으면 [00AFEF]구출[773326]할수가 없어요.\n\n[FF0000]아이템[773326]을 누르고 렛서를 눌러주세요.");
                                            break;
                                    }*/

                                    _guiPlayMiddle.SetTutorialText("[773326] Se outra [003260] Keypet [773326] está bloqueando a estrada, e não ter sido capaz de [00AFEF] resgate. [773326] Por favor, pressione o Lesser clicando no [FF0000] item.");
                                         
                                }

                                if (_selectedKeypet1 != _selectedKeypet2)
                                {
                                    SetMoveCount();
                                }

                                CancelSelectSourceKeypet(_selectedKeypet1);
                                CancelSelectTargetKeypet(_selectedKeypet2);

                                if (SaveData.effectSoundOnOff == true)
                                {
                                    AudioMgr.Instance.Play("FailKeypet");
                                }

                                _selectedKeypet1 = null;
                                _selectedKeypet2 = null;
                                
                            }
                        }
                        _guiPlayTop.SetSocre(_curScore);


                        //
                        if (!isReballPlay && _isCount && !_bTutorial)// 카운트 게임 게임오버 체크
                        {
                            ResultType resultType = PlayGameResult();

                            switch (_resultType)
                            {
                                case ResultType.Invalid:
                                    {
                                    }
                                    break;

                                case ResultType.Success:
                                    {
                                        DoBonusTime(_resultType, _isTime ? _maxTime - _curTime : 0.0f, _isCount ? _maxCount - _curCount : 0);

                                        _isTime = false;
                                    }
                                    break;
                                default:
                                    {
                                        _resultType = _resultType;
                                        _state = State.EndPlay;
                                    }
                                    break;
                            }

                            if (_curCount >= _maxCount && resultType != ResultType.Success && !_bTutorial)
                            {
                                _resultType = ResultType.CountOut;
                                _state = State.EndPlay;

                                continue;
                            }
                        }

                        if (!isReballPlay && 0 == _dirCount && !_bTutorial)
                        {
                            ResultType resultType = PlayGameResult();

                            switch (resultType)
                            {
                                case ResultType.Invalid:
                                    {
                                    }
                                    break;

                                case ResultType.Success:
                                    {
                                        DoBonusTime(resultType, _isTime ? _maxTime - _curTime : 0.0f, _isCount ? _maxCount - _curCount : 0);

                                        _isTime = false;
                                    }
                                    break;
                                default:
                                    {
                                        _resultType = resultType;
                                        _state = State.EndPlay;
                                    }
                                    break;
                            }
                        }

                        if(isReballPlay)
                        {
                            if (_isCombine)
                            {
                                yield return null;

                                continue;
                            }

                            yield return new WaitForSeconds(0.2f);

                            ResultType resultType = PlayGameResult();

                            switch (resultType)
                            {
                                case ResultType.Invalid:
                                    {
                                    }
                                    break;

                                case ResultType.Success:
                                    {
                                        isReballPlay = false;

                                        DoBonusTime(resultType, _isTime ? _maxTime - _curTime : 0.0f, _isCount ? _maxCount - _curCount : 0);

                                        _isTime = false;
                                    }
                                    break;
                                default:
                                    {
                                        isReballPlay = false;
                                        _resultType = resultType;
                                        _state = State.EndPlay;
                                    }
                                    break;
                            }

                            if (_isCount && _curCount >= _maxCount && resultType != ResultType.Success && !_bTutorial)
                            {
                                _resultType = ResultType.CountOut;
                                _state = State.EndPlay;

                                continue;
                            }

                        }

                        #endregion Play
                    }
                    break;

                /*
                 * 플레이 종료 상태.
                 * 
                 */

                case State.EndPlay:
                    {
                        #region EndPlay

                        if (KakaoMain.Instance.isReballPlay)
                            KakaoMain.Instance.isReballPlay = false;

                        HideFeverTime(false);

                        foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
                        {
                            UIEventListener.Get(puzzleItem).onClick = null;
                        }

                        _state = State.Result;

                        #endregion EndPlay
                    }
                    break;

                /*
                 * 플레이 결과 상태.
                 * 
                 */

                case State.Result:
                    {
                        #region Result

                        if (_guiPlayFeverCount != null)
                            _guiPlayFeverCount.HideFiverCount();

                        yield return new WaitForSeconds(1.0f);
                        switch (_resultType)
                        {

                            case ResultType.Success:
                                {
                                    if (isReballReward)
                                    {
                                        KakaoMain.Instance.isReballPlay = false;

                                        SaveData.ClearTopStage(ReballStage);
                                    }

                                    //KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._nScoreCurrent = (int)_curScore;
                                    //KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._sStage = int.Parse(_params["stage_si"]).ToString();

                                    string packagename = "";

                                    if (SaveData.HighClearStage(int.Parse(_params["stage_si"]), _curScore))
                                    {
                                        int itemSu, rewardItemSI;
                                        GetRewardResult(out rewardItemSI, out itemSu, out packagename);	// 어떤 보상 아이템을 몇 개 획득했는지..
                                        
                                        for(int  i = 8 ; i < 14; i++)
                                        {
                                            if (rewardItemSI == i) 
                                            {
                                                PlayerPrefs.SetInt("Keypet", 1);

                                                PlayerPrefs.Save();
                                            }
                                        }

                                        _guiPlayResultSuccess = GuiMgr.Instance.Show<GuiPlayResultSuccess>(GuiMgr.ELayerType.Front, true, null);

                                        SetRewardPoint();

                                        // 보상상자 지급 에니메이션이 모두 끝났는지 확인하는 변수 세팀...
                                        bool isEndRewardSuccess = false;
                                        _guiPlayResultSuccess.OnDirectionFinished = () => { isEndRewardSuccess = true; };

                                        _guiPlayResultSuccess.RewardBtn_01.onClick = (GameObject pGO) =>
                                        {
                                            Choice_PlayRewardBox(rewardItemSI, itemSu);
                                        };

                                        _guiPlayResultSuccess.RewardBtn_02.onClick = (GameObject pGO) =>
                                        {
                                            Choice_PlayRewardBox(rewardItemSI, itemSu);
                                        };

                                        _guiPlayResultSuccess.RewardBtn_03.onClick = (GameObject pGO) =>
                                        {
                                            Choice_PlayRewardBox(rewardItemSI, itemSu);
                                        };


                                        _guiPlayResultSuccess.OkBtn.onClick = (GameObject pGO) =>
                                        {
                                            Debug.Log("isEnd : " + isEndRewardSuccess);

                                            // 보상상자 지급 에니메이션이 모두 끝났을 경우에만 확인버튼을 누르고 나갈 수 있다..
                                            if (isEndRewardSuccess)
                                            {
                                                _guiPlayResultSuccess.OkBtn.onClick = null;
                                                _guiPlayResultSuccess.CloseBtn.onClick = null;


                                                //Hwang Wark
                                                SaveData.MapScene();
                                                //Hwang End

                                                int openstage = SaveData.clearStageTopNumber;
                                                int _nStage = int.Parse(_params["stage_si"]);

                                                SetNextStage(openstage, _nStage);

                                                //GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
                                                //Chage
                                            }
                                        };

                                        _guiPlayResultSuccess.CloseBtn.onClick = (GameObject pGO) =>
                                        {
                                            // 보상상자 지급 에니메이션이 모두 끝났을 경우에만 버튼을 누르고 나갈 수 있다..
                                            // 현재는 CloseBtn 오브젝트가 비활성화되어 있어서 아래 코드는 동작하지 않는 듯...
                                            if (isEndRewardSuccess)
                                            {
                                                _guiPlayResultSuccess.OkBtn.onClick = null;
                                                _guiPlayResultSuccess.CloseBtn.onClick = null;

                                                //StartCoroutine(IE_ENDProcess(FBMain.Instance.isFB));
                                            }
                                        };

                                        //캡슐화를 통한 Object에서 가져온다.
                                        //GSPlay는 게임 로직과 관련된 처리만 한다. 그런데, 불필요한 Facebook  함수가 있으면 코드가 시각화가 좋지 않음.
                                        //그런데....
                                        //페이스북 싱글톤으로 만들어 버림
                                        //괜찮을려나? 오늘의 Comment !ß

                                        if (SaveData.FirstStageClear(int.Parse(_params["stage_si"])))
                                        {
                                            //if(FBMain.Instance.isFB && !isReballPlay)
                                            //{ 
                                            //    if ((SaveData.clearStageTopNumber % (int)MaxLinkedStageInfo.count != 0) &&
                                            //        SaveData.clearStageTopNumber <= int.Parse(_params["stage_si"]) && !isReballReward)//&& SaveData.ReturnMyReballPlay() == false)
                                            //    {
                                            //        InteractiveConsole.GetInstance().CallFBPost(false, int.Parse(_params["stage_si"]) + 1);
                                            //    }
                                            //}

                                            _guiPlayResultSuccess.BoastBtn.gameObject.SetActive(true);

                                            _guiPlayResultSuccess.BoastBtn.onClick = (GameObject pGO) =>
                                            {
                                                if (isEndRewardSuccess)
                                                {
                                                    //페이스북 인스톨
                                                    //InteractiveConsole.GetInstance().CallFBActivateApp();

                                                    int _nStage = int.Parse(_params["stage_si"]);

                                                    
                                                    JSONNode msgParam = new JSONClass();
                                                    msgParam["message"] = "Tem certeza de que quer se vangloriar no Facebook?";
                                                    msgParam["show_type"] = "two";

                                                    GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                                                    guiMessageBox.OnButtonClick = (bool pIsOk) =>
                                                    {
                                                        if(pIsOk)
                                                        { 
                                                            //StartCoroutine(FacebookCallAPI(_nStage, packagename, msgParam));
                                                        }
                                                    };


                                                }
                                            };
                                        }
                                        else
                                        {
                                            _guiPlayResultSuccess.BoastBtn.gameObject.SetActive(false);
                                        }

                                    }
                                    else
                                    {
                                        _guiPlayResultSuccess = GuiMgr.Instance.Show<GuiPlayResultSuccess>(GuiMgr.ELayerType.Front, true, null);
                                        _guiPlayResultSuccess.ActiveNormal();

                                        _guiPlayResultSuccess.SetStage(int.Parse(_params["stage_si"]));
                                        _guiPlayResultSuccess.SetMyScore(_curScore);

                                        _guiPlayResultSuccess.OkBtn.onClick = (GameObject pGO) =>
                                        {
                                            _guiPlayResultSuccess.OkBtn.onClick = null;
                                            _guiPlayResultSuccess.CloseBtn.onClick = null;
                                            //Hwang Wark
                                            SaveData.MapScene();
                                            //Hwang End
                                            //GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
                                            // test next
                                            SetNextStage(SaveData.clearStageTopNumber, int.Parse(_params["stage_si"]));
                                        };

                                        _guiPlayResultSuccess.BoastBtn.gameObject.SetActive(false);
                                    }
                                    
                                    
                                    SaveData.nowScore = _curScore;

                                    if(!isReballReward)
                                        SaveData.ClearStage(int.Parse(_params["stage_si"]));
                                }
                                break;

                            case ResultType.CanNotMove:
                            case ResultType.TimeOut:
                            case ResultType.CountOut:
                                {
                                    //TotalServerRank("1_" + uint.Parse(_params["stage_si"]).ToString(), (float)_curScore, false);

                                    _guiPlayResultFail = GuiMgr.Instance.Show<GuiPlayResultFail>(GuiMgr.ELayerType.Front, true, null);
                                    _guiPlayResultFail.SetStageNumber(_params["stage_si"].ToString());

                                    //141015 - Ahn
                                    _guiPlayResultFail.RePlayBtn.onClick = (GameObject pGO) =>
                                    {
                                        //Hwang Wark Message Popup Start
                                        JSONNode msgParam = new JSONClass();

                                        /*switch (Application.systemLanguage)
                                        {
                                            case SystemLanguage.Portuguese:
                                                break;
                                            case SystemLanguage.English:
                                                msgParam["message"] = "The screen to get ready.";
                                                break;
                                            case SystemLanguage.Korean:
                                                msgParam["message"] = "준비 화면으로 이동합니다.";
                                                break;
                                        }*/
                                        msgParam["message"] = "A tela para ficar pronto";
                                                
                                        msgParam["show_type"] = "two";

                                        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                                        //Hwang Wark Message Popup End
                                        guiMessageBox.OnButtonClick = (bool pIsOk) =>
                                        {
                                            if (pIsOk)
                                            {
                                                _guiPlayResultFail.RePlayBtn.onClick = null;
                                                _guiPlayResultFail.CloseBtn.onClick = null;

                                                _guiPlayResultFail = null;

                                                GuiMgr.Instance.Show<GuiPlayResultFail>(GuiMgr.ELayerType.Front, false, null);

                                                //_state = State.BeginPlay;
                                                SaveData.rePlay = true;

                                                if (KakaoMain.Instance._bTutorailStart)
                                                    KakaoMain.Instance._bTutorailStart = false;

                                                if (null != _guiPlayFever)
                                                {
                                                    _guiPlayFever.Pause(false);
                                                }

                                                _isPause = false;


#if UNITY_EDITOR
                                                Debug.Log("Editor Load");
#elif UNITY_ANDROID
                                                /*
                                                if(stageInfoSI > 10)
                                                {
                                                AdlibPlugin.LoadInterstitialAd();

                                                AdlibPlugin.ShowPopBanner("#ff444444",
                                                                          AdlibPlugin.PopButton.White,
                                                                          true,
                                                                          false,
                                                                          AdlibPlugin.PopAlign.Bottom,
                                                                          60);
                                                }*/
                                                Debug.Log("haven't AD Editor Load");
#endif

                                                GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
                                            }
                                        };
                                    };

                                    _guiPlayResultFail.CloseBtn.onClick = (GameObject pGO) =>
                                    {
                                        _guiPlayResultFail.RePlayBtn.onClick = null;
                                        _guiPlayResultFail.CloseBtn.onClick = null;

                                        SaveData.MapScene();

                                        GameStateMgr.Instance.ChangeState<GSStageMap>(0,  null);
                                    };
                                }
                                break;
                        }

                        _state = State.FinishWait;

                        #endregion Result
                    }
                    break;

                /*
                 * 플레이 종료 대기 상태.
                 * 
                 */

                case State.FinishWait:
                    {
                    }
                    break;
            }

            yield return null;
        }
    }

    private bool _bGsPlaySuccess;
    private float _fGsTotalScore = 0.0f;

    /*
    private void TotalServerRank(string _area_round, float _TotalScore, bool isSuccess)
    {
        _bGsPlaySuccess = isSuccess;
        _fGsTotalScore = _TotalScore;

        Dictionary<string, int> scores = new Dictionary<string, int>();

        uint _nStage = uint.Parse(_params["stage_si"]);

        int[] ixarr_slot = new int[3] { 0, 0, 0 };
        for (int i = 0; i < MyInfoMgr.Instance.m_UseItems.Count; i++)
        {
            ixarr_slot[i] = (int)MyInfoMgr.Instance.m_UseItems[i];
        }
        if (_bGsPlaySuccess == false)
        {
            Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ false");
            StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_END_GAME_REQ(false,
                0, (int)_nStage, ixarr_slot[0], ixarr_slot[1], ixarr_slot[2]));

            scores.Add(_area_round, 0);
        }
        else
        {
            StartCoroutine(IE_SuccessProc(true, (int)_fGsTotalScore, (int)_nStage, ixarr_slot[0], ixarr_slot[1], ixarr_slot[2]));
            scores.Add(_area_round, (int)_TotalScore);
        }

//        #region isKakao End 카카오톡 버전이 아닌 버전이므로 삭제를 한다.
//#if UNITY_EDITOR
//#elif UNITY_ANDROID || UNITY_IPHONE
       
//        byte[] publicData = Encoding.UTF8.GetBytes("level|001");
//        byte[] privateData = Encoding.UTF8.GetBytes("money|1200");

//        KakaoNativeExtension.Instance.updateMultipleResults(scores,
//            1000, publicData, privateData, onUpdateResultComplete,
//            onUpdateResultError);
//#endif
//        #endregion
    }
    */

    /*
    IEnumerator IE_SuccessProc(bool _bClear, int _nScore, int _nStage, int _slot1, int _slot2, int _slot3)
    {
        Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~IE_SuccessProc - _bClear:" + _bClear);
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

        int openstage = 0;
        bool isBest = false;
        int resultPack_idx = 0;
        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
        cmd._sOutputString = "";
        //
        yield return StartCoroutine(cmd.Send_PROTOCOL_END_GAME_REQ(true, _nScore, (int)_nStage, _slot1, _slot2, _slot3));

        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

        if (cmd._sOutputString == "")
        {
            //            Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
            cmd.PlayErrorMessage(cmd._DefaultMessage, 0);
        }
        else
        {
            JSONNode root = JSON.Parse(cmd._sOutputString);

            var resultCode = int.Parse(root["rc"]);

            try
            {
                if (resultCode == 0)
                {
                    resultPack_idx = int.Parse(root["pack_idx"]);
                    openstage = int.Parse(root["open_stage"]);

                    SaveData.clearStageTopNumber = openstage;

                    StartCoroutine(cmd.Send_PROTOCOL_LIST_ITEM_REQ());
                    cmd.PackItem_SetGemFunny(cmd._sOutputString);

                    string rewardIcon = "";
                    string strName = "";
                    if (resultPack_idx == 0)
                    {
                        isBest = false;
                    }
                    else
                    {
                        isBest = true;
                        PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)resultPack_idx);
                        if (packageInfo != null)
                        {
                            rewardIcon = packageInfo.icon_image;
                            strName = packageInfo.name;
                            Debug.Log("~~~~~~~~~~~~~ Package Index : " + resultPack_idx.ToString() + " name:" + strName + " icon:" + rewardIcon);
                        }
                    }

                    if (isBest)
                    {
                        _guiPlayResultSuccess = GuiMgr.Instance.Show<GuiPlayResultSuccess>(GuiMgr.ELayerType.Front, true, null);
                        SetRewardPoint();

                        bool isEndRewardSuccess = false;
                        _guiPlayResultSuccess.OnDirectionFinished = () => { isEndRewardSuccess = true; };

                        _guiPlayResultSuccess.RewardBtn_01.onClick = (GameObject pGO) =>
                        {
                            Choice_PlayRewardBox(rewardIcon, strName);
                        };

                        _guiPlayResultSuccess.RewardBtn_02.onClick = (GameObject pGO) =>
                        {
                            Choice_PlayRewardBox(rewardIcon, strName);
                        };

                        _guiPlayResultSuccess.RewardBtn_03.onClick = (GameObject pGO) =>
                        {
                            Choice_PlayRewardBox(rewardIcon, strName);
                        };

                        _guiPlayResultSuccess.KakaotalkBtn.onClick = (GameObject pGO) =>
                        {
                            _guiPlayResultSuccess.ShowKakaotalkBoast(true);
                        };

                        _guiPlayResultSuccess.OkBtn.onClick = (GameObject pGO) =>
                        {
                            // 보상상자 지급 에니메이션이 모두 끝났을 경우에만 확인버튼을 누르고 나갈 수 있다..
                            if (isEndRewardSuccess)
                            {
                                _guiPlayResultSuccess.KakaotalkBtn.onClick = null;
                                _guiPlayResultSuccess.OkBtn.onClick = null;
                                _guiPlayResultSuccess.CloseBtn.onClick = null;

                                //Hwang Wark
                                SaveData.MapScene();
                                //Hwang End
                                // test next
                                SetNextStage(openstage, _nStage);

                                //GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
                            }
                        };

                        _guiPlayResultSuccess.CloseBtn.onClick = (GameObject pGO) =>
                        {
                            // 보상상자 지급 에니메이션이 모두 끝났을 경우에만 버튼을 누르고 나갈 수 있다..
                            // 현재는 CloseBtn 오브젝트가 비활성화되어 있어서 아래 코드는 동작하지 않는 듯...
                            if (isEndRewardSuccess)
                            {
                                _guiPlayResultSuccess.KakaotalkBtn.onClick = null;
                                _guiPlayResultSuccess.OkBtn.onClick = null;
                                _guiPlayResultSuccess.CloseBtn.onClick = null;

                                GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
                            }
                        };
                    }
                    else
                    {
                        _guiPlayResultSuccess = GuiMgr.Instance.Show<GuiPlayResultSuccess>(GuiMgr.ELayerType.Front, true, null);
                        _guiPlayResultSuccess.ActiveNormal();

                        _guiPlayResultSuccess.SetStage(_nStage);
                        _guiPlayResultSuccess.SetMyScore(_nScore);

                        _guiPlayResultSuccess.OkBtn.onClick = (GameObject pGO) =>
                        {
                            _guiPlayResultSuccess.KakaotalkBtn.onClick = null;
                            _guiPlayResultSuccess.OkBtn.onClick = null;
                            _guiPlayResultSuccess.CloseBtn.onClick = null;
                            //Hwang Wark
                            SaveData.MapScene();
                            //Hwang End
                            //GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
                            // test next
                            SetNextStage(openstage, _nStage);
                        };
                    }
                }
                else
                {
                    cmd.PlayErrorMessage(cmd._DefaultMessage, resultCode);
                }
            }
            catch
            {
                cmd.PlayErrorMessage(cmd._DefaultMessage, resultCode);
            }
        }

        yield return null;
    }
     */

    void SetNextStage(int open_stage, int current)
    {
        Debug.Log("OpenStage !");

        if (current + 1 <= open_stage)
        {
            SaveData.clearStageNumber = current + 1;
            SaveData.rePlay = true;

            //  //UNITY_ANDROID
            //#if UNITY_EDITOR
            //            Debug.Log("Editor Load");
            //#elif UNITY_ANDROID
            //                                                if(stageInfoSI > 10)
            //                                                {
            //                                                AdlibPlugin.LoadInterstitialAd();

            //                                                AdlibPlugin.ShowPopBanner("#ff444444",
            //                                                                          AdlibPlugin.PopButton.White,
            //                                                                          true,
            //                                                                          false,
            //                                                                          AdlibPlugin.PopAlign.Bottom,
            //                                                                          60);
            //                                                }
            //#endif
            //CaulyAdmobMain.GetInstance().RequestInterstitial();

        }

        Debug.Log("IsFB");

        //if (FBMain.Instance.isFB)
        //{ 
        //    StartCoroutine(IE_ENDProcess(FBMain.Instance.isFB));
        //}
        //else 
        //{ 
            GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
        //}
    }

    //UpdateResult
    private void onUpdateResultComplete()
    {
    }

    private void onUpdateResultError(string status, string message)
    {
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    //연결, 장애물, 아이템(긴급구출), 피버카운트
    private bool _bPuzzleConnent = false;
    private bool _bPuzzleHudle = false;
    private bool _bPuzzleItem = false;


    //리볼 연결하기
    private bool _bPuzzleContiune = false;
    private bool _bPuzzleLeftdiagonal = false;
    private bool _bPuzzleRightdiagonal = false;
    /*
     * 첫번째 키펫이 선택될때 호출하는 함수.
     * 
     */

    List<GameObject> keypetListKey = new List<GameObject>();
    private GameObject SelectSource = null;
    protected void SelectSourceKeypet(GameObject pKeypet)
    {
        //키펫이 선택되는것들이 아니라고 하면 리턴 시켜라~
        if (!PlayTutorial(pKeypet))
            return;

        SelectSource = pKeypet;

        KeypetCtrl keypetCtrl = pKeypet.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        keypetBGCtrl.Press(true);
        keypetCtrl.ActPress(false);

        if (SaveData.effectSoundOnOff == true)
        {
            AudioMgr.Instance.Play("SelectKeypet");
        }

        /*
         * 키펫 브레이크에서는 키펫 퍼즐이 떨어저 있어도 연결하여 없앨수 있습니다.
         */
        foreach (KeypetBGCtrl selKeypetBGCtrl in _selectedKeypetList)
        {
            selKeypetBGCtrl.Selected(false);
        }

        _selectedKeypetList.Clear();

        keypetListKey.Clear();
                
        for (PlacementDir placementDir = PlacementDir.Begin; placementDir < PlacementDir.Max; ++placementDir)
        {
            GuardCombineKeypets(placementDir, pKeypet, ref keypetListKey, null);

            if (keypetListKey.Count >= 1)
            {
                foreach (GameObject keypetGO in keypetListKey)
                {
                    KeypetBGCtrl selKeypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetGO.GetComponentInChildren<KeypetCtrl>().CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

                    selKeypetBGCtrl.Selected(true);

                    _selectedKeypetList.Add(selKeypetBGCtrl);

                    if (isReballPlay && !_bTutorial)
                    {
                        UIEventListener.Get(keypetGO).onClick = (GameObject pGO) =>
                        {
                            if (_isRescueMode)
                            {
                                if (null == _rescueKeypte)
                                {
                                    _rescueKeypte = pGO;
                                }
                            }
                            else
                            {
                                if (null == _selectedKeypet1)
                                {
                                    _selectedKeypet1 = pGO;
                                    _selectedKeypet2 = null;

                                    SelectSourceKeypet(_selectedKeypet1);
                                }
                                else if (null == _selectedKeypet2)
                                {
                                    _selectedKeypet2 = pGO;
                                }
                            }
                        };
                    }
                }
            }
            
        }

    }

    IEnumerator IE_Tutorial()
    {
        if (isReballPlay)
        {
            while (true)
            {
                if (_bPuzzleContiune)
                {
                    SettingPuzzleItemName("PuzzleItem_031");

                    break;
                }

                yield return new WaitForFixedUpdate();
            }

            while (true)
            {
                if (_bPuzzleLeftdiagonal)
                {
                    SettingPuzzleItemName("PuzzleItem_061");

                    break;
                }

                yield return new WaitForFixedUpdate();
            }
            while (true)
            {
                if (_bEndTurtorial)
                {
                    StartCoroutine(IE_TutorialEnd());

                    break;
                }
                yield return new WaitForFixedUpdate();
            }
        }
        else
        {
            while (true)
            {
                if (_bPuzzleConnent)
                {
                    SettingPuzzleItemName("PuzzleItem_013");

                    break;
                }

                yield return new WaitForFixedUpdate();
            }

            while (true)
            {
                if (_bPuzzleItem)
                {
                    _guiPlayMiddle.SetTalkBoxZeroX();
                    SettingPuzzleItemName("PuzzleItem_003");

                    break;
                }
                yield return new WaitForFixedUpdate();
            }

            while (true)
            {
                if (_bEndTurtorial)
                {
                    StartCoroutine(IE_TutorialEnd());

                    break;
                }
                yield return new WaitForFixedUpdate();
            }
        }
    }

    //연결, 장애물, 아이템(긴급구출), 피버카운트
    bool PlayTutorial(GameObject pKeypet)
    {
        if (_bTutorial)
        {
            UIEventListener.Get(pKeypet).onClick = (GameObject pGO) =>
            {
                /*switch (Application.systemLanguage)
                {
                    case SystemLanguage.Portuguese:
                        break;
                    case SystemLanguage.Korean:
                        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                        break;
                    case SystemLanguage.English:
                        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                        break;
                }*/
                
                _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                  
            };

            if (!isReballPlay)
            {
                if (_bPuzzleConnent == false)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_021")
                    {
                        SettingPuzzleItemName("PuzzleItem_006");

                        return true;
                    }
                    else
                    {
                        /*
                        switch (Application.systemLanguage)
                        {
                            case SystemLanguage.Portuguese:
                                break;
                            case SystemLanguage.Korean:
                                _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                                break;
                            case SystemLanguage.English:
                                _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                                break;
                        }*/
                        
                        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                        
                        return false;
                    }
                }
                else if (_bPuzzleHudle == false && _bPuzzleConnent)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_013")
                    {
                        //_bPuzzleHudle = true;

                        SettingPuzzleItemName("PuzzleItem_001");

                        return true;
                    }
                }
                else if (_bPuzzleHudle && _bPuzzleConnent)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_012")
                    {
                        //_bPuzzleHudle = true;

                        SettingPuzzleItemName("PuzzleItem_014");
                        _guiPlayMiddle.SetTalkBoxPostion2();

                        return true;
                    }
                }

                if (_bPuzzleItem && _bPuzzleHudle && _bPuzzleConnent)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_003")
                    {
                        _guiPlayMiddle.SetTalkBoxPostion3();
                        SettingPuzzleItemName("PuzzleItem_015");

                        return true;
                    }
                }
            }
            else
            {
                if (_bPuzzleContiune == false)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_009")
                    {
                        SettingPuzzleItemName("PuzzleItem_036");
                        return true;
                    }
                    else
                    {
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                        //        break;
                        //    case SystemLanguage.English:
                        //        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                        //        break;
                        //} 
                        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                                
                        return false;
                    }
                }
                else if (_bPuzzleLeftdiagonal == false && _bPuzzleContiune)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_031")
                    {
                        //_bPuzzleHudle = true;

                        SettingPuzzleItemName("PuzzleItem_027");

                        return true;
                    }
                    else
                    {
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                        //        break;
                        //    case SystemLanguage.English:
                        //        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                        //        break;
                        //}
                        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                               
                        return false;
                    }
                }
                else if (_bPuzzleLeftdiagonal && _bPuzzleContiune)
                {
                    if (pKeypet.gameObject.name == "PuzzleItem_061")
                    {
                        //_bPuzzleHudle = true;

                        SettingPuzzleItemName("PuzzleItem_016");
                        //_guiPlayMiddle.SetTalkBoxPostion2();

                        return true;
                    }
                    else
                    {
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                        //        break;
                        //    case SystemLanguage.English:
                        //        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                        //        break;
                        //}
                        _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                               
                        return false;
                    }
                }
            }
        }
        else
        {
            return true;
        }

        return false;
    }

    /*
     * 첫번째 키펫의 선택을 취소하는 함수.
     * 
     */
    protected void CancelSelectSourceKeypet(GameObject pKeypet)
    {
        foreach (KeypetBGCtrl selKeypetBGCtrl in _selectedKeypetList)
        {
            selKeypetBGCtrl.Selected(false);
        }

        _selectedKeypetList.Clear();

        KeypetCtrl keypetCtrl = pKeypet.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        keypetBGCtrl.Press(false);
        keypetCtrl.ActFail();
    }

    /*
     * 타겟 키펫을 선택할 때 호출하는 함수.
     * 
     */
    protected void SelectTargetKeypet(GameObject pKeypet)
    {
        if (_bTutorial)
        {
            if (!isReballPlay)
            {
                //튜토 할때 연결 안되어 있으면 리턴
                if (!_bPuzzleConnent)
                {
                    if (pKeypet.name == "PuzzleItem_006")
                    {
                        //_guiPlayMiddle.SetHandLoopCancle();
                        _bPuzzleConnent = true;

                        Debug.Log("_bPuzzleConnent");
                    }
                    else
                    {
                        return;
                    }
                }

                if (_bPuzzleHudle && _bPuzzleConnent)
                {
                    if (pKeypet.name == "PuzzleItem_014")
                    {
                        _bPuzzleItem = true;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            else
            {
                //튜토 할때 연결 안되어 있으면 리턴
                if (!_bPuzzleContiune)
                {
                    if (pKeypet.name == "PuzzleItem_036")
                    {
                        //_guiPlayMiddle.SetHandLoopCancle();
                        _bPuzzleContiune = true;
                    }
                    else
                    {
                        return;
                    }
                }

                if (_bPuzzleContiune)
                {
                    if (pKeypet.name == "PuzzleItem_027")
                    {
                        _bPuzzleLeftdiagonal = true;
                    }
                    else
                    {
                        return;
                    }
                }
            }

        }

        foreach (KeypetBGCtrl selKeypetBGCtrl in _selectedKeypetList)
        {
            selKeypetBGCtrl.Selected(false);
        }

        _selectedKeypetList.Clear();
        /*
        KeypetCtrl keypetCtrl = pKeypet.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        keypetBGCtrl.Press(true);
        keypetCtrl.ActPress(false);
        */
        if (SaveData.effectSoundOnOff == true)
        {
            AudioMgr.Instance.Play("SelectKeypet");
        }
    }

    /*
     * 타겟 키펫의 선택을 취소하는 함수.
     * 
     */
    protected void CancelSelectTargetKeypet(GameObject pKeypet)
    {
        foreach (KeypetBGCtrl selKeypetBGCtrl in _selectedKeypetList)
        {
            selKeypetBGCtrl.Selected(false);
        }

        _selectedKeypetList.Clear();

        KeypetCtrl keypetCtrl = pKeypet.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        keypetBGCtrl.Press(false);
        keypetCtrl.ActFail();
    }

    /*
     * 구출 아이템.
     * 
     */
    protected void DoUseItem_Rescue()
    {
        if (_isRescueMode)
        {
            return;
        }

        if (_bTutorial) 
        {
            SaveData.playIntro = false;
        }

        _isRescueMode = true;
        _rescueKeypte = null;

        if (null != _selectedKeypet1)
        {
            CancelSelectSourceKeypet(_selectedKeypet1);
            _selectedKeypet1 = null;
        }

        if (null != _selectedKeypet2)
        {
            CancelSelectTargetKeypet(_selectedKeypet2);
            _selectedKeypet2 = null;
        }

        StartCoroutine(UseItem_Rescue());
    }

    protected void DoUseItem_RandomRescue()
    {
        if (_isRescueMode)
        {
            return;
        }

        _isRescueMode = true;
        _rescueKeypte = null;

        if (null != _selectedKeypet1)
        {
            CancelSelectSourceKeypet(_selectedKeypet1);
            _selectedKeypet1 = null;
        }

        if (null != _selectedKeypet2)
        {
            CancelSelectTargetKeypet(_selectedKeypet2);
            _selectedKeypet2 = null;
        }
        SaveData.randomRescue = true;
        StartCoroutine(UseItem_Rescue());
    }

    /*
     * 시간 되돌리기 아이템.
     * 
     */
    protected void DoUseItem_SaveTime(float pTime)
    {
        _curTime -= pTime;

        if (_curTime < 0.0f)
        {
            //_curTime = 0.0f;
        }

        _guiPlayTop.SetTime(_curTime, _maxTime);

        Camera backCam = GuiMgr.Instance.GetBackCamera();
        Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

        Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(_guiPlayTop.GetTimePos()));
        effectTargetPos.z = 0.0f;

        GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), effectTargetPos, Quaternion.identity);
        //Debug.Log (_curTime);

        itemUseWark = false;
    }

    void Show_feverWaitTime()
    {
        List<GameObject> puzzleItemList = new List<GameObject>();


        foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
        {
            KeypetCtrl keypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();
            if (keypetCtrl == null)
                continue;
            if (keypetCtrl.IsBlank || keypetCtrl.gameObject.activeInHierarchy == false)
            {
                continue;
            }
            puzzleItemList.Add(puzzleItem);
        }
        if (puzzleItemList.Count <= 0)
            return;

        _feverTargetGO = puzzleItemList[Random.Range(0, puzzleItemList.Count)];
        _guiPlayFeverCount.SetCount(_feverWaitTime);
        _guiPlayFeverCount.ShowFiverCount();
        _guiPlayFeverCount.ShowOn(false);

        Vector3 curPos = _feverTargetGO.transform.position;
        curPos = GuiMgr.Instance.GetFrontCamera().ScreenToWorldPoint(GuiMgr.Instance.GetBackCamera().WorldToScreenPoint(curPos));
        _guiPlayFeverCount.Move(curPos);
    }

    protected void DoUseItem_FeverSaveTime(float pTime)
    {
        if (_isFeverTime == false)
        {
            if (_feverWaitTime <= 0)
            {
                _feverWaitTime = 10f;
                Show_feverWaitTime();
            }
            else
            {
                _feverWaitTime += 10;
            }
        }
        else
        {
            //_feverReaminTime -= pTime;
            _feverReaminTime += 10;
            if (_feverReaminTime < 0.0f)
            {
                //_feverReaminTime = 0.0f;
            }
        }


        //_guiPlayTop.SetTime(_feverReaminTime, _maxTime);

        Camera frontCam = GuiMgr.Instance.GetFrontCamera();
        Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();
        
        Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(frontCam.WorldToScreenPoint(_guiPlayFeverCount.transform.position));
        effectTargetPos.z = 0.0f;

        GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), effectTargetPos, Quaternion.identity);

        itemUseWark = false;
    }

    /*
     * 카운트 감소 아이템.
     * 
     */
    protected void DoUseItem_SaveCount(int pCount)
    {
        _curCount -= pCount;

        if (_curCount < 0)
        {
            //_curCount = 0;
        }

        _guiPlayTop.SetCount(_curCount, _maxCount, false);

        Camera backCam = GuiMgr.Instance.GetBackCamera();
        Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

        Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(_guiPlayTop.GetMoveInfoPos()));
        effectTargetPos.z = 0.0f;

        GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), effectTargetPos, Quaternion.identity);

        itemUseWark = false;
    }

    /*
     * 되돌리기 아이템.
     * 
     */
    protected void DoUseItem_Undo()
    {
        if (_undoList.Count <= 0)
        {
            return;
        }

        UndoInfo undoInfo = _undoList[_undoList.Count - 1];
        _undoList.RemoveAt(_undoList.Count - 1);

        foreach (KeypetDirInfo keypetInfoDir in undoInfo.undoList)
        {
            keypetInfoDir.KeypetGO.SetActive(true);

            keypetInfoDir.Keypet.ActIdle();

            keypetInfoDir.KeypetBG.Selected(false);
            keypetInfoDir.KeypetBG.Press(false);
            keypetInfoDir.KeypetBG.Rescue(false);

            keypetInfoDir.Keypet.Restore();

            UIEventListener.Get(keypetInfoDir.KeypetGO.gameObject).onClick = (GameObject pGO) =>
            {
                if (_isRescueMode)
                {
                    if (null == _rescueKeypte)
                    {
                        _rescueKeypte = pGO;
                    }
                }
                else
                {
                    if (null == _selectedKeypet1)
                    {
                        _selectedKeypet1 = pGO;
                        _selectedKeypet2 = null;

                        //SelectSourceKeypet(_selectedKeypet1);
                    }
                    else if (null == _selectedKeypet2)
                    {
                        _selectedKeypet2 = pGO;
                    }

                }
            };
        }

        _curScore -= undoInfo.score;

        _guiPlayTop.SetSocre(_curScore);
    }

    /*
     * 키펫 함체 가능 여부 판단 및 함체되는 키펫들의 정보를 얻기 위한 함수.
     * 
     */
    protected void GuardCombineKeypets(PlacementDir pPlacementDir, GameObject pCurKeypet, ref List<GameObject> pKeypetList, GameObject pSrcKeypet, bool isReballCombine = false)
    {
        KeypetCtrl curKeypetCtrl = pCurKeypet.GetComponentInChildren<KeypetCtrl>();

        KeypetType srcKeypetType = curKeypetCtrl.CurKeypetType;

        int curX = curKeypetCtrl.CurSlotIndex % _puzzleWidth;
        int curY = curKeypetCtrl.CurSlotIndex / _puzzleWidth;

        int maxCount = Mathf.Max(_puzzleWidth, _puzzleHeight);

        for (int curCount = 0; curCount < maxCount; ++curCount)
        {
            curX += _dirLeanX[(int)pPlacementDir];
            curY += _dirLeanY[(int)pPlacementDir];

            if (curX < 0 || curX >= _puzzleWidth)
            {
                return;
            }

            if (curY < 0 || curY >= _puzzleHeight)
            {
                return;
            }

            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(curX + curY * _puzzleWidth);

            if (isReballPlay)
            {
                if (puzzleItem.activeSelf)
                {
                    KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                    KeypetBGCtrl selKeypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(puzzleItem.GetComponentInChildren<KeypetCtrl>().CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

                    if (isReballCombine)
                    {
                        if (pKeypetList.Contains(pSrcKeypet) == false)
                        {
                            pKeypetList.Add(pSrcKeypet);
                        }

                        if (pKeypetList.Contains(pCurKeypet) == false)
                        {
                            pKeypetList.Add(pCurKeypet);
                        }

                        KeypetCtrl SelectSource = pSrcKeypet.GetComponentInChildren<KeypetCtrl>();

                        KeypetType GoodsrcKeypetType = SelectSource.CurKeypetType;

                        if (nextKeypetCtrl.IsBlank)
                        {
                            return;
                        }

                        pKeypetList.Add(puzzleItem);
                    }
                    else
                    {
                        if (nextKeypetCtrl.IsBlank == false)
                        {
                            return;
                        }

                        pKeypetList.Add(puzzleItem);
                    }
                }
            }
            else
            {
                if (puzzleItem.activeSelf && null != UIEventListener.Get(puzzleItem).onClick)
                {
                    KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                    if (nextKeypetCtrl.IsBlank)
                    {
                        continue;
                    }

                    if (srcKeypetType != nextKeypetCtrl.CurKeypetType)
                    {
                        return;
                    }

                    pKeypetList.Add(puzzleItem);
                }
            }
        }
    }

    PlacementDir pDir;

    /*
     * 리볼 게임 여부 판단
     */
    void ReballGuardCombineKeypet(PlacementDir placementDir, GameObject pSource, GameObject pTarget, ref KeypetCombinationInfo pKeypetCombinationInfo)
    {
        KeypetCtrl surKeypetCtrl = pSource.GetComponentInChildren<KeypetCtrl>();
        KeypetCtrl curKeypetCtrl = pTarget.GetComponentInChildren<KeypetCtrl>();

        if (!pTarget.GetComponentInChildren<KeypetCtrl>().IsBlank)
            return;

        KeypetType srcKeypetType = surKeypetCtrl.CurKeypetType;

        int curX = curKeypetCtrl.CurSlotIndex % _puzzleWidth;
        int curY = curKeypetCtrl.CurSlotIndex / _puzzleWidth;

        int maxCount = Mathf.Max(_puzzleWidth, _puzzleHeight);

        pDir = placementDir;

        for (int curCount = 0; curCount < maxCount; ++curCount)
        {
            curX += _dirLeanX[(int)placementDir];
            curY += _dirLeanY[(int)placementDir];

            if (curX < 0 || curX >= _puzzleWidth)
            {
                return;
            }

            if (curY < 0 || curY >= _puzzleHeight)
            {
                return;
            }

            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(curX + curY * _puzzleWidth);
            
            if (puzzleItem != null ) 
            {
                KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                if (nextKeypetCtrl.IsBlank || srcKeypetType != nextKeypetCtrl.CurKeypetType)
                {
                    return;
                }

                if(System.String.Compare(pSource.name, puzzleItem.name) == 0)
                {
                    return;
                }

                pKeypetCombinationInfo.KeypetList.Add(puzzleItem);

                //대각선 //세로 //가로 판단하기가 필요함
            }
        }
    }
    /*
     * 키펫 함체 가능 여부 판단 및 함체되는 키펫들의 정보를 얻기 위한 함수.
     * 
     */
    protected bool CombineKeypets(GameObject pSource, GameObject pTarget, ref KeypetCombinationInfo pKeypetCombinationInfo)
    {
        pKeypetCombinationInfo.KeypetList.Clear();

        //pSource 는 퍼즐의 자식이다...

        //리볼모드 Is Raball Play True!
        if (isReballPlay)
        {
            bool Vertical = false;
            bool Horizontal = false;
            bool DiagonalL = false;
            bool DiagonalR = false;

            if (pSource.transform.parent.gameObject == pTarget)
            {
                return false;
            }

            if (!Vertical)
            {
                if (pKeypetCombinationInfo.KeypetList.Count >= 0)
                {
                    pKeypetCombinationInfo.KeypetList.Clear();
                }

                ReballGuardCombineKeypet(PlacementDir.Begin, pSource, pTarget, ref pKeypetCombinationInfo);

                ReballGuardCombineKeypet(PlacementDir.DD, pSource, pTarget, ref pKeypetCombinationInfo);

                if (pKeypetCombinationInfo.KeypetList.Contains(pSource.transform.parent.gameObject))
                {
                    pKeypetCombinationInfo.KeypetList.Remove(pSource.transform.parent.gameObject);
                }

                if (pKeypetCombinationInfo.KeypetList.Count >= 2)
                {   
                    Vertical = true;
                    Horizontal = true;
                    DiagonalL = true;
                    DiagonalR = true;
                }
            }

            if (!DiagonalL)
            {
                if (pKeypetCombinationInfo.KeypetList.Count >= 0)
                {
                    pKeypetCombinationInfo.KeypetList.Clear();
                }

                //Left Diagonal Line
                ReballGuardCombineKeypet(PlacementDir.DDL, pSource, pTarget, ref pKeypetCombinationInfo);

                ReballGuardCombineKeypet(PlacementDir.DUR, pSource, pTarget, ref pKeypetCombinationInfo);

                if (pKeypetCombinationInfo.KeypetList.Contains(pSource.transform.parent.gameObject))
                {
                    pKeypetCombinationInfo.KeypetList.Remove(pSource.transform.parent.gameObject);
                }
                if (pKeypetCombinationInfo.KeypetList.Count >= 2)
                {
                    Vertical = true;
                    Horizontal = true;
                    DiagonalL = true;
                    DiagonalR = true;
                }
            }

            if (!Horizontal)
            {
                if (pKeypetCombinationInfo.KeypetList.Count >= 0)
                {
                    pKeypetCombinationInfo.KeypetList.Clear();
                }

                ReballGuardCombineKeypet(PlacementDir.DDR, pSource, pTarget, ref pKeypetCombinationInfo);

                ReballGuardCombineKeypet(PlacementDir.DUL, pSource, pTarget, ref pKeypetCombinationInfo);

                if (pKeypetCombinationInfo.KeypetList.Contains(pSource.transform.parent.gameObject))
                {
                    pKeypetCombinationInfo.KeypetList.Remove(pSource.transform.parent.gameObject);
                }
                if (pKeypetCombinationInfo.KeypetList.Count >= 2)
                {
                    Vertical = true;
                    Horizontal = true;
                    DiagonalL = true;
                    DiagonalR = true;
                }
            }

            if (!DiagonalR)
            {
                if (pKeypetCombinationInfo.KeypetList.Count >= 0)
                {
                    pKeypetCombinationInfo.KeypetList.Clear();
                }

                //Line
                ReballGuardCombineKeypet(PlacementDir.DL, pSource, pTarget, ref pKeypetCombinationInfo);

                ReballGuardCombineKeypet(PlacementDir.DR, pSource, pTarget, ref pKeypetCombinationInfo);

                if (pKeypetCombinationInfo.KeypetList.Contains(pSource.transform.parent.gameObject))
                {
                    pKeypetCombinationInfo.KeypetList.Remove(pSource.transform.parent.gameObject);
                }

                if (pKeypetCombinationInfo.KeypetList.Count >= 2)
                {
                    Vertical = true;
                    Horizontal = true;
                    DiagonalL = true;
                    DiagonalR = true;
                }
            }

            if (pKeypetCombinationInfo.KeypetList.Count >= 2)
            {
                if (pKeypetCombinationInfo.KeypetList.Contains(pTarget) == false)
                    pKeypetCombinationInfo.KeypetList.Add(pTarget);
            }
            else
            {
                return false;
            }
        }
        else
        {
            KeypetCtrl srcKeypetCtrl = pSource.GetComponentInChildren<KeypetCtrl>();
            KeypetCtrl tarKeypetCtrl = pTarget.GetComponentInChildren<KeypetCtrl>();

            KeypetType srcKeypetType = srcKeypetCtrl.CurKeypetType;

            int startX = srcKeypetCtrl.CurSlotIndex % _puzzleWidth;
            int startY = srcKeypetCtrl.CurSlotIndex / _puzzleWidth;

            int endX = tarKeypetCtrl.CurSlotIndex % _puzzleWidth;
            int endY = tarKeypetCtrl.CurSlotIndex / _puzzleWidth;

            int gapX = Mathf.Abs(startX - endX);
            int gapY = Mathf.Abs(startY - endY);

            if (0 == gapX && 0 == gapY)
            {
                return false;
            }
            else if (gapX != gapY)
            {
                if (0 != gapX && 0 != gapY)
                {
                    return false;
                }
            }

            if (startY < endY)
            {
                for (int y = startY; y <= endY; ++y)
                {
                    if (startX < endX)
                    {
                        for (int x = startX; x <= endX; ++x)
                        {
                            if (0 != gapX && 0 != gapY)
                            {
                                if (Mathf.Abs(x - startX) != Mathf.Abs(y - startY))
                                {
                                    continue;
                                }
                            }

                            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(x + y * _puzzleWidth);

                            if (puzzleItem.activeSelf && null != UIEventListener.Get(puzzleItem).onClick)
                            {
                                KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                                if (nextKeypetCtrl.IsBlank)
                                {
                                    continue;
                                }

                                if (srcKeypetType != puzzleItem.GetComponentInChildren<KeypetCtrl>().CurKeypetType)
                                {
                                    pKeypetCombinationInfo.KeypetList.Clear();
                                    return false;
                                }

                                pKeypetCombinationInfo.KeypetList.Add(puzzleItem);
                            }
                        }
                    }
                    else
                    {
                        for (int x = startX; x >= endX; --x)
                        {
                            if (0 != gapX && 0 != gapY)
                            {
                                if (Mathf.Abs(x - startX) != Mathf.Abs(y - startY))
                                {
                                    continue;
                                }
                            }

                            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(x + y * _puzzleWidth);

                            if (puzzleItem.activeSelf && null != UIEventListener.Get(puzzleItem).onClick)
                            {
                                KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                                if (nextKeypetCtrl.IsBlank)
                                {
                                    continue;
                                }

                                if (srcKeypetType != puzzleItem.GetComponentInChildren<KeypetCtrl>().CurKeypetType)
                                {
                                    pKeypetCombinationInfo.KeypetList.Clear();
                                    return false;
                                }

                                pKeypetCombinationInfo.KeypetList.Add(puzzleItem);
                            }
                        }
                    }
                }
            }
            else
            {
                for (int y = startY; y >= endY; --y)
                {
                    if (startX < endX)
                    {
                        for (int x = startX; x <= endX; ++x)
                        {
                            if (0 != gapX && 0 != gapY)
                            {
                                if (Mathf.Abs(x - startX) != Mathf.Abs(y - startY))
                                {
                                    continue;
                                }
                            }

                            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(x + y * _puzzleWidth);

                            if (puzzleItem.activeSelf && null != UIEventListener.Get(puzzleItem).onClick)
                            {
                                KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                                if (nextKeypetCtrl.IsBlank)
                                {
                                    continue;
                                }

                                if (srcKeypetType != puzzleItem.GetComponentInChildren<KeypetCtrl>().CurKeypetType)
                                {
                                    pKeypetCombinationInfo.KeypetList.Clear();
                                    return false;
                                }

                                pKeypetCombinationInfo.KeypetList.Add(puzzleItem);
                            }
                        }
                    }
                    else
                    {
                        for (int x = startX; x >= endX; --x)
                        {
                            if (0 != gapX && 0 != gapY)
                            {
                                if (Mathf.Abs(x - startX) != Mathf.Abs(y - startY))
                                {
                                    continue;
                                }
                            }

                            GameObject puzzleItem = _guiPlayMiddle.GetPuzzleItem(x + y * _puzzleWidth);

                            if (puzzleItem.activeSelf && null != UIEventListener.Get(puzzleItem).onClick)
                            {
                                KeypetCtrl nextKeypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

                                if (nextKeypetCtrl.IsBlank)
                                {
                                    continue;
                                }

                                if (srcKeypetType != puzzleItem.GetComponentInChildren<KeypetCtrl>().CurKeypetType)
                                {
                                    pKeypetCombinationInfo.KeypetList.Clear();
                                    return false;
                                }

                                pKeypetCombinationInfo.KeypetList.Add(puzzleItem);
                            }
                        }
                    }
                }
            }

            foreach (GameObject puzzleItem in pKeypetCombinationInfo.KeypetList)
            {
                UIEventListener.Get(puzzleItem).onClick = null;
            }
        }

        return true;
    }

    private List<GameObject> m_OneBackKeypet;

    //< 한 수 물리기 스크립트 / Ahn
    void RePlayKeypetCombineation()
    {
        GameObject KeypetItem_parent = GameObject.Find("GameItems");

        for (int i = 0; i < m_OneBackKeypet.Count; i++)
        {
            GameObject puzzleItem = Instantiate(m_OneBackKeypet[i]) as GameObject;

            puzzleItem.name = m_OneBackKeypet[i].name.Replace("(Clone)", "");

            string par = puzzleItem.name.Replace("PuzzleItem_", "");

            Debug.Log("par :" + int.Parse(par).ToString());

            puzzleItem.SetActive(true);
            puzzleItem.transform.parent = KeypetItem_parent.transform;

            puzzleItem.transform.localPosition = new Vector3(m_OneBackKeypet[i].transform.localPosition.x,
                                                    m_OneBackKeypet[i].transform.localPosition.y,
                                                    m_OneBackKeypet[i].transform.localPosition.z);
            puzzleItem.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

            puzzleItem.transform.Find("Keypet").transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

            KeypetCtrl keypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();
            int _nindex = int.Parse(par);
            keypetCtrl.CurSlotIndex = _nindex;
            keypetCtrl.CurKeypetType = keypetList[_nindex].KeypetResType;
            keypetCtrl.IsBlank = keypetList[_nindex].KeypetResType == KeypetType.blank ? true : false;

            KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

            _guiPlayMiddle._puzzleItemList.Add(puzzleItem);

            UIEventListener.Get(puzzleItem).onClick = (GameObject pGO) =>
            {
                if (_isRescueMode)
                {
                    if (null == _rescueKeypte)
                    {
                        _rescueKeypte = pGO;
                    }
                }
                else
                {
                    if (null == _selectedKeypet1)
                    {
                        _selectedKeypet1 = pGO;
                        _selectedKeypet2 = null;

                        SelectSourceKeypet(_selectedKeypet1);
                    }
                    else if (null == _selectedKeypet2)
                    {
                        _selectedKeypet2 = pGO;
                    }

                }
            };
        }
    }
    /*
     * 키펫 합체 및 관련 연출 함수.
     * 
     */
    protected IEnumerator DirectKeypetsCombination(KeypetCombinationInfo pInfo)
    {
        if (isReballPlay)
        {
            _isCombine = true;
        }

        m_OneBackKeypet = new List<GameObject>();

        if (_isCount)
        {
            _curCount++;

            _guiPlayTop.SetCount(_curCount, _maxCount, true);
        }

        _dirCount++;

        KeypetCtrl SurgetCtrl = _KeypetBox1.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl SurgetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(SurgetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        SurgetBGCtrl.Press(false);
        SurgetCtrl.ActPress(false);

        KeypetCtrl TargetCtrl = _KeypetBox2.GetComponentInChildren<KeypetCtrl>();
        KeypetBGCtrl TargetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(TargetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

        TargetBGCtrl.Press(true);
        TargetCtrl.ActPress(true);

        foreach (GameObject go in pInfo.KeypetList)
        {
            KeypetCtrl keypetCtrl = go.GetComponentInChildren<KeypetCtrl>();
            KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

            keypetBGCtrl.Press(true);
            keypetCtrl.ActPress(true);

            m_OneBackKeypet.Add(go);
        }

        yield return new WaitForSeconds(keypetMoveStartDely); // 키펫 합치기전 딜레이

        UndoInfo undoInfo = new UndoInfo();

        int sumScore = 0;

        int count = pInfo.KeypetList.Count;		// 합처질 키펫의 수

        #region Reball일때 연출 효과 보여주기
        Vector3 oldPos = _KeypetBox1.transform.position;

        if (_KeypetBox1.transform.position != _KeypetBox2.transform.position && isReballPlay)
        {
            float time = 0.0f;

            Vector3 curPos;

            while (time <= 0.5f)
            {
                time += Time.deltaTime;

                curPos = Vector3.Lerp(_KeypetBox1.transform.position, _KeypetBox2.transform.position, time);

                _KeypetBox1.transform.position = curPos;

                if (_KeypetBox1.transform.position == _KeypetBox2.transform.position)
                {
                    break;
                }

                yield return null;
            }
        }
        #endregion

        for (int i = 0; i < count; ++i)
        {
            GameObject keypetGO = pInfo.KeypetList[i];
            KeypetCtrl keypetCtrl = keypetGO.GetComponentInChildren<KeypetCtrl>();
            KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

            KeypetDirInfo keypetDirInfo = new KeypetDirInfo();
            keypetDirInfo.KeypetGO = keypetGO;
            keypetDirInfo.Keypet = keypetCtrl;
            keypetDirInfo.KeypetBG = keypetBGCtrl;
            undoInfo.undoList.Add(keypetDirInfo);

            Vector3 srcPos = keypetGO.transform.position;
            Vector3 tarPos = (i + 1) >= count ? srcPos : pInfo.KeypetList[i + 1].transform.position;

            bool isMoveFinished = false;
            //Hwang Wark
            SaveData.keypetMoveFinished = false;
            //Hwang End

            if ((tarPos - srcPos).magnitude < 0.0001f)
            {
                tarPos = _guiPlayTop.GetMoveInfoPos();

                if (SaveData.effectSoundOnOff == true)
                {
                    AudioMgr.Instance.Play("CombineKeypet");
                }

                keypetCtrl.ActMoveToScore(srcPos, tarPos, _isFeverTime, () =>
                {
                    isMoveFinished = true;
                    SaveData.keypetMoveFinished = true;
                });

                keypetBGCtrl.Press(false);
            }
            else
            {
                if (SaveData.effectSoundOnOff == true)
                {
                    AudioMgr.Instance.Play("MoveKeypet");
                }

                keypetCtrl.ActMoveToNextKeypet(srcPos, tarPos, _isFeverTime, () =>
                {
                    isMoveFinished = true;

                    //Hwang Wark
                    SaveData.keypetMoveFinished = true;
                    //Hwang End
                });

                keypetBGCtrl.Press(false);

                while (!isMoveFinished)
                {
                    yield return null;
                }
            }

            if (isReballPlay)
            {
                string par = keypetGO.name.Replace("PuzzleItem_", "");

                int _nindex = int.Parse(par);

                keypetList[_nindex].KeypetResType = KeypetType.blank;
                keypetList[_nindex].KeypetResObj = Resources.Load("Keypet/keypet_blank");

                Destroy(keypetCtrl.gameObject);

                GameObject Res = (GameObject)GameObject.Instantiate(keypetList[_nindex].KeypetResObj); // pKeyPetList[index].KeypetResObj);
                Vector3 curScale = Res.transform.localScale;
                Res.name = "Keypet";
                Res.transform.parent = keypetGO.transform;
                Res.transform.localPosition = Vector3.zero;
                Res.transform.localRotation = Quaternion.identity;
                Res.transform.localScale = curScale;
                KeypetCtrl InskeypetCtrl = Res.GetComponent<KeypetCtrl>();
                InskeypetCtrl.CurSlotIndex = _nindex;
                InskeypetCtrl.CurKeypetType = keypetList[_nindex].KeypetResType;
                InskeypetCtrl.IsBlank = keypetList[_nindex].KeypetResType == KeypetType.blank ? true : false;

                UIEventListener.Get(keypetGO).onClick = null;
            }
            else
            {
                keypetGO.SetActive(false);
            }

            int index = 50;

            if (i < 2){
                sumScore = index * 2;
            }
            else if (i > 1 && i < 3){
                sumScore = index * 5;
            }
            else if (i > 2 && i < 4){
                sumScore = index * 10;
            }
            else{
                sumScore = index * 20;
            }
        }

        if (isReballPlay)
        {
            _KeypetBox1.transform.position = oldPos;

            string par = _KeypetBox1.name.Replace("PuzzleItem_", "");

            int _nindex = int.Parse(par);

            keypetList[_nindex].KeypetResType = KeypetType.blank;
            keypetList[_nindex].KeypetResObj = Resources.Load("Keypet/keypet_blank");

            Destroy(SurgetCtrl.gameObject);

            GameObject Res = (GameObject)GameObject.Instantiate(keypetList[_nindex].KeypetResObj); // pKeyPetList[index].KeypetResObj);
            Vector3 curScale = Res.transform.localScale;
            Res.name = "Keypet";
            Res.transform.parent = _KeypetBox1.transform;
            Res.transform.localPosition = Vector3.zero;
            Res.transform.localRotation = Quaternion.identity;
            Res.transform.localScale = curScale;
            KeypetCtrl InskeypetCtrl = Res.GetComponent<KeypetCtrl>();
            InskeypetCtrl.CurSlotIndex = _nindex;
            InskeypetCtrl.CurKeypetType = keypetList[_nindex].KeypetResType;
            InskeypetCtrl.IsBlank = keypetList[_nindex].KeypetResType == KeypetType.blank ? true : false;

            _KeypetBox1 = null;
            _KeypetBox2 = null;
        }

        undoInfo.score = (_isFeverTime ? sumScore * 2 : sumScore);
        _undoList.Add(undoInfo);

        _curScore += (_isFeverTime ? sumScore * 2 : sumScore);
        _guiPlayTop.SetSocre(_curScore);

        if (_isFeverTime)
        {
            Vector3 targetPos = GuiMgr.Instance.GetFrontCamera().ScreenToWorldPoint(GuiMgr.Instance.GetBackCamera().WorldToScreenPoint(_guiPlayTop.GetScorePos())); ;

            Debug.Log("targetPos :" + targetPos);

            GameObject scoreEffect = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiPlayFeverScore"));
            scoreEffect.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            scoreEffect.transform.position = new Vector3(Random.Range(targetPos.x - 0.1f, targetPos.x + 0.1f), Random.Range(targetPos.y - 0.05f, targetPos.y + 0.05f), targetPos.z);
            scoreEffect.transform.localRotation = Quaternion.identity;
            scoreEffect.transform.localScale = Vector3.one;
        }

        _dirCount--;

        if (_isCombine)
            _isCombine = false;

    }


    protected IEnumerator GuiPlayMenuBtnClick()
    {
        MenuType menuType = MenuType.Invalid;

        _guiPlayMenu = GuiMgr.Instance.Show<GuiPlayMenu>(GuiMgr.ELayerType.Front, true, null);
        /*
        _guiPlayMenu.ChangeSettingBtn.onClick = (GameObject pGO) =>
        {
            menuType = MenuType.ChangeSetting;
        };*/

        _guiPlayMenu.ContinueBtn.onClick = (GameObject pGO) =>
        {
            menuType = MenuType.Countine;
        };

        _guiPlayMenu.GotoMainMenuBtn.onClick = (GameObject pGO) =>
        {
            //Hwang Wark Message Popup Start
            JSONNode msgParam = new JSONClass();
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Portuguese:
            //        msgParam["message"] = "Realmente prefiro ficar em casa?";
            //        break;
            //    case SystemLanguage.English:
            //        msgParam["message"] = "Really rather stay home?";
            //        break;
            //    case SystemLanguage.Korean:
            //        msgParam["message"] = "정말 나가시겠습니까?";
            //        break;
            //}
            msgParam["message"] = "Deseja mesmo voltar à seleção de fases?";
                    
            msgParam["show_type"] = "two";

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

            //Hwang Wark Message Popup End
            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk)
                {
                    SaveData.MapScene();
                    menuType = MenuType.GotoMainMenu;

                    KakaoMain.Instance._bTutorailStart = false;
/*#if UNITY_EDITOR
                    Debug.Log("Editor Load");
#elif UNITY_ANDROID
                                                if(stageInfoSI > 10)
                                                {
                                                AdlibPlugin.LoadInterstitialAd();

                                                AdlibPlugin.ShowPopBanner("#ff444444",
                                                                          AdlibPlugin.PopButton.White,
                                                                          true,
                                                                          false,
                                                                          AdlibPlugin.PopAlign.Bottom,
                                                                          60);
                                                }
#endif
                    */
                }
            };
            //Hwang End
        };

        _guiPlayMenu.ReplayBtn.onClick = (GameObject pGO) =>
        {
            //menuType = MenuType.Replay;
            //Hwang Wark
            //Hwang Wark Message Popup Start
            JSONNode msgParam = new JSONClass();
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Portuguese:
            //        msgParam["message"] = "A tela para ficar pronto";
            //        break;
            //    case SystemLanguage.English:
            //        msgParam["message"] = "The screen to get ready.";
            //        break;
            //    case SystemLanguage.Korean:
            //        msgParam["message"] = "준비 화면으로 이동합니다.";
            //        break;
            //}
            msgParam["message"] = "Deseja mesmo reiniciar a fase?";
                    
            msgParam["show_type"] = "two";

            GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

            //Hwang Wark Message Popup End
            guiMessageBox.OnButtonClick = (bool pIsOk) =>
            {
                if (pIsOk)
                {
                    SaveData.rePlay = true;
                    menuType = MenuType.GotoMainMenu;

                    KakaoMain.Instance._bTutorailStart = false;

                    #region 광고
                    /*
#if UNITY_EDITOR
                    Debug.Log("Editor Load");
#elif UNITY_ANDROID
                                                if(stageInfoSI > 10)
                                                {
                                                AdlibPlugin.LoadInterstitialAd();

                                                AdlibPlugin.ShowPopBanner("#ff444444",
                                                                          AdlibPlugin.PopButton.White,
                                                                          true,
                                                                          false,
                                                                          AdlibPlugin.PopAlign.Bottom,
                                                                          60);
                                                }
#endif
                    */
                    #endregion
                }
            };
            //Hwang End
        };

        _isPause = true;

        if (null != _guiPlayFever)
        {
            _guiPlayFever.Pause(true);
        }

        while (MenuType.Invalid == menuType)
        {
            yield return null;
        }

        switch (menuType)
        {
            case MenuType.ChangeSetting:
                {
                    _guiGameSetting = GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, true, null);

                    _guiGameSetting.OkBtn.onClick = (GameObject pGO) =>
                    {
                        if (null != _guiPlayFever)
                        {
                            _guiPlayFever.Pause(false);
                        }

                        _isPause = false;

                        _guiGameSetting = null;

                        GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
                    };

                    _guiGameSetting.TutorialBtn.onClick = (GameObject pGO) =>
                    {

                        Debug.Log("map");
                        GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

                        KakaoMain.Instance._bTutorailStart = true;
                        KakaoMain.Instance.isReballPlay = false;

                        JSONNode param = new JSONClass();
                        param["stage_si"] = "1";

                        if (KakaoMain.Instance.isReballPlay)
                            KakaoMain.Instance.isReballPlay = false;

                        if (KakaoMain.Instance._bTutorailStart)
                            KakaoMain.Instance._bTutorailStart = false;

                        GameStateMgr.Instance.ChangeState<GSPlay>(0, param);

                        /*
                        if (null != _guiPlayFever)
                        {
                            _guiPlayFever.Pause(false);
                        }
						
                        _isPause = false;
						
                        _guiGameSetting = null;
						
                        GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

                        //SaveData.startTutorialIntro = true;
                        
                        NGUITools.AddChild(parentObject, tutorialPlay);
                        */
                    };
                }
                break;

            case MenuType.Countine:
                {
                    if (null != _guiPlayFever)
                    {
                        _guiPlayFever.Pause(false);
                    }

                    _isPause = false;
                }
                break;

            case MenuType.GotoMainMenu:
                {
                    if (null != _guiPlayFever)
                    {
                        _guiPlayFever.Pause(false);
                    }

                    if (KakaoMain.Instance.isReballPlay)
                        KakaoMain.Instance.isReballPlay = false;

                    if (KakaoMain.Instance._bTutorailStart)
                        KakaoMain.Instance._bTutorailStart = false;

                    _isPause = false;

                    GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
                }
                break;

            case MenuType.Replay:
                {
                    if (null != _guiPlayFever)
                    {
                        _guiPlayFever.Pause(false);
                    }

                    if (KakaoMain.Instance.isReballPlay)
                        KakaoMain.Instance.isReballPlay = false;

                    if (KakaoMain.Instance._bTutorailStart)
                        KakaoMain.Instance._bTutorailStart = false;

                    _isPause = false;

                    _state = State.BeginPlay;
                }
                break;
        }

        if (null != _guiPlayMenu)
        {
            //_guiPlayMenu.ChangeSettingBtn.onClick = null;
            _guiPlayMenu.ContinueBtn.onClick = null;
            _guiPlayMenu.GotoMainMenuBtn.onClick = null;
            _guiPlayMenu.ReplayBtn.onClick = null;

            _guiPlayMenu = null;
            GuiMgr.Instance.Show<GuiPlayMenu>(GuiMgr.ELayerType.Front, false, null);
        }

        yield return null;
    }


    /*
     * 피버 타임 시작.
     * 
     */
    protected void ShowFeverTime(float pFeverReaminTime)
    {
        if (null != _guiPlayFever && null != _guiPlayFeverFront)
        {
            Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~ ShowFeverTime(float pFeverReaminTime)  if (null != _guiPlayFever && null != _guiPlayFeverFront) ");
            return;
        }

        if (null != _guiPlayFeverCount)
        {
            Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~ _guiPlayFeverCount.ShowOn(true); ");
            _guiPlayFeverCount.ShowOn(true);
        }

        Debug.LogError("ShowFever!");

        _isFeverTime = true;

        _feverReaminTime = pFeverReaminTime;

        _guiBG.Show(false);

        _guiPlayFever = GuiMgr.Instance.Show<GuiPlayFever>(GuiMgr.ELayerType.Back, true, null);
        _guiPlayFeverFront = GuiMgr.Instance.Show<GuiPlayFeverFront>(GuiMgr.ELayerType.Front, true, null);

        if (null == _feverEffect1)
        {
            //_feverEffect1 = (GameObject)GameObject.Instantiate(Resources.Load("Effects/feverStarL"));
        }

        if (null == _feverEffect2)
        {
            //_feverEffect2 = (GameObject)GameObject.Instantiate(Resources.Load("Effects/feverStarR"));
        }

        foreach (GameObject bgGO in _guiPlayMiddle.GetPuzzleItemBG())
        {
            KeypetBGCtrl bg = bgGO.GetComponentInChildren<KeypetBGCtrl>();
            if (bg)
            {
                bg.FeverMode(true);
            }
            InfoSave.playScene = false;
            InfoSave.feverScene = true;
        }

        if (SaveData.bgmOnOff == true)
        {
            AudioMgr.Instance.Play("keypetbreak");

            AudioMgr.Instance.Stop("InGameBG");
            AudioMgr.Instance.Play("FeverBG");
        }
    }

    /*
     * 피버 타임 종료.
     * 
     */
    protected void HideFeverTime(bool pIsEnd)
    {
        if (null == _guiPlayFever && null == _guiPlayFeverFront)
        {
            return;
        }

        _isFeverTime = false;
        _feverReaminTime = 0.0f;

        if (null != _guiPlayFeverCount)
        {
            _guiPlayFeverCount.HideFiverCount();	//피버카운트 객체를 보이지 않게 처리..
            // _guiPlayFeverCount.ShowOn(false);
        }

        _guiBG.Show(true);

        if (null != _feverEffect1)
        {
            GameObject.DestroyImmediate(_feverEffect1);
            _feverEffect1 = null;
        }

        if (null != _feverEffect2)
        {
            GameObject.DestroyImmediate(_feverEffect2);
            _feverEffect2 = null;
        }

        foreach (GameObject bgGO in _guiPlayMiddle.GetPuzzleItemBG())
        {
            bgGO.GetComponentInChildren<KeypetBGCtrl>().FeverMode(false);
            InfoSave.feverScene = false;
            InfoSave.playScene = true;
        }

        _guiPlayFever = null;
        _guiPlayFeverFront = null;

        GuiMgr.Instance.Show<GuiPlayFever>(GuiMgr.ELayerType.Back, false, null);
        GuiMgr.Instance.Show<GuiPlayFeverFront>(GuiMgr.ELayerType.Front, false, null);

        if (null != AudioMgr.Instance)
        {
            AudioMgr.Instance.Stop("FeverBG");

            if (!pIsEnd)
            {
                if (SaveData.bgmOnOff == true)
                {
                    AudioMgr.Instance.Play("InGameBG");
                }
            }
        }
    }


    protected void DoBonusTime(ResultType resultType, float pRemainTime, int pRemainCount)
    {
        if (pRemainTime <= 0.0f && pRemainCount <= 0)
        {
            _resultType = resultType;
            _state = State.EndPlay;
        }
        else
        {
            int bonusKeypetCount = Mathf.Max(((int)(pRemainTime + 0.5f)) / 4, pRemainCount);

            AudioMgr.Instance.Play("bonustime");

            StartCoroutine(BonusTime(resultType, bonusKeypetCount));
        }

        if (isReballPlay) 
        {
            KakaoMain.Instance.isReballPlay = false;
        }
    }

    ResultType ResultReball;

    void ResultTypeReball()
    {
        ResultReball = ResultType.Invalid;

        bool bCheck = false;

        foreach (GameObject puzzleItemGO in _guiPlayMiddle.GetPuzzleReball())
        {
            KeypetCtrl curKeypetCtrl = puzzleItemGO.GetComponentInChildren<KeypetCtrl>();

            if (bCheck == true)
                break;

            for (PlacementDir placementDir = PlacementDir.Begin; placementDir < PlacementDir.Max; ++placementDir)
            {
                keypetListKey.Clear();

                GuardCombineKeypets(placementDir, puzzleItemGO, ref keypetListKey, null);

                List<bool> isCheck = new List<bool>();

                isCheck.Clear();

                KeypetCombinationInfo KeypetCombinationInfo = new KeypetCombinationInfo();

                foreach (GameObject keypetGO in keypetListKey)
                {
                    if (CombineKeypets(puzzleItemGO, keypetGO, ref KeypetCombinationInfo) == false)
                    {
                        isCheck.Add(false);
                    }
                    else
                    {
                        bCheck = true;
                        
                        isCheck.Add(true);
                        break;
                    }
                }

                if (isCheck.Contains(true))
                {
                    ResultReball = ResultType.Invalid;
                    
                    return;
                }
                else 
                {
                    ResultReball = ResultType.CanNotMove;
                }
            }
        }
        //return ResultType.CanNotMove;
    }

    /*
     * 게임 결과 알아오는 함수.
     */
    protected ResultType PlayGameResult()
    {
        int clearCount = 0;

        if (isReballPlay)
        {
            ResultTypeReball();
            /*
            if (puzzleActiveCount >= _guiPlayMiddle.GetPuzzleActiveCount() - 5)
            {
            }
            */
            if (puzzleActiveCount >= _guiPlayMiddle.GetPuzzleActiveCount() - 1)			// 성공체크
            {
                return ResultType.Success;
            }

            return ResultReball;
        }
        else 
        { 
            //< 게임 하는 중에 퍼즐아이템을 불런온다.
            foreach (GameObject puzzleItemGO in _guiPlayMiddle.GetPuzzleItem())
            {
                KeypetCtrl curKeypetCtrl = puzzleItemGO.GetComponentInChildren<KeypetCtrl>();

                if (!puzzleItemGO.activeSelf || curKeypetCtrl.IsBlank || null == UIEventListener.Get(puzzleItemGO).onClick)
                {
                    clearCount++;

                    continue;
                }

                List<GameObject> combinableKeypetList = new List<GameObject>();

                for (PlacementDir placementDir = PlacementDir.Begin; placementDir < PlacementDir.Max; ++placementDir)
                {
                    combinableKeypetList.Clear();

                    GuardCombineKeypets(placementDir, puzzleItemGO, ref combinableKeypetList, null);

                    if (combinableKeypetList.Count > 0)
                    {
                        return ResultType.Invalid;
                    }
                }
            }
        

            if (clearCount == _guiPlayMiddle.GetPuzzeleItemCount())			// 성공체크
            {
                return ResultType.Success;
            }

            if (_isTime && !_guiPlayBottom.isEmptyItem())
            {
                return ResultType.Invalid;
            }

            if (_isCount && !_guiPlayBottom.isEmptyItem())
            {
                return ResultType.Invalid;
            }
        }
        return ResultType.CanNotMove;

        //		return clearCount == _guiPlayMiddle.GetPuzzeleItemCount() ? ResultType.Success : (_isTime ? ResultType.Invalid : ResultType.CanNotMove);
    }


    protected IEnumerator UseItem_Rescue()
    {
        List<int> keypetNumber = new List<int>();
        int findCount = 0;
        int randomNumber = 0;

        while (null == _rescueKeypte && SaveData.randomRescue == false)
        {
            yield return new WaitForFixedUpdate();
        }

        _dirCount++;

        //if (_isTime )
        //{
        //    _guiPlayTop.SetTime(_maxTime, _maxTime);
        //}
        //if ( _isCount )
        //{
        //    _guiPlayTop.SetCount(_maxCount, _maxCount, false);
        //}

        //Debug.Log (_rescueKeypte);
        
        if(_bTutorial)
        {
            _bPuzzleHudle = true;
            SettingPuzzleItemName("PuzzleItem_012");
        }

        if (SaveData.randomRescue == true)
        {			//렌덤 구출이면 srcKeypetCtrl << 렌덤 키펫 타입 넣기

            //            KeypetCtrl[] arrCtrl = _guiPlayMiddle.GetComponentsInChildren<KeypetCtrl>();

            List<GameObject> arrCtrl = new List<GameObject>();
            foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
            {
                KeypetCtrl keypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();
                if (keypetCtrl == null)
                {
                    continue;
                }
                if (keypetCtrl.IsBlank)
                {
                    continue;
                }
                arrCtrl.Add(keypetCtrl.gameObject);
            }

            int Idx = Random.Range(0, arrCtrl.Count);
            _rescueKeypte = arrCtrl[Idx].gameObject;

            // 수정.
            //foreach (GameObject keypetGO in _guiPlayMiddle.GetPuzzleItem())
            //{
            //    //Debug.Log(keypetGO.transform.FindChild("Keypet").GetComponent<KeypetCtrl>().CurKeypetType);	
            //    if(keypetGO.activeSelf && keypetGO.transform.FindChild("Keypet").GetComponent<KeypetCtrl>().CurKeypetType != KeypetType.blank){
            //        //Debug.Log(keypetGO.transform.FindChild("Keypet").GetComponent<KeypetCtrl>().CurKeypetType);	
            //        keypetNumber.Add(findCount);
            //    }
            //    findCount++;
            //}
            //findCount = 0;
            ////Debug.Log(keypetNumber[0]);
            //randomNumber = Random.Range(0,keypetNumber.Count);
            //_rescueKeypte = GameObject.Find("Gui/Back/Camera/GuiPlayMiddle/AnchorCenter/BG/Items").transform.GetChild(keypetNumber[randomNumber]).gameObject;

            //keypetNumber.Clear();
        }

        KeypetCtrl srcKeypetCtrl = _rescueKeypte.GetComponentInChildren<KeypetCtrl>();

        _rescueKeypte = null;

        //Debug.Log (srcKeypetCtrl.CurKeypetType);//		키펫 타입 Get

        List<KeypetDirInfo> keypetList = new List<KeypetDirInfo>();

        foreach (GameObject keypetGO in _guiPlayMiddle.GetPuzzleItem())
        {
            if (!keypetGO.activeSelf && null == UIEventListener.Get(keypetGO).onClick)
            {
                continue;
            }

            KeypetCtrl keypetCtrl = keypetGO.GetComponentInChildren<KeypetCtrl>();

            if (keypetCtrl == null)
                continue;

            if (srcKeypetCtrl.CurKeypetType == keypetCtrl.CurKeypetType)
            {
                UIEventListener.Get(keypetGO).onClick = null;

                KeypetBGCtrl keypetBGCtrl = _guiPlayMiddle.GetPuzzleItemBG(keypetCtrl.CurSlotIndex).GetComponentInChildren<KeypetBGCtrl>();

                KeypetDirInfo rescueDirInfo = new KeypetDirInfo();

                rescueDirInfo.KeypetGO = keypetGO;
                rescueDirInfo.Keypet = keypetCtrl;
                rescueDirInfo.KeypetBG = keypetBGCtrl;

                keypetList.Add(rescueDirInfo);
            }
        }

        foreach (KeypetDirInfo target in keypetList)
        {
            target.KeypetBG.Rescue(true);
            target.Keypet.ActPress(true);
        }

        yield return new WaitForSeconds(0.5f);

        int sumScore = 0;
        int count = 0;

        foreach (KeypetDirInfo target in keypetList)
        {
            target.KeypetBG.Rescue(false);
            target.Keypet.ActPress(false);

            Camera backCam = GuiMgr.Instance.GetBackCamera();
            Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

            Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(target.KeypetGO.transform.position));
            effectTargetPos.z = 0.0f;

            GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), effectTargetPos, Quaternion.identity);

            target.KeypetGO.SetActive(false);			// 키펫 비활성

            if (_feverTargetGO == target.KeypetGO)
            {
                ShowFeverTime(_feverWaitTime);

                _feverWaitTime = 0.0f;
                _feverTargetGO = null;

                _guiPlayFeverCount.SetCount(_feverWaitTime);
            }

            if (count++ < 2)
            {
                sumScore = 200;
            }
            else
            {
                sumScore = sumScore * 3;
            }

            yield return new WaitForSeconds(0.1f);
        }

        _curScore += (_isFeverTime ? sumScore * 2 : sumScore);

        _guiPlayTop.SetSocre(_curScore);

        if (_isFeverTime)
        {
            Vector3 targetPos = GuiMgr.Instance.GetFrontCamera().ScreenToWorldPoint(GuiMgr.Instance.GetBackCamera().WorldToScreenPoint(_guiPlayTop.GetScorePos())); ;

            GameObject scoreEffect = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiPlayFeverScore"));
            scoreEffect.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            scoreEffect.transform.position = new Vector3(Random.Range(targetPos.x - 0.1f, targetPos.x + 0.1f), Random.Range(targetPos.y - 0.05f, targetPos.y + 0.05f), targetPos.z);
            scoreEffect.transform.localRotation = Quaternion.identity;
            scoreEffect.transform.localScale = Vector3.one;
        }

        _isRescueMode = false;

        _dirCount--;

        if (SaveData.randomRescue == true)
        {
            SaveData.randomRescue = false;
        }
        itemUseWark = false;
        //StartCoroutine (ItemWarkFinsh (2.0f));

     
    }

    protected IEnumerator BonusTime(ResultType resultType, int pKeypetCount)
    {
        HideFeverTime(false);

        _isBonusMode = true;


        _dirCount++;

        yield return null;

        // Display Cutin description
        GuiMgr.Instance.Show<GuiPlayBonusCutin>(GuiMgr.ELayerType.Front, true, null);
        //AudioMgr.Instance.Play("keypetbreak");

        yield return new WaitForSeconds(1.5f);

        GuiMgr.Instance.Show<GuiPlayBonusCutin>(GuiMgr.ELayerType.Front, false, null);

        Camera backCam = GuiMgr.Instance.GetBackCamera();
        Camera effectCam = GameObject.Find("EffectCamera").GetComponent<Camera>();

        int puzzleBGCount = _guiPlayMiddle.GetPuzzleItemBGCount();

        List<int> indexList = new List<int>();

        for (int i = 0; i < puzzleBGCount; ++i)
        {
            indexList.Add(i);
        }

        List<GameObject> bonusKeypet = new List<GameObject>();

        for (int i = 0; i < pKeypetCount; ++i)
        {
            int indexValue;

            if (0 == indexList.Count)
            {
                indexValue = Random.Range(0, puzzleBGCount);
            }
            else
            {
                int index = Random.Range(0, indexList.Count);
                indexValue = indexList[index];
                indexList.RemoveAt(index);
            }

            Vector3 slotPos = _guiPlayMiddle.GetPuzzleItemBG(indexValue).transform.position;
            Vector3 effectTargetPos = effectCam.ScreenToWorldPoint(backCam.WorldToScreenPoint(slotPos)); effectTargetPos.z = 0.0f;

            GameObject.Instantiate(Resources.Load("Effects/keypetBonusTime"), effectTargetPos, Quaternion.identity);

            yield return new WaitForSeconds(0.2f);

            effectTargetPos.z = -1.0f;
            bonusKeypet.Add((GameObject)GameObject.Instantiate(Resources.Load("Effects/bonus"), effectTargetPos, Quaternion.identity));

            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(0.5f);

        int sumScore = bonusKeypet.Count > 1 ? 300 : 0;

        foreach (GameObject bonusGO in bonusKeypet)
        {
            GameObject.Instantiate(Resources.Load("Effects/keypetEscape"), bonusGO.transform.position, Quaternion.identity);

            GameObject.DestroyImmediate(bonusGO);

            _curScore += sumScore / 2;

            sumScore += sumScore / 2;

            _guiPlayTop.SetSocre((double)_curScore);

            yield return new WaitForSeconds(0.1f);
        }

        int Stageindex =  (int)uint.Parse(_params["stage_si"]);

        float ScoreStage = (float)Stageindex / 4;

        _curScore *= ScoreStage;

        _guiPlayTop.SetSocre(Mathf.Abs((float)_curScore));

        _resultType = resultType;
        _state = State.EndPlay;

        yield return null;

        _dirCount--;

        _isBonusMode = false;
    }

    //-----------------------------------------------------------------
    //
    //  corouine을 stop 하고, cutin 없애고, twinkle/escape 이펙트 객체 와 보너스 객체 Destroy,
    //  점수 올려 준다. 게임상태 EndPlay 로,
    //
    //-----------------------------------------------------------------
    protected void StopBonusTime()
    {
        if (_isBonusMode == false)
            return;

    }

    private uint stageIndex;

    // 스테이지 완료(성공) 후 주어지는 보상 패키지를 결정하고 관련된 아이템 정보를 얻는다.. 
    protected void GetRewardResult(out int pRewardItemSI, out int pRewardMuch, out string strName)
    {
        stageIndex = uint.Parse(_params["stage_si"]);	// 현재 스테이지 인덱스를 찾고..

        Debug.Log("Stage :" + stageIndex);

        int randIndex = Random.Range(0, 5);	// StageInfo 데이터에 있는 보상 패키지 5개 중 하나를 랜덤하게 고르기 위해.. 						
        uint packageIndex;
        int packageSu;
        
        if (randIndex == 0)
        {
            StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(stageIndex);
            packageIndex = stageInfo.ri_si_1;
            packageSu = stageInfo.ri_si_1_su;
        }
        else if (randIndex == 1)
        {
            StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(stageIndex);
            packageIndex = stageInfo.ri_si_2;
            packageSu = stageInfo.ri_si_2_su;
        }
        else if (randIndex == 2)
        {
            StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(stageIndex);
            packageIndex = stageInfo.ri_si_3;
            packageSu = stageInfo.ri_si_3_su;
        }
        else if (randIndex == 3)
        {
            StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(stageIndex);
            packageIndex = stageInfo.ri_si_4;
            packageSu = stageInfo.ri_si_4_su;
        }
        else// if( randIndex == 4)
        {
            StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(stageIndex);
            packageIndex = stageInfo.ri_si_5;
            packageSu = stageInfo.ri_si_5_su;
        }

        //Debug.Log("packageIndex :" + packageIndex);

        //PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo(packageIndex);

        //pRewardItemSI = (int)packageInfo.item_1_idx;	// 보상 아이템의 인덱스..
        //pRewardMuch = (int)packageInfo.item_1_su;		// 보상 아이템 수량..

        //Debug.Log("pReawrd :" + pRewardMuch);

        //ItemInfo itemInfo = GameDataMgr.Instance.GetInfo<ItemInfoMgr>().GetItemInfo((uint)pRewardItemSI);

        //GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(packageIndex);
        //uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;

        //SaveData.ItemGetRewardHave(getItemIndex, pRewardMuch);

        //itemHave((int)packageIndex, (int)pRewardMuch);

        ////// ------------------ 서버 연결로 처리 해야 함.
        ////SaveData.InGameItemHaveUpdate((int)packageIndex, (int)pRewardMuch);

        PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo(packageIndex);

        pRewardItemSI = (int)packageIndex;	// 보상 아이템의 인덱스..packageInfo.item_1_idx
        pRewardMuch = (int)packageInfo.item_1_su;		// 보상 아이템 수량..

        strName = packageInfo.name;
              
        ItemInfo itemInfo = GameDataMgr.Instance.GetInfo<ItemInfoMgr>().GetItemInfo((uint)pRewardItemSI);

        //// ------------------ 서버 연결로 처리 해야 함.
        SaveData.InGameItemHaveUpdate((int)packageIndex, (int)pRewardMuch);
        SaveData.RouletteKeypetCountCheck();
        ////
    }

    void SetRewardPoint()
    {
        uint stageIndex = uint.Parse(_params["stage_si"]);	// 현재 스테이지 인덱스를 찾고..

        _guiPlayResultSuccess.SetStage((int)stageIndex);
        _guiPlayResultSuccess.SetMyScore(_curScore);	// 임시로, 지금 완료한 스테이지의 점수를 보여준다.

        //GameObject.Find ("InfoMgr").GetComponent<InfoSave> ().StageScoreSave ();
    }

    void itemHave(int packageIndex, int pRewardMuch) 
    {

        if (packageIndex == 29 || (packageIndex >= 49 && packageIndex <= 53) || (packageIndex >= 79 && packageIndex <= 85))
        {
            SaveData.PowerSuPlus(pRewardMuch);
            //useitem [setNumber] -=addCount;
        }

        if ((packageIndex >= 19 && packageIndex <= 21) || (packageIndex >= 25 && packageIndex <= 27) || (packageIndex >= 59 && packageIndex <= 62) || (packageIndex >= 70 && packageIndex <= 78))
        {
            SaveData.FunnySuPlus(pRewardMuch);
            //useitem [setNumber] -=addCount;
        }

        if ((packageIndex >= 22 && packageIndex <= 23) || (packageIndex == 28) || (packageIndex >= 63 && packageIndex <= 69))
        {
            SaveData.GemSuPlus(pRewardMuch);
            //useitem [setNumber] -=addCount;
        }
    }

    // 스테이지 완료(성공)후 보상상자를 선택할 때 호출...
    void Choice_PlayRewardBox(int pRewardItemSI, int itemSu)
    {
        // 모든 보상상자들이 다시 연속으로 클릭되는 것을 방지.. 
        _guiPlayResultSuccess.RewardBtn_01.GetComponent<BoxCollider>().enabled = false;
        _guiPlayResultSuccess.RewardBtn_02.GetComponent<BoxCollider>().enabled = false;
        _guiPlayResultSuccess.RewardBtn_03.GetComponent<BoxCollider>().enabled = false;

        // 보상 결과 화면 보이기..
        _guiPlayResultSuccess.DirectionReward(pRewardItemSI, itemSu);
    }


    public bool IsPlayState()
    {
        return (_state == State.Play) ? true : false;
    }

    /*
        public void OnGUI()
        {
            if (GUILayout.Button("Undo", GUILayout.Width(100), GUILayout.Height(40)))
            {
                DoUseItem_Undo();
            }
        }
    */

    private void SetMoveCount()
    {
        _curCount++;

        _guiPlayTop.SetCount(_curCount, _maxCount, true);
    }

    void SettingPuzzleItemName(string _str)
    {
        foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
        {
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Portuguese:
            //        _guiPlayMiddle.SetTutorialText ("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
            //    break;
            //    case SystemLanguage.Korean:
            //        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
            //        break;
            //    case SystemLanguage.English:
            //        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
            //        break;
            //}
            _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                
        }

        foreach (GameObject puzzleItem in _guiPlayMiddle.GetPuzzleItem())
        {
            KeypetCtrl _keypetCtrl = puzzleItem.GetComponentInChildren<KeypetCtrl>();

            if (_keypetCtrl == null)
                continue;

            if (puzzleItem.name == _str)
            {
                _guiPlayMiddle.SetHandPosition(puzzleItem.transform.localPosition);

                switch (_str)
                {
                case "PuzzleItem_009":
                        
                    _guiPlayMiddle.SetTalkBoxPostion ();
                    _guiPlayMiddle.SetTutorialText ("[773326]Você deve vir aqui [FF0000]para salvar os [003260]keypets! [773326]Toque no [003260]keypet [773326]porco.");//[773326] Você deve vim aqui [FF0000] resgate [003260] keypets [773326] Por favor, guia o frango [003260] Keypet!");
                    break;
                case "PuzzleItem_021":
                    _guiPlayMiddle.SetTutorialText("[773326]Você deve vir aqui [FF0000]para salvar os [003260]keypets! [773326]Toque no [003260]keypet [773326]porco.");
                    break;
                case "PuzzleItem_006":
                    _guiPlayMiddle.SetTutorialText("[773326] Você pode salvar os [003260]keypets [773326]tocando em keypets iguais na diagonal, horizontal e vertical.  [773326]Toque neste keypet [00AF50]porco.");

                    //Você pode resgatar [003260] keypets [773326] por guia os mesmos keypets em diaglnal, horizental, linha vertical Então, por favor [00AF50] guia [773326] esta pig keypet.");
                    break;
                case "PuzzleItem_036":
                    _guiPlayMiddle.SetTutorialText ("[773326]Você pode salvar os keypets tocando em keypets iguais na diagonal, horizontal e vertical.  Toque neste espaço vazio."); //"[773326] Você pode resgatar keypets por guia os mesmos [003260] keypets em diaglnal, horizental, linha vertical Então, por favor [00AF50] guia [773326] este lugar vazio..");
                    _guiPlayMiddle.SetTalkBoxZeroX ();
                    break;
                case "PuzzleItem_013":
                    _guiPlayMiddle.SetTutorialText ("[773326] Viu? Você salvou o keypet porco no centro! Toque no keypet 'panda menor'.");
                    _guiPlayMiddle.SetTalkBoxZeroX ();
                    break;
                case "PuzzleItem_001":
                    _guiPlayMiddle.SetTutorialText ("[773326]Ah, o keypet galinha está incomodando. Mas a gente vai dar um jeito nesse pequeno keypet.");//[773326] Oh, [D78725] frango [773326] keypet está perturbando o caminho Mas nós estamos indo para empurrar este pequeno keypet..");
                    break;
                case "PuzzleItem_012":
                    _guiPlayMiddle.SetTutorialText("[773326]Isso! O menor foi salvo. Muito bem! Você pode tocar em mim?");
                    break;
                case "PuzzleItem_014":
                    _guiPlayMiddle.SetTutorialText ("[773326]Se deseja salvar o keypet, você pode tocar a linha horizontal inteira. Toque em mim e me salve!");//Se você quer economizar [003260] keypet, [773326] você pode tocar final horizontal para terminar [00AF50] Tab [773326] me e salvamento.!");
                    break;
                case "PuzzleItem_003":
                    _guiPlayMiddle.SetTutorialText("[773326]Você pode salvar os [003260]keypets [773326] tocando em keypets iguais por toda linha diagonal, horizontal e vertical. Toque em mim!");
                    break;
                case "PuzzleItem_015":
                    _guiPlayMiddle.SetTutorialText("[773326]Você pode salvar os [003260]keypets [773326] tocando em keypets iguais na diagonal. Você pode me salvar ao tocar em mim.");
                    break;
                case "PuzzleItem_031":
                    _guiPlayMiddle.SetTutorialText ("[773326]3 keypets do mesmo tipo podem ser salvos. Você pode tocar no espaço vazio e salvar os keypets. Agora toque em mim!"); //mesmos 3 tipos de [003260] keypets [773326] pode ser rescued.You pode [00AF50] de toque [773326] lugar vazio e salvamento keypets Agora [773326] guia. [00AFEF] me, por favor! ");
                    _guiPlayMiddle.SetTalkBoxPostion2 ();
                    break;
                case "PuzzleItem_027":
                    _guiPlayMiddle.SetTutorialText ("[003260]Os keypets [773326]podem ser conectados na diagonal, horizontal e vertical. Pode tocar no espaço vazio?"); //Keypet [773326] podem ser conectados em diaglnal, horizental, linha vertical Você [00AF50] contato [773326] o lugar vazio.?");
                    _guiPlayMiddle.SetTalkBoxZeroX ();
                    break;
                case "PuzzleItem_061":
                    _guiPlayMiddle.SetTutorialText("[773326]Ah, olha o espaço vazio. Você pode tocar nesse espaço vazio para salvar os keypets.");
                    break;
                case "PuzzleItem_016":
                    _guiPlayMiddle.SetTutorialText ("[773326]Ah, olha o espaço vazio. Você pode tocar nesse espaço vazio para salvar os keypets.");
                    _guiPlayMiddle.SetTalkBoxPostion3 ();
                    break;
                }   
                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.Korean:

                //        switch (_str)
                //        {
                //            case "PuzzleItem_009":
                //                _guiPlayMiddle.SetTalkBoxPostion();
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 [FF0000]구출[773326]하러 오셨군요!\n\n먼저 닭 모양 치키티 [003260]키펫[773326]을 눌러볼까요?");
                //                break;
                //            case "PuzzleItem_021":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 [FF0000]구출[773326]하러 오셨군요!\n\n먼저 닭 모양 치키티 [003260]키펫[773326]을 눌러볼까요?");
                //                break;
                //            case "PuzzleItem_006":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 구하려면 가로, 세로, 대각선으로 끝과 끝을 [00AF50]터치[773326]하면 된답니다.\n\n그럼 이 피그를 눌러볼까요?");
                //                break;
                //            case "PuzzleItem_036":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 구하려면 같은 키펫의 가로, 세로, 대각선으로 키펫에 빈공간에 [00AF50]터치[773326]하면 된답니다.\n\n그럼 이 빈공간을 눌러볼까요?");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;
                //            case "PuzzleItem_013":
                //                _guiPlayMiddle.SetTutorialText("[773326]가운데에 있던 피그까지 [70309F]구출[773326]되었죠?\n\n그럼 다음 렛서 [003260]키펫[773326]을 눌러 볼까요?");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;
                //            case "PuzzleItem_001":
                //                _guiPlayMiddle.SetTutorialText("[773326]자세히 보니 가운데에 닭 모양 [D78725]치키티[773326]가 가로막고 있네요.\n\n그래도 눌러 볼까요?");
                //                break;
                //            case "PuzzleItem_012":
                //                _guiPlayMiddle.SetTutorialText("[773326]렛서가 [00AFEF]구출[773326]됐어요! 앞으로도 이렇게 풀어주시면 돼요.\n\n절 눌러 보시겠어요?");
                //                break;
                //            case "PuzzleItem_014":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 구하려면 가로 끝과 끝을 [00AF50]터치[773326]하면 된답니다.\n\n절 눌러 구출 할 수 있어요.");
                //                break;
                //            case "PuzzleItem_003":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 구하려면 가로, 세로, 대각선으로 끝과 끝을 [00AF50]터치[773326]하면 된답니다.\n\n절 눌러 보시겠어요?");
                //                break;
                //            case "PuzzleItem_015":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들을 대각선도 끝과 끝을 [00AF50]터치[773326]하면 된답니다.\n\n절 눌러 구출 할 수 있어요.");
                //                break;
                //            case "PuzzleItem_031":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들은 같은 종의 키펫 3마리가 뭉치면 된답니다. 빈 공간을 [00AF50]터치[773326]해서 구출 할 수 있어요. 이제는 [00AFEF]저를 [773326] 눌러 보겠어요?");
                //                _guiPlayMiddle.SetTalkBoxPostion2();
                //                break;
                //            case "PuzzleItem_027":
                //                _guiPlayMiddle.SetTutorialText("[003260]키펫[773326]들은 가로, 세로, 대각선등 다양하게 이어 질 수 있어요. 빈 공간을 [00AF50]터치[773326]해서 구출 할 수 있어요. [00AFEF]빈 공간 [773326]을 눌러 보겠어요?");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;
                //            case "PuzzleItem_061":
                //                _guiPlayMiddle.SetTutorialText("[773326]자세히 보니 가운데에 빈 공간이 있네요. 빈 공간을 [00AF50]터치[773326]해서 구출 할 수 있어요. [00AFEF]빈 공간 [773326]을 눌러 보겠어요?");
                //                break;
                //            case "PuzzleItem_016":
                //                _guiPlayMiddle.SetTutorialText("[773326]자세히 보니 가운데에 빈 공간이 있네요. 빈 공간을 [00AF50]터치[773326]해서 구출 할 수 있어요. [00AFEF]빈 공간 [773326]을 눌러 보겠어요?");
                //                _guiPlayMiddle.SetTalkBoxPostion3();
                //                break;
                //        }
                //        break;
                //    case SystemLanguage.English:

                //        switch (_str)
                //        {
                //            case "PuzzleItem_009":
                //                _guiPlayMiddle.SetTalkBoxPostion();
                //                _guiPlayMiddle.SetTutorialText("[773326]You must came here to [FF0000]rescue [003260]keypets! [773326]Please tab the chicken [003260]Keypet.");
                //                break;                        //Você deve vir aqui para salvar os keypets! Toque no keypet galinha.

                //            case "PuzzleItem_021":
                //                _guiPlayMiddle.SetTutorialText("[773326]You must came here to [FF0000]rescue [003260]keypets! [773326]Please tab the chicken [003260]Keypet.");
                //                break;                        //Você deve vir aqui para salvar os keypets! Toque no keypet galinha.

                //            case "PuzzleItem_006":
                //                _guiPlayMiddle.SetTutorialText("[773326]You can rescue [003260]keypets [773326]by tab the same keypets in diaglnal, horizental, vertical line.  Then please [00AF50]tab[773326] this 'pig'keypet.");
                //                break;                        //Você pode salvar os keypets tocando em keypets iguais na diagonal, horizontal e vertical.  Toque neste keypet porco.
                //            case "PuzzleItem_036":
                //                _guiPlayMiddle.SetTutorialText("[773326]You can rescue keypets by tab the same [003260]keypets in diaglnal, horizental, vertical line.  Then please [00AF50]tab [773326]this empty place.");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;                        //Você pode salvar os keypets tocando em keypets iguais na diagonal, horizontal e vertical.  Toque neste espaço vazio.

                //            case "PuzzleItem_013":
                //                _guiPlayMiddle.SetTutorialText("[773326]See? You [70309F]saved [773326]the pig keypet in center! Then, please tab the 'lesserpanda' [003260]keypet.");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;                        // Viu? Você salvou o keypet porco no centro! Toque no keypet 'panda menor'.
                //            case "PuzzleItem_001":
                //                _guiPlayMiddle.SetTutorialText("[773326]Oh, [D78725]chicken [773326]keypet is disturbing the way. But we are going to push this little keypet.");
                //                break;                        //Ah, o keypet galinha está incomodando. Mas a gente vai dar um jeito nesse pequeno keypet.
                //            case "PuzzleItem_012":
                //                _guiPlayMiddle.SetTutorialText("[773326]Yeah! Lesser was [00AFEF]rescued. [773326]Good job! Then would you touch me?");
                //                break;                        //Isso! O menor foi salvo. Muito bem! Você pode tocar em mim?
                //            case "PuzzleItem_014":
                //                _guiPlayMiddle.SetTutorialText("[773326]If you want to save [003260]keypet, [773326]you can touch horizontal end to end. [00AF50]Tab [773326]me and rescue!");
                //                break;                        //Se deseja salvar o keypet, você pode tocar a linha horizontal inteira. Toque em mim e me salve!

                //            case "PuzzleItem_003":
                //                _guiPlayMiddle.SetTutorialText("[773326]You can rescue [003260]keypets [773326]by [00AF50]tab [773326]the same keypets end to end in diaglnal, horizental, vertical line.   Please tab me!");
                //                break;                        //Você pode salvar os keypets tocando em keypets iguais por toda linha diagonal, horizontal e vertical. Toque em mim!
                //            case "PuzzleItem_015":
                //                _guiPlayMiddle.SetTutorialText("[773326]You can rescue [003260]keypets [773326]by tab the same keypets in diaglnal line. You can rescue me when you touch me.");
                //                break;                        //Você pode salvar os keypets tocando em keypets iguais na diagonal. Você pode me salvar ao tocar em mim.
                //            case "PuzzleItem_031":
                //                _guiPlayMiddle.SetTutorialText("[773326]Same 3 kinds of [003260]keypets [773326]can be rescued.You can [00AF50]touch [773326]empty place and rescue keypets. Now [773326]tab [00AFEF]me, please!");
                //                _guiPlayMiddle.SetTalkBoxPostion2(); 
                                                                //3 keypets do mesmo tipo podem ser salvos. Você pode tocar no espaço vazio e salvar os keypets. Agora toque em mim!

                //                break;
                //            case "PuzzleItem_027":
                //                _guiPlayMiddle.SetTutorialText("[003260]Keypet [773326]can be connected in diaglnal, horizental, vertical line. Would you [00AF50]touch [773326]the empty place?");
                //                _guiPlayMiddle.SetTalkBoxZeroX();
                //                break;                        //Os keypets podem ser conectados na diagonal, horizontal e vertical. Pode tocar no espaço vazio?
                //            case "PuzzleItem_061":
                //                _guiPlayMiddle.SetTutorialText("[773326]Oh, look at that empty place. You can [00AF50]touch [773326]there [00AFEF]empty [773326]place and rescue keypets.");
                //                break;                        //Ah, olha o espaço vazio. Você pode tocar nesse espaço vazio para salvar os keypets.

                //            case "PuzzleItem_016":
                //                _guiPlayMiddle.SetTutorialText("[773326]Oh, look at that empty place. You can [00AF50]touch [773326]there [00AFEF]empty [773326]place and rescue keypets.");
                //                _guiPlayMiddle.SetTalkBoxPostion3();
                                                                //Ah, olha o espaço vazio. Você pode tocar nesse espaço vazio para salvar os keypets.

                //                break;
                //        }
                //        break;
                //    case SystemLanguage.Portuguese:
                //        switch (_str)
                //        {
                //            case "PuzzleItem_009":
                //                     _guiPlayMiddle.SetTalkBoxPostion ();
                //                     _guiPlayMiddle.SetTutorialText ("[773326] Você deve vim aqui [FF0000] resgate [003260] keypets [773326] Por favor, guia o frango [003260] Keypet!");
                //                 break;
                //             case "PuzzleItem_021":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Você deve vim aqui [FF0000] resgate [003260] keypets [773326] Por favor, guia o frango [003260] Keypet!");
                //                 break;
                //             case "PuzzleItem_006":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Você pode resgatar [003260] keypets [773326] por guia os mesmos keypets em diaglnal, horizental, linha vertical Então, por favor [00AF50] guia [773326] esta pig keypet.");
                //                 break;
                //             case "PuzzleItem_036":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Você pode resgatar keypets por guia os mesmos [003260] keypets em diaglnal, horizental, linha vertical Então, por favor [00AF50] guia [773326] este lugar vazio..");
                //                 _guiPlayMiddle.SetTalkBoxZeroX ();
                //                 break;
                //             case "PuzzleItem_013":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Ver Você [70309F] salvou [773326] o keypet porco no centro Então, por favor, a página 'lesserpanda' [003260] keypet?!.");
                //                 _guiPlayMiddle.SetTalkBoxZeroX ();
                //                 break;
                //             case "PuzzleItem_001":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Oh, [D78725] frango [773326] keypet está perturbando o caminho Mas nós estamos indo para empurrar este pequeno keypet..");
                //                 break;
                //             case "PuzzleItem_012":
                //                 _guiPlayMiddle.SetTutorialText ("!.! [773326] Yeah Lesser foi [00AFEF] resgatou [773326] Good job Então você iria me tocar?");
                //                 break;
                //             case "PuzzleItem_014":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Se você quer economizar [003260] keypet, [773326] você pode tocar final horizontal para terminar [00AF50] Tab [773326] me e salvamento.!");
                //                 break;
                //             case "PuzzleItem_003":
                //                 _guiPlayMiddle.SetTutorialText (". [773326] Você pode resgatar [003260] keypets [773326] por [00AF50] guia [773326] os mesmos keypets de ponta a ponta em diaglnal, horizental, linha vertical Por favor, guia-me!");
                //                 break;
                //             case "PuzzleItem_015":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Você pode resgatar [003260] keypets [773326] por guia os mesmos keypets em linha diaglnal Você pode me salvar quando você me toca.");
                //                 break;
                //             case "PuzzleItem_031":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] mesmos 3 tipos de [003260] keypets [773326] pode ser rescued.You pode [00AF50] de toque [773326] lugar vazio e salvamento keypets Agora [773326] guia. [00AFEF] me, por favor! ");
                //                 _guiPlayMiddle.SetTalkBoxPostion2 ();
                //                 break;
                //             case "PuzzleItem_027":
                //                 _guiPlayMiddle.SetTutorialText ("[003260] Keypet [773326] podem ser conectados em diaglnal, horizental, linha vertical Você [00AF50] contato [773326] o lugar vazio.?");
                //                 _guiPlayMiddle.SetTalkBoxZeroX ();
                //                 break;
                //             case "PuzzleItem_061":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Oh, olhe para aquele lugar vazio Você pode [00AF50] Toque em [773326] lá [00AFEF] vazia [773326] lugar e salvamento keypets..");
                //                 break;
                //             case "PuzzleItem_016":
                //                 _guiPlayMiddle.SetTutorialText ("[773326] Oh, olhe para aquele lugar vazio Você pode [00AF50] Toque em [773326] lá [00AFEF] vazia [773326] lugar e salvamento keypets..");
                //                 _guiPlayMiddle.SetTalkBoxPostion3 ();
                //                 break;
                //        }   
                //        break;

                UIEventListener.Get(puzzleItem).onClick = (GameObject pGO) =>
                {
                    if (_isRescueMode)
                    {
                        if (null == _rescueKeypte)
                        {
                            _rescueKeypte = pGO;
                        }
                    }
                    else
                    {
                        if (null == _selectedKeypet1)
                        {
                            _selectedKeypet1 = pGO;
                            _selectedKeypet2 = null;

                            SelectSourceKeypet(_selectedKeypet1);
                        }
                        else if (null == _selectedKeypet2)
                        {
                            _selectedKeypet2 = pGO;
                        }
                    }
                };
            }
            else
            {
                UIEventListener.Get(puzzleItem).onClick = (GameObject pGO) =>
                {
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Korean:
                    //        _guiPlayMiddle.SetTutorialText("[773326]잘못 누르셨어요.\n\n[FFC800]손가락[773326]이 가리키는 곳을\n\n눌러 주세요~!");
                    //        break;
                    //    case SystemLanguage.English:
                    //        _guiPlayMiddle.SetTutorialText("[773326]Please press the place where the [FFC800]finger [773326]is pointing.");
                    //        break;
                    //    case SystemLanguage.Portuguese:
                    //         _guiPlayMiddle.SetTutorialText ("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                    //        break;
                    //}
                    _guiPlayMiddle.SetTutorialText("[773326] Por favor, pressione o local onde o dedo [FFC800] [773326] está apontando.");
                         
                };
            }
        }
    }

    void ItemSettingClick(bool _bDoTutorial) 
    {
        _guiPlayBottom.OnClickItem = (uint item_idx, GameObject go) =>
        {
            if (itemUseWark == false)
            {
                itemUseWark = true;

                //Debug.Log(itemUseWark);
                // 아이템에 따른 실행 처리(구출류, 횟수/시간 증가류 등)
                ItemInfo itemInfo = GameDataMgr.Instance.GetInfo<ItemInfoMgr>().GetItemInfo(item_idx);
                
                if (!_bDoTutorial)
                {
                    // 서버에 알림.
                    //Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
                    //StartCoroutine(cmd.Send_PROTOCOL_USE_ITEM_REQ((int)item_idx));

                }
                else
                {
                    _guiPlayMiddle.SetHandPosition(_guiPlayMiddle.GetMiddleBG());

                    SettingPuzzleItemName("PuzzleItem_013");
                }

                GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(item_idx);
                uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;

                if(_bTutorial == false)
                    SaveData.ItemUseHave(getItemIndex);

                if (itemInfo.si == 4)
                {     // 긴급구출
                    /*
                        disable button
                        go.SetActive(false);
                        이미지와 OnClick 을 초기화 한다.
                        go.GetComponent<UITexture>().mainTexture = null;
                        UIEventListener.Get(go).onClick = null;
                    */

                    DoUseItem_Rescue();

                    _guiPlayBottom.PlayBubbleMessage(go.transform);
                }
                if (itemInfo.si == 8)      // 렌덤구출
                {
                    DoUseItem_RandomRescue();
                }
                if (itemInfo.si == 7)		// 시간증가
                {
                    DoUseItem_SaveTime(itemInfo.attr_1);
                }
                if (itemInfo.si == 6)
                {		// 턴 증가
                    DoUseItem_SaveCount(itemInfo.attr_2);
                }
                if (itemInfo.si == 17)
                {		// 피버스타트
                    _feverMaxTime = _feverWaitTime;
                    ShowFeverTime(_feverWaitTime);
                }
                if (itemInfo.si == 16)
                {		// 피버시간증가
                    DoUseItem_FeverSaveTime(itemInfo.attr_1);
                }
                if (itemInfo.si == 5)
                {		// 한번 물리기
                    DoUseItem_Undo();
                }
            }
        };
    }

    uint _nIndex = 2;

    bool _bEndTutorial = false;

    public IEnumerator IE_TutorialEnd()
    {
        if(!_bEndTutorial)
        {
            _bEndTutorial = true;

            _isPause = true;

            _guiPlayMiddle.TutorialActive(false);

            //<
            yield return new WaitForSeconds(3.0f);

            if (isReballPlay)
            {
                if (KakaoMain.Instance._bTutorailStart)
                {
                    //switch (Application.systemLanguage) 
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        _guiPlayMiddle.SetTutorialText("Sair do tutorial.");
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        _guiPlayMiddle.SetTutorialText("튜토리얼을 종료합니다.");
                    //        break;
                    //    case SystemLanguage.English:
                    //        _guiPlayMiddle.SetTutorialText("Exit the tutorial");
                    //        break;

                    //}

                    _guiPlayMiddle.SetTutorialText("Sair do tutorial.");

                    GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, true, null);
                    _guiPlayTutorialCutin.TutorialAlarm(GuiPlayTutorialCutin.Tutorial.Tutorial_End);

                    _guiPlayTutorialCutin.SetEndLabel(false);
                }
            }
            else
            {
                if (!KakaoMain.Instance._bTutorailStart)
                {
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        _guiPlayMiddle.SetTutorialText ("Para agradecer você seguir o [773.326] [FFC800] resgate conjunto [773 326] e [FFC800] Deixe o aumento definir [773326] \n\n, então, todo o KeypetBreak!");
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        _guiPlayMiddle.SetTutorialText("[773326]따라와주신 보답으로 [FFC800]구출 세트[773326]와 [FFC800]증가 세트[773326]를 드릴게요.\n\n그럼 모두 키펫 브레이크!");
                    //        break;
                    //    case SystemLanguage.English:
                    //        _guiPlayMiddle.SetTutorialText("To thank you you follow the [773326] [FFC800] rescue set [773 326] and [FFC800] Let the increase set [773326] \n\n then, all of the KeypetBreak!");

                    //        break;
                    //}

                    _guiPlayMiddle.SetTutorialText("Para agradecer você seguir o [773.326] [FFC800] resgate conjunto [773 326] e [FFC800] Deixe o aumento definir [773326] \n\n, então, todo o KeypetBreak!");
                    
                    SaveData.StartTutorialIntro(false); 

                    GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, true, null);
                    _guiPlayTutorialCutin.TutorialAlarm(GuiPlayTutorialCutin.Tutorial.Tutorial_End);

                    _guiPlayTutorialCutin.SetEndLabel(true);
                }
                else
                {
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //       _guiPlayMiddle.SetTutorialText ("Sair do tutorial.");
                    //       break;
                    //    case SystemLanguage.Korean:
                    //        _guiPlayMiddle.SetTutorialText("튜토리얼을 종료합니다.");
                    //        break;
                    //    case SystemLanguage.English: 
                    //        _guiPlayMiddle.SetTutorialText(". Exit the tutorial");
                    //        break;

                    //}
                    _guiPlayMiddle.SetTutorialText("Sair do tutorial.");
                    
                    GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, true, null);
                    _guiPlayTutorialCutin.TutorialAlarm(GuiPlayTutorialCutin.Tutorial.Tutorial_End);

                    _guiPlayTutorialCutin.SetEndLabel(false);
                }
            }

            _selectedKeypet1 = null;
            _selectedKeypet2 = null;

            if (KakaoMain.Instance._bTutorailStart)
            {
                if (SaveData.StartTutorialIntro() && isReballPlay == false)
                    SaveData.StartTutorialIntro(false);

                yield return new WaitForSeconds(2.0f);

                GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, false, null);

                KakaoMain.Instance._bTutorailStart = false;

                GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
            }
            else 
            {
                if (isReballPlay)
                {
                    Debug.Log(SaveData.StartReballIntro());

                    if (KakaoMain.Instance.isReballPlay && SaveData.StartReballIntro())
                    {
                        KakaoMain.Instance.isReballPlay = false;
                     
                        RbTutorial();
                    }

                    if (KakaoMain.Instance.isReballPlay) 
                    {
                        KakaoMain.Instance.isReballPlay = false;
                    }
                }
                else 
                { 
                    //< 아이템 셋팅하고, 사실 이부분은 쓸모가 없다...
                    //왜 있는지 모르겠다. 그러니깐 if{} <- 이 구문은 필요가 없다.
                    bool isMoveFinished = false;

                   // Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();

                    if (_bTutorial && !KakaoMain.Instance._bTutorailStart && SaveData.StartTutorialIntro())
                        SaveData.StartTutorialIntro(false);

                    _guiPlayTutorialCutin.OnButtonSkipClick = (bool pIsOk) =>
                    {
                        if (pIsOk)
                        {
                            isMoveFinished = true;
                        }
                    };

                    while (!isMoveFinished)
                    {
                        yield return null;
                    }

                    if (!_bTutorial)
                    {
                        //게임 셋팅하자
                        StartCoroutine(Sequence());
                    }
                    else 
                    {
                        _bTutorial = false;
                    
                        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

                        GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, true, null);
                        _guiPlayTutorialCutin.TutorialAlarm(GuiPlayTutorialCutin.Tutorial.Tutorial_Fish);

                        _nIndex = 2;

                        PlayItem02(packageInfoMgr, _nIndex);
                    }
                }
            }

            if (KakaoMain.Instance.isReballPlay)
                KakaoMain.Instance.isReballPlay = false;

            if (KakaoMain.Instance._bTutorailStart)
                KakaoMain.Instance._bTutorailStart = false;
        }


    }

    void PlayItem02(PackageInfoMgr packageInfoMgr, uint PackIndex, bool isEnd = false)
    {
        SaveData.ItemHaveUpdate(PackIndex, 1);

        _guiPlayTutorialCutin.SetTextureItem(
            (Texture2D)Resources.Load("Images/Item/" + packageInfoMgr.GetPackageInfo(PackIndex).icon_image), packageInfoMgr.GetPackageInfo(PackIndex).name, 100.0f);

        _guiPlayTutorialCutin.OnButtonClick = (bool pIsOk) =>
        {
            if (pIsOk)
            {
                if(isEnd)
                { 
                    SaveData.rePlay = true;

                    _isPause = false;

                    GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
                
                    GuiMgr.Instance.Show<GuiPlayTutorialCutin>(GuiMgr.ELayerType.Front, false, null);
                }//
                else 
                {
                    Debug.Log("_nIndex :" + _nIndex);

                    _nIndex++;

                    if (PackIndex < 4)
                    {
                        PlayItem02(packageInfoMgr, _nIndex);
                    }
                    else 
                    {
                        PlayItem02(packageInfoMgr, _nIndex, true);
                    }
                }
            }
        };
    }

    void RbTutorial()
    {
        GuiBonusInfo guiBonusInfo = GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, true, null);

        guiBonusInfo.StatusAlarm(GuiBonusInfo.BonusStauts.Item);

        int[] PackIDX = new int[2];
        int openstage = SaveData.clearStageTopNumber;

        PackIDX[0] = 70;
        PackIDX[1] = 63;

        Debug.Log(SaveData.StartReballIntro());

        if(SaveData.StartReballIntro())
            SaveData.StartReballIntro(false);
        
        int clearStageReball = -1;
        
        SaveData.clearTutorialReball(clearStageReball);

        string rewardIcon = "";
        string strName = "";

        if (PackIDX[0] == 0)
        {

        }
        else
        {
            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)PackIDX[0]);

            if (packageInfo != null)
            {
                rewardIcon = packageInfo.icon_image;
                strName = packageInfo.name;

                itemHave(PackIDX[0], 1000);
                
                guiBonusInfo.SetTextureItem((Texture2D)Resources.Load("Images/Item/" + packageInfo.icon_image), strName, 100.0f);
            }
        }

        guiBonusInfo.OnButtonClick = (bool gi) =>
        {
            guiBonusInfo.OnButtonClick = null;

            Debug.Log("True");

            GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, false, null);

            PlayReballItem((uint)PackIDX[1], openstage);

            itemHave(PackIDX[1], 10);
        };
    }

    void PlayReballItem(uint PackIdx, int openstage)
    {
        string rewardIcon = "";
        string strName = "";

        GuiBonusInfo guiBonusInfo = GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, true, null);

        guiBonusInfo.StatusAlarm(GuiBonusInfo.BonusStauts.Item);

        try
        {
            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)PackIdx);

            if (packageInfo != null)
            {
                rewardIcon = packageInfo.icon_image;
                strName = packageInfo.name;
                Debug.Log("~~~~~~~~~~~~~ Package Index : " + PackIdx.ToString() + " name:" + strName + " icon:" + rewardIcon);

                guiBonusInfo.SetTextureItem((Texture2D)Resources.Load("Images/Item/" + packageInfo.icon_image), strName, 100.0f);
            }

            guiBonusInfo.OnButtonClick = (bool gi) =>
            {
                Debug.Log("Bonus!");

                GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, false, null);

                SetNextStage(openstage, SaveData.clearStageTopNumber);
            };
        }
        catch
        {
            Debug.Log("E :");
        }
    }

    //프로세스를 바꿔야 한다.
    //처음 도입 부분에서 Permissions를 허락 받았기 때문에 그렇다.
    //프로세스를 만들어서 Permissions를 확인하는 역활을 해야한다.
    //IEnumerator FacebookCallAPI(int stage, string packageName, JSONNode msgParam)
    //{
    //    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

    //    InteractiveConsole.GetInstance().CallFBInit();

    //    Debug.Log(FB.IsInitialized);

    //    while (!FB.IsLoggedIn)
    //        yield return null;

    //    if (FBMain.Instance.isFB == false)
    //    {
    //        InteractiveConsole.GetInstance().CallSigleName();

    //        while (InteractiveConsole.GetInstance().fbname == "")
    //            yield return null;
    //    }
    //    else 
    //    {
    //        InteractiveConsole.GetInstance().fbname = InteractiveConsole.GetInstance().MyFbInfo.name;
    //    }

    //    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

    //    yield return new WaitForSeconds(0.5f);

    //    if (!InteractiveConsole.GetInstance().fbname.Contains("ValkyriError"))
    //    {
    //        StartCoroutine(InteractiveConsole.GetInstance().TakeScreenshot(stage, packageName));

    //        yield return new WaitForSeconds(0.5f);

    //        yield return StartCoroutine(IE_ENDProcess(FBMain.Instance.isFB));

    //        msgParam["message"] = "Sucesso, posts!";
    //        msgParam["show_type"] = "one";

    //        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

    //        if (FBMain.Instance.isFB)
    //            InteractiveConsole.GetInstance().FriendCall();

    //        yield return new WaitForSeconds(0.5f);

    //        guiMessageBox.OnButtonClick = (bool pIsOk) =>
    //        {
    //            if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
    //            {
    //                _guiPlayResultSuccess.OkBtn.onClick = null;
    //                _guiPlayResultSuccess.CloseBtn.onClick = null;

    //                GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
    //            }
    //        };
                     
    //    }
    //    else 
    //    {
    //        msgParam["message"] = "Conexão com o servidor foi desligado. Verifique o estado da ligação à Internet, por favor, tente novamente.";
    //        msgParam["show_type"] = "one";

    //        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);	// 확인 창 출력...

    //        guiMessageBox.OnButtonClick = (bool pIsOk) =>
    //        {
    //            if (pIsOk) 	// 이미 이 단계까지 왔다는 것은 아직까지 메시지를 받지 않은 친구라고 생각하고...
    //            {
    //            }
    //        };
    //    }

    //}

    IEnumerator IE_ENDProcess(bool isFB = false) 
    {
        Debug.Log(isFB);

        if (isFB)
        {
            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

            //InteractiveConsole.GetInstance().CallFBAPI();

            yield return new WaitForSeconds(0.5f);

            //InteractiveConsole.GetInstance().FriendCall();

            //yield return new WaitForSeconds(0.5f);

            //while (!InteractiveConsole.GetInstance().isFriendCall)
            //{
            //    yield return null;
            //}

            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

            GameStateMgr.Instance.ChangeState<GSStageMap>(0,  null);
        }
    }

}
