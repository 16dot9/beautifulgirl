﻿using System.Collections;
using UnityEngine;
using SimpleJSON;


public class GSMessage : GameStateBase
{

	protected GuiBG							_guiBG = null;
	protected GuiMessage					_guiMessage = null;


	public override void OnEnter()
    {
		_guiBG = GuiMgr.Instance.Show<GuiBG>(GuiMgr.ELayerType.Back, true, null);
		_guiMessage = GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Back, true, null);



	}

    public override void OnLeave()
    {
		if (null != GuiMgr.Instance)
		{
			_guiMessage.OkBtn.onClick = null;
			_guiMessage.AllOkBtn.onClick = null;

			_guiBG = null;
			_guiMessage = null;

			GuiMgr.Instance.Show<GuiBG>(GuiMgr.ELayerType.Back, false, null);
			GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Back, false, null);
		}
	}

}
