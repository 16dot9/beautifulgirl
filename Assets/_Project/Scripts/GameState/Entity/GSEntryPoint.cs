﻿using UnityEngine;
using SimpleJSON;


public class GSEntryPoint : GameStateBase
{

    public override void OnEnter()
    {
        GameStateMgr.Instance.ChangeState<GSLogo>(0, null);
    }

    public override void OnLeave()
    {
    }

}
