﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System;
using SimpleJSON;

public class GSStageMap : GameStateBase
{
    protected GuiPlayerInfo _guiPlayerInfo = null;
    protected GuiStageMapInfo _guiStageMapInfo = null;
    protected GuiStageMapInfoScrollView _guiStageMapInfoScrollView = null;
    protected GuiSettingMenu _guiSettingMenu = null;

    protected GuiChargeWindow _guiChargeWindow = null;

    public GameObject guiMessageItemGO;
    public GuiMessageItem guiMessageItem;

    public bool _bFriendInfoStageLoad;

    private InfoSave _InfoSave;

    void Awake()
    {
        _InfoSave = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
    }

    public override void OnEnter()
    {
        _guiPlayerInfo = GuiMgr.Instance.Show<GuiPlayerInfo>(GuiMgr.ELayerType.Back, true, null);
        _guiStageMapInfo = GuiMgr.Instance.Show<GuiStageMapInfo>(GuiMgr.ELayerType.Back, true, null);
        _guiStageMapInfoScrollView = GuiMgr.Instance.Show<GuiStageMapInfoScrollView>(GuiMgr.ELayerType.ScrollView, true, null);
        _guiSettingMenu = GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, true, null);

        //KakaoMain.Instance._guiPlayerInfo = _guiPlayerInfo;

        //< 싱글버전 하트체크
        KakaoMain.Instance.CheckUpdateHeart(_guiPlayerInfo);

        _guiPlayerInfo.PowerBtn.onClick = (GameObject pGO) =>
        {
            GuiChargePower guiChargePower = GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, true, null);

            #region dummy power item setting

            GameObject itemGO;
            GuiChargePowerItem guiChargePowerItem;

            // shop table 에서 Power type 들만 추출
            ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
            PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

            foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
            {
                if (shopInfo.type != ShopType.Power)
                    continue;

                // 리스트 항목을 생성 추가 한다.
                itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargePowerItem"));
                guiChargePowerItem = itemGO.GetComponent<GuiChargePowerItem>();
                guiChargePowerItem.SerialID = shopInfo.pack_idx;

                uint powersu = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).item_1_su;
                float cost = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value;

                //switch (Application.systemLanguage) 
                //{
                //    case SystemLanguage.Korean:
                //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " 젬", "x " + powersu.ToString());
                //        break;
                //    case SystemLanguage.English:
                //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x " + powersu.ToString());
                //        break;
                //    case SystemLanguage.Portuguese:
                //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x" + powersu.ToString());
                //        break;
                //}
                guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x" + powersu.ToString());
                       
                int pack_idx = (int)shopInfo.pack_idx;
                guiChargePowerItem.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
                {
                    if (MyInfoMgr.Instance.m_Gem >= cost)
                    {
                        JSONNode msgParam = new JSONClass();

                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        msgParam["message"] = "Você gostaria de comprar?";
                        //        break;
                        //    case SystemLanguage.English:
                        //        msgParam["message"] = "Would you like to buy?";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        msgParam["message"] = "구매 하시겠습니까?";
                        //        break;
                        //}
                        msgParam["message"] = "Você gostaria de comprar?";
                                
                        msgParam["show_type"] = "two";

                        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                        guiMessageBox.OnButtonClick = (bool pIsOk) =>
                        {
                            if (pIsOk)
                            {
                                /////////////////////// buy pack 필요.
                                _InfoSave.PowerSuPlus((int)powersu);
                                _InfoSave.GemSuMinus((int)cost);

                                _InfoSave.ItemHaveUpdate(pack_idx, 1);
                                return;
                            }
                        };
                    }

                    //Hwang Wark 파워상점에서 잼이 코스트보다 부족할때 잼상점 팝업 Start
                    if (MyInfoMgr.Instance.m_Gem < cost)
                    {
                        JSONNode msgParampg = new JSONClass();
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        msgParampg["message"] = "Gem Não o suficiente";
                        //        break;
                        //    case SystemLanguage.English:
                        //        msgParampg["message"] = "Not enough Jóias";
                        //        break;
                        //}

                        msgParampg["message"] = "Gem Não o suficiente";
                        msgParampg["show_type"] = "two";

                        GuiMessageBox guiMessageBoxpg = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParampg);

                        guiMessageBoxpg.OnButtonClick = (bool pIsOkpg) =>
                        {
                            if (pIsOkpg)
                            {
                                //Debug.Log("aaaa");				

                                guiChargePower.CloseBtn.onClick = null;
                                
                                guiChargePower = null;

                                GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, false, null);

                                // 젬 윈도우 팝업....
                                GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
                                _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
                                if (_guiChargeWindow != null)
                                    _guiChargeWindow.ShowGemWindow();
                            }
                        };
                    }
                    //Hwang Wark 파워상점에서 잼이 코스트보다 부족할때 잼상점 팝업 End
                };
                guiChargePower.AddChargePowerItem(guiChargePowerItem);
            }

            #endregion dummy power item setting

            guiChargePower.CloseBtn.onClick = (GameObject pCloseGO) =>
            {
                guiChargePower.CloseBtn.onClick = null;
                
                guiChargePower = null;

                GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, false, null);
            };

        };

        _guiPlayerInfo.FunnyBtn.onClick = (GameObject pGO) =>
        {
            ShowChargeFunny();
        };

        _guiPlayerInfo.GemBtn.onClick = (GameObject pGO) =>
        {
            // 젬 윈도우 팝업....
            GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
            _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
            if (_guiChargeWindow != null)
                _guiChargeWindow.ShowGemWindow();
        };

        _guiSettingMenu.StoreBtn.onClick = (GameObject pGO) =>
        {
            GuiItemShop guiItemShop = GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, true, null);

            guiItemShop.ClearItemShopItem();

            // shop table 에서 Item type 들만 추출
            ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
            PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

            GuiItemShopItem item;
            //Hwang Wark
            GuiItemShopItem guiItemShopItem;
            int _nCount = 0;
            //Hwang End
            foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
            {
                if (shopInfo.type != ShopType.Item)
                    continue;

                // 리스트 항목을 생성 추가 한다.
                item = ((GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItemShopItem"))).GetComponent<GuiItemShopItem>();

                PackageInfo pkginfo = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx);

                float _funnysu = pkginfo.price_value;
                PriceType valuetype = pkginfo.price_type;

                item.SerialID = shopInfo.pack_idx;

                //switch(Application.systemLanguage)
                //{ 
                //    case SystemLanguage.Korean:
                //    {
                //        if (valuetype == PriceType.Funny)
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\n퍼니",
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //        else if (valuetype == PriceType.Gem)
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " 젬",
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //        else
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //    }
                //    break;

                //    case SystemLanguage.English:
                //    {
                //        if (valuetype == PriceType.Funny)
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //        else if (valuetype == PriceType.Gem)
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //        else
                //        {
                //            item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                //                                "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //        }
                //    }
                //        break;


                //    case SystemLanguage.Portuguese:
                //        {
                //            if (valuetype == PriceType.Funny)
                //            {
                //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                //                                    "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //            }
                //            else if (valuetype == PriceType.Gem)
                //            {
                //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                //                                    "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //            }
                //            else
                //            {
                //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                //                                    packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                //                                    "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                //            }
                //        }
                //        break;
                //}

                if (valuetype == PriceType.Funny)
                {
                    item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                                        "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                }
                else if (valuetype == PriceType.Gem)
                {
                    item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                                        "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                }
                else
                {
                    item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                        packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                                        "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                }

                item.transform.Find("BG/BuyBtn").GetComponent<ShopItemIndexNumber>().thisIndex = (int)shopInfo.pack_idx;

                guiItemShop.AddItemShopItem(item);

                item.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
                {
                    Debug.LogError("Button Click!");

                    //Hwang Wark
                    int isIndex = pBuyBtnGO.GetComponent<ShopItemIndexNumber>().thisIndex;
                    //Hwang End
                    if (MyInfoMgr.Instance.m_Funny >= _funnysu)
                    {
                        JSONNode msgParam = new JSONClass();

                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        msgParam["message"] = "Você gostaria de comprar?";
                        //        break;
                        //    case SystemLanguage.English:
                        //        msgParam["message"] = "Would you like to buy?";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        msgParam["message"] = "구매 하시겠습니까?";
                        //        break;
                        //}
                        msgParam["message"] = "Você gostaria de comprar?";
                        msgParam["show_type"] = "two";

                        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                        guiMessageBox.OnButtonClick = (bool pIsOk) =>
                        {
                            if (pIsOk)
                            {
                                _InfoSave.FunnySuMinus((int)_funnysu);
                                _InfoSave.ItemHaveUpdate((int)isIndex, 1);
                                _InfoSave.RouletteKeypetCountCheck();
                                _guiSettingMenu.OnEnter();
                            }
                        };
                    }

                    //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 Start
                    if (MyInfoMgr.Instance.m_Funny < _funnysu)
                    {
                        JSONNode msgParamfs = new JSONClass();
                        
                        //switch (Application.systemLanguage)
                        //{
                        //    case SystemLanguage.Portuguese:
                        //        msgParamfs["message"] = "Ir para Engraçado Shop";
                        //        break;
                        //    case SystemLanguage.English:
                        //        msgParamfs["message"] = "Go to Funny Shop";
                        //        break;
                        //    case SystemLanguage.Korean:
                        //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                        //        break;
                        //}

                        msgParamfs["message"] = "Ir para Engraçado Shop";
                        msgParamfs["show_type"] = "two";

                        GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                        guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                        {
                            if (pIsOkfs)
                            {
                                guiItemShop = null;
                                GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                                // 구매창으로
                                ShowChargeFunny();

                            }
                        };
                    }
                    //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 End
                };
                //Hwang End
            }

            guiItemShop.BackBtn.onClick = (GameObject) =>
            {
                guiItemShop = null;
                GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);
            };

            guiItemShop.GachaBtn.onClick = (GameObject) =>
            {

                Debug.Log("~~~~~~~~~~~~ GachaBtn ");
                if (MyInfoMgr.Instance.m_Funny >= MyInfoMgr._gachaFunnySu)
                {
                    //AudioMgr.Instance.Play("Gacha");
                    //_InfoSave.FunnySuMinus((uint)MyInfoMgr._gachaFunnySu);
                    guiItemShop.ShowGachaDlg(true);
                }
                else
                {
                    JSONNode msgParamfs = new JSONClass();
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParamfs["message"] = "Ir para Engraçado Shop";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParamfs["message"] = "Go to Funny Shop";
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                    //        break;
                    //}
                    msgParamfs["message"] = "Ir para Engraçado Shop";
                    msgParamfs["show_type"] = "two";

                    GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                    guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                    {
                        if (pIsOkfs)
                        {
                            guiItemShop = null;
                            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                            // 구매창으로
                            ShowChargeFunny();

                        }
                    };
                }
            };

            guiItemShop.CloseBtn.onClick = (GameObject) =>
            {
                AudioMgr.Instance.Stop("Gacha");

                guiItemShop.ShowGachaDlg(false);
            };

            guiItemShop.TakingItemBtn.onClick = (GameObject) =>
            {
                AudioMgr.Instance.Stop("Gacha");

                if (MyInfoMgr.Instance.m_Funny >= 5000)
                {
                    //5000원 만큼 가격 깍기
                    _InfoSave.FunnySuMinus((int)5000);

                    string packageName = "";
                    string strDesc = "";
                    string rewardIcon = "";

                    uint pay = 0;
                    GetGachaResult(packageInfoMgr, out packageName, out pay);	// 랜덤하게 가차뽑기..				

                    PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)pay);
                    if (packageInfo != null)
                    {
                        packageName = packageInfo.name;
                        strDesc = packageInfo.desc;
                        rewardIcon = packageInfo.icon_image;

                        _InfoSave.InGameItemHaveUpdate((int)pay, (int)packageInfo.item_1_su);
                    }
                    guiItemShop.ShowResult(true, packageName, strDesc, rewardIcon);
                }
                else 
                {
                    JSONNode msgParamfs = new JSONClass();
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParamfs["message"] = "Ir para Engraçado Shop";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParamfs["message"] = "Go to Funny Shop";
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                    //        break;
                    //}
                    msgParamfs["message"] = "Ir para Engraçado Shop";
                    msgParamfs["show_type"] = "two";

                    GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                    guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                    {
                        if (pIsOkfs)
                        {
                            guiItemShop = null;
                            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                            ShowChargeFunny();
                        }
                    };
                }
                //guiItemShop.ShowResult(true, "힌트 4개", "999 퍼니");

            };

            guiItemShop.OneMoreBtn.onClick = (GameObject) =>
            {
                ////AudioMgr.Instance.Play("Gacha");
                //_InfoSave.FunnySuMinus((uint)999);
                guiItemShop.ShowResult(false, "", "", "");

                guiItemShop.GachaBtn.onClick(GameObject);
            };

            guiItemShop.ResultBackBtn.onClick = (GameObject) =>
            {
                guiItemShop.ShowGachaDlg(false);
                guiItemShop.ShowResult(false, "", "", "");
            };
        };

        /*
        _guiSettingMenu.MsgBtn.onClick = (GameObject) =>
        {
            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

            KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage = GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Front, true, null);

            // 받은 하트 갯수 대로 만들어 준다.
            KeypetMessageLoadCount();

            KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage.OkBtn.onClick = (GameObject pOkGO) =>
            {
                KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage.ClearItems();
                GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Front, false, null);
            };

            KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage.AllOkBtn.onClick = (GameObject pAllOkGO) =>
            {
                byte[] _publicData =
                    KakaoGameUserInfo.Instance.publicData == null ? null : (KakaoGameUserInfo.Instance.publicData);
                byte[] _privateData = KakaoGameUserInfo.Instance.privateData == null ? null : (KakaoGameUserInfo.Instance.privateData);

                KakaoNativeExtension.Instance.updateUser(
                    KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().g_PackHeartCount.Count, _publicData, _privateData,
                    this.onUpdateUserComplete,
                    this.onUpdateUserError);

                KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage.ClearItems();
                //guiMessage = null;
                GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Front, false, null);
                StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_READ_MSG_ALL_REQ());
                
                if (KakaoGameMessages.Instance.gameMessages.Count > 0)
                {
                    KakaoNativeExtension.Instance.acceptAllGameMessages(this.onAcceptAllGameMessagesComplete, this.onAcceptAllGameMessagesError);
                }
                else
                {

                }
            };
        };*/



        _guiSettingMenu.SettingBtn.onClick = (GameObject pGO) =>
        {
            GuiGameSetting guiGameSetting = GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, true, null);

            guiGameSetting.OkBtn.onClick = (GameObject) =>
            {
                guiGameSetting = null;
                GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
            };

            guiGameSetting.TutorialBtn.onClick = (GameObject) =>
            {
                Debug.Log("map");
                guiGameSetting = null;
                GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

                KakaoMain.Instance._bTutorailStart = true;
                KakaoMain.Instance.isReballPlay = false;

                JSONNode param = new JSONClass();
                param["stage_si"] = "1";

                GameStateMgr.Instance.ChangeState<GSPlay>(0, param);

                //_InfoSave.startTutorialIntro = true;
                //{
                //}
            };
            guiGameSetting.BonusBtn.gameObject.SetActive(false);

            Debug.Log("Reball Intro :" + _InfoSave.StartReballIntro().ToString());

            if (!_InfoSave.StartReballIntro())
            {
                guiGameSetting.BonusBtn.gameObject.SetActive(true);

                guiGameSetting.BonusBtn.onClick = (GameObject) =>
                {
                    _guiSettingMenu = null;
                    GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

                    KakaoMain.Instance._bTutorailStart = true;
                    KakaoMain.Instance.isReballPlay = true;

                    JSONNode param = new JSONClass();
                    param["stage_si"] = "1";

                    GameStateMgr.Instance.ChangeState<GSPlay>(0, param);
                };
            }

            //guiGameSetting.FBSetBtn.onClick = (GameObject) => 
            //{
            //    if (FBMain.Instance.isFB)
            //    {
            //        GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
            //        FBMain.Instance.Logout();
            //    }
            //    else
            //    {
            //        GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

            //        _InfoSave.FbUserNew(false);
                    
            //        FBMain.Instance.Init();
            //    }

            //    Debug.Log("LogOut");
            //};
            //else 
            //{
                
            //}

            guiGameSetting.ContactBtn.onClick = (GameObject) =>
            {
                SendEmail();
            };
        };

        /*
        _guiSettingMenu.FriendBtn.onClick = (GameObject pGO) =>
        {
            if (FBMain.Instance.isFB)
            {
                InteractiveConsole.GetInstance().CallAppRequestAsFriendSelector();
            }
            else 
            {
                JSONNode msgParam = new JSONClass();
                msgParam["message"] = "Please, Login to Facebook.";
                msgParam["show_type"] = "one";

                GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);
            }
        //    KakaoNativeExtension.Instance.loadGameFriends(onLoadGameFriendsComplete, onLoadGameFriendsError);
        //    KakaoFriendsController.Instance.ShowKakaoInviteWindow();	// 카카오 친구초대 윈도우 보여주기...
        };*/

        _guiSettingMenu.MykeypetBtn.onClick = (GameObject pGO) =>
        {
            GameStateMgr.Instance.ChangeState<GSKeypetCollection>(0, null);
        };


        /*
         * Reball ClearTop Stage 수정
         */
        /*
        _guiSettingMenu.ClearTopStage.onClick = (GameObject pGo) => 
        {
            Debug.Log(" ClearTopStage ");

            if (_InfoSave.clearStageReball > 0) 
            {
                _InfoSave.clearStageReball *= -1;
            }
        };

        _guiSettingMenu.ReballStage.onClick = (GameObject pGo) =>
        {
            Debug.Log("Reball Stage !");
        };*/

        StartCoroutine(Setting());
    }
    void SendEmail ()
    {
        string email = "mobile@playspot.com.br";

        string subject = MyEscapeURL("[PetBreak]Report a problem!");//[회원번호 : " + KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._userid + "] 키펫 브레이크 문의 드립니다.");

        string body = MyEscapeURL("Contact: really helps if purchase decisions sent together to take a screen shot"); //카카오톡 회원번호:" + KakaoMain.Instance.GetComponent<Cmd_Handler_CS>()._userid + "\n [위의 카카오톡 회원번호를 삭제하지 마세요!] \n 문의 사항:(스크린샷을 찍어 함께 보내주시면 많은 도움이 됩니다.)");


        var now = DateTime.Now.ToLocalTime();
        var span = (now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
        int timestamp = (int)span.TotalSeconds;


        string url = string.Format("mailto:mobile@playspot.com.br?subject=Contato de Supporte PetBreak&body= Caso sua dúvida seja relacionada à compras, anexe um screenshot para agilizar seu suporte %0D%0A%0D%0A %0D%0A %0D%0A %0D%0A %0D%0A %0D%0A %0D%0A  ----------POR FAVOR NAO APAGUE!-------%0D%0A UTC Time: {0}  %0D%0A Version: {1}  %0D%0A User ID : {2} %0D%0A Platform : {3} %0D%0A Device : {4} %0D%0A OS : {5}", timestamp.ToString(), "1.1.6", "null", "Android", SystemInfo.deviceModel.ToString(), SystemInfo.operatingSystem.ToString());
        Application.OpenURL(Encoding.UTF8.GetString(System.Text.Encoding.UTF8.GetBytes(url)));//"mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
 
    string MyEscapeURL (string url)
    {
        Encoding E = System.Text.Encoding.UTF8;

        return WWW.EscapeURL(url, E).Replace("+", "%20");
    }

   

    void ShowChargeFunny()
    {
        Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~ ShowChargeFunny() ");

        GuiChargeFunny guiChargeFunny = GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, true, null);

        GameObject fitemGO;
        GuiChargeFunnyItem guiChargeFunnyItemf;

        ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

        foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
        {
            if (shopInfo.type != ShopType.Funny)
                continue;

            // 리스트 항목을 생성 추가 한다.								
            fitemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargeFunnyItem"));
            guiChargeFunnyItemf = fitemGO.GetComponent<GuiChargeFunnyItem>();
            guiChargeFunnyItemf.SerialID = shopInfo.pack_idx;
            uint powersu = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).item_1_su;
            float cost = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value;
            
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Korean:
            //        guiChargeFunnyItemf.SetLabel("퍼니 충전");
            //            guiChargeFunnyItemf.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " 젬", "x " + powersu.ToString());
            //        break;
            //    case SystemLanguage.English:
            //        guiChargeFunnyItemf.SetLabel("Funny Charge");
            //        guiChargeFunnyItemf.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x " + powersu.ToString());
            //        break;
            //    case SystemLanguage.Portuguese:
            //        guiChargeFunnyItemf.SetLabel ("Charge engraçado");
            //         guiChargeFunnyItemf.SetItemInfo (shopInfo.pack_idx, cost.ToString () + " Jóias", "x" + powersu.ToString ());
            //        break;
            //}
            guiChargeFunnyItemf.SetLabel("Carga de Funny");
            guiChargeFunnyItemf.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x" + powersu.ToString());
                   
            int pack_idx = (int)shopInfo.pack_idx;
            guiChargeFunnyItemf.BuyBtn.onClick = (GameObject pBuyBtnGOf) =>
            {
                JSONNode msgParamf = new JSONClass();
                //switch (Application.systemLanguage)
                //{
                //    case SystemLanguage.Portuguese:
                //        msgParamf["message"] = "Você gostaria de comprar?";
                //        break;
                //    case SystemLanguage.English:
                //        msgParamf["message"] = "Would you like to buy?";
                //        break;
                //    case SystemLanguage.Korean:
                //        msgParamf["message"] = "구매 하시겠습니까?";
                //        break;
                //}
                msgParamf["message"] = "Você gostaria de comprar?";
                msgParamf["show_type"] = "two";

                GuiMessageBox guiMessageBoxf = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamf);
                guiMessageBoxf.OnButtonClick = (bool pIsOkf) =>
                {
                    if (pIsOkf)
                    {
                        if (MyInfoMgr.Instance.m_Gem >= cost)
                        {
                            Debug.Log("Funny Charege");
                            //MyInfoMgr.Instance.m_Funny += (uint)powersu;				
                            _InfoSave.FunnySuPlus((int)powersu);
                            _InfoSave.GemSuMinus((int)cost);
                            _InfoSave.ItemHaveUpdate(pack_idx, 1);
                            return;
                        }

                        //Hwang Wark 퍼니상점에서 잼이 코스트보다 부족할때 잼상점 팝업 Start					
                        if (MyInfoMgr.Instance.m_Gem < cost)
                        {
                            //Debug.Log("aaaa");				

                            //guiChargeFunny.CloseBtn.onClick = null;
                            ////guiChargeFunny.FunnyReqBtn.onClick = null;		
                            //guiChargeFunny = null;

                            GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);

                            // 젬 윈도우 팝업....
                            GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
                            _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
                            if (_guiChargeWindow != null)
                                _guiChargeWindow.ShowGemWindow();
                        }
                    }
                };
            };

            guiChargeFunny.AddChargeFunnyItem(guiChargeFunnyItemf);
        }

        guiChargeFunny.CloseBtn.onClick = (GameObject pCloseGO) =>
        {
            guiChargeFunny.CloseBtn.onClick = null;

            guiChargeFunny = null;

            GuiMgr.Instance.Show<GuiChargeFunny>(GuiMgr.ELayerType.Front, false, null);
        };
    }

    /// <summary>
    /// 카카오 친구 목록 로드 실패시 호출..
    /// </summary>
    private void onLoadGameFriendsError(string status, string message)
    {
        //KakaoMain.Instance.ShowKakaoMessageBox (status, message, "Kakao Friends : Loading Error");

        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }

    public override void OnLeave()
    {
        if (null != GuiMgr.Instance)
        {
            if (null != _guiSettingMenu)
            {
                _guiSettingMenu.StoreBtn.onClick = null;
                //_guiSettingMenu.MsgBtn.onClick = null;
                _guiSettingMenu.SettingBtn.onClick = null;
                _guiSettingMenu.MykeypetBtn.onClick = null;
              //  _guiSettingMenu.ClearTopStage.onClick = null;
              //  _guiSettingMenu.ReballStage.onClick = null;
            }

            if (null != _guiStageMapInfoScrollView)
            {
                _guiStageMapInfoScrollView.ClearMapItem();
                _guiStageMapInfoScrollView.ClearMapObjectItem();
                _guiStageMapInfoScrollView.ClearNextStageItem();
                _guiStageMapInfoScrollView.ClearStageItem();
            }

            _guiPlayerInfo = null;
            _guiStageMapInfo = null;
            _guiStageMapInfoScrollView = null;
            _guiSettingMenu = null;

            GuiMgr.Instance.Show<GuiPlayerInfo>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiStageMapInfo>(GuiMgr.ELayerType.Back, false, null);
            GuiMgr.Instance.Show<GuiStageMapInfoScrollView>(GuiMgr.ELayerType.ScrollView, false, null);
            GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, false, null);
            GuiMgr.Instance.Show<GuiChargePowerItem>(GuiMgr.ELayerType.Front, false, null);
        }

        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    float BackMoveCameraPosY_offset = -1024f;

    private bool _bStartTutorial = false;

    protected IEnumerator Setting()
    {
 //       Debug.LogError ("~~~~~~~~~~~~~~~~~~~~~~~~Map Setting");

        GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, true, null);

        _guiStageMapInfoScrollView.Setting(GuiMgr.Instance.GetFrontUICamera(), GuiMgr.Instance.GetFrontCamera(), _guiStageMapInfo.GetViewTargetTopLeft(), _guiStageMapInfo.GetViewTargetBottomRight());

        float maxHeight = 0.0f;

        StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
        StageInfoMgr stageInfoMgr = GameDataMgr.Instance.GetInfo<StageInfoMgr>();

        bool isFirst = true;

        _bStartTutorial = _InfoSave.StartTutorialIntro();

        int yieldCount = 0;

        _guiStageMapInfoScrollView.CreateBaseMap();	// map scroll에 사용할 기본 맵 2개를 생성한다...

        CreateBaseStageItem();	// map scroll에 사용할 기본 stage 버튼을 생성한다...
//        Debug.LogError("~~~~~~~~~~~~~~~~~~~~~~~~~~~ CreateBaseStageItem");

        int clearTopStageNo = _InfoSave.clearStageTopNumber;
        int moveStageNo = _InfoSave.clearStageNumber;
        if(moveStageNo == 0)
        {
            moveStageNo = clearTopStageNo;
        }
        float posY_TopStage = -260f;

        foreach (StageMapInfo stageMapInfo in stageMapInfoMgr.GetStageMapInfo())
        {
            foreach (LinkedStageInfo linkedStageInfo in stageMapInfo.linked_stage_info)
            {
                StageInfo stageInfo = stageInfoMgr.GetStageInfo(linkedStageInfo.stage_si);

                if(linkedStageInfo.stage_si == moveStageNo)
                {
                    posY_TopStage = linkedStageInfo.pos_y;
                }

                if (null == stageInfo)
                {
                    continue;
                }

                if (maxHeight < linkedStageInfo.pos_y)
                {
                    maxHeight = linkedStageInfo.pos_y;
                }
                yieldCount++;
                if (yieldCount > 20)
                {
                    yieldCount = 0;
                    yield return null;
                }
            }

            if (maxHeight < stageMapInfo.nextVillPos.y)
            {
                maxHeight = stageMapInfo.nextVillPos.y;
            }

            /// moon
            /*
            GameObject nextStage = _guiStageMapInfoScrollView.AddNextStageItem(stageMapInfo.si.ToString(), stageMapInfo.nextVillPos);

            UIEventListener.Get(nextStage).onClick = (GameObject pGO) =>
            {
                StartCoroutine(NextVillageSequence());
            };
            */
            ////

            // 표지만 객체 생성..
            ///////////_guiStageMapInfoScrollView.AddMapObjectItem(stageMapInfo.si.ToString(), 001, stageMapInfo.SignPos, 388, 148, stageMapInfo.vill_name);

            if (isFirst)
            {
                isFirst = false;
            }

            yieldCount++;
            if (yieldCount > 20)
            {
                yieldCount = 0;
                yield return null;
            }
        }


        yieldCount = 0;

        int mapCount = ((int)maxHeight / 2048) + 1;

        for (int i = 0; i < mapCount; ++i)
        {

            // moon 전체 주석 처리함...
            /*
            string keyName = string.Format("Map_{0}", (i + 1).ToString("000"));
// 			_guiStageMapInfoScrollView.AddMapItem(keyName, i, i == 0 || i == 1 ? false : true);
            _guiStageMapInfoScrollView.AddMapItem(keyName, i, false);
            */

            yieldCount++;
            if (yieldCount > 20)
            {
                yieldCount = 0;
                yield return null;
            }
        }

        // 이전에 플레이 하던 스테이지로 카메라, 맵 원위치 시키기.
//        Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ clearTopStageNo:"+clearTopStageNo+", posY_TopStage:" + posY_TopStage);
        ComeBackToCurrentStageMap(posY_TopStage + BackMoveCameraPosY_offset);

        ////Hwang Wark
        //transform.position = _InfoSave.clearTopStageCameraPosition;
        ////Hwang End

        _guiSettingMenu.KeypetMessageLoadCount();

        yield return null;
        
        GuiMgr.Instance.Show<GuiFullLoading>(GuiMgr.ELayerType.Front, false, null);

        EnvLocalData localData = GameDataMgr.Instance.GetLocalData<EnvLocalData>();

        // 인트로 튜토리얼을 완료하지 않았을 경우에 인트로 튜토리얼 보여주기...
        InfoSave saveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();

        if (saveData.cutScene == true)
        {
            GameObject cutScene_Intro = Instantiate(Resources.Load("Gui/CutScene")) as GameObject;
            cutScene_Intro.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            cutScene_Intro.transform.name = "CutScene_Intro";
            cutScene_Intro.transform.localPosition = Vector3.zero;
            cutScene_Intro.transform.localRotation = Quaternion.identity;
            cutScene_Intro.transform.localScale = Vector3.one;

            while (cutScene_Intro.GetComponent<GuiCutScene>()._quitCutScene != true)	// 컷신을 끝낼 때까지 기다리기.. 
                yield return null;

            _InfoSave.cutScene = false;
        }

        if (saveData.startScnario == true)
        {
            //switch (Application.systemLanguage) 
            //{
            //    case SystemLanguage.Korean:
            //        GameObject tutorial_Intro = Instantiate(Resources.Load("Gui/Tutorial_Intro")) as GameObject;
            //        tutorial_Intro.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            //        tutorial_Intro.transform.name = "Tutorial_Intro";
            //        tutorial_Intro.transform.localPosition = Vector3.zero;
            //        tutorial_Intro.transform.localRotation = Quaternion.identity;
            //        tutorial_Intro.transform.localScale = Vector3.one;//= Vector3.one;

            //        isPlay = true;
            //        //
            //        _InfoSave.startScnario = false;
            //        break;
            //    case SystemLanguage.English:
            //        GameObject tutorial_Intro_EN = Instantiate(Resources.Load("Gui/Tutorial_Intro_EN")) as GameObject;
            //        tutorial_Intro_EN.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            //        tutorial_Intro_EN.transform.name = "Tutorial_Intro";
            //        tutorial_Intro_EN.transform.localPosition = Vector3.zero;
            //        tutorial_Intro_EN.transform.localRotation = Quaternion.identity;
            //        tutorial_Intro_EN.transform.localScale = new Vector3(640.0f, 640.0f, 0.0f);

            //        while (tutorial_Intro_EN.GetComponent<GuiTutorial>()._quitTutorial != true)	// 튜토리얼을 끝낼 때까지 기다리기.. 
            //            yield return null;
                    

            //        isPlay = true;
            //        //
            //        _InfoSave.startScnario = false;
            //        break;

            //    case SystemLanguage.Portuguese:
            //        GameObject tutorial_Intro_PO = Instantiate(Resources.Load("Gui/Tutorial_Intro_PO")) as GameObject;
            //        tutorial_Intro_PO.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            //        tutorial_Intro_PO.transform.name = "Tutorial_Intro";
            //        tutorial_Intro_PO.transform.localPosition = Vector3.zero;
            //        tutorial_Intro_PO.transform.localRotation = Quaternion.identity;
            //        tutorial_Intro_PO.transform.localScale = Vector3.one;//= Vector3.one;

            //        while (tutorial_Intro_PO.GetComponent<GuiTutorial>()._quitTutorial != true)	// 튜토리얼을 끝낼 때까지 기다리기.. 
            //            yield return null;


            //        isPlay = true;
            //        //
            //        _InfoSave.startScnario = false;
            //        break;
            //}

            GameObject tutorial_Intro_PO = Instantiate(Resources.Load("Gui/Tutorial_Intro_PO")) as GameObject;
            tutorial_Intro_PO.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
            tutorial_Intro_PO.transform.name = "Tutorial_Intro";
            tutorial_Intro_PO.transform.localPosition = Vector3.zero;
            tutorial_Intro_PO.transform.localRotation = Quaternion.identity;
            tutorial_Intro_PO.transform.localScale = Vector3.one;//= Vector3.one;

            while (tutorial_Intro_PO.GetComponent<GuiTutorial>()._quitTutorial != true)	// 튜토리얼을 끝낼 때까지 기다리기.. 
                yield return null;

            isPlay = true;
            //
            _InfoSave.startScnario = false;
        }

        // 출석체크 보상을 받지 않았다면 출석체크 화면 보여주기..
        //        //---------------------------------------------------------------- 로컬 시간 체크 방식

        string strNow = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        string lastLoginDate = _InfoSave._LoginDateTime;
        
        _InfoSave.SetLoginDateTime(strNow);// 로긴 타임 먼저 찍고.

        if (lastLoginDate.Length > 10)
        {
            string strtmp1 = lastLoginDate.Substring(0, 10);
            string strtmp2 = strNow.Substring(0, 10);

            if (strtmp1 != strtmp2)
            {
                StartCoroutine(DayCountAttendance());
            }
        }
        else
        {
            StartCoroutine(DayCountAttendance());
        }

        //        //----------------------------------------------------------------

        //--------------------- 서버 로긴 시 체크 방식
        /*
        int user_status = MyInfoMgr.Instance.m_user_status & (1 << Cmd_Handler_CS.USER_STATE_ATTENDANCE);

        if (user_status > 0)
        {
            MyInfoMgr.Instance.m_user_status &= ~(1 << Cmd_Handler_CS.USER_STATE_ATTENDANCE);
            StartCoroutine(AttendanceSequence());
        }*/
        //StartCoroutine(AttendanceSequence());

        yield return new WaitForSeconds(1.5f);
        //Hwang Wark LoadingEndSave Start
        if (_InfoSave.rePlay)
        {
            _InfoSave.RePlay();
        }
        //Hwang Wark LoadingEndSave End
    }


    /// <summary>
    /// 스테이지 플레이를 종료한 후에, 해당 스테이지가 위치한 맵 화면으로 다시 돌아오기 위해 카메라 및 맵을 다시 원위치시키는 함수...
    /// </summary>
    private void ComeBackToCurrentStageMap(float yPos)
    {
        //float fY = 2048f / 10.0f * MyInfoMgr.Instance.m_CurPlayStage;
        float minY = -217; // -260

        if (yPos < minY)
        {
            yPos = minY;
        }

        _guiStageMapInfoScrollView.MoveCamera(new Vector2(0, yPos), false, true);

        //if (InfoSave.appStart == false)
        //{
        //    _guiStageMapInfoScrollView.MoveCamera(new Vector2(0, _InfoSave.clearTopStageCameraPosition.y), true);
        //}

        //if (InfoSave.appStart == true)
        //{
        //    _guiStageMapInfoScrollView.MoveCamera(new Vector2(0, _InfoSave.clearTopStageCameraPosition.y), true);
        //    InfoSave.appStart = false;
        //}

        /*
                // 플레이 한 스테이지로 카메라를 이동시킴...데스트용...
                // 사전에 구름, 자물쇠 등을 해제해야 작동함... 
                float mapHeight = 2048.0f;
                int totalStageCount = 20;
                uint stageIndex = MyInfoMgr.Instance.m_CurPlayStage - 1;

                int stageMapInfo_Index = (int)stageIndex / totalStageCount;

                stageIndex++;
                stageMapInfo_Index++;


                StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
                StageInfoMgr stageInfoMgr = GameDataMgr.Instance.GetInfo<StageInfoMgr>();

                StageMapInfo stageMapInfo = stageMapInfoMgr.GetStageMapInfo ((uint)stageMapInfo_Index); // 2개의 맵과 20개의 스테이지 정보를 가지고 있는 클래스...

                //LinkedStageInfo linkedStageInfo = stageMapInfo.linked_stage_info.Find( t => t.stage_si == stageIndex );	// 현재 스테이지 인덱스에 해당하는 스테이지를 찾고...

                foreach (LinkedStageInfo linkedStageInfo in stageMapInfo.linked_stage_info)
                {
                    if (linkedStageInfo.stage_si == stageIndex)
                    {
                        float yPos = linkedStageInfo.pos_y - mapHeight*0.5f;
                        _guiStageMapInfoScrollView.MoveCamera (new Vector2(0, yPos), true);
                    }
                }
        */



        /*
        //Hwang Wark
        float fY = 2048.0f / 10.0f * MyInfoMgr.Instance.m_CurPlayStage;
        float minY = 761.6005f;
				
        if (fY < minY)
        {
            fY = minY;
        }
		
        if(InfoSave.appStart == false){
            _guiStageMapInfoScrollView.MoveCamera(new Vector2(512, fY));
        }
		
        if(InfoSave.appStart == true){
            _guiStageMapInfoScrollView.MoveCamera(new Vector2(512, _InfoSave.clearTopStageCameraPosition.y));
            InfoSave.appStart =false;
        }
        */
    }

    protected IEnumerator DayCountAttendance()
    {
        yield return new WaitForSeconds(0.5f);

        //Debug.Log("<------------------------->");
        //Debug.Log("DayCountAttendance");
        //Debug.Log("<------------------------->");

        _InfoSave._GetDayFreeHeart = 3;

        _InfoSave.SetGetDayFreeHeart(_InfoSave._GetDayFreeHeart);
    }

    protected IEnumerator AttendanceSequence()
    {
        yield return new WaitForSeconds(0.5f);

        GuiAttendance guiAttendance = GuiMgr.Instance.Show<GuiAttendance>(GuiMgr.ELayerType.Front, true, null);

        #region dummy attendance item setting

        int AttendanceDay = _InfoSave.getDay;

        Debug.Log("<------------------------->");
        Debug.Log("AttendanceDay :" + AttendanceDay);
        Debug.Log("<------------------------->");

        for (int nowDay = 0; nowDay < AttendanceDay - 1; nowDay++)
        {

            bool markOn = false;

            if (nowDay < AttendanceDay)
            {
                markOn = true;
            }
            try
            {
                guiAttendance.SetDayClear(nowDay + 1, markOn, false);
            }
            catch
            {
            }
        }

        #endregion dummy attendance item setting

        //        guiAttendance.SetDayClear(AttendanceDay, true, true);

        bool isOk = false;

        guiAttendance.OkBtn.onClick = (GameObject pGo) =>
        {
            isOk = true;
        };

        while (!isOk)
        {
            yield return null;
        }

        guiAttendance.OkBtn.onClick = null;

        GuiMgr.Instance.Show<GuiAttendance>(GuiMgr.ELayerType.Front, false, null);
    }

    private bool isPlay = false;

    protected IEnumerator StageInfoSequence(uint pStageNum, uint pStageInfoSI, uint ClearTopstage, int RStage)
    {
        ///// 마을의 첫 스테이지일 경우 normal 시나리오 출력하기 ..

        // pStageInfoSI 변수는 1부터 시작한다...
        if (pStageInfoSI % 20 == 1)	// 마을에서 첫 스테이지라면(1번, 21번, 41번... 스테이지)..
        {
            // 게임 시작 후 처음 시나리오가 출력되면 "GuiStageRanking"의 좌표가 변함으로 이를 방지하기 위해 라래 Show 함수를 호출.. 
            // 그러나, "GuiStageRanking" 프리팹의 "AnchorBottom" 아래에 "Run Only Once" 옵션을 체크 해제하면 UIAnchor가 업데이트 되면서 해당 프리팹의 좌표를 다시 갱신시켜주기 때문에 문제가 해결된다..
            // 이럴때는 아래 Show 함수를 제거하자.. 
            //GuiMgr.Instance.Show<GuiStageInfo>(GuiMgr.ELayerType.Front, true, null);
            //GuiMgr.Instance.Show<GuiStageRanking>(GuiMgr.ELayerType.Front, true, null);

            StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();

            uint stageMapInfoIndex = (uint)(pStageInfoSI - 1) / 20;			// 현재 스테이지의 인덱스인 "pStageInfoSI-1"을 한 마을의 스테이지 수인 20으로 나누면, 몇 번째 마을에 있는  스테이지인지 알 수 있다..
            StageMapInfo stageMapInfo = stageMapInfoMgr.GetStageMapInfo(stageMapInfoIndex + 1); // 인덱스를 번호로 전환하기 위해 +1...
            int scenarioNumber = (int)stageMapInfo.enter_dir_si;	// 현재 마을에서 진행해야 하는 노멀 스나리오 번호..


            ScenarioGroup senarioGroup = GameDataMgr.Instance.GetInfo<ScenarioInfoMgr>().GetScenarioGroup(scenarioNumber - 1);	// 번호를 인덱스르로 전환하기 위해 -1..

            if(senarioGroup != null)
            { 
                GameObject senarioGO = Instantiate(Resources.Load("Gui/GUIScenario")) as GameObject;
                senarioGO.transform.name = "GUIScenario";
                senarioGO.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
                senarioGO.transform.localPosition = Vector3.zero;
                senarioGO.transform.localRotation = Quaternion.identity;
                senarioGO.transform.localScale = Vector3.one;


                senarioGO.GetComponent<GuiScenario>().SetSenarioGroup(senarioGroup);	// 시나리오 정보를 세팅하고..

                while (senarioGO.GetComponent<GuiScenario>()._quitScenario != true)	// 시나리오를 끝낼 때까지 기다리기.. 
                {
                    yield return null;
                }

                Destroy(senarioGO);
            }
        }


        ////// 이하 스테이지 정보 다이얼로그 박스 출력....

        _guiSettingMenu = null;
        GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, false, null);


        int maxItemCount = 3;
        int curSelItemCount = 0;

        StageInfo stageInfo = GameDataMgr.Instance.GetInfo<StageInfoMgr>().GetStageInfo(pStageInfoSI);

        if (_InfoSave.StartTutorialIntro())
        {
            JSONNode param = new JSONClass();
            param["stage_si"] = pStageInfoSI.ToString();

            GameStateMgr.Instance.ChangeState<GSPlay>(0, param);
        }
        else
        {
            #region 리볼 맵 오픈

            if (ClearTopstage == pStageInfoSI && pStageInfoSI > 40 
                && pStageInfoSI % 4 == 0 && _InfoSave.ReballClearStage() == false)
            {
                GuiBonusInfo guiBonusInfo = GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, true, null);

                //상태 환경 변화 셋팅
                guiBonusInfo.StatusAlarm(GuiBonusInfo.BonusStauts.Reball);

                //게임 시작 스테이지 몇인지 판단할 수 있게 해야한다.
                guiBonusInfo.OnButtonStClick = (bool gi) =>
                {
                    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

                    GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, false, null);
                    isPlay = true;

                    KakaoMain.Instance.isReballPlay = isPlay;
                };

                guiBonusInfo.OnButtonBAitClick = (bool gi) =>
                {
                    GuiMgr.Instance.Show<GuiBonusInfo>(GuiMgr.ELayerType.Front, false, null);

                    GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, true, null);
                };

                while (!isPlay)
                {
                    yield return null;
                }

                guiBonusInfo.OnButtonStClick = null;
                guiBonusInfo.OnButtonBAitClick = null;

                JSONNode param = new JSONClass();
                param["stage_si"] = pStageInfoSI.ToString();

                GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

                GameStateMgr.Instance.ChangeState<GSPlay>(0, param);

            #endregion
            }
            else
            {
                KakaoMain.Instance.CheckUpdateHeart(_guiPlayerInfo);

                GuiStageInfo guiStageInfo = GuiMgr.Instance.Show<GuiStageInfo>(GuiMgr.ELayerType.Front, true, null);
                //_bFriendInfoStageLoad = true;

                #region Real item setting
                {
                    // 선택 아이템 초기화
                    MyInfoMgr.Instance.m_UseItems.Clear();

                    GameObject itemGO;
                    GuiItem guiItem;
                    ItemInfo itemInfo;
                    ItemInfoMgr itemInfoMgr = GameDataMgr.Instance.GetInfo<ItemInfoMgr>();

                    //Debug.Log(stageInfo.fever_time);

                    // MyInfo 의 보유 아이템 수 만큼 Add
                    if (stageInfo.game_mode == GameMode.TimeAndCount && stageInfo.fever_time > 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsAll)
                        {

                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);

                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);
                            //

                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];
                            guiItem.SetItemCount(guiItem.item_su);

                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };

                            //                    Debug.Log("~~~~~~~~~~~~~ guiItem.item_su:" + guiItem.item_su + " guiItem.item_index:" + guiItem.item_index);

                            guiStageInfo.AddItem(guiItem);
                        }
                    }

                    if (stageInfo.game_mode == GameMode.Time && stageInfo.fever_time > 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsTime)
                        {
                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);


                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);

                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];

                            guiItem.SetItemCount(guiItem.item_su);
                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };
                            //                    Debug.Log("~~~~~~~~~~~~~ guiItem.item_su:" + guiItem.item_su + " guiItem.item_index:" + guiItem.item_index);

                            guiStageInfo.AddItem(guiItem);
                        }
                    }

                    if (stageInfo.game_mode == GameMode.Count && stageInfo.fever_time > 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsCount)
                        {
                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);

                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);
                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];
                            guiItem.SetItemCount(guiItem.item_su);

                            //guiItem.GetComponent<GuiItemCount>()._lbCount.text = guiItem.item_su.ToString();

                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };
                            //                    Debug.Log("~~~~~~~~~~~~~ guiItem.item_su:" + guiItem.item_su + " guiItem.item_index:" + guiItem.item_index);

                            guiStageInfo.AddItem(guiItem);
                        }
                    }
                    if (stageInfo.game_mode == GameMode.TimeAndCount && stageInfo.fever_time == 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsAllNotFever)
                        {

                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);


                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);

                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];

                            guiItem.SetItemCount(guiItem.item_su);
                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };
                            //                    Debug.Log("~~~~~~~~~~~~~ guiItem.item_su:" + guiItem.item_su + " guiItem.item_index:" + guiItem.item_index);

                            guiStageInfo.AddItem(guiItem);
                        }
                    }

                    if (stageInfo.game_mode == GameMode.Time && stageInfo.fever_time == 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsTimeNotFever)
                        {
                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);

                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);

                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;

                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];

                            guiItem.SetItemCount(guiItem.item_su);
                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };
                            //                    Debug.Log("~~~~~~~~~~~~~ guiItem.item_su:" + guiItem.item_su + " guiItem.item_index:" + guiItem.item_index);


                            guiStageInfo.AddItem(guiItem);
                        }
                    }

                    if (stageInfo.game_mode == GameMode.Count && stageInfo.fever_time == 0)
                    {
                        foreach (KeyValuePair<uint, uint> kvp in MyInfoMgr.Instance.m_dicItemsCountNotFever)
                        {

                            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItem"));
                            guiItem = itemGO.GetComponent<GuiItem>();

                            guiItem.item_index = kvp.Key;
                            guiItem.item_su = kvp.Value;
                            itemInfo = itemInfoMgr.GetItemInfo((uint)guiItem.item_index);


                            guiItem.SetItemImage((Texture2D)Resources.Load("Images/Item/" + itemInfo.icon_image));
                            guiItem.SetItemName(itemInfo.name);
                            guiItem.SetItemDesc(itemInfo.desc);

                            GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().GetIndexShopToItem(guiItem.item_index);
                            uint getItemIndex = GameObject.Find("InfoMgr").GetComponent<IndexShopToItem>().returnItemIndex;
                            guiItem.item_su = (uint)_InfoSave.useitem[(int)getItemIndex];

                            guiItem.SetItemCount(guiItem.item_su);
                            guiItem.BuyBtn.onClick = (GameObject pGO) =>
                            {
                                guiStageInfo.ShowShop();
                            };
                            guiStageInfo.AddItem(guiItem);
                        }
                    }
                }

                #endregion item setting

                guiStageInfo.SetStageNum(pStageNum);

                guiStageInfo.SetStageGoal(stageInfo.mode_desc);

                guiStageInfo.OnItemClick = (GuiItem pGuiItem) =>
                {
                    int itemSelectCount = (int)pGuiItem.GetSelectCount();
                    int itemSu = (int)pGuiItem.GetSu();
                   
                    if ((curSelItemCount + 1 > maxItemCount) || (itemSelectCount + 1 > itemSu))
                    {
                        curSelItemCount -= itemSelectCount;
                        pGuiItem.ResetCount();
                        for (int i = MyInfoMgr.Instance.m_UseItems.Count - 1; i >= 0; i--)
                        {
                            if (pGuiItem.item_index == MyInfoMgr.Instance.m_UseItems[i])
                                MyInfoMgr.Instance.m_UseItems.RemoveAt(i);
                        }

                    }
                    else
                    {
                        curSelItemCount++;
                        pGuiItem.AddCount();

                        MyInfoMgr.Instance.m_UseItems.Add(pGuiItem.item_index);
                    }
                };

                //RankingInfo();

                isPlay = false;

                //Play Start Button 
                guiStageInfo.PlayBtn.onClick = (GameObject pGo) =>
                {
                    //LeaderBoard Attach ! - 140903

                    if (MyInfoMgr.Instance.m_Power > 0)
                    {
                        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

                        Vector3 srcPos = _guiPlayerInfo.PowerBtn.transform.position;
                        Vector3 destPos = _guiPlayerInfo.PowerBtn.transform.position;
                        //Vector3 destPos = guiStageInfo.PowerIcon.position;	// 일단 사용하지 않는 것으로. 주석처리...

                        guiStageInfo.PlayBtn.onClick = null;
                        guiStageInfo.ShowPowerDir(srcPos, destPos, () =>
                        {
#if UNITY_EDITOR
                            isPlay = true;

                            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

                            KakaoMain.Instance.UseHeart();

                            //_InfoSave.PowerSuMinus();
#elif UNITY_ANDROID

                    if(pStageNum > 10)
                    {
                        isPlay = true;

                        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
                        
                        KakaoMain.Instance.UseHeart();

                       // MyInfoMgr.Instance.m_Power--;
                        //KakaoNativeExtension.Instance.useHeart(1, this.onUseHeartComplete, this.onUseHeartError);   
                    }
                    else
                    {
                        isPlay = true;

                        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
                        //KakaoMain.Instance.CheckUpdateHeart();
                    }
#endif
                        }

                        );
                        return;
                    }

                    //Hwang Wark 파워가 0이면 파워구매 팝업 Start
                    if (MyInfoMgr.Instance.m_Power == 0)
                    {
                        GuiPowerPopupBox PowerPopUp = GuiMgr.Instance.Show<GuiPowerPopupBox>(GuiMgr.ELayerType.Front, true, null);

                        /*
                        try
                        {
                            PowerPopUp.GetComponent<GuiPowerPopupBox>().SettingMessage(_InfoSave._GetDayFreeHeart);
                        }
                        catch (Exception e)
                        {
                            Debug.Log("e :" + e.ToString());
                        }
                        PowerPopUp.LoadAD.onClick = (GameObject pGO) =>
                        {
                            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
#if UNITY_EDITOR
                            if (_InfoSave._GetDayFreeHeart > 0)
                            {
                                isPlay = true;

                                _InfoSave._GetDayFreeHeart--;
                                 
                                if (_InfoSave._GetDayFreeHeart <= 0)
                                {
                                    _InfoSave._GetDayFreeHeart = 0;
                                }

                                _InfoSave.SetGetDayFreeHeart(_InfoSave._GetDayFreeHeart);

                                Debug.Log("Load AD : " + _InfoSave._GetDayFreeHeart);
                            }
                            else
                            {
                                PowerChargePopup(guiStageInfo);
                            }
                            GuiMgr.Instance.Show<GuiPowerPopupBox>(GuiMgr.ELayerType.Front, false, null);
#elif UNITY_ANDROID
                        if (_InfoSave._GetDayFreeHeart > 0)
                        {
                            _InfoSave.isGameStage = true;

                            AdlibPlugin.LoadInterstitialAd();
                        
                            StartCoroutine(IE_PowerADPlay());
                        }
                        else 
                        {
                            PowerChargePopup(guiStageInfo);
                        }
                        
                        GuiMgr.Instance.Show<GuiPowerPopupBox>(GuiMgr.ELayerType.Front, false, null);
#endif

                            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
                        };
                        */
                        PowerPopUp.PowerCharge.onClick = (GameObject pGO) =>
                        {
                            GuiMgr.Instance.Show<GuiPowerPopupBox>(GuiMgr.ELayerType.Front, false, null);

                            PowerChargePopup(guiStageInfo);
                        };

                        PowerPopUp.Exit.onClick = (GameObject pGO) =>
                        {
                            GuiMgr.Instance.Show<GuiPowerPopupBox>(GuiMgr.ELayerType.Front, false, null);
                        };
                    }
                    //Hwang End 파워가 0이면 파워구매 팝업 End
                };

                guiStageInfo.CloseBtn.onClick = (GameObject pGo) =>
                {
                    //Hwang Wark
                    guiStageInfo.transform.Find("AnchorCenter/BG/ItemDesc").GetComponent<UILabel>().text = "";
                    guiStageInfo.transform.Find("AnchorCenter/BG/ItemName").GetComponent<UILabel>().text = "";
                    //Debug.Log(guiStageInfo.name);
                    //Hwang End
                    guiStageInfo.PlayBtn.onClick = null;
                    guiStageInfo.CloseBtn.onClick = null;

                    _bFriendInfoStageLoad = false;

                    GuiMgr.Instance.Show<GuiStageInfo>(GuiMgr.ELayerType.Front, false, null);

                    _guiSettingMenu = GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, true, null);

                    _guiSettingMenu.StoreBtn.onClick = (GameObject pGO) =>
                    {
                        GuiItemShop guiItemShop = GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, true, null);
                        guiItemShop.ClearItemShopItem();
                        // shop table 에서 Item type 들만 추출
                        ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
                        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

                        GuiItemShopItem item;
                        //Hwang Wark
                        GuiItemShopItem guiItemShopItem;
                        //Hwang End
                        foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
                        {
                            if (shopInfo.type != ShopType.Item)
                                continue;

                            // 리스트 항목을 생성 추가 한다.
                            item = ((GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiItemShopItem"))).GetComponent<GuiItemShopItem>();

                            PackageInfo pkginfo = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx);

                            float _funnysu = pkginfo.price_value;
                            PriceType valuetype = pkginfo.price_type;

                            item.SerialID = shopInfo.pack_idx;

                            //switch(Application.systemLanguage)
                            //{ 
                            //    case SystemLanguage.Korean:
                            //        { 
                            //            if (valuetype == PriceType.Funny)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\n퍼니",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else if (valuetype == PriceType.Gem)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " 젬",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //        }
                            //        break;
                            //    case SystemLanguage.English:
                            //        {
                            //            if (valuetype == PriceType.Funny)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else if (valuetype == PriceType.Gem)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //        }
                            //        break;
                            //    case SystemLanguage.Portuguese:
                            //        {
                            //            if (valuetype == PriceType.Funny)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else if (valuetype == PriceType.Gem)
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //            else
                            //            {
                            //                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                            //                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                            //                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            //            }
                            //        }
                            //        break;
                            //}

                            if (valuetype == PriceType.Funny)
                            {
                                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + "\nFunny",
                                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            }
                            else if (valuetype == PriceType.Gem)
                            {
                                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString() + " Jóias",
                                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            }
                            else
                            {
                                item.SetItemInfo(shopInfo.pack_idx, packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).name,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).desc,
                                                 packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value.ToString(),
                                                 "Images/Item/" + packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).icon_image);
                            }
                            item.transform.Find("BG/BuyBtn").GetComponent<ShopItemIndexNumber>().thisIndex = (int)shopInfo.pack_idx;
                            guiItemShop.AddItemShopItem(item);
                            //Hwang Wark
                            //Debug.Log(shopInfo.pack_idx);
                            guiItemShopItem = item;

                            guiItemShopItem.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
                            {
                                //Hwang Wark
                                int isIndex = pBuyBtnGO.GetComponent<ShopItemIndexNumber>().thisIndex;
                                //Hwang End
                                if (MyInfoMgr.Instance.m_Funny >= _funnysu)
                                {
                                    JSONNode msgParam = new JSONClass();
                                    //switch (Application.systemLanguage)
                                    //{
                                    //    case SystemLanguage.Portuguese:
                                    //        msgParam["message"] = "Você gostaria de comprar?";
                                    //        break;
                                    //    case SystemLanguage.English:
                                    //        msgParam["message"] = "Would you like to buy?";
                                    //        break;
                                    //    case SystemLanguage.Korean:
                                    //        msgParam["message"] = "구매 하시겠습니까?";
                                    //        break;
                                    //}

                                    msgParam["message"] = "Você gostaria de comprar?";
                                    msgParam["show_type"] = "two";

                                    GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                                    guiMessageBox.OnButtonClick = (bool pIsOk) =>
                                    {
                                        if (pIsOk)
                                        {
                                            _InfoSave.FunnySuMinus((int)_funnysu);
                                            _InfoSave.ItemHaveUpdate((int)isIndex, 1);
                                            _InfoSave.RouletteKeypetCountCheck();
                                            _guiSettingMenu.OnEnter();
                                        }
                                    };
                                }
                                //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 Start
                                if (MyInfoMgr.Instance.m_Funny < _funnysu)
                                {
                                    JSONNode msgParamfs = new JSONClass();
                                    //switch (Application.systemLanguage)
                                    //{
                                    //    case SystemLanguage.Portuguese:
                                    //        msgParamfs["message"] = "Ir para Engraçado Shop";
                                    //        break;
                                    //    case SystemLanguage.English:
                                    //        msgParamfs["message"] = "Go to Funny Shop";
                                    //        break;
                                    //    case SystemLanguage.Korean:
                                    //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                                    //        break;
                                    //}

                                    msgParamfs["message"] = "Ir para Engraçado Shop";
                                    msgParamfs["show_type"] = "two";

                                    GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                                    guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                                    {
                                        if (pIsOkfs)
                                        {
                                            guiItemShop = null;
                                            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                                            ShowChargeFunny();
                                        }
                                    };
                                }
                                //Hwang Wark 상점에서 퍼니가 부족하면 퍼니상점 팝업 End

                            };
                            //Hwang End
                        }

                        guiItemShop.BackBtn.onClick = (GameObject) =>
                        {
                            guiItemShop = null;
                            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);
                        };

                        guiItemShop.GachaBtn.onClick = (GameObject) =>
                        {
                            //Debug.Log("aaa");
                            if (MyInfoMgr.Instance.m_Funny >= MyInfoMgr._gachaFunnySu)
                            {
                                //AudioMgr.Instance.Play("Gacha");
                                //_InfoSave.FunnySuMinus((uint)MyInfoMgr._gachaFunnySu);
                                guiItemShop.ShowGachaDlg(true);
                            }

                            if (MyInfoMgr.Instance.m_Funny < MyInfoMgr._gachaFunnySu)
                            {

                                JSONNode msgParamfs = new JSONClass();
                                //switch (Application.systemLanguage)
                                //{
                                //    case SystemLanguage.Portuguese:
                                //        msgParamfs["message"] = "Ir para Engraçado Shop";
                                //        break;
                                //    case SystemLanguage.English:
                                //        msgParamfs["message"] = "Go to Funny Shop";
                                //        break;
                                //    case SystemLanguage.Korean:
                                //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                                //        break;
                                //}
                                msgParamfs["message"] = "Ir para Engraçado Shop";
                                msgParamfs["show_type"] = "two";

                                GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                                guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                                {
                                    if (pIsOkfs)
                                    {
                                        guiItemShop = null;
                                        GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                                        ShowChargeFunny();
                                    }
                                };
                            }
                        };

                        guiItemShop.CloseBtn.onClick = (GameObject) =>
                        {
                            AudioMgr.Instance.Stop("Gacha");

                            guiItemShop.ShowGachaDlg(false);
                        };

                        guiItemShop.TakingItemBtn.onClick = (GameObject) =>
                        {
                            AudioMgr.Instance.Stop("Gacha");

                            if (MyInfoMgr.Instance.m_Funny >= 5000)
                            {
                                //5000원 만큼 가격 깍기
                                _InfoSave.FunnySuMinus((int)5000);

                                string packageName = "";
                                string strDesc = "";
                                string rewardIcon = "";

                                uint pay = 0;
                                GetGachaResult(packageInfoMgr, out packageName, out pay);	// 랜덤하게 가차뽑기..				

                                PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)pay);
                                if (packageInfo != null)
                                {
                                    packageName = packageInfo.name;
                                    strDesc = packageInfo.desc;
                                    rewardIcon = packageInfo.icon_image;

                                    _InfoSave.InGameItemHaveUpdate((int)pay, (int)packageInfo.item_1_su);
                                    
                                    //_rouletResult.stopAngle = resultPack_idx * 36.0f;
                                    //strDesc = packageInfo.desc;
                                    Debug.Log("~~~~~~~~~~~~~ Package Index : " + pay.ToString() + " packageInfo.desc:" + packageInfo.desc + " icon:" + packageInfo.icon_image);
                                }
                                guiItemShop.ShowResult(true, packageName, strDesc, rewardIcon);
                            }
                            else
                            {
                                JSONNode msgParamfs = new JSONClass();
                                //switch (Application.systemLanguage)
                                //{
                                //    case SystemLanguage.Portuguese:
                                //        msgParamfs["message"] = "Ir para Engraçado Shop";
                                //        break;
                                //    case SystemLanguage.English:
                                //        msgParamfs["message"] = "Go to Funny Shop";
                                //        break;
                                //    case SystemLanguage.Korean:
                                //        msgParamfs["message"] = "퍼니 구매창으로 이동";
                                //        break;
                                //}
                                msgParamfs["message"] = "Ir para Engraçado Shop";
                                msgParamfs["show_type"] = "two";

                                GuiMessageBox guiMessageBoxfs = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParamfs);

                                guiMessageBoxfs.OnButtonClick = (bool pIsOkfs) =>
                                {
                                    if (pIsOkfs)
                                    {
                                        guiItemShop = null;
                                        GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);

                                        ShowChargeFunny();
                                    }
                                };
                            }
                        };

                        guiItemShop.OneMoreBtn.onClick = (GameObject) =>
                        {
                            //AudioMgr.Instance.Play("Gacha");
                            //_InfoSave.FunnySuMinus((uint)MyInfoMgr._gachaFunnySu);
                            guiItemShop.ShowResult(false, "", "", "");

                            guiItemShop.GachaBtn.onClick(GameObject);
                        };

                        guiItemShop.ResultBackBtn.onClick = (GameObject) =>
                        {
                            guiItemShop.ShowGachaDlg(false);
                            guiItemShop.ShowResult(false, "", "", "");
                        };
                    };

                    _guiSettingMenu.SettingBtn.onClick = (GameObject pGO) =>
                    {
                        GuiGameSetting guiGameSetting = GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, true, null);

                        guiGameSetting.OkBtn.onClick = (GameObject) =>
                        {
                            guiGameSetting = null;
                            GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);
                        };

                        guiGameSetting.TutorialBtn.onClick = (GameObject) =>
                        {
                            guiGameSetting = null;
                            GuiMgr.Instance.Show<GuiGameSetting>(GuiMgr.ELayerType.Front, false, null);

                            isPlay = true;

                            KakaoMain.Instance._bTutorailStart = true;

                            //_InfoSave.startTutorialIntro = true;
                            //{
                            //    isPlay = true;

                            //    KakaoMain.Instance._bTutorailStart = true;
                            //    /*
                            //    // 인트로 튜토리얼을 완료하지 않았을 경우에 인트로 튜토리얼 보여주기...
                            //    InfoSave saveData = GameObject.Find("InfoMgr").GetComponent<InfoSave>();
                            //    if (saveData.startTutorialIntro == true)
                            //    {
                            //        GameObject tutorial_Intro = Instantiate(Resources.Load("Gui/Tutorial_Intro")) as GameObject;
                            //        tutorial_Intro.transform.parent = GuiMgr.Instance.GetFrontCamera().transform;
                            //        tutorial_Intro.transform.name = "Tutorial_Intro";
                            //        tutorial_Intro.transform.localPosition = Vector3.zero;
                            //        tutorial_Intro.transform.localRotation = Quaternion.identity;
                            //        tutorial_Intro.transform.localScale = Vector3.one;

                            //        //while (tutorial_Intro.GetComponent<GuiTutorial>()._quitTutorial != true)	// 튜토리얼을 끝낼 때까지 기다리기.. 
                            //        //	yield return null;

                            //        _InfoSave.startTutorialIntro = false;
                            //    }*/
                            
                            //}
                        };
                    };

                    _guiSettingMenu.MykeypetBtn.onClick = (GameObject pGO) =>
                    {
                        GameStateMgr.Instance.ChangeState<GSKeypetCollection>(0,  null);
                    };

                    _guiSettingMenu.MykeypetBtn.onClick = (GameObject pGO) =>
                    {
                        GameStateMgr.Instance.ChangeState<GSKeypetCollection>(0, null);
                    };
                };

                while (!isPlay)
                {
                    yield return null;
                }

                guiStageInfo.PlayBtn.onClick = null;
                guiStageInfo.CloseBtn.onClick = null;

                GuiMgr.Instance.Show<GuiStageInfo>(GuiMgr.ELayerType.Front, false, null);

                JSONNode param = new JSONClass();
                param["stage_si"] = pStageInfoSI.ToString();

                GameStateMgr.Instance.ChangeState<GSPlay>(0, param);
            }
        }
    }

    protected IEnumerator NextVillageSequence()
    {
        KakaoMain.Instance.SetcheckTime(false);

        StartCoroutine(KakaoMain.Instance.IE_RegenNextViliage());

        GuiNextVillagePopup guiNextVillagePopup = GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, true, null);

        guiNextVillagePopup.ShowNextViliageBtn();

        guiNextVillagePopup.ShowBaseDlg();

        bool isRun = true;

        guiNextVillagePopup.GetCloseBtn.onClick = (GameObject pGO) =>
        {
            isRun = false;

            GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);
        };
        
        guiNextVillagePopup.GetRequireGemBtn.onClick = (GameObject pGO) =>
        {
            if (MyInfoMgr.Instance.m_Gem >= MyInfoMgr._UnlockStageGemSu)
            {
                // _InfoSave.GemSuMinus(10);
                GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

                KakaoMain.Instance.SetcheckTime(true);
                _InfoSave.NextViliageTime = 0;
                _InfoSave.gemLockClear = true;
                _InfoSave.ClearStageNumberSave();
                _InfoSave.GemSuMinus(MyInfoMgr._UnlockStageGemSu);

                ES3.DeleteKey("NextViliageTime");

                GameObject.Find("Gui/GuiStageMapInfoScrollView/Items/Maps/Map_001/1/StageBtn").GetComponent<ShopItemIndexNumber>().LockImage();
                isRun = false;

                GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);

                StartCoroutine(IE_ENDProcess(false));
            }
            else if (MyInfoMgr.Instance.m_Gem < MyInfoMgr._UnlockStageGemSu)
            {
                guiNextVillagePopup.ShowGemDlg();
            }
        };

        guiNextVillagePopup.GetGemBackBtn.onClick = (GameObject pGO) =>
        {
            guiNextVillagePopup.ShowBaseDlg();
        };

        guiNextVillagePopup.GetGemOkBtn.onClick = (GameObject pGO) =>
        {
            // 젬 윈도우 팝업....
            GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
            _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
            if (_guiChargeWindow != null)
                _guiChargeWindow.ShowGemWindow();
        };

        guiNextVillagePopup.GetFreeOkBtn.onClick = (GameObject pGo) =>
        {
            _InfoSave.gemLockClear = true;
            _InfoSave.ClearStageLockViliage();

            GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);

            GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
        };

        if (_InfoSave.clearStageTopNumber <= GameStateMgr._LastStage_LockPoint)
        {
            //무료 마을 이동까지 남은 시간 
            while (true)
            {
                if (KakaoMain.Instance.CheckTime <= 0)
                {
                    guiNextVillagePopup.ShowFreeVililage();

                    guiNextVillagePopup.EnoughTimeNextViliage("");
                    break;
                }
                else
                {
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:

                    //        guiNextVillagePopup.EnoughTimeNextViliage(string.Format("tempo restante até se mudar para a aldeia mais próxima {0: 0 #}: {1: 0 #}: {2: 0 #}", (KakaoMain.Instance.CheckTime / 3600) % 24, (KakaoMain.Instance.CheckTime / 60) % 60, KakaoMain.Instance.CheckTime % 60));
                    //        break;
                    //    case SystemLanguage.English: 
                    //        guiNextVillagePopup.EnoughTimeNextViliage(string.Format("Remaining time until moving to the next village {0:0#}:{1:0#}:{2:0#}", (KakaoMain.Instance.CheckTime / 3600) % 24, (KakaoMain.Instance.CheckTime / 60) % 60, KakaoMain.Instance.CheckTime % 60));
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        guiNextVillagePopup.EnoughTimeNextViliage(string.Format("다음 마을 이동까지 남은 시간 {0:0#}:{1:0#}:{2:0#}", (KakaoMain.Instance.CheckTime / 3600) % 24, (KakaoMain.Instance.CheckTime / 60) % 60, KakaoMain.Instance.CheckTime % 60));

                    //        break;
                    //}
                    guiNextVillagePopup.EnoughTimeNextViliage($"다음 마을 이동까지 남은 시간 {(KakaoMain.Instance.CheckTime / 3600) % 24: 0 #}: {(KakaoMain.Instance.CheckTime / 60) % 60: 0 #}: {KakaoMain.Instance.CheckTime % 60: 0 #}");
                }
                yield return null;
            }
        }

        while (isRun)
        {
            yield return null;
        }

        GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);

    }

    private int[] temp;

    // "가차뽑기"시 랜덤하게 패키지를 뽑는 함수...
    protected void GetGachaResult(PackageInfoMgr packageInfoMgr,out string packageName, out uint pay)
    {
        RandomboxInfoMgr randomboxInfoMgr = GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>();

        foreach (RandomboxInfo randomboxInfo in randomboxInfoMgr.GetRandomboxInfo())
        {
            if (randomboxInfo.randombox_type != RandomboxType.ShopGacha) // 샵에서 가차를 돌릴 경우만 이하 실행..
                continue;

            // 확률을 담는 배열 만들기..
            temp = new int[1000];
            int inc = 0;

            for (int i = 0; i < 10; i++)
            {
                int dropPercent = (int)randomboxInfo.packagesetInfo[i].drop_percent; // 패키지당 드랍 확률...
                
                for (int j = 0; j < dropPercent; j++)
                {
                    temp[inc] = i;	// 패키지 자신의 인덱스값 i를 담는다..
                    inc++;
                }
            }

            int randNum = UnityEngine.Random.Range(0, 1000);
            int index = temp[randNum];

            // 가차에서 뽑힌 패키지 데이터 정보 뽑기..
            uint package_SI = randomboxInfo.packagesetInfo[index].package_si;
            //Hwang Wark
            //Debug.Log(package_SI);
            //_InfoSave.RouletteKeypetCountCheck();
            //_guiSettingMenu.OnEnter();
            //Hwang End
            packageName = packageInfoMgr.GetPackageInfo(package_SI).name;
            pay = package_SI;
            return;
        }
        packageName = "GachaPackageName Error";
        pay = 0;

        return;
    }

    // "가차뽑기"시 랜덤하게 패키지를 뽑는 함수...
    IEnumerator IE_GetGachaResult(GuiItemShop guiItemShop) // PackageInfoMgr packageInfoMgr, out string packageName, out uint pay)
    {
        string packageName = "";
        string strDesc = "", rewardIcon = "";
        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();
        cmd._sOutputString = "";
        int resultPack_idx = 0;
        int resultPack_qty = 0;

        guiItemShop.ShowGachaDlg(false);
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);
        yield return StartCoroutine(KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().Send_PROTOCOL_GACHA_REQ());
        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
        if (cmd._sOutputString == "")
        {
            Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
            //            cmd.PlayErrorMessage();
        }
        else
        {
            Debug.Log(cmd._sOutputString);
            JSONNode root = JSON.Parse(cmd._sOutputString);
            var resultCode = int.Parse(root["rc"]);

            if (resultCode == 0)
            {
                resultPack_idx = int.Parse(root["pack_idx"]);
                resultPack_qty = int.Parse(root["pack_qty"]);
                Debug.Log("~~~~~~~~~~~~~~~~~ resultPack_idx:" + resultPack_idx);

                // resultPack_idx = 11; // test 25
            }
        }
        if (resultPack_idx == 0)
        {
            // message box 필요.
            Debug.Log("~~~~~~~~~~~~~~~~~ Network Error!");
            GuiMgr.Instance.Show<GuiItemShop>(GuiMgr.ELayerType.Front, false, null);
            cmd.PlayErrorMessage("가차뽑기 중 " + cmd._DefaultMessage, 0);
        }
        else
        {
            PackageInfo packageInfo = GameDataMgr.Instance.GetInfo<PackageInfoMgr>().GetPackageInfo((uint)resultPack_idx);
            if (packageInfo != null)
            {
                packageName = packageInfo.name;
                strDesc = packageInfo.desc;
                rewardIcon = packageInfo.icon_image;

                //_rouletResult.stopAngle = resultPack_idx * 36.0f;
                //strDesc = packageInfo.desc;
                Debug.Log("~~~~~~~~~~~~~ Package Index : " + resultPack_idx.ToString() + " packageInfo.desc:" + packageInfo.desc + " icon:" + packageInfo.icon_image);

                cmd.PackItem_SetGemFunny(cmd._sOutputString);
                StartCoroutine(cmd.Send_PROTOCOL_LIST_ITEM_REQ());

            }
            guiItemShop.ShowResult(true, packageName, strDesc, rewardIcon);
        }


        // 싱글용
        //        RandomboxInfoMgr randomboxInfoMgr = GameDataMgr.Instance.GetInfo<RandomboxInfoMgr>();

        //        foreach(RandomboxInfo randomboxInfo in randomboxInfoMgr.GetRandomboxInfo())
        //        {
        //            if(randomboxInfo.randombox_type != RandomboxType.ShopGacha) // 샵에서 가차를 돌릴 경우만 이하 실행..
        //                continue;

        //            // 확률을 담는 배열 만들기..
        //            int[] temp = new int[100];
        //            int inc = 0;

        //            for( int i = 0; i < 10; i++ )
        //            {
        //                int dropPercent = (int)randomboxInfo.packagesetInfo[i].drop_percent; // 패키지당 드랍 확률...

        //                Debug.Log(string.Format("PackagesetInfo {0}의 확률 : {1}", i, dropPercent));
        //                //
        //                for( int j = 0; j < dropPercent; j++ )
        //                {							
        //                    temp[inc] = i;	// 패키지 자신의 인덱스값 i를 담는다..
        //                    Debug.Log(string.Format("확률 인덱스 플래그 : {0}", temp[inc]));
        //                    inc++;
        //                }
        //            }

        //            int randNum = Random.Range(0,100);
        //            int index = temp[randNum];

        //            // 가차에서 뽑힌 패키지 데이터 정보 뽑기..
        //            uint package_SI = randomboxInfo.packagesetInfo[index].package_si;
        //            //Hwang Wark
        //            //Debug.Log(package_SI);
        ////			_InfoSave.ItemHaveUpdate((int)package_SI,1);
        //// 서버 작업 부분 체크
        //            _InfoSave.RouletteKeypetCountCheck();
        //            _guiSettingMenu.OnEnter();
        //            //Hwang End
        //            packageName = packageInfoMgr.GetPackageInfo(package_SI).name;				
        //            pay = randomboxInfo.attr_1;

        //            return;
        //}
        //packageName = "GachaPackageName Error";
        //pay = 0;
        //return;
    }



    void Update()
    {
        _guiStageMapInfoScrollView.ScrollMap();	// 맵 스크롤에 따라 맵 바꿔치기...
    }



    public bool isFirstStageItemLoading = true;	// test....


    /// <summary>
    /// 기본이 되는 1 ~ 20 번 까지의 stage 생성하기...
    /// </summary>
    public void CreateBaseStageItem()
    {
        //if (isFirstStageItemLoading) 
        {
            isFirstStageItemLoading = false;

            StageMapInfoMgr stageMapInfoMgr = GameDataMgr.Instance.GetInfo<StageMapInfoMgr>();
            StageInfoMgr stageInfoMgr = GameDataMgr.Instance.GetInfo<StageInfoMgr>();

            for (int i = 0; i < 1; i++) 		// 그냥...
            {
                uint mapNumber = (uint)(1 + i);	// 그냥...

                StageMapInfo stageMapInfo = stageMapInfoMgr.GetStageMapInfo(mapNumber);

                foreach (LinkedStageInfo linkedStageInfo in stageMapInfo.linked_stage_info)
                {
                    StageInfo stageInfo = stageInfoMgr.GetStageInfo(linkedStageInfo.stage_si);

                    if (null == stageInfo)
                    {

                        Debug.LogWarning("최초 스테이지 데이터 로딩 시에 ScrollStageItem() 내에서 " + linkedStageInfo.stage_si.ToString() + " 에 해당하는 stageInfo 가 없습니다.");
                        continue;
                    }

                    #region dummy images setting

                    Texture2D[] images = null;

                    int random = UnityEngine.Random.Range(0, 100);

                    if (random < 10)
                    {
                        images = new Texture2D[] {
							(Texture2D)Resources.Load ("Images/Friends/profile_0001"),
							(Texture2D)Resources.Load ("Images/Friends/profile_0001"),
							(Texture2D)Resources.Load ("Images/Friends/profile_0001"),
							(Texture2D)Resources.Load ("Images/Friends/profile_0001")};
                    }

                    #endregion dummy images setting
                    
                    // 기본이 되는 1 ~ 20 번까지의 스테이지 생성			
                    GuiStageMapInfoScrollViewStageItemStage stageItem = _guiStageMapInfoScrollView.AddStage(stageInfo.si, linkedStageInfo.stage_si, new Vector2(linkedStageInfo.pos_x, linkedStageInfo.pos_y), false, images);
                    double RStage = _InfoSave.clearStageReball;

                    // 맵에서 스테이지 버튼 클릭 시 스테이지 플레이 준비 다이얼로그 박스 나타내기...
                    stageItem.OnStageMapClick = (GuiStageMapInfoScrollViewStageItemStage pComp) =>
                    {
                        MyInfoMgr.Instance.m_CurPlayStage = pComp.GetSI();

                        StartCoroutine(StageInfoSequence(pComp.GetStageNum(), pComp.GetSI(), (uint)_InfoSave.clearStageTopNumber,(int)RStage));
                    };

                    stageItem.OnButtonClick = (bool isOK) => 
                    {
                        stageItem.TweenMoved();
                    };
                }

                if(_InfoSave.Unlock_Precondition())
                { 
                    // 다음 마을로 올라가는 화살표 버튼 생성...
                    GameObject nextStage = _guiStageMapInfoScrollView.AddNextStageButton(stageMapInfo.si.ToString(), stageMapInfo.nextVillPos);

                    UIEventListener.Get(nextStage).onClick = (GameObject pGO) =>
                    {
                        if (_InfoSave.clearStageTopNumber == GameObject.Find("Gui/GuiStageMapInfoScrollView/Items/Maps/Map_002/12/StageBtn").GetComponent<ShopItemIndexNumber>().thisIndex + 8)
                        {
                            StartCoroutine(NextVillageSequence());
                        }
                    };
                }
                // 표지만 객체 생성..
                _guiStageMapInfoScrollView.AddMapSignBoard(stageMapInfo.si.ToString(), 001, stageMapInfo.SignPos, 90, 110, stageMapInfo.vill_name);

            }
        }
    }

    public int _nAppFriendCount;

    //public void KeypetAppGameMessage() 
    //{
    //    KakaoNativeExtension.Instance.Friends(onFriendsComplete, onFriendsError);
    //}

    //private void onFriendsComplete()
    //{
    //    Debug.Log("Loading OnFriendComplete");
    //    // _myKakaoNoKeypetFriends 리스트에 카카오 친구들 저장하기...
    //    StartCoroutine(SetMyKakaoFriends());
    //}

    //private IEnumerator SetMyKakaoFriends()
    //{
    //    GuiAttendance guiAttendance = GuiMgr.Instance.Show<GuiAttendance>(GuiMgr.ELayerType.Front, true, null);


    //    int appFriendsCount = KakaoFriends.Instance.appFriends.Count;

    //    yield return null;

    //}

    public int _nHeartMessageCount;

    public void KeypetAppGameMessage()
    {
        KakaoNativeExtension.Instance.Friends(onFriendsComplete, onFriendsError);
    }

    private void onFriendsComplete()
    {
        Debug.Log("~~~~~~~~~~ Loading OnFriendComplete");
        // _myKakaoNoKeypetFriends 리스트에 카카오 친구들 저장하기...
        StartCoroutine(SetMyKakaoFriends());
    }

    private IEnumerator SetMyKakaoFriends()
    {
        ////        GuiAttendance guiAttendance = GuiMgr.Instance.Show<GuiAttendance>(GuiMgr.ELayerType.Front, true, null);


        //        int appFriendsCount = KakaoFriends.Instance.appFriends.Count;

        //        KakaoFriends.Friend friend = null;

        //        GameObject attendanceItemGO;
        ////		GuiAttendanceItem guiAttendanceItem;

        //        GuiMessage guiMessage = GuiMgr.Instance.Show<GuiMessage>(GuiMgr.ELayerType.Front, true, null);

        //        for (int i = 0; i < appFriendsCount; ++i)
        //        {
        //            friend = KakaoFriends.Instance.appFriends[i];
        //            if (friend == null)
        //                continue;

        //            attendanceItemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiAttendanceItem"));
        ////            guiAttendanceItem = attendanceItemGO.GetComponent<GuiAttendanceItem>();
        ////            guiAttendance.AddRakingItem(guiAttendanceItem);

        //            // MyFriend 클래스 생성...
        //            //MyFriend myKakaoFriend = new MyFriend(ref defaultTexture);	// 친구 이미지 파일이 없는 경우도 있으므로 디폴트 이미지로 세팅...

        //            //myKakaoFriend.nickname = friend.nickname;
        //            //myKakaoFriend.userid = friend.userid;
        //            //myKakaoFriend.messageBlocked = friend.messageBlocked;

        //            yield return new WaitForFixedUpdate();
        //        }
        yield return null;
    }

    /// <summary>
    /// 카카오 친구 목록 로드 실패시 호출..
    /// </summary>
    private void onFriendsError(string status, string message)
    {
        Debug.Log("LogError");
    }
    /*
     * <------------------------------>
     * | 메세지 수신함 구현 완료      |
     * | 140929 메시지 수신함         |
     * <------------------------------>
     * 
     */
    public void KeypetMessageLoadCount()
    {
#if UNITY_EDITOR
        ServerMailBox();
#elif UNITY_ANDROID
            //KakaoNativeExtension.Instance.loadGameMessages(this.onLoadGameMessagesComplete, this.onLoadGameMessagesError);
#endif

    }
    private void onLoadGameMessagesComplete()
    {
        Debug.LogError("onLoadGameMessagesComplete");

        StartCoroutine(MailBox());
        ServerMailBox();
    }

    private void onLoadGameMessagesError(string status, string message)
    {
        Debug.Log("onLoadGameMessagesError");
        Debug.Log("### Error Mesage:" + status + message);
    }
    private void onUseHeartComplete()
    {
        Debug.Log("onUseHeartComplete");

        isPlay = true;

        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

//        KakaoMain.Instance.CheckUpdateHeart();

        // You must call the KakaoNativeExtension::loadGameUserInfo method
        //KakaoNativeExtension.Instance.loadGameUserInfo(KakaoMain.Instance.onGameUserInfoComplete, KakaoMain.Instance.onGameUserInfoError);
    }
    private void onUseHeartError(string status, string message)
    {
        Debug.Log("onUseHeartError");
        //Test Error View
        //KakaoMain.Instance.showAlertErrorMessage(status,message);

        GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);
    }

    IEnumerator MailBox()
    {
        KakaoGameMessages.GameMessage gameMessage = null;

        uint _nCount = 3000;

        foreach (var pair in KakaoGameMessages.Instance.gameMessages)
        {
            gameMessage = pair.Value;
            if (gameMessage == null)
                continue;

            Debug.Log("Message NickName :" + gameMessage.senderNickName);

            guiMessageItemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiMessageItem"));
            guiMessageItem = guiMessageItemGO.GetComponent<GuiMessageItem>();

            guiMessageItem.SerialID = _nCount;
            guiMessageItem.SetItemInfo(gameMessage.messageId,
                gameMessage.senderProfileImageUrl,
                "파워도착",
                gameMessage.senderNickName, "파워가 도착했어요.");

            KakaoMain.Instance.GetComponent<Cmd_Handler_CS>().guiMessage.AddMessageItem(guiMessageItem);

            yield return new WaitForFixedUpdate();

            _nCount++;
        }
    }

    void ServerMailBox()
    {
        Debug.Log("server MailBox()");

        Cmd_Handler_CS cmd = KakaoMain.Instance.GetComponent<Cmd_Handler_CS>();

        StartCoroutine(cmd.Send_PROTOCOL_LIST_MSG_REQ());

        if (cmd.g_new_msg > 0)
        {
        }
    }

    public void DeleteMailBox(int _nSeral)
    {
    }

    private void onAcceptAllGameMessagesComplete()
    {
        Debug.Log("onAcceptAllGameMessagesComplete");
        //please call reset method to user game info

        KakaoNativeExtension.Instance.loadGameUserInfo(KakaoMain.Instance.onGameUserInfoComplete, KakaoMain.Instance.onGameUserInfoError);
    }
    private void onAcceptAllGameMessagesError(string status, string message)
    {
        Debug.Log("onAcceptAllGameMessagesError");
    }

    private void onLoadLeaderboardComplete()
    {
        Debug.Log("onLoadLeaderboardComplete");
        // test
        KakaoLeaderboards.Instance.printToConsole();
    }

    //Error Load Map
    private void onLoadLeaderboardError(string status, string message)
    {
        Debug.Log("onLoadLeaderboardError");
        KakaoMain.Instance.showAlertErrorMessage(status, message);
    }
    private void onUpdateUserComplete()
    {
        Debug.Log("onUpdateUserComplete");

//        KakaoMain.Instance.CheckUpdateHeart();
    }

    private void onUpdateUserError(string status, string message)
    {
        Debug.Log("onUpdateUserError");
        KakaoMain.Instance.ShowKakaoMessageBox(status, message, "전체 받기의 파워 충전");
    }

    private void PowerChargePopup(GuiStageInfo guiStageInfo) 
    {
        //스테이지 인포 클로즈 Start
        guiStageInfo.PlayBtn.onClick = null;
        guiStageInfo.CloseBtn.onClick = null;

        GuiMgr.Instance.Show<GuiStageInfo>(GuiMgr.ELayerType.Front, false, null);
        //스테이지 인포 클로즈 End

        GuiChargePower guiChargePower = GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, true, null);
        _guiSettingMenu = GuiMgr.Instance.Show<GuiSettingMenu>(GuiMgr.ELayerType.Front, true, null);

        #region dummy power item setting

        GameObject itemGO;
        GuiChargePowerItem guiChargePowerItem;

        // shop table 에서 Power type 들만 추출
        ShopInfoMgr shopInfoMgr = GameDataMgr.Instance.GetInfo<ShopInfoMgr>();
        PackageInfoMgr packageInfoMgr = GameDataMgr.Instance.GetInfo<PackageInfoMgr>();

        foreach (ShopInfo shopInfo in shopInfoMgr.GetShopInfo())
        {
            if (shopInfo.type != ShopType.Power)
                continue;

            // 리스트 항목을 생성 추가 한다.
            itemGO = (GameObject)GameObject.Instantiate(Resources.Load("Gui/GuiChargePowerItem"));
            guiChargePowerItem = itemGO.GetComponent<GuiChargePowerItem>();
            guiChargePowerItem.SerialID = shopInfo.pack_idx;
            uint powersu = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).item_1_su;
            float cost = packageInfoMgr.GetPackageInfo(shopInfo.pack_idx).price_value;
            
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Korean:
            //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " 젬", "x " + powersu.ToString());
            //        break;
            //    case SystemLanguage.English:
            //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x " + powersu.ToString());
            //        break;
            //    case SystemLanguage.Portuguese:
            //        guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x " + powersu.ToString());
            //        break;
            //}
            guiChargePowerItem.SetItemInfo(shopInfo.pack_idx, cost.ToString() + " Jóias", "x " + powersu.ToString());
            
            int pack_idx = (int)shopInfo.pack_idx;
            guiChargePowerItem.BuyBtn.onClick = (GameObject pBuyBtnGO) =>
            {
                if (MyInfoMgr.Instance.m_Gem >= cost)
                {
                    JSONNode msgParam = new JSONClass();
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParam["message"] = "Você gostaria de comprar?";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParam["message"] = "Would you like to buy?";
                    //        break;
                    //    case SystemLanguage.Korean:
                    //        msgParam["message"] = "구매 하시겠습니까?";
                    //        break;
                    //}

                    msgParam["message"] = "Você gostaria de comprar?";
                    msgParam["show_type"] = "two";

                    GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

                    guiMessageBox.OnButtonClick = (bool pIsOk) =>
                    {
                        if (pIsOk)
                        {
                            Debug.Log("Power Charge :" + cost);

                            _InfoSave.PowerSuPlus((int)powersu);
                            _InfoSave.GemSuMinus((int)cost);
                            _InfoSave.ItemHaveUpdate(pack_idx, 1);

                            return;
                        }
                    };
                }

                //Hwang Wark 파워상점에서 잼이 코스트보다 부족할때 잼상점 팝업 Start
                if (MyInfoMgr.Instance.m_Gem < cost)
                {
                    JSONNode msgParampg = new JSONClass();
                    
                    //switch (Application.systemLanguage)
                    //{
                    //    case SystemLanguage.Portuguese:
                    //        msgParampg["message"] = "Gem Não o suficiente";
                    //        break;
                    //    case SystemLanguage.English:
                    //        msgParampg["message"] = "Not enough Jóias";
                    //        break;
                    //}

                    msgParampg["message"] = "Gem Não o suficiente";
                    msgParampg["show_type"] = "two";

                    GuiMessageBox guiMessageBoxpg = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParampg);

                    guiMessageBoxpg.OnButtonClick = (bool pIsOkpg) =>
                    {
                        if (pIsOkpg)
                        {
                            //Debug.Log("aaaa");				

                            guiChargePower.CloseBtn.onClick = null;

                            guiChargePower = null;

                            GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, false, null);

                            // 젬 윈도우 팝업....
                            GameObject GO = GameObject.Instantiate(Resources.Load("Gui/GuiChargeWindow")) as GameObject;
                            _guiChargeWindow = GO.GetComponent<GuiChargeWindow>();
                            if (_guiChargeWindow != null)
                                _guiChargeWindow.ShowGemWindow();
                        }
                    };
                }
                //Hwang Wark 파워상점에서 잼이 코스트보다 부족할때 잼상점 팝업 End
            };
            guiChargePower.AddChargePowerItem(guiChargePowerItem);
        }

        #endregion dummy power item setting

        guiChargePower.CloseBtn.onClick = (GameObject pCloseGO) =>
        {
            guiChargePower.CloseBtn.onClick = null;

            guiChargePower = null;

            GuiMgr.Instance.Show<GuiChargePower>(GuiMgr.ELayerType.Front, false, null);
        };

    }

    IEnumerator IE_PowerADPlay() 
    {
        Debug.Log("bClosed: " + _InfoSave.bClosedAD);

        while (_InfoSave.bClosedAD != true)
            yield return null;

        _InfoSave.isGameStage = false;
        _InfoSave.bClosedAD = false;

        isPlay = true;

        _InfoSave._GetDayFreeHeart--;

        if (_InfoSave._GetDayFreeHeart <= 0)
        {
            _InfoSave._GetDayFreeHeart = 0;
        }

        _InfoSave.SetGetDayFreeHeart(_InfoSave._GetDayFreeHeart);
    }

    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(10, Screen.height * 0.75f, 60, 60), "Cheat"))
    //    {
    //        //_InfoSave.FunnySuPlus((int)10000000);
    //        //_InfoSave.GemSuPlus((int)10000);
    //        //_InfoSave.clearStageTopNumber = 44;
    //        //_InfoSave.clearStageReball++;

    //        //for (int i = 8; i < 14; i++)
    //        //{
    //        //    _InfoSave.useitem[i] = 20;
    //        //}

    //        _InfoSave.FunnySuPlus((int)10000000);
    //        _InfoSave.GemSuPlus(99999999);
    //        //InteractiveConsole.GetInstance().DeleteScores();
    //    }
    //}

    //Profile 할때 사용할건데..아직 구체적인 구현 방법은 생각나지 않음.
    void FriendProFile() 
    {
    
    }

    IEnumerator IE_ENDProcess(bool isFB = false)
    {
        Debug.Log(isFB);

        //if (isFB)
        //{
        //    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, true, null);

        //    FBMain.Instance._str = "";

        //    InteractiveConsole.GetInstance().CallFBPost(false, _InfoSave.clearStageTopNumber);

        //    while (true)
        //    {
        //        if (FBMain.Instance._str.Contains("true"))
        //        {
        //            break;
        //        }

        //        yield return null;
        //    }

        //    InteractiveConsole.GetInstance().CallFBAPI();

        //    yield return new WaitForSeconds(0.5f);

        //    InteractiveConsole.GetInstance().FriendCall();

        //    yield return new WaitForSeconds(0.5f);

        //    while (!InteractiveConsole.GetInstance().isFriendCall)
        //    {
        //        yield return null;
        //    }

        //    GuiMgr.Instance.Show<GuiNextVillagePopup>(GuiMgr.ELayerType.Front, false, null);

        //    GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

        //    GameStateMgr.Instance.ChangeState(0, typeof(GSStageMap), null);
        //}
        //else
        //{
        yield return new WaitForEndOfFrame();

            GuiMgr.Instance.Show<GuiProcessing>(GuiMgr.ELayerType.Front, false, null);

            GameStateMgr.Instance.ChangeState<GSStageMap>(0, null);
        //}
    }

    //void OnApplicationPause(bool pauseStatus) 
    //{
    //    JSONNode msgParam = new JSONClass();

    //    msgParam["message"] = "Deseja mesmo sair do jogo";
    //    msgParam["show_type"] = "two";

    //    GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);

    //    guiMessageBox.OnButtonClick = (bool pIsOk) =>
    //    {
    //        if (!pIsOk)
    //        {
    //            //< 싱글버전 하트체크
    //            KakaoMain.Instance.CheckUpdateHeart(_guiPlayerInfo);
    //        }
    //    };
    //}
}
