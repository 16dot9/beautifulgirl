﻿using System;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public class GameStateMgr : MonoBehaviour
{
    public static int _LastStage_LockPoint = 1640;
    public GameObject itemFriendInfo;

    #region Singleton

    private static GameStateMgr _instance = null;

    public static GameStateMgr Instance
    {
        get
        {
            return _instance;
        }
    }

    #endregion Singleton

    protected int                                           _limitOfPools = 0;
    protected List<GameStateBase>                           _gameStateEntityPools = new List<GameStateBase>();

    protected Dictionary<int, GameStateBase>                _activatedGameStateList = new Dictionary<int, GameStateBase>();

    string Stage = "";

    public string GetStatusStage() 
    {
        return Stage;
    }

    public void ChangeState<T>(int pLayer, JSONNode pParams) where T : GameStateBase
    {
        var pStateType = typeof(T);

        string stateTypeName = pStateType.ToString();

        GameStateBase gameStateBase;

        if (_activatedGameStateList.TryGetValue(pLayer, out gameStateBase))
        {
            if (null != gameStateBase)
            {
                _activatedGameStateList[pLayer] = null;

                gameStateBase.StopAllCoroutines();
                gameStateBase.gameObject.SetActive(false);

                _gameStateEntityPools.Add(gameStateBase);
            }
        }

        foreach (GameStateBase poolEntity in _gameStateEntityPools)
        {
            if (poolEntity.GetType().ToString() == stateTypeName)
            {
                _gameStateEntityPools.Remove(poolEntity);

                _activatedGameStateList[pLayer] = gameStateBase;

                gameStateBase.SetParameter(pParams);

                gameStateBase.gameObject.SetActive(true);

                return;
            }
        }

        GameObject gameStateGO = new GameObject();

        gameStateGO.name = pStateType.ToString();
        Stage = pStateType.ToString();
        gameStateGO.transform.parent = transform;
        gameStateGO.transform.localPosition = Vector3.zero;
        gameStateGO.transform.localRotation = Quaternion.identity;
        gameStateGO.transform.localScale = Vector3.one;

        Type myType = Type.GetType(pStateType.ToString());

        gameStateBase = (GameStateBase)gameStateGO.AddComponent<T>();

        _activatedGameStateList[pLayer] = gameStateBase;

        gameStateBase.SetParameter(pParams);
    }


	/// <summary>
	/// 게임 플레이 상태인지...
	/// </summary>
	public bool IsGSPlay()
	{
		if( _activatedGameStateList[0].GetType().ToString() == "GSPlay")
			return true;
		else
			return false;
	}


    protected void Awake()
    {
        _instance = this;

        InvokeRepeating("RefreshPoolQueue", 0.0f, 1.0f);

        ChangeState<GSEntryPoint>(0, null);
    }
    /// <summary>
    ///  업데이트중 종료된 것과 새로 시작 되는 것에대한 오류가 있음.
    /// </summary>
    protected void Update()
    {
		// 스마트폰에서 뒤로가기 버튼 클릭시 게임 종료...
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			// 게임 종료할건지 물어보는 부분이 들어가야 한다.
			JSONNode msgParam = new JSONClass();

            //switch (Application.systemLanguage) 
            //{
            //    case SystemLanguage.Portuguese:
            //        msgParam["message"] = "Sair do jogo, você quer";
            //        break;
            //    case SystemLanguage.English:
            //        msgParam["message"] = "Exit the game, Do you want?";
            //        break;
            //    case SystemLanguage.Korean:
            //        msgParam["message"] = "게임을 종료 하시겠습니까?";
            //        break;
            //}

            msgParam["message"] = "Deseja mesmo sair do jogo";
			msgParam["show_type"] = "two";
			
			GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, msgParam);
			
			guiMessageBox.OnButtonClick = (bool pIsOk) =>
			{
				if (pIsOk)
				{
					//Application.LoadLevel("Main");
					Application.Quit ();
				}
			};
		}

        if (0 == _activatedGameStateList.Count)
        {
            return;
        }

        List<int> goToPools = null;

        foreach (int layer in _activatedGameStateList.Keys)
        {
            GameStateBase gameStateBase = _activatedGameStateList[layer];

            if (null != gameStateBase && gameStateBase.IsFinished)
            {
                if (null == goToPools)
                {
                    goToPools = new List<int>();
                }

                goToPools.Add(layer);
            }
        }

        if (null != goToPools)
        {
            foreach (int layer in goToPools)
            {
                GameStateBase gameStateBase = _activatedGameStateList[layer];

                gameStateBase.StopAllCoroutines();
                gameStateBase.gameObject.SetActive(false);

                _activatedGameStateList.Remove(layer);

                _gameStateEntityPools.Add(gameStateBase);
            }
        }
    }

    protected void OnDestroy()
    {
        _instance = null;
    }


    protected void RefreshPoolQueue()
    {
        if (0 == _gameStateEntityPools.Count)
        {
            return;
        }

        int delCount = _gameStateEntityPools.Count - _limitOfPools;

        if (delCount <= 0)
        {
            return;
        }

        List<GameStateBase> removeList = null;

        foreach (GameStateBase gameStateBase in _gameStateEntityPools)
        {
            if (delCount > 0)
            {
                delCount--;

                if (null == removeList)
                {
                    removeList = new List<GameStateBase>();
                }

                removeList.Add(gameStateBase);
            }
            else
            {
                break;
            }
        }

        if (null != removeList)
        {
            foreach (GameStateBase gameStateBase in removeList)
            {
                _gameStateEntityPools.Remove(gameStateBase);

                GameObject.DestroyImmediate(gameStateBase.gameObject);
            }
        }
    }

    public int GetCount_gameStateEntityPools()
    {
        return _gameStateEntityPools.Count;
    }
}
