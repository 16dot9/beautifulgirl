﻿using UnityEngine;
using System.Collections;


public class AudioMgr : MonoBehaviour
{

	#region Singleton

	private static AudioMgr _instance = null;

	public static AudioMgr Instance
	{
		get
		{
			return _instance;
		}
	}

	#endregion Singleton


	public bool IsPlay(string pSoundName)
	{
		if (null == AudioController.Instance)
		{
			return false;
		}

		return AudioController.IsPlaying(pSoundName);
	}

	public void Play(string pSoundName)
	{
		if (null == AudioController.Instance)
		{
			return;
		}

		GameObject parentGO = GameObject.FindGameObjectWithTag("AudioListener");

		if (null == parentGO)
		{
			return;
		}

		AudioController.Play(pSoundName, parentGO.transform);
	}

	public void Stop(string pSoundName)
	{
		if (null == AudioController.Instance)
		{
			return;
		}

		AudioController.Stop(pSoundName);
	}


	protected void Awake()
	{
		_instance = this;
	}

	protected void OnDestroy()
	{
		_instance = null;
	}

}
