﻿1	파워	게임플레이에 필요한 아이템	1	0	0	0	itemIcon_power
2	젬	현금 구입 	2	0	0	0	itemIcon_Gem
3	퍼니	게임머니	3	0	0	0	itemIcon_funny
4	긴급구출	선택한 키펫 구출	5	1	0	0	itemIcon_rescue
5	한번물리기	플레이어가 한번 했던 행동을 취소함.(차후도입)	6	1	0	0	itemIcon_turnBack
6	턴증가	횟수제한 모드에서 턴증가	7	0	3	0	itemIcon_turnAdd
7	시간증가	시간제한 모드에서 시간증가	7	10	0	0	itemIcon_timeAdd
8	랜덤구출	무작위 동일한 색상 키펫 구출	5	0	0	0	itemIcon_randomRescue
9	키펫1	수집, 구입 키펫	8	5	10	0	itemIcon_keypet01
10	키펫2	수집, 구입 키펫	8	5	10	0	itemIcon_keypet02
11	키펫3	수집, 구입 키펫	8	5	10	0	itemIcon_keypet03
12	키펫4	수집, 구입 키펫	8	5	10	0	itemIcon_keypet04
13	키펫5	수집, 구입 키펫	8	5	10	0	itemIcon_keypet05
14	키펫6	수집, 구입 키펫	8	5	10	0	itemIcon_keypet06
15	아이템보호	게임오버시 사용한 아이템을 일정 스테이지 횟수 동안 돌려받음(차후도입)	9	5	0	0	itemIcon_protectItem
16	피버시간증가	피버 모드에서 시간이 증가	7	10	0	0	itemIcon_fevertimeAdd
17	피버스타트	스테이지 시작부터 피버(차후도입)	9	0	0	0	itemIcon_feverStart
18	게임머니부스터	일정 게임 스테이지 횟수 만큼 게임머니 획득량 증가(차후도입)	9	5	0	0	itemIcon_funnyBooster
19	키펫7	수집, 구입 키펫	8	5	10	0	itemIcon_keypet01
20	키펫8	수집, 구입 키펫	8	5	10	0	itemIcon_keypet02
21	키펫9	수집, 구입 키펫	8	5	10	0	itemIcon_keypet03
22	키펫10	수집, 구입 키펫	8	5	10	0	itemIcon_keypet04
23	키펫11	수집, 구입 키펫	8	5	10	0	itemIcon_keypet05
24	키펫12	수집, 구입 키펫	8	5	10	0	itemIcon_keypet06
25	키펫13	수집, 구입 키펫	8	5	10	0	itemIcon_keypet01
26	키펫14	수집, 구입 키펫	8	5	10	0	itemIcon_keypet02
27	키펫15	수집, 구입 키펫	8	5	10	0	itemIcon_keypet03
28	키펫16	수집, 구입 키펫	8	5	10	0	itemIcon_keypet04
29	키펫17	수집, 구입 키펫	8	5	10	0	itemIcon_keypet05
30	키펫18	수집, 구입 키펫	8	5	10	0	itemIcon_keypet06
31	키펫19	수집, 구입 키펫	8	5	10	0	itemIcon_keypet01
32	키펫20	수집, 구입 키펫	8	5	10	0	itemIcon_keypet02
33	키펫21	수집, 구입 키펫	8	5	10	0	itemIcon_keypet03
34	키펫22	수집, 구입 키펫	8	5	10	0	itemIcon_keypet04
35	키펫23	수집, 구입 키펫	8	5	10	0	itemIcon_keypet05
36	탈출	테스트용,확정안됨,선택한 키펫 하나가 사라짐	5	1	1	0	itemIcon_rescue
