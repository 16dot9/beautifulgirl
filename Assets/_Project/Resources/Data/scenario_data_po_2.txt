﻿1	1	1	1	scenarioBg	1	cat_anger	Vixe! O que vamos fazer?	Oh não! Os Keypets foram pegos pelos caçadores malvados!	4
2	1	1	2	scenarioBg	2	dog_anger	Estou contigo!	Temos que ir atrás deles!	4
3	1	1	3	scenarioBg	1	cat_normal	Precisamos de ajuda...	Mas não podemos resgatá-los só com dois Keypets...	4
4	1	1	4	scenarioBg	2	dog_talk	Precisamos ajudá-los.	Os Keypets não conseguem escapar sozinhos.	4
5	1	1	5	scenarioBg	1	cat_normal	Vamos juntar todos os Keypets!	"Se você juntar os queypets em linha, eles ativarão o Poder Keypet e poderão escapar."	4
6	1	1	6	scenarioBg	2	dog_talk	Estou torcendo por você!	Não se esqueça do limite de tempo e de jogadas.	4
7	1	1	7	scenarioBg	1	cat_normal	"Sim, vamos!"	Vamos para o mundo dos Keypets!	4
8	1	2	1	scenarioBg	1	cat_anger	"Aff, tá muito quente..."	Acho que estou um pouco tonto...	4
9	1	2	2	scenarioBg	2	dog_sad	Meu pelo tá molhado...	Eu não entendo...	4
10	1	2	3	scenarioBg	1	cat_anger	Como eles são maus...	O clima do mundo dos Keypets está mudando porque os caçadores pegaram todos os Keypets...	4
11	1	2	4	scenarioBg	2	dog_anger	Vamos atrás deles!	Caçadores malvados!	4
12	1	3	1	scenarioBg	1	cat_cute	Tudo bem?	Olá!	4
13	1	3	2	scenarioBg	2	dog_happy	Bem...	"Eu estou, e você?"	4
14	1	3	3	scenarioBg	1	cat_happy	Que legal!	Joguei na roleta e ganhei presentes!	4
15	1	3	4	scenarioBg	2	dog_surprise	Já perdi as contas...	Quantas vezes você já foi na roleta?	4
16	1	3	5	scenarioBg	1	cat_cute	Vou tentar minha sorte no Gacha!	Vamos rumo à vitória!	4
17	1	4	1	scenarioBg	1	cat_joy	Ainda bem.	Já salvamos vários amigos!	4
18	1	4	2	scenarioBg	2	dog_sad	Que triste...	Mas ainda temos muitos amigos presos...	4
19	1	4	3	scenarioBg	1	cat_sad	Sim...	Coitados dos nossos amigos...	4
20	1	4	4	scenarioBg	2	dog_normal	Estou contigo!	Precisamos ser mais fortes.	4
21	1	4	5	scenarioBg	1	cat_cute	Vamos!	Então vamos!	4
22	1	5	1	scenarioBg	1	cat_normal	Sério?	Nosso mundo tem vários caminhos secretos.	4
23	1	5	2	scenarioBg	2	dog_talk	O que estamos esperando?	Estão apenas esperando para serem descobertos.	4
24	1	5	3	scenarioBg	1	cat_normal	Chocante!	Os mundos novos!	4
25	1	6	1	scenarioBg	1	cat_sad	"Olha, um oásis!"	Estou com tanta sede...	4
26	1	6	2	scenarioBg	2	dog_happy	Logo...ali...	Onde?!	4
27	1	6	3	scenarioBg	1	cat_surprise	Não vá!	Quero sair logo daqui...	4
28	1	7	1	scenarioBg	1	cat_normal	Não tenho muitas...	Quantas cartas você tem?	4
29	1	7	2	scenarioBg	2	dog_talk	Na roleta?	"Você notou que sempre que rola, aparece uma luz?"	4
30	1	7	3	scenarioBg	1	cat_normal	"Quanto mais você joga, mais vai ter."	Ouvi dizer que pode ganhar vários prêmios se tiver várias luzes.	4
31	1	8	1	scenarioBg	1	cat_normal	Parece ser....do mar?	Sinto uma brisa...	4
32	1	8	2	scenarioBg	2	dog_talk	Sério? Quero ir nadar!	"Estamos chegando na pria, então..."	4
33	1	8	3	scenarioBg	1	cat_surprise	Tão divertido...	Pensei que você não gostava de água...	4
34	1	9	1	scenarioBg	1	cat_normal	Só um momento...	Quer saber qual é a sorte do dia?	4
35	1	9	2	scenarioBg	2	dog_talk	Que você é sortudo!	A sua sorte de hoje é: ...	4
36	1	9	3	scenarioBg	1	cat_normal	Os quebra-cabeças estão ficando mais difíceis!	Mas fique esperto.	4
37	1	10	1	scenarioBg	1	cat_normal	Realmente está frio.	Que frio!	4
38	1	10	2	scenarioBg	2	dog_talk	"Obrigado, não teríamos conseguido sem você!"	O inverno chegou...	4
39	1	10	3	scenarioBg	1	cat_cute	Eu sempre quis fazer isso!	Vamos fazer guerra de bolas de neve?	4
40	1	11	1	scenarioBg	1	cat_normal	Mas eu não quero mostrar pra ninguém.	Você pode mostrar sua pontuação para os seus amigos.	4
41	1	11	2	scenarioBg	2	dog_talk	Prefiro jogar sozinho.	"Ah, vai, não deve ser tão ruim assim..."	4
42	1	12	1	scenarioBg	1	Cat_normal	Assim como eu sou o cachorro e você o gato?	Você sabia que cada keypet é referente a um animal?	4
43	1	12	2	scenarioBg	2	dog_happy	Quero ter todos!	"Isso mesmo. Tem o porco, a galinha e muitos outros por aí."	4
44	1	13	1	scenarioBg	1	cat_happy	Isso é tão legal!	Nossos amigos estão sendo libertados.	4
45	1	13	2	scenarioBg	2	dog_happy	Eu também!	Eu quero mais amigos.	4
46	1	14	1	scenarioBg	1	cat_happy	Será legal ver todo mundo de novo.	"Se continuarmos assim, libertaremos todos num piscar de olhos."	4
47	1	14	2	scenarioBg	2	dog_happy	Isso é tão triste.	Faz tempo que não falo com alguns deles.	4
48	1	15	1	scenarioBg	1	cat_happy	Foi tão rápido.	Estamos na 15ª vila.	4
49	1	15	2	scenarioBg	2	dog_happy	Vamos.	Vamos continuar.	4
50	1	16	1	scenarioBg	1	cat_happy	Quantos presentes!	Olha o que eu consegui.	4
51	1	16	2	scenarioBg	2	dog_happy	"Não importa, continue jogando."	Será que isso é normal? 	4
52	1	17	1	scenarioBg	1	cat_happy	Jogue conosco.	Está gostando do jogo?	4
53	1	17	2	scenarioBg	2	dog_happy	Nós adoramos poder fazer esta jornada com você.	Estamos felizes em te ver.	4
