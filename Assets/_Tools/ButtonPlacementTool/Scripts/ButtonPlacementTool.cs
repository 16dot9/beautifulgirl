﻿using System.Collections.Generic;
using UnityEngine;


public class ButtonPlacementTool : MonoBehaviour
{

	protected static GUISkin									_guiSkin = null;
	protected static string										_loadPath;
	protected static string										_savePath;
	protected static string										_exportPath;


	protected ButtonPlacementInfoTable							_buttonPlacementInfoTable = new ButtonPlacementInfoTable();
	protected ButtonPlacementInfoTable.ButtonPlacementInfo		_selectedButtonPlacementInfo = null;


	protected bool												_isNewEntityMode = false;
	protected string											_newEntityID = "0";

	protected Vector2											_listScrollView = Vector2.zero;
	protected Vector2											_inputScrollView = Vector2.zero;
	protected Vector2											_resultScrollView = Vector2.zero;

	protected string											_listError = "";
	protected string											_inputError = "";
	protected string											_resultError = "";


	protected void Awake()
	{
		_loadPath = Application.dataPath + "/_Tools/ButtonPlacementTool/Data/placement.csv";
		_savePath = Application.dataPath + "/_Tools/ButtonPlacementTool/Data/placement.csv";
		_exportPath = Application.dataPath + "/_Tools/ButtonPlacementTool/Data/placement_info_table.csv";
	}

	protected void OnGUI()
	{
		GUI.skin = _guiSkin;


		GUILayout.BeginVertical();


		if (_isNewEntityMode)
		{
			GUINewEntityMode();
		}
		else
		{
			GUIList();

			GUIInput();

			GUIResult();
		}


		GUILayout.EndVertical();
	}


	protected void GUINewEntityMode()
	{
		GUILayout.Box("Please input a ID number.", GUILayout.Width(Screen.width));


		GUILayout.Space(10);


		GUILayout.BeginHorizontal();

		GUILayout.Space(50);

		_newEntityID = GUILayout.TextField(_newEntityID, GUILayout.Width(Screen.width - 100));

		GUILayout.EndHorizontal();


		GUILayout.Space(10);


		GUILayout.BeginHorizontal();

		if (GUILayout.Button("Ok"))
		{
			_isNewEntityMode = false;

			ushort iD;
			if (!ushort.TryParse(_newEntityID, out iD))
			{
				_listError = "엔티티 생성 실패.";
			}
			else
			{
				if (!NewEntity(iD))
				{
					_listError = "엔티티 생성 실패.";
				}
			}
		}

		if (GUILayout.Button("Cancel"))
		{
			_isNewEntityMode = false;
		}

		GUILayout.EndHorizontal();

	}

	protected void GUIList()
	{
		int layerMagin = 5;
		int layerWidth = Screen.width / 3;
		int layerHeight = Screen.height;


		GUILayout.BeginArea(new Rect(layerMagin + layerWidth * 0, layerMagin, layerWidth - layerMagin * 2, layerHeight - layerMagin * 2));


		GUILayout.Box("List", GUILayout.Width(layerWidth - layerMagin * 3));


		if (!string.IsNullOrEmpty(_listError))
		{
			GUILayout.Space(10);

			GUILayout.BeginHorizontal();

			GUI.color = Color.yellow;

			if (GUILayout.Button("Clear"))
			{
				_listError = "";
			}

			GUI.color = Color.red;

			GUILayout.Label(_listError, GUILayout.Width(Screen.width));

			GUI.color = Color.white;

			GUILayout.EndHorizontal();
		}


		GUILayout.Space(10);


		GUILayout.BeginHorizontal();

		if (GUILayout.Button("Load project"))
		{
			LoadProject();
		}

		if (GUILayout.Button("Save project"))
		{
			SaveProject();
		}

		if (GUILayout.Button("Export project"))
		{
			ExportProject();
		}

		if (GUILayout.Button("New entity"))
		{
			_isNewEntityMode = true;
		}

		if (GUILayout.Button("CheckCRC"))
		{
			CheckCRC();
		}

		GUILayout.EndHorizontal();


		GUILayout.Space(10);


		_listScrollView = GUILayout.BeginScrollView(_listScrollView);


		foreach (ButtonPlacementInfoTable.ButtonPlacementInfo buttonPlacementInfo in _buttonPlacementInfoTable.GetButtonPlacementInfo())
		{
			GUILayout.BeginHorizontal();

			if (GUILayout.Button(string.Format("ID : {0}", buttonPlacementInfo.iD)))
			{
				_selectedButtonPlacementInfo = buttonPlacementInfo;
			}
			else if (GUILayout.Button("Remove", GUILayout.Width(80)))
			{
				if (!UnregisterEntity(buttonPlacementInfo))
				{
					_listError = "엔티티 제거 실패";
				}

				break;
			}

			GUILayout.EndHorizontal();
		}


		GUILayout.EndScrollView();


		GUILayout.EndArea();
	}

	protected void GUIInput()
	{
		int layerMagin = 5;
		int layerWidth = Screen.width / 3;
		int layerHeight = Screen.height;


		GUILayout.BeginArea(new Rect(layerMagin + layerWidth * 1, layerMagin, layerWidth - layerMagin * 2, layerHeight - layerMagin * 2));


		GUILayout.Box("Input", GUILayout.Width(layerWidth - layerMagin * 3));


		if (!string.IsNullOrEmpty(_inputError))
		{
			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUI.color = Color.yellow;

			if (GUILayout.Button("Clear"))
			{
				_inputError = "";
			}

			GUI.color = Color.red;

			GUILayout.Label(_inputError, GUILayout.Width(Screen.width));

			GUI.color = Color.white;

			GUILayout.EndHorizontal();
		}


		if (null != _selectedButtonPlacementInfo)
		{
			_inputScrollView = GUILayout.BeginScrollView(_inputScrollView);


			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUILayout.Label("ID");

			GUILayout.Label(string.Format("{0}", _selectedButtonPlacementInfo.iD));

			GUILayout.EndHorizontal();


			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUILayout.Label("Attr1");

			_selectedButtonPlacementInfo.attr1 = GUILayout.TextField(_selectedButtonPlacementInfo.attr1);

			GUILayout.EndHorizontal();


			GUILayout.BeginHorizontal();

			GUILayout.Label("Attr2");

			_selectedButtonPlacementInfo.attr2 = GUILayout.TextField(_selectedButtonPlacementInfo.attr2);

			GUILayout.EndHorizontal();


			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUILayout.Label("Plane width");

			string planeWidth = GUILayout.TextField(_selectedButtonPlacementInfo.planeWidth.ToString());

			if (planeWidth != _selectedButtonPlacementInfo.planeWidth.ToString())
			{
				if (!int.TryParse(planeWidth, out _selectedButtonPlacementInfo.planeWidth))
				{
					_inputError = "가로 크기 오류.";
				}
			}


			GUILayout.Space(10);


			GUILayout.Label("Plane height");

			string planeHeight = GUILayout.TextField(_selectedButtonPlacementInfo.planeHeight.ToString());

			if (planeHeight != _selectedButtonPlacementInfo.planeHeight.ToString())
			{
				if (!int.TryParse(planeHeight, out _selectedButtonPlacementInfo.planeHeight))
				{
					_inputError = "세로 크기 오류.";
				}
			}

			GUILayout.EndHorizontal();


			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUI.color = Color.yellow;

			GUILayout.Label("Button Type Info");

			GUI.color = Color.white;

			if (GUILayout.Button("Add"))
			{
				_buttonPlacementInfoTable.AddButtonType(_selectedButtonPlacementInfo, new AlgorithmOfPlacingButtons.ButtonTypeInfo());
			}

			if (GUILayout.Button("Clear"))
			{
				_buttonPlacementInfoTable.RemoveAllButtonType(_selectedButtonPlacementInfo);
			}

			if (GUILayout.Button("Auto balancing"))
			{
				_buttonPlacementInfoTable.AutoBalancingButtonType(_selectedButtonPlacementInfo);
			}

			GUILayout.EndHorizontal();


			foreach (AlgorithmOfPlacingButtons.ButtonTypeInfo buttonTypeInfo in _buttonPlacementInfoTable.GetButtonType(_selectedButtonPlacementInfo))
			{
				GUILayout.BeginHorizontal();

				GUILayout.Label(string.Format("[Button type {0}]", buttonTypeInfo.buttonID));

				if (GUILayout.Button("Remove", GUILayout.Width(80)))
				{
					_buttonPlacementInfoTable.RemoveButtonType(_selectedButtonPlacementInfo, buttonTypeInfo);

					break;
				}

				GUILayout.EndHorizontal();


				GUILayout.BeginHorizontal();

				GUILayout.Space(20);

				GUILayout.Label("ID");

				string buttonID = GUILayout.TextArea(buttonTypeInfo.buttonID.ToString(), GUILayout.Width(50));

				if (buttonID != buttonTypeInfo.buttonID.ToString())
				{
					if (!int.TryParse(buttonID, out buttonTypeInfo.buttonID))
					{
						_inputError = "버튼 아이디 오류.";
					}
				}

				GUILayout.EndHorizontal();


				GUILayout.BeginHorizontal();

				GUILayout.Space(20);

				GUILayout.Label("Balance Value");

				string coeffBTBV = GUILayout.TextArea(buttonTypeInfo.coeffBTBV.ToString(), GUILayout.Width(50));

				if (coeffBTBV != buttonTypeInfo.coeffBTBV.ToString())
				{
					if (!int.TryParse(coeffBTBV, out buttonTypeInfo.coeffBTBV))
					{
						_inputError = "버튼 밸런싱 계수 오류.";
					}
				}

				GUILayout.EndHorizontal();
			}


			GUILayout.Space(10);


			GUI.color = Color.yellow;

			GUILayout.Label("Block Info");

			GUI.color = Color.white;


			for (int y = 0; y < _selectedButtonPlacementInfo.planeHeight; ++y)
			{
				GUILayout.BeginHorizontal();

				GUILayout.Space(10);

				for (int x = 0; x < _selectedButtonPlacementInfo.planeWidth; ++x)
				{
					int blockSlotIndex = x + y * _selectedButtonPlacementInfo.planeWidth;

					if (_buttonPlacementInfoTable.IsBlock(_selectedButtonPlacementInfo, blockSlotIndex))
					{
						if (GUILayout.Button("B", GUILayout.Width(30), GUILayout.Height(30)))
						{
							_buttonPlacementInfoTable.RemoveBlock(_selectedButtonPlacementInfo, blockSlotIndex);
						}
					}
					else
					{
						if (GUILayout.Button("", GUILayout.Width(30), GUILayout.Height(30)))
						{
							_buttonPlacementInfoTable.AddBlock(_selectedButtonPlacementInfo, blockSlotIndex);
						}
					}
				}

				GUILayout.EndHorizontal();
			}


			GUILayout.EndScrollView();
		}


		GUILayout.EndArea();
	}

	protected void GUIResult()
	{
		int layerMagin = 5;
		int layerWidth = Screen.width / 3;
		int layerHeight = Screen.height;


		GUILayout.BeginArea(new Rect(layerMagin + layerWidth * 2, layerMagin, layerWidth - layerMagin * 2, layerHeight - layerMagin * 2));


		GUILayout.Box("Result", GUILayout.Width(layerWidth - layerMagin * 3));


		if (!string.IsNullOrEmpty(_resultError))
		{
			GUILayout.Space(10);


			GUILayout.BeginHorizontal();

			GUI.color = Color.yellow;

			if (GUILayout.Button("Clear"))
			{
				_resultError = "";
			}

			GUI.color = Color.red;

			GUILayout.Label(_resultError, GUILayout.Width(Screen.width));

			GUI.color = Color.white;

			GUILayout.EndHorizontal();
		}


		GUILayout.Space(10);


		GUILayout.BeginHorizontal();

		GUILayout.Label("Placement result");

		if (GUILayout.Button("Refresh"))
		{
			_buttonPlacementInfoTable.PlacementButtons(_selectedButtonPlacementInfo);
		}

		GUILayout.EndHorizontal();

		AlgorithmOfPlacingButtons.PlacedButtonInfo[] placedButtonInfoList = _buttonPlacementInfoTable.GetPlacedButtonInfoList(_selectedButtonPlacementInfo);

		if (null != _selectedButtonPlacementInfo && null != placedButtonInfoList)
		{
			_resultScrollView = GUILayout.BeginScrollView(_resultScrollView);

			for (int y = 0; y < _selectedButtonPlacementInfo.planeHeight; ++y)
			{
				GUILayout.BeginHorizontal();

				GUILayout.Space(10);

				for (int x = 0; x < _selectedButtonPlacementInfo.planeWidth; ++x)
				{
					int blockSlotIndex = x + y * _selectedButtonPlacementInfo.planeWidth;

					AlgorithmOfPlacingButtons.PlacedButtonInfo placedButtonInfo = placedButtonInfoList[blockSlotIndex];

					if (null != placedButtonInfo)
					{
						switch (placedButtonInfo.placementType)
						{
							case AlgorithmOfPlacingButtons.PlacementType.Button:
								{
									GUILayout.Button(placedButtonInfo.iD.ToString(), GUILayout.Width(30), GUILayout.Height(30));
								}
								break;

							case AlgorithmOfPlacingButtons.PlacementType.Block:
								{
									GUILayout.Button("B", GUILayout.Width(30), GUILayout.Height(30));
								}
								break;

							default:
								{
									GUILayout.Button("", GUILayout.Width(30), GUILayout.Height(30));
								}
								break;
						}

					}
					else
					{
						GUILayout.Button("", GUILayout.Width(30), GUILayout.Height(30));
					}
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndScrollView();
		}


		GUILayout.EndArea();
	}


	protected bool LoadProject()
	{
		_selectedButtonPlacementInfo = null;

		if (!_buttonPlacementInfoTable.Load(_loadPath))
		{
			_listError = _buttonPlacementInfoTable.Error;

			return false;
		}

		return true;
	}

	protected bool SaveProject()
	{
		if (!_buttonPlacementInfoTable.Save(_savePath))
		{
			_listError = _buttonPlacementInfoTable.Error;

			return false;
		}

		return true;
	}

	protected bool ExportProject()
	{
		if (!_buttonPlacementInfoTable.Export(_exportPath))
		{
			_listError = _buttonPlacementInfoTable.Error;

			return false;
		}

		return true;
	}

	protected bool NewEntity(ushort pNewEntityID)
	{
		if (0 == pNewEntityID)
		{
			return false;
		}

		if (null != _buttonPlacementInfoTable.Find(pNewEntityID))
		{
			return false;
		}

		_selectedButtonPlacementInfo = new ButtonPlacementInfoTable.ButtonPlacementInfo();
		_selectedButtonPlacementInfo.iD = pNewEntityID;

		_buttonPlacementInfoTable.Add(_selectedButtonPlacementInfo);

		return true;
	}

	protected void CheckCRC()
	{
		_listError = "";

		Dictionary<uint, ButtonPlacementInfoTable.ButtonPlacementInfo> checker = new Dictionary<uint, ButtonPlacementInfoTable.ButtonPlacementInfo>();

		AlgorithmOfPlacingButtons algorithm = new AlgorithmOfPlacingButtons();
		 
		foreach ( ButtonPlacementInfoTable.ButtonPlacementInfo buttonPlacementInfo in _buttonPlacementInfoTable.GetButtonPlacementInfo() )
		{
			uint crc = algorithm.CalcualteCRC(buttonPlacementInfo.placedButtonInfoList);

			if (checker.ContainsKey(crc))
			{
				_listError += string.Format("중복 : {0}, {1}\n", checker[crc].iD, buttonPlacementInfo.iD);
			}
			else
			{
				checker.Add(crc, buttonPlacementInfo);
			}
		}

		if (string.IsNullOrEmpty(_listError))
		{
			_listError = "OK";
		}
	}

	protected bool UnregisterEntity(ButtonPlacementInfoTable.ButtonPlacementInfo pButtonPlacementInfo)
	{
		if (null == pButtonPlacementInfo)
		{
			return false;
		}

		_buttonPlacementInfoTable.Remove(pButtonPlacementInfo);

		return true;
	}

}