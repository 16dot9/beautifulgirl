﻿using System.Collections.Generic;
using UnityEngine;


public class AlgorithmOfPlacingButtons
{

	public enum PlacementType
	{
		Invalid,
		Button,
		Block
	}

	public class ButtonTypeInfo
	{
		public int						buttonID = -1;
		public int						coeffBTBV = 0;		// 정밀도의 문제로 정수 사용 ( 0 - 100 ).
	}

	public class PlacedButtonInfo
	{
		public PlacementType			placementType = PlacementType.Invalid;
		public int						iD = -1;
	}

	public class PlacedBlockInfo
	{
		public int						blockSlotIndex = -1;
	}


	protected enum EPlacementDir
	{
		Begin,

		DU = Begin,			// up
		DUR,				// up-right
		DR,					// right
		DDR,				// down-right
		DD,					// down
		DDL,				// down-left
		DL,					// left
		DUL,				// up-left

		Max
	}

	protected class BS
	{
		public int						buttonID = -1;
		public int						buttonCountToPlace = 0;
	}

	protected class BSS
	{
		public int						buttonID = -1;
		public int						buttonCountToPlace = 0;
		public List<BS>					bSList = new List<BS>();
	}

	protected class BTBV
	{
		public int						buttonID = -1;
		public int						buttonCountToPlace = 0;
		public bool						needAdjust = false;
	}

	protected class BSEntity
	{
		public BS						bS = null;
	}

	protected class SlotEntity
	{
		public int						slotIndex = -1;
	}

	protected class DirEntity
	{
		public EPlacementDir			placementDir = EPlacementDir.Max;
	}

	protected class PosEntity
	{
		public List<int>                slotOffsetList = new List<int>();
	}


	protected static int[]				_dirLeanX = new int[] { 0, 1, 1, 1, 0, -1, -1, -1, 0 };
	protected static int[]				_dirLeanY = new int[] { 1, 1, 0, -1, -1, -1, 0, 1, 0 };

	protected int						_planeWidth = 0;
	protected int						_planeHeight = 0;
	protected PlacedBlockInfo[]			_placedBlockInfoList = null;
	protected ButtonTypeInfo[]			_buttonTypeInfoList = null;


	public bool SetInputData(int pPlaneWidth, int pPlaneHeight, PlacedBlockInfo[] pPlacedBlockInfoList, ButtonTypeInfo[] pButtonTypeInfoList)
	{
		if (!CheckInputData(pPlaneWidth, pPlaneHeight, pPlacedBlockInfoList, pButtonTypeInfoList))
		{
			return false;
		}

		_planeWidth = pPlaneWidth;
		_planeHeight = pPlaneHeight;
		_placedBlockInfoList = pPlacedBlockInfoList;
		_buttonTypeInfoList = pButtonTypeInfoList;

		return true;
	}

	public PlacedButtonInfo[] CalculateButtonPlacementInfo()
	{
		PlacedButtonInfo[] placedButtonInfoList = new PlacedButtonInfo[_planeWidth * _planeHeight];

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Place the block.
		///////////////////////////////////////////////////////////////////////////////////////////////////

		PlaceBlock(placedButtonInfoList);

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Decide the button type balance value.
		///////////////////////////////////////////////////////////////////////////////////////////////////

		// 1. Decide the button type balance value.

		BTBV[] bBTVList = DecideButtonTypeBalanceValue();

		// 2. Adjust the button type balance values.

		if (!AdjustButtonTypeBalanceValue(bBTVList))
		{
			return null;
		}

		// 3. Check the button type balance vales.

		if (!CheckButtonTypeBalanceValue(bBTVList))
		{
			return null;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Decide the button segment set.
		///////////////////////////////////////////////////////////////////////////////////////////////////

		List<BS> bSList = new List<BS>();

		for (int i = 0; i < bBTVList.Length; ++i)
		{
			// 1. Decide the button segment set.

			BSS bSS = DecideButtonSegmentSet(bBTVList[i]);

			// 2. Check the button segment set.

			if (!CheckButtonSegmentSet(bSS))
			{
				return null;
			}

			bSList.AddRange(bSS.bSList);
		}

		// 3. Mixing the button segment set.

		bSList = MixingButtonSegmentSet(bSList);

		///////////////////////////////////////////////////////////////////////////////////////////////////
		// Decide button segment placement direction and position
		///////////////////////////////////////////////////////////////////////////////////////////////////

		if (!PlacementBS(0, bSList, placedButtonInfoList))
		{
			return null;
		}

		return placedButtonInfoList;
	}

	public uint CalcualteCRC(PlacedButtonInfo[] pPlaceButtonInfoList)
	{
		string crcSumData = "";

		foreach (PlacedButtonInfo placedButtonInfo in pPlaceButtonInfoList)
		{
			crcSumData += (null == placedButtonInfo) ? "Empty" : string.Format("{0}_{1}", placedButtonInfo.placementType.ToString(), placedButtonInfo.iD.ToString());
		}

		char[] crcSumDataToChar = crcSumData.ToCharArray();

		List<byte> crcInput = new List<byte>();

		foreach (char c in crcSumDataToChar)
		{
			crcInput.Add(System.Convert.ToByte(c));
		}

		byte[] inputArray = crcInput.ToArray();

		return crc_calc(ref inputArray, (uint)crcInput.Count);
	}


	protected bool CheckInputData(int pPlaneWidth, int pPlaneHeight, PlacedBlockInfo[] pPlacedBlockInfoList, ButtonTypeInfo[] pButtonTypeInfoList)
	{
		if (pPlaneWidth <= 0 || pPlaneHeight <= 0)
		{
			return false;
		}

		if (null != pPlacedBlockInfoList)
		{
			int maxSlotCount = pPlaneWidth * pPlaneHeight;

			foreach (PlacedBlockInfo placedBlockInfo in pPlacedBlockInfoList)
			{
				if (placedBlockInfo.blockSlotIndex < 0 || placedBlockInfo.blockSlotIndex >= maxSlotCount)
				{
					return false;
				}
			}
		}

		if (null == pButtonTypeInfoList || 0 == pButtonTypeInfoList.Length)
		{
			return false;
		}

		int sumCoeffBTBV = 0;

		foreach (ButtonTypeInfo buttonTypeInfo in pButtonTypeInfoList)
		{
			sumCoeffBTBV += buttonTypeInfo.coeffBTBV;
		}

		if (100 != sumCoeffBTBV)
		{
			return false;
		}

		return true;
	}


	protected void PlaceBlock(PlacedButtonInfo[] pPlacedButtonInfoList)
	{
		if (null == _placedBlockInfoList)
		{
			return;
		}

		foreach (PlacedBlockInfo placedBlockInfo in _placedBlockInfoList)
		{
			pPlacedButtonInfoList[placedBlockInfo.blockSlotIndex] = new PlacedButtonInfo();
			pPlacedButtonInfoList[placedBlockInfo.blockSlotIndex].placementType = PlacementType.Block;
		}
	}

	protected BTBV[] DecideButtonTypeBalanceValue()
	{
		int buttonCountToPlace = _planeWidth * _planeHeight - (null != _placedBlockInfoList ? _placedBlockInfoList.Length : 0);

		BTBV[] bBTVList = new BTBV[_buttonTypeInfoList.Length];

		for (int i = 0; i < _buttonTypeInfoList.Length; ++i)
		{
			bBTVList[i] = new BTBV();

			bBTVList[i].buttonID = _buttonTypeInfoList[i].buttonID;
			bBTVList[i].buttonCountToPlace = (_buttonTypeInfoList[i].coeffBTBV * buttonCountToPlace) / 100;
			bBTVList[i].needAdjust = (_buttonTypeInfoList[i].coeffBTBV * buttonCountToPlace) % 100 != 0 ? true : false;
		}

		return bBTVList;
	}

	protected bool AdjustButtonTypeBalanceValue(BTBV[] pBTVList)
	{
		if (null == pBTVList)
		{
			return false;
		}

		int buttonCountToPlace = _planeWidth * _planeHeight - (null != _placedBlockInfoList ? _placedBlockInfoList.Length : 0);

		int curButtonCountToPlace = 0;

		for (int i = 0; i < pBTVList.Length; ++i)
		{
			curButtonCountToPlace += pBTVList[i].buttonCountToPlace;
		}

		if (curButtonCountToPlace != buttonCountToPlace)
		{
			for (int i = 0; i < pBTVList.Length; ++i)
			{
				if (pBTVList[i].needAdjust)
				{
					pBTVList[i].buttonCountToPlace++;
					curButtonCountToPlace++;

					if (curButtonCountToPlace == buttonCountToPlace)
					{
						break;
					}
				}
			}
		}

		if (curButtonCountToPlace != buttonCountToPlace)
		{
			return false;
		}

		return true;
	}

	protected bool CheckButtonTypeBalanceValue(BTBV[] pBTVList)
	{
		if (null == pBTVList || 0 == pBTVList.Length)
		{
			return false;
		}

		int buttonCountToPlace = _planeWidth * _planeHeight - (null != _placedBlockInfoList ? _placedBlockInfoList.Length : 0);

		int curButtonCountToPlace = 0;

		for (int i = 0; i < pBTVList.Length; ++i)
		{
			curButtonCountToPlace += pBTVList[i].buttonCountToPlace;

			if (pBTVList[i].buttonCountToPlace < 2)
			{
				return false;
			}
		}

		if (curButtonCountToPlace != buttonCountToPlace)
		{
			return false;
		}

		return true;
	}


	protected BSS DecideButtonSegmentSet(BTBV pBTBV)
	{
		if (null == pBTBV)
		{
			return null;
		}

		BSS bSS = new BSS();

		bSS.buttonID = pBTBV.buttonID;
		bSS.buttonCountToPlace = pBTBV.buttonCountToPlace;

		SplitButtonSegmentSet(pBTBV.buttonCountToPlace, bSS);

		return bSS;
	}

	protected bool CheckButtonSegmentSet(BSS pBSS)
	{
		if (null == pBSS)
		{
			return false;
		}

		if (0 == pBSS.bSList.Count)
		{
			return false;
		}

		int buttonCountToPlace = 0;

		foreach (BS bS in pBSS.bSList)
		{
			buttonCountToPlace += bS.buttonCountToPlace;
		}

		if (buttonCountToPlace != pBSS.buttonCountToPlace)
		{
			return false;
		}

		return true;
	}

	protected List<BS> MixingButtonSegmentSet(List<BS> pBSList)
	{
		List<BSEntity> bSEntityList = new List<BSEntity>();

		foreach (BS bS in pBSList)
		{
			BSEntity bSEntity = new BSEntity();
			bSEntity.bS = bS;

			bSEntityList.Add(bSEntity);
		}

		List<BS> mixedBSList = new List<BS>();

		while (0 != bSEntityList.Count)
		{
			int randIndex = Random.Range(0, bSEntityList.Count);
			mixedBSList.Add(bSEntityList[randIndex].bS);
			bSEntityList.RemoveAt(randIndex);
		}

		return mixedBSList;
	}

	protected void SplitButtonSegmentSet(int pButtonCountToPlace, BSS pBSS)
	{
		if (2 == pButtonCountToPlace || 3 == pButtonCountToPlace)
		{
			BS bS = new BS();

			bS.buttonID = pBSS.buttonID;
			bS.buttonCountToPlace = pButtonCountToPlace;

			pBSS.bSList.Add(bS);

			return;
		}

		int halfButtonCountToPlace = pButtonCountToPlace / 2;

		if (pButtonCountToPlace * 50 % 100 != 0)
		{
			SplitButtonSegmentSet(halfButtonCountToPlace, pBSS);
			SplitButtonSegmentSet(halfButtonCountToPlace + 1, pBSS);
		}
		else
		{
			SplitButtonSegmentSet(halfButtonCountToPlace, pBSS);
			SplitButtonSegmentSet(halfButtonCountToPlace, pBSS);
		}
	}


	protected bool PlacementBS(int pCurBSIndex, List<BS> pBSList, PlacedButtonInfo[] pPlacedButtonInfoList)
	{
		if (pCurBSIndex >= pBSList.Count)
		{
			return true;
		}

		BS curBS = pBSList[pCurBSIndex];

		for (int emptySlot = 0; emptySlot < pPlacedButtonInfoList.Length; ++emptySlot)
		{
			PlacedButtonInfo placeButtonInfo = pPlacedButtonInfoList[emptySlot];

			if (null == placeButtonInfo)
			{
				List<EPlacementDir> placementDirList = DecideButtonSegmentPlacementDirection();

				if (null == placementDirList)
				{
					return false;
				}

				int curX = emptySlot % _planeWidth;
				int curY = emptySlot / _planeWidth;

				List<SlotEntity> slotEntityList = new List<SlotEntity>();

				foreach (EPlacementDir placementDir in placementDirList)
				{
					List<PosEntity> placementPosList = DecideButtonSegmentPlacementPosition(curBS.buttonCountToPlace, Mathf.Max(_planeWidth, _planeHeight));

					foreach (PosEntity posEntity in placementPosList)
					{
						if (!HasBlock(curX, curY, placementDir, posEntity, pPlacedButtonInfoList))
						{
							continue;
						}

						slotEntityList.Clear();

						foreach (int slotIndex in posEntity.slotOffsetList)
						{
							int nextX = curX + slotIndex * _dirLeanX[(int)placementDir];

							if (nextX < 0)
							{
								continue;
							}

							if (nextX >= _planeWidth)
							{
								continue;
							}

							int nextY = curY + slotIndex * _dirLeanY[(int)placementDir];

							if (nextY < 0)
							{
								continue;
							}

							if (nextY >= _planeHeight)
							{
								continue;
							}

							int nextIndex = nextY * _planeWidth + nextX;

							if (null == pPlacedButtonInfoList[nextIndex])
							{
								SlotEntity slotEntity = new SlotEntity();
								slotEntity.slotIndex = nextIndex;
								slotEntityList.Add(slotEntity);
							}
						}

						if (curBS.buttonCountToPlace != slotEntityList.Count)
						{
							continue;
						}

						foreach (SlotEntity slotEntity in slotEntityList)
						{
							pPlacedButtonInfoList[slotEntity.slotIndex] = new PlacedButtonInfo();
							pPlacedButtonInfoList[slotEntity.slotIndex].placementType = PlacementType.Button;
							pPlacedButtonInfoList[slotEntity.slotIndex].iD = curBS.buttonID;
						}

						if (PlacementBS(pCurBSIndex + 1, pBSList, pPlacedButtonInfoList))
						{
							return true;
						}

						foreach (SlotEntity slotEntity in slotEntityList)
						{
							pPlacedButtonInfoList[slotEntity.slotIndex] = null;
						}
					}
				}
			}
		}

		return false;
	}

	protected List<EPlacementDir> DecideButtonSegmentPlacementDirection()
	{
		List<DirEntity> dirEntityList = new List<DirEntity>();

		for (EPlacementDir ePlacementDir = EPlacementDir.Begin; ePlacementDir < EPlacementDir.Max; ++ePlacementDir)
		{
			DirEntity dirEntity = new DirEntity();
			dirEntity.placementDir = ePlacementDir;

			dirEntityList.Add(dirEntity);
		}

		List<EPlacementDir> placementDirList = new List<EPlacementDir>();

		while (0 != dirEntityList.Count)
		{
			int randIndex = Random.Range(0, dirEntityList.Count);
			placementDirList.Add(dirEntityList[randIndex].placementDir);
			dirEntityList.RemoveAt(randIndex);
		}

		return placementDirList;
	}

	protected List<PosEntity> DecideButtonSegmentPlacementPosition(int pButtonCount, int pMaxSlotCount)
	{
		if (pButtonCount > pMaxSlotCount)
		{
			return null;
		}

		List<PosEntity> posEntityList = null;

		switch (pButtonCount)
		{
			case 2:
				{
					posEntityList = new List<PosEntity>();

					for (int endSlotIndex = 1; endSlotIndex < pMaxSlotCount; ++endSlotIndex)
					{
						PosEntity posEntity = new PosEntity();
						posEntity.slotOffsetList.Add(0);
						posEntity.slotOffsetList.Add(endSlotIndex);

						posEntityList.Add(posEntity);
					}
				}
				break;

			case 3:
				{
					posEntityList = new List<PosEntity>();

					for (int middleSlotIndex = 1; middleSlotIndex < pMaxSlotCount - 1; ++middleSlotIndex)
					{
						for (int endSlotIndex = middleSlotIndex + 1; endSlotIndex < pMaxSlotCount; ++endSlotIndex)
						{
							PosEntity posEntity = new PosEntity();
							posEntity.slotOffsetList.Add(0);
							posEntity.slotOffsetList.Add(middleSlotIndex);
							posEntity.slotOffsetList.Add(endSlotIndex);

							posEntityList.Add(posEntity);
						}
					}
				}
				break;
		}

		List<PosEntity> mixedPosEntityList = null;

		if (null != posEntityList)
		{
			mixedPosEntityList = new List<PosEntity>();

			while (0 != posEntityList.Count)
			{
				int randIndex = Random.Range(0, posEntityList.Count);
				mixedPosEntityList.Add(posEntityList[randIndex]);
				posEntityList.RemoveAt(randIndex);
			}
		}

		return mixedPosEntityList;
	}

	protected bool HasBlock(int pCurSlotX, int pCurSlotY, EPlacementDir pPlacementDir, PosEntity pPosEntity, PlacedButtonInfo[] pPlacedButtonInfoList)
	{
		int slotIndexMin = 0;
		int slotIndexMax = 0;

		foreach( int slotOffset in pPosEntity.slotOffsetList )
		{
			if (slotOffset < slotIndexMin)
			{
				slotIndexMin = slotOffset;
			}

			if (slotOffset > slotIndexMax)
			{
				slotIndexMax = slotOffset;
			}
		}

		for (int slotIndex = slotIndexMin; slotIndex <= slotIndexMax; ++slotIndex)
		{
			int nextX = pCurSlotX + slotIndex * _dirLeanX[(int)pPlacementDir];

			if (nextX < 0 || nextX >= _planeWidth)
			{
				return false;
			}

			int nextY = pCurSlotY + slotIndex * _dirLeanY[(int)pPlacementDir];

			if (nextY < 0 || nextY >= _planeHeight)
			{
				return false;
			}

			int nextIndex = nextY * _planeWidth + nextX;

			PlacedButtonInfo placedButtonInfo = pPlacedButtonInfoList[nextIndex];

			if (null != placedButtonInfo)
			{
				if (PlacementType.Block == placedButtonInfo.placementType)
				{
					return false;
				}
			}
		}

		return true;
	}


    protected void Crc16_calc_work(ref byte[] ret, byte[] sp, uint sln)
    {
        uint crc_sum = 0;
 
        crc_sum = crc_calc(ref sp, sln);
 
        ret[0] = (byte)((crc_sum >> 12) & 0x0f);
        ret[1] = (byte)((crc_sum >> 8) & 0x0f);
        ret[2] = (byte)((crc_sum >> 4) & 0x0f);
        ret[3] = (byte)((crc_sum) & 0x0f);
    }

	protected uint crc_calc(ref byte[] sp, uint sln)
    {
        uint i, crc_sum = 0;
 
        for (i = 0; i < sln; i++)
        {
            crc_sum = crc_1byte_calc(sp[i], crc_sum);
        }
        return crc_sum;
    }

	protected uint crc_1byte_calc(byte sd, uint crcd)
    {
        byte i, ch;
        uint crce;
 
        for (i = 0; i < 8; i++)
        {
            crce = crcd >> 1;
            if ((sd & 0x01) == 0x01)
            {                              /* high */
                if ((crcd & 0x0001) == 0) ch = 1;
                else ch = 0;
            }
            else
            {                                     /* low */
                if ((crcd & 0x0001) == 0x0001) ch = 1;
                else ch = 0;
            }
            if (ch == 1)
            {                                 /* high */
                crce |= 0x8000;
                if ((crcd & 0x0010) == 0) crce |= 0x0008;
                else crce &= 0xfff7;
                if ((crcd & 0x0800) == 0) crce |= 0x0400;
                else crce &= 0xfbff;
            }
            else
            {
                crce &= 0x7fff;
                if ((crcd & 0x0010) == 0x0010) crce |= 0x0008;
                else crce &= 0xfff7;
                if ((crcd & 0x0800) == 0x0800) crce |= 0x0400;
                else crce &= 0xfbff;
            }
            sd = (byte)(sd >> 1);
            crcd = crce;
        }
        return crcd;
    }

}
