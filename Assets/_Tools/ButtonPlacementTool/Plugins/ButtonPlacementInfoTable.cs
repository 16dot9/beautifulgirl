﻿using System.IO;
using System.Collections;
using System.Collections.Generic;


public class ButtonPlacementInfoTable
{

	public class ButtonPlacementInfo
	{
		public int													iD = 0;
		public string												attr1 = "";
		public string												attr2 = "";
		public int													planeWidth = 0;
		public int													planeHeight = 0;
		public List<AlgorithmOfPlacingButtons.PlacedBlockInfo>		placedBlockInfoList = new List<AlgorithmOfPlacingButtons.PlacedBlockInfo>();
		public List<AlgorithmOfPlacingButtons.ButtonTypeInfo>		buttonTypeInfoList = new List<AlgorithmOfPlacingButtons.ButtonTypeInfo>();

		public AlgorithmOfPlacingButtons.PlacedButtonInfo[]			placedButtonInfoList = null;
	}

	public string													Error = "";


	protected enum CommandType
	{
		Invalid,

		New,
		Id,
		Attr1,
		Attr2,
		PlaneSize,
		ButtonTypeInfo,
		PI,
		End
	}


	protected Dictionary<int, ButtonPlacementInfo>					_buttonPlacementInfoList = new Dictionary<int, ButtonPlacementInfo>();


	public IEnumerable GetButtonPlacementInfo()
	{
		foreach (ButtonPlacementInfo buttonPlacementInfo in _buttonPlacementInfoList.Values)
		{
			yield return buttonPlacementInfo;
		}
	}

	public ButtonPlacementInfo Find(int pID)
	{
		ButtonPlacementInfo buttonPlacementInfo;

		if (_buttonPlacementInfoList.TryGetValue(pID, out buttonPlacementInfo))
		{
			return buttonPlacementInfo;
		}

		return null;
	}

	public void Add(ButtonPlacementInfo pButtonPlacementInfo)
	{
		_buttonPlacementInfoList.Add(pButtonPlacementInfo.iD, pButtonPlacementInfo);
	}

	public void Remove(ButtonPlacementInfo pButtonPlacementInfo)
	{
		_buttonPlacementInfoList.Remove(pButtonPlacementInfo.iD);
	}


	public void SetAttr1(ButtonPlacementInfo pButtonPlacementInfo, string pAttr)
	{
		pButtonPlacementInfo.attr1 = pAttr;
	}

	public void SetAttr2(ButtonPlacementInfo pButtonPlacementInfo, string pAttr)
	{
		pButtonPlacementInfo.attr2 = pAttr;
	}


	public IEnumerable GetBlock(ButtonPlacementInfo pButtonPlacementInfo)
	{
		foreach (AlgorithmOfPlacingButtons.PlacedBlockInfo placedBlockInfo in pButtonPlacementInfo.placedBlockInfoList)
		{
			yield return placedBlockInfo;
		}
	}

	public bool IsBlock(ButtonPlacementInfo pButtonPlacementInfo, int pBlockSlotIndex)
	{
		foreach (AlgorithmOfPlacingButtons.PlacedBlockInfo placedBlockInfo in pButtonPlacementInfo.placedBlockInfoList)
		{
			if (placedBlockInfo.blockSlotIndex == pBlockSlotIndex)
			{
				return true;
			}
		}

		return false;
	}

	public void AddBlock(ButtonPlacementInfo pButtonPlacementInfo, int pBlockSlotIndex)
	{
		foreach (AlgorithmOfPlacingButtons.PlacedBlockInfo placedBlockInfo in pButtonPlacementInfo.placedBlockInfoList)
		{
			if (placedBlockInfo.blockSlotIndex == pBlockSlotIndex)
			{
				return;
			}
		}

		AlgorithmOfPlacingButtons.PlacedBlockInfo blockInfo = new AlgorithmOfPlacingButtons.PlacedBlockInfo();
		blockInfo.blockSlotIndex = pBlockSlotIndex;

		pButtonPlacementInfo.placedBlockInfoList.Add(blockInfo);
	}

	public void RemoveBlock(ButtonPlacementInfo pButtonPlacementInfo, int pBlockSlotIndex)
	{
		foreach (AlgorithmOfPlacingButtons.PlacedBlockInfo placedBlockInfo in pButtonPlacementInfo.placedBlockInfoList)
		{
			if (placedBlockInfo.blockSlotIndex == pBlockSlotIndex)
			{
				pButtonPlacementInfo.placedBlockInfoList.Remove(placedBlockInfo);

				return;
			}
		}
	}


	public IEnumerable GetButtonType(ButtonPlacementInfo pButtonPlacementInfo)
	{
		foreach (AlgorithmOfPlacingButtons.ButtonTypeInfo buttonTypeInfo in pButtonPlacementInfo.buttonTypeInfoList)
		{
			yield return buttonTypeInfo;
		}
	}

	public void AddButtonType(ButtonPlacementInfo pButtonPlacementInfo, AlgorithmOfPlacingButtons.ButtonTypeInfo pButtonTypeInfo)
	{
		pButtonPlacementInfo.buttonTypeInfoList.Add(pButtonTypeInfo);
	}

	public void RemoveButtonType(ButtonPlacementInfo pButtonPlacementInfo, AlgorithmOfPlacingButtons.ButtonTypeInfo pButtonTypeInfo)
	{
		pButtonPlacementInfo.buttonTypeInfoList.Remove(pButtonTypeInfo);
	}

	public void RemoveAllButtonType(ButtonPlacementInfo pButtonPlacementInfo)
	{
		pButtonPlacementInfo.buttonTypeInfoList.Clear();
	}

	public void AutoBalancingButtonType(ButtonPlacementInfo pButtonPlacementInfo)
	{
		int buttonTypeCount = pButtonPlacementInfo.buttonTypeInfoList.Count;

		if (buttonTypeCount <= 0)
		{
			return;
		}

		int averageBalanceValue = 100 / buttonTypeCount;
		int adjustBalnaceValue = 100 - averageBalanceValue * buttonTypeCount;

		for (int i = 0; i < buttonTypeCount; ++i)
		{
			pButtonPlacementInfo.buttonTypeInfoList[i].coeffBTBV = averageBalanceValue;
		}

		pButtonPlacementInfo.buttonTypeInfoList[0].coeffBTBV = pButtonPlacementInfo.buttonTypeInfoList[0].coeffBTBV + adjustBalnaceValue;
	}


	public AlgorithmOfPlacingButtons.PlacedButtonInfo[] GetPlacedButtonInfoList(ButtonPlacementInfo pButtonPlacementInfo)
	{
		if (null == pButtonPlacementInfo)
		{
			return null;
		}

		return pButtonPlacementInfo.placedButtonInfoList;
	}


	public bool PlacementButtons(ButtonPlacementInfo pButtonPlacementInfo)
	{
		AlgorithmOfPlacingButtons algorithm = new AlgorithmOfPlacingButtons();

		if (!algorithm.SetInputData(pButtonPlacementInfo.planeWidth, pButtonPlacementInfo.planeHeight, pButtonPlacementInfo.placedBlockInfoList.ToArray(), pButtonPlacementInfo.buttonTypeInfoList.ToArray()))
		{
			return false;
		}

		pButtonPlacementInfo.placedButtonInfoList = algorithm.CalculateButtonPlacementInfo();

		return null == pButtonPlacementInfo.placedButtonInfoList ? false : true;
	}


	public bool Load(string pPath)
	{
		_buttonPlacementInfoList.Clear();

		StreamReader reader = File.OpenText(pPath);

		if (null == reader)
		{
			Error = string.Format("파일이 존재하지 않습니다.");

			return false;
		}

		bool isReading = true;

		int lineCount = 0;
		ButtonPlacementInfo buttonPlacementInfo = null;
		int curPlacedButtonPosY = 0;

		while (isReading)
		{
			if (reader.EndOfStream)
			{
				break;
			}

			lineCount++;

			string line = reader.ReadLine();

			if (string.IsNullOrEmpty(line))
			{
				continue;
			}

			string[] lineParser = line.Split(',');

			if (lineParser[0].Trim() == CommandType.New.ToString())
			{
				curPlacedButtonPosY = 0;
				buttonPlacementInfo = new ButtonPlacementInfo();
			}
			if (lineParser[0].Trim() == CommandType.Id.ToString())
			{
				buttonPlacementInfo.iD = int.Parse(lineParser[1].Trim());
			}
			if (lineParser[0].Trim() == CommandType.Attr1.ToString())
			{
				buttonPlacementInfo.attr1 = lineParser[1].Trim();
			}
			if (lineParser[0].Trim() == CommandType.Attr2.ToString())
			{
				buttonPlacementInfo.attr2 = lineParser[1].Trim();
			}
			if (lineParser[0].Trim() == CommandType.PlaneSize.ToString())
			{
				buttonPlacementInfo.planeWidth = int.Parse(lineParser[1].Trim());
				buttonPlacementInfo.planeHeight = int.Parse(lineParser[2].Trim());
			}
			if (lineParser[0].Trim() == CommandType.ButtonTypeInfo.ToString())
			{
				AlgorithmOfPlacingButtons.ButtonTypeInfo buttonTypeInfo = new AlgorithmOfPlacingButtons.ButtonTypeInfo();
				buttonTypeInfo.buttonID = int.Parse(lineParser[1].Trim());
				buttonTypeInfo.coeffBTBV = int.Parse(lineParser[2].Trim());

				buttonPlacementInfo.buttonTypeInfoList.Add(buttonTypeInfo);
			}
			if (lineParser[0].Trim() == CommandType.PI.ToString())
			{
				if (null == buttonPlacementInfo.placedButtonInfoList)
				{
					buttonPlacementInfo.placedButtonInfoList = new AlgorithmOfPlacingButtons.PlacedButtonInfo[buttonPlacementInfo.planeWidth * buttonPlacementInfo.planeHeight];
				}

				for (int x = 0; x < buttonPlacementInfo.planeWidth; ++x)
				{
					int slotIndex = x + curPlacedButtonPosY * buttonPlacementInfo.planeWidth;

					string info = lineParser[x + 1].Trim();

					if (string.IsNullOrEmpty(info))
					{
						buttonPlacementInfo.placedButtonInfoList[slotIndex] = null;
					}
					else
					{
						string [] split = info.Split('_');

						AlgorithmOfPlacingButtons.PlacementType placementType = (AlgorithmOfPlacingButtons.PlacementType)System.Enum.Parse(typeof(AlgorithmOfPlacingButtons.PlacementType), split[0].Trim());
						int iD = int.Parse(split[1].Trim());

						if (AlgorithmOfPlacingButtons.PlacementType.Block == placementType)
						{
							AlgorithmOfPlacingButtons.PlacedBlockInfo placedBlockInfo = new AlgorithmOfPlacingButtons.PlacedBlockInfo();
							placedBlockInfo.blockSlotIndex = slotIndex;

							buttonPlacementInfo.placedBlockInfoList.Add(placedBlockInfo);
						}

						AlgorithmOfPlacingButtons.PlacedButtonInfo placedButtonInfo = new AlgorithmOfPlacingButtons.PlacedButtonInfo();
						placedButtonInfo.placementType = placementType;
						placedButtonInfo.iD = iD;

						buttonPlacementInfo.placedButtonInfoList[slotIndex] = placedButtonInfo;
					}
				}

				curPlacedButtonPosY++;
			}
			if (lineParser[0].Trim() == CommandType.End.ToString())
			{
				if (_buttonPlacementInfoList.ContainsKey(buttonPlacementInfo.iD))
				{
					Error = string.Format("[Line : {0}] {1} 아이디 중복", lineCount, buttonPlacementInfo.iD);

					reader.Close();

					return false;
				}

				_buttonPlacementInfoList.Add(buttonPlacementInfo.iD, buttonPlacementInfo);

				buttonPlacementInfo = null;
				curPlacedButtonPosY = 0;
			}
		}

		if (null != buttonPlacementInfo)
		{
			Error = string.Format("{0} New / And 쌍이 맞지 않습니다.", buttonPlacementInfo.iD);

			reader.Close();

			return false;
		}

		reader.Close();

		return true;
	}

	public bool Save(string pPath)
	{
		StreamWriter writer = File.CreateText(pPath);

		if (null == writer)
		{
			Error = string.Format("파일을 쓸 수 없습니다.");

			return false;
		}

		string line;

		foreach ( ButtonPlacementInfo buttonPlacementInfo in _buttonPlacementInfoList.Values )
		{
			line = CommandType.New.ToString();
			writer.WriteLine(line);

			line = string.Format("{0},{1}", CommandType.Id.ToString(), buttonPlacementInfo.iD.ToString());
			writer.WriteLine(line);

			line = string.Format("{0},{1}", CommandType.Attr1.ToString(), buttonPlacementInfo.attr1);
			writer.WriteLine(line);

			line = string.Format("{0},{1}", CommandType.Attr2.ToString(), buttonPlacementInfo.attr2);
			writer.WriteLine(line);

			line = string.Format("{0},{1},{2}", CommandType.PlaneSize.ToString(), buttonPlacementInfo.planeWidth.ToString(), buttonPlacementInfo.planeHeight.ToString());
			writer.WriteLine(line);

			for (int i = 0; i < buttonPlacementInfo.buttonTypeInfoList.Count; ++i)
			{
				line = string.Format("{0},{1},{2}", CommandType.ButtonTypeInfo.ToString(), buttonPlacementInfo.buttonTypeInfoList[i].buttonID.ToString(), buttonPlacementInfo.buttonTypeInfoList[i].coeffBTBV.ToString());
				writer.WriteLine(line);
			}

			for (int y = 0; y < buttonPlacementInfo.planeHeight; ++y)
			{
				string param = "";

				for (int x = 0; x < buttonPlacementInfo.planeWidth; ++x)
				{
					int slotIndex = x + y * buttonPlacementInfo.planeWidth;

					param += string.Format(",{0}_{1}", buttonPlacementInfo.placedButtonInfoList[slotIndex].placementType.ToString(), buttonPlacementInfo.placedButtonInfoList[slotIndex].iD.ToString());
				}

				line = string.Format("{0}{1}", CommandType.PI.ToString(), param);
				writer.WriteLine(line);
			}

			line = CommandType.End.ToString();
			writer.WriteLine(line);
		}

		writer.Flush();

		writer.Close();

		return true;
	}

	public bool Export(string pPath)
	{
		List<PlacementInfo> placementInfoList = new List<PlacementInfo>();

		foreach (ButtonPlacementInfo buttonPlacementInfo in _buttonPlacementInfoList.Values)
		{
			PlacementInfo placementInfo = new PlacementInfo();

			placementInfo.si = (uint)buttonPlacementInfo.iD;
			placementInfo.attr_1 = buttonPlacementInfo.attr1;
			placementInfo.attr_2 = buttonPlacementInfo.attr2;
			placementInfo.plane_width = buttonPlacementInfo.planeWidth;
			placementInfo.plane_height = buttonPlacementInfo.planeHeight;
			placementInfo.placed_button_info_list = buttonPlacementInfo.placedButtonInfoList;

			placementInfoList.Add(placementInfo);
		}

		PlacementInfoMgr placementInfoMgr = new PlacementInfoMgr();

		placementInfoMgr.SetData(placementInfoList);

		if ( !placementInfoMgr.Save(pPath))
		{
			Error = string.Format("파일 익스포트 실패");

			return false;
 		}

		return true;
	}

}
