﻿using UnityEngine;
using System.Collections;
using System.Text;

public class KakaoGameFriendsView : KakaoBaseView {
	private Vector2 scrollPosition;
	
	public KakaoGameFriendsView() : base(KakaoViewType.GameFriends) {
	}
	
	override public void Render() {
		if( GUI.Button(new Rect(0,0,Screen.width,buttonHeight),"Go to Leaderboard Main")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Leaderboard);
		}
		
		int leaderFriendsCount = KakaoGameFriends.Instance.leaderboardFriends.Count;
		int kakaotalkFriendsCount = KakaoGameFriends.Instance.kakaotalkFriends.Count;
		
		int scrollContentsWidth = Screen.width-30;
		scrollPosition = GUI.BeginScrollView( new Rect(0,buttonHeight,Screen.width-10,Screen.height-buttonHeight), scrollPosition, new Rect(0,0,scrollContentsWidth,leaderFriendsCount*buttonHeight+kakaotalkFriendsCount*buttonHeight+buttonHeight*2));
		{
			int y = 0;
			
			GUI.Label(new Rect(0,0,scrollContentsWidth,buttonHeight), "Leaderboard Friends");
			
			
			foreach( var pair in KakaoGameFriends.Instance.leaderboardFriends ) {
				KakaoGameFriends.LeaderboardFriend leaderboardFriend = pair.Value;
				if( leaderboardFriend==null )
					continue;
				if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), leaderboardFriend.nickname)==true ) {
					string publicData = "itemId|0000101";
					KakaoNativeExtension.Instance.sendGameMessage(
						leaderboardFriend.userid,
						"Hi there! Would you play game with me?",
						"This is game message. You can check this message in game.",
						1,
						null,
						Encoding.UTF8.GetBytes(publicData),
						onSendGameMessageComplete,
						onSendGameMessageError);
				}
			}
			
			GUI.Label(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), "Friens");
			
			foreach( var pair in KakaoGameFriends.Instance.kakaotalkFriends ) {
				KakaoGameFriends.KakaotalkFriend kakaotalkFriend = pair.Value;
				if( kakaotalkFriend==null )
					continue;
				
				if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), kakaotalkFriend.nickname)==true ) {					
					KakaoNativeExtension.Instance.sendInviteGameMessage(kakaotalkFriend.userid,"Hi there! Would you play game with me?",null,onSendInviteGameMessageComplete, onSendInviteGameMessageError);
				}
			}
		}
		
		GUI.EndScrollView();
	}
	
	private void onSendGameMessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendGameMessage");
	}
	private void onSendGameMessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onSendInviteGameMessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendInviteGameMessage");
	}
	private void onSendInviteGameMessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
}
