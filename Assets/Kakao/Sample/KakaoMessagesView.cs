﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KakaoMessagesView : KakaoBaseView {
	
	private Vector2 scrollPosition = Vector2.zero;
	
	public KakaoMessagesView() : base(KakaoViewType.Messages) {
	}
	
	override public void Render() {
		if( GUI.Button(new Rect(0,0,Screen.width,buttonHeight),"Go to Leaderboard")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Leaderboard);
		}
		
		int messageCount = KakaoGameMessages.Instance.gameMessages.Count;
		int scrollContentsWidth = Screen.width-30;
		scrollPosition = GUI.BeginScrollView( new Rect(0,buttonHeight,Screen.width-10,Screen.height-buttonHeight), scrollPosition, new Rect(0,0,scrollContentsWidth,messageCount*buttonHeight));
		{
			int y = 0;
			KakaoGameMessages.GameMessage gameMessage = null;
			foreach( var pair in KakaoGameMessages.Instance.gameMessages ) {
				gameMessage = pair.Value;
				if( gameMessage==null )
					continue;
				
				if( GUI.Button(new Rect(0,y,scrollContentsWidth,buttonHeight), string.Format("{0} : {1}",gameMessage.senderNickName, gameMessage.message==null?"empty message":gameMessage.message))==true ) {
					KakaoNativeExtension.Instance.acceptGameMessage(gameMessage.messageId, onAcceptGameMessageComplete, onAcceptGameMessageError);
				}
				
				y+=buttonHeight;
			}
		}		
		GUI.EndScrollView();
	}
	
	private void onAcceptGameMessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Completed accept message!");
	}
	private void onAcceptGameMessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
		
}
