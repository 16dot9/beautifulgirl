﻿using UnityEngine;
using System.Collections;

public class KakaoLoginView : KakaoBaseView {
	
	public KakaoLoginView() : base(KakaoViewType.Login) {
	}
	
	
	override public void Render() {
		int y = 0;
		if( GUI.Button(new Rect(0, y, Screen.width, buttonHeight),"Login with KakaoTalk")==true ) {
			KakaoNativeExtension.Instance.Login(onLoginComplete, onLoginError);
		}
		if( GUI.Button(new Rect(0, y+=buttonHeight, Screen.width, buttonHeight),"Login with Account Webview")==true ) {
			KakaoResponseHandler.Instance.loginComplete = this.onLoginComplete;
			KakaoResponseHandler.Instance.loginError = this.onLoginError;
			KakaoNativeExtension.Instance.LoginWithWebview(onLoginComplete, onLoginError);
		}
	}
	
	private void onLoginComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Login Success!");
	}
	
	private void onLoginError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
}
