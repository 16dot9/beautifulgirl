﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KakaoMainView : KakaoBaseView {
	
	public KakaoMainView() : base(KakaoViewType.Main) {
	}
	
	private Vector2 scrollPosition;
	override public void Render() {
		int y = 0;
		
		int scrollContentsWidth = Screen.width-30;
		scrollPosition = GUI.BeginScrollView( new Rect(0,0,Screen.width-10,Screen.height), scrollPosition, new Rect(0,0,scrollContentsWidth,buttonHeight*9));
		
		if( GUI.Button(new Rect(0,y,scrollContentsWidth,buttonHeight),"LocalUser")==true ) {
			KakaoNativeExtension.Instance.LocalUser(onLocalUserComplete, onLocalUserError);
		}
		
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Friends")==true ) {
			KakaoNativeExtension.Instance.Friends(onFriendsComplete, onFriendsError);
		}
		
		GUI.enabled = KakaoFriends.Instance.hasFriends();
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"SendMessage")==true ) {
			
			KakaoSample.Instance.moveToView(KakaoViewType.Friends);
			KakaoBaseView currentView = KakaoSample.Instance.getCurrentView();
			if( currentView.type==KakaoViewType.Friends ) {
				((KakaoFriendsView)currentView).messageType = KakaoMessageType.Message;
			}
		}
		
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"SendImaGemessage")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Friends);
			KakaoBaseView currentView = KakaoSample.Instance.getCurrentView();
			if( currentView.type==KakaoViewType.Friends ) {
				((KakaoFriendsView)currentView).messageType = KakaoMessageType.ImaGemessage;
			}
		}
		
		GUI.enabled = true;
		
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Post To KakaoStroy")==true ) {
			string fileName = "capture_for_post_story.png";
			string imagePath = Application.persistentDataPath+"/"+fileName;
			ScreenCapture.CaptureScreenshot(fileName);
			KakaoNativeExtension.Instance.PostToKakaoStory("This is testing",imagePath,"itemid=03", onPostStoryComplete, onPostStoryError);
		}

		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Logout")==true ) {
			KakaoNativeExtension.Instance.Logout(onLogoutComplete, onLogoutError);
		}
		
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Unregister")==true ) {
			KakaoNativeExtension.Instance.Unregister(onUnregisterComplete, onUnregisterError);
		}
		
		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Leaderboard Sample")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Leaderboard);
		}

		if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight),"Test")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Test);
		}
		
		GUI.EndScrollView();
	}
	
	private void onPostStoryComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Complete post story!");
	}
	
	private void onPostStoryError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onLocalUserComplete() {
		
		/*
		 * please below propery after called localUserComplete
		 */
		string nickName 		= KakaoLocalUser.Instance.nickName;
		string hashedTalkUserId = KakaoLocalUser.Instance.hashedTalkUserId;
		string userId			= KakaoLocalUser.Instance.userId;
		string profileImageUrl 	= KakaoLocalUser.Instance.profileImageUrl;
		string countryIso 		= KakaoLocalUser.Instance.countryIso;
		bool messageBlocked 	= KakaoLocalUser.Instance.messageBlocked;
		
		string alertMessage = "";
		
		if( nickName!=null && nickName.Length>0 ) {
			alertMessage += "nickName : ";
			alertMessage += nickName;
			alertMessage += "\n";
		}
		
		if( hashedTalkUserId!=null && hashedTalkUserId.Length>0 ) {
			alertMessage += "hashedTalkUserId :";
			alertMessage += hashedTalkUserId;
			alertMessage += "\n";
		}
		
		if( userId!=null && userId.Length>0 ) {
			alertMessage += "userId :";
			alertMessage += userId;
			alertMessage += "\n";
		}
		
		if( profileImageUrl!=null && profileImageUrl.Length>0 ) {
			alertMessage += "profileImageUrl :";
			alertMessage += profileImageUrl;
			alertMessage += "\n";
		}
		
		if( countryIso!=null && countryIso.Length>0 ) {
			alertMessage += "countryIso :";
			alertMessage += countryIso;
			alertMessage += "\n";
		}
		
		alertMessage += (messageBlocked==true?"true":"false");
		
		////KakaoNativeExtension.Instance.ShowAlertMessage(alertMessage);
	}
	
	private void onLocalUserError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onFriendsComplete() 
	{
		// something to do
	}
	
	private void onFriendsError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onLogoutComplete() {
	}
	
	private void onLogoutError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onUnregisterComplete() {
	}
	
	private void onUnregisterError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
}
