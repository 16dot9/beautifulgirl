﻿using UnityEngine;
using System.Collections;

public enum KakaoViewType {
	Login,
	Main,
	Friends,
	Leaderboard,
	Messages,
	GameFriends,
	Test,
};

public abstract class KakaoBaseView  {
	public KakaoViewType type;
	static readonly protected int buttonHeight = 80;
	
	public KakaoBaseView(KakaoViewType _type) {
		type = _type;
	}
	
	protected void showAlertErrorMessage(string code, string message) {		
		string alertMessage = "";
		if( code!=null ) {
			alertMessage += ("Error Code : "+code);
		}
		
		if( message!=null ) {
			alertMessage += "\n";
			alertMessage += ("Error Message : "+message);
		}
		
		//KakaoNativeExtension.Instance.ShowAlertMessage(alertMessage);
	}
	
	abstract public void Render();
}
