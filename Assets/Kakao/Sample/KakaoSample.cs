﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;
using System;

public class KakaoSample : MonoBehaviour {
	private static KakaoSample _instance;
    public static KakaoSample Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(KakaoSample)) as KakaoSample;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "KakaoSample";
                    _instance = container.AddComponent(typeof(KakaoSample)) as KakaoSample;
					DontDestroyOnLoad(_instance);
                }
            }
            return _instance;
        }
    }
	
	private List<KakaoBaseView> viewList = new List<KakaoBaseView>();
	private KakaoBaseView currentView = null;
	public KakaoBaseView getCurrentView() { return currentView; }
	
	public void moveToView(KakaoViewType type) {
		
		foreach( KakaoBaseView sampleView in viewList ) {
			if(sampleView.type==type) {
				currentView = sampleView;				
				return;
			}
		}
		
		KakaoBaseView view = null;
		if( type==KakaoViewType.Login ) {
			view = new KakaoLoginView();
		}
		else if( type==KakaoViewType.Friends ) {
			view = new KakaoFriendsView();
		}
		else if( type==KakaoViewType.Main ) {
			view = new KakaoMainView();
		}
		else if( type==KakaoViewType.Leaderboard ) {
			view = new KakaoLeaderboardView();
		}
		else if( type==KakaoViewType.Messages ) {
			view = new KakaoMessagesView();
		}
		else if( type==KakaoViewType.GameFriends ) {
			view = new KakaoGameFriendsView();
		}
		else if( type==KakaoViewType.Test ) {
			view = new KakaoTestView();
		}
		
		if( view!=null ) {
			viewList.Add(view);
			currentView = view;
		}
	}
	
	void Start() {
		KakaoNativeExtension.Instance.Init(onInitComplete, onTokens);
	}
	void OnGUI() {
		if( viewList==null )
			return;
		
		if( currentView==null )
			return;
		
		currentView.Render();
	}
	
	private void onAuthorized(bool _authorized) {
		if( _authorized==true ) {
			////KakaoNativeExtension.Instance.ShowAlertMessage("Move to Main, Because Already finished Login Process!");
			KakaoSample.Instance.moveToView(KakaoViewType.Main);
		}
		else {
			KakaoSample.Instance.moveToView(KakaoViewType.Login);
		}
	}
	
	private void onInitComplete() {
		KakaoNativeExtension.Instance.Authorized(onAuthorized);
	}
	private void onTokens(string accessToken, string refreshToken) {
		KakaoNativeExtension.Instance.updateTokenCache(accessToken,refreshToken);
		
		if( KakaoNativeExtension.Instance.hasValidTokenCache()==true ) {
			moveToView(KakaoViewType.Main);
		}
		else {
			moveToView(KakaoViewType.Login);
		}
	}
}
