﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KakaoFriendsView : KakaoBaseView {
		
	public KakaoMessageType messageType = KakaoMessageType.Unknown;
	
	public KakaoFriendsView() : base(KakaoViewType.Friends) {
	}
	
	private Vector2 scrollPosition = Vector2.zero;
	
	override public void Render() {
		if( GUI.Button(new Rect(0,0,Screen.width,buttonHeight),"Go to Main")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Main);
		}
		
		int appFriendsCount = KakaoFriends.Instance.appFriends.Count;
		int friendsCount = KakaoFriends.Instance.friends.Count;
		
		int scrollContentsWidth = Screen.width-30;
		scrollPosition = GUI.BeginScrollView( new Rect(0,buttonHeight,Screen.width-10,Screen.height-buttonHeight), scrollPosition, new Rect(0,0,scrollContentsWidth,appFriendsCount*buttonHeight+friendsCount*buttonHeight+buttonHeight*2));
		{
			int y = 0;
			KakaoFriends.Friend friend = null;
			
			GUI.Label(new Rect(0,0,scrollContentsWidth,buttonHeight), "App Friends");
			
			for( int i=0; i<appFriendsCount; ++i ) {
				friend = KakaoFriends.Instance.appFriends[i];
				if( friend==null )
					continue;
				
				if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), friend.nickname)==true ) {
					
					friend = KakaoFriends.Instance.appFriends[i];
					
					if( messageType==KakaoMessageType.Message ) {
						KakaoNativeExtension.Instance.SendMessage("Message from Unity3D Plugin.",friend.userid,"itemid=01&count=1", onSendMessageComplete, onSendMessageError);
					}
					else if( messageType==KakaoMessageType.ImaGemessage ) {						
						string fileName = "capture_for_image_message.png";
						string imagePath = Application.persistentDataPath+"/"+fileName;
						ScreenCapture.CaptureScreenshot(fileName);
						
						Dictionary<string,string> metaInfo = new Dictionary<string, string>();
						metaInfo.Add("nickname","A Good Friend");
						KakaoNativeExtension.Instance.SendImageMessage("196",friend.userid,imagePath,"itemid=01&count=1",metaInfo, onSendImaGemessageComplete, onSendImaGemessageError);
					}
					else if( messageType==KakaoMessageType.GameMessage ) {
					}
				}
			}
			
			GUI.Label(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), "Friens");
			
			for( int i=0; i<friendsCount; ++i ) {
				friend = KakaoFriends.Instance.friends[i];
				if( friend==null )
					continue;
				
				if( GUI.Button(new Rect(0,y+=buttonHeight,scrollContentsWidth,buttonHeight), friend.nickname)==true ) {
					
					friend = KakaoFriends.Instance.friends[i];
					
					if( messageType==KakaoMessageType.Message ) {
						KakaoNativeExtension.Instance.SendMessage("Invite Message from Unity3D Plugin.",friend.userid,"itemid=01&count=2", onSendMessageComplete, onSendMessageError);
					}
					else if( messageType==KakaoMessageType.ImaGemessage ) {
						Dictionary<string,string> metaInfo = new Dictionary<string, string>();
						metaInfo.Add("nickname","A Good Friend");
						KakaoNativeExtension.Instance.SendInviteImageMessage("113",friend.userid,"itemid=02",metaInfo, onSendInviteImaGemessageComplete, onSendInviteImaGemessageError);
					}
					else if( messageType==KakaoMessageType.GameMessage ) {
					}
				}
			}
		}
		
		GUI.EndScrollView();
	}
	
	private void onSendMessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendMessage");
	}
	private void onSendMessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onSendImaGemessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendImaGemessage");
	}
	private void onSendImaGemessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
	
	private void onSendInviteImaGemessageComplete() {
		////KakaoNativeExtension.Instance.ShowAlertMessage("Succeed SendInviteImaGemessage");
	}
	private void onSendInviteImaGemessageError(string status, string message) {
		showAlertErrorMessage(status,message);
	}
}
