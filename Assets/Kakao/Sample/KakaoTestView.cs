﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KakaoTestView : KakaoBaseView {
	
	public KakaoTestView() : base(KakaoViewType.Test) {
	}
	
	private Vector2 scrollPosition;
	override public void Render() {
		int y = 0;
		
		int scrollContentsWidth = Screen.width-30;
		scrollPosition = GUI.BeginScrollView( new Rect(0,0,Screen.width-10,Screen.height), scrollPosition, new Rect(0,0,scrollContentsWidth,buttonHeight*9));
		
		if( GUI.Button(new Rect(0,0,Screen.width,buttonHeight),"Go to Main")==true ) {
			KakaoSample.Instance.moveToView(KakaoViewType.Main);
		}

		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 1")==true ) {
			KakaoNativeExtension.Instance.test(1);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 2")==true ) {
			KakaoNativeExtension.Instance.test(2);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 3")==true ) {
			KakaoNativeExtension.Instance.test(3);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 4")==true ) {
			KakaoNativeExtension.Instance.test(4);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 5")==true ) {
			KakaoNativeExtension.Instance.test(5);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 6")==true ) {
			KakaoNativeExtension.Instance.test(6);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 7")==true ) {
			KakaoNativeExtension.Instance.test(7);
		}
		if( GUI.Button(new Rect(0,y+=buttonHeight,Screen.width,buttonHeight),"Test 8")==true ) {
			KakaoNativeExtension.Instance.test(8);
		}
		
		GUI.EndScrollView();
	}
}
