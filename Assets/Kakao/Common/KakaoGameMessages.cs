﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoGameMessages {
	
	public class GameMessage {
		public string messageId 			{ get; private set; }
		public string senderId 				{ get; private set; }
		public string senderNickName 		{ get; private set; }
		public string senderProfileImageUrl { get; private set; }
		public int heart 					{ get; private set; }
		public byte[] data 					{ get; private set; }
		public string message 				{ get; private set; }
		public double sentAt 				{ get; private set; }
		public int messageCount 			{ get; private set; }
		
		public GameMessage(JSONNode gameMessage) {
			
			messageId 				= gameMessage[KakaoStringKeys.Parsers.Leaderboard.messageId].Value.ToString();
			senderId 				= gameMessage[KakaoStringKeys.Parsers.Leaderboard.senderId].Value.ToString();
			senderNickName 			= gameMessage[KakaoStringKeys.Parsers.Leaderboard.senderNickName].Value.ToString();
			senderProfileImageUrl 	= gameMessage[KakaoStringKeys.Parsers.Leaderboard.senderProfileImageUrl].Value.ToString();
			heart 					= gameMessage[KakaoStringKeys.Parsers.Leaderboard.heart].AsInt;
			
			message 				= gameMessage[KakaoStringKeys.Parsers.Leaderboard.message].Value.ToString();
			sentAt 					= gameMessage[KakaoStringKeys.Parsers.Leaderboard.sentAt].AsDouble;
			messageCount 			= gameMessage[KakaoStringKeys.Parsers.Leaderboard.messageCount].AsInt;

			string encodedData = gameMessage[KakaoStringKeys.Parsers.Leaderboard.data].Value.ToString();
			if( encodedData!=null && encodedData.Length>0 ) {
				data = System.Text.Encoding.UTF8.GetBytes(encodedData);
			}
		}
	}
	
	public Dictionary<string,GameMessage> gameMessages = new Dictionary<string,GameMessage>();
	
	static KakaoGameMessages _instance;
	
	public static KakaoGameMessages Instance {
        get {
            if (_instance == null) {
				_instance = new KakaoGameMessages();
            }
            return _instance;
        }
    }
	
	public void clearMessage() {
		gameMessages.Clear();
	}
	
	public void setGameMessagesFromJSON(JSONNode root) {
		clearMessage();

		JSONArray messages = root[KakaoStringKeys.Parsers.Leaderboard.messages].AsArray;
		foreach( JSONNode gameMessage in messages ) {
			if( gameMessage==null )
				continue;
			
			GameMessage message = new GameMessage(gameMessage);
			gameMessages.Add(message.messageId,message);
		}
	}
	public void updateGameMessagesFromJSON(JSONNode root) {
		string messageId = root[KakaoStringKeys.Parsers.Leaderboard.messageId].Value.ToString();
		if( messageId!=null && gameMessages.ContainsKey(messageId)==true ) {
			gameMessages.Remove(messageId);
		}
	}
}