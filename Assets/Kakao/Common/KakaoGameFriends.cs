﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;

public class KakaoGameFriends {
	
	public class LeaderboardFriend {
		public string nickname 			{ get; private set; }
		public string friendNickName 	{ get; private set; }
		public string userid 			{ get; private set; }
		public string profileImageUrl 	{ get; private set; }
		public int exp 					{ get; private set; }
		public double lastMessageSentAt { get; set; }
		public bool messageBlocked 		{ get; private set; }
		
		public int rank 				{ get; private set; }
		public int bestScore 			{ get; private set; }
		public int seasonScore 			{ get; private set; }
		public int lastSeasonScore 		{ get; private set; }
		public byte[] publicData 		{ get; private set; }
		
		public LeaderboardFriend(JSONNode friend) {
			friendNickName = friend[KakaoStringKeys.Parsers.friendNickName].Value.ToString();
			nickname = friend[KakaoStringKeys.Parsers.nickName].Value.ToString();
			profileImageUrl = friend[KakaoStringKeys.Parsers.profileImageUrl].Value.ToString();
			userid = friend[KakaoStringKeys.Parsers.userId].Value.ToString();
			
			exp = friend[KakaoStringKeys.Parsers.Leaderboard.exp].AsInt;
			lastMessageSentAt = friend[KakaoStringKeys.Parsers.Leaderboard.lastMessageSentAt].AsDouble;
			
			string blocked = friend[KakaoStringKeys.Parsers.messageBlocked].Value.ToString();
			if( blocked!=null && string.Equals(blocked,"true") )
				messageBlocked = true;
			else 
				messageBlocked = false;
			
			rank 			= friend[KakaoStringKeys.Parsers.Leaderboard.rank].AsInt;
			bestScore 		= friend[KakaoStringKeys.Parsers.Leaderboard.bestScore].AsInt;
			seasonScore 	= friend[KakaoStringKeys.Parsers.Leaderboard.seasonScore].AsInt;
			lastSeasonScore = friend[KakaoStringKeys.Parsers.Leaderboard.lastSeasonScore].AsInt;
			
			string publicDataString = friend[KakaoStringKeys.Parsers.Leaderboard.publicData].Value.ToString();
			if( publicDataString!=null && publicDataString.Length>0 ) {
				publicData = Encoding.UTF8.GetBytes(publicDataString);
			}
		}
	}
	
	public class KakaotalkFriend {
		public string nickname 			{ get; private set; }
		public string friendNickName 	{ get; private set; }
		public string userid 			{ get; private set; }
		public string profileImageUrl 	{ get; private set; }
		public bool supportedDevice 	{ get; private set; }
		
		public double lastMessageSentAt { get; set; }
		public bool messageBlocked 		{ get; private set; }
		
		public KakaotalkFriend(JSONNode friend) {
			friendNickName = friend[KakaoStringKeys.Parsers.friendNickName].Value.ToString();
			nickname = friend[KakaoStringKeys.Parsers.nickName].Value.ToString();
			profileImageUrl = friend[KakaoStringKeys.Parsers.profileImageUrl].Value.ToString();
			userid = friend[KakaoStringKeys.Parsers.userId].Value.ToString();
			
			lastMessageSentAt = friend[KakaoStringKeys.Parsers.Leaderboard.lastMessageSentAt].AsDouble;
			
			string blocked = friend[KakaoStringKeys.Parsers.messageBlocked].Value.ToString();
			if( blocked!=null && string.Equals(blocked,"true") )
				messageBlocked = true;
			else 
				messageBlocked = false;
			
			string supported = friend[KakaoStringKeys.Parsers.supportedDevice].Value.ToString();
			if( supported!=null && string.Equals(supported,"true") )
				supportedDevice = true;
			else 
				supportedDevice = false;
		}
	}
	
	static KakaoGameFriends _instance;
	public static KakaoGameFriends Instance
    {
        get
        {
            if (_instance == null)
            {
				_instance = new KakaoGameFriends();
            }

            return _instance;
        }
    }
	
	public Dictionary<string,LeaderboardFriend> leaderboardFriends = new Dictionary<string, LeaderboardFriend>();
	public Dictionary<string,KakaotalkFriend> kakaotalkFriends = new Dictionary<string, KakaotalkFriend>();
	
	public void setGameFriendFromJSON( JSONNode root ) {
		leaderboardFriends.Clear();
		kakaotalkFriends.Clear();
		
		JSONArray appFriendsInfo = root[KakaoStringKeys.Parsers.Leaderboard.appFriends].AsArray;
		
		int count = appFriendsInfo.Count;
		for( int i=0; i<count; ++i ) {
			JSONNode data = appFriendsInfo[i];
			if( data==null ) {
				Debug.LogWarning("app_friends_info is null");
				continue;
			}
			
			LeaderboardFriend friend = new LeaderboardFriend(data);
			leaderboardFriends.Add(friend.userid,friend);
		}
		
		JSONArray friendsInfo = root[KakaoStringKeys.Parsers.Leaderboard.friends].AsArray;
		count = friendsInfo.Count;
		for( int i=0; i<count; ++i ) {
			JSONNode data = friendsInfo[i];
			if( data==null ) {
				Debug.LogWarning("friends_info data is null");
				continue;
			}
			
			KakaotalkFriend friend = new KakaotalkFriend(data);
			kakaotalkFriends.Add(friend.userid,friend);
		}
	}
	
	public void updateGameFriendsWithJSON(JSONNode node) {
		string receiverId = node[KakaoStringKeys.Parsers.Leaderboard.receiverId].Value.ToString();
		double messageSentAt = node[KakaoStringKeys.Parsers.Leaderboard.messageSentAt].AsDouble;
		
		if( leaderboardFriends.ContainsKey(receiverId)==true ) {
			LeaderboardFriend friend = leaderboardFriends[receiverId];
			friend.lastMessageSentAt = messageSentAt;
			leaderboardFriends[receiverId] = friend;
			return;
		}
		
		if( kakaotalkFriends.ContainsKey(receiverId)==true ) {
			KakaotalkFriend friend = kakaotalkFriends[receiverId];
			friend.lastMessageSentAt = messageSentAt;
			kakaotalkFriends[receiverId] = friend;
			return;
		}
	}
		
	public void clearFriends()
	{
		if( leaderboardFriends!=null )
			leaderboardFriends.Clear();
		
		if( kakaotalkFriends!=null )
			kakaotalkFriends.Clear();
	}
	
	public void printToConsole() {
		Debug.Log("Print LeaderboardFriend Information.");
		foreach( var pair in leaderboardFriends ) {
			LeaderboardFriend friend = pair.Value;
			if( friend==null )
				continue;

			Debug.Log(string.Format("Name:{0} / Rank:{1} / Best Score:{2}",friend.nickname, friend.rank.ToString(), friend.bestScore.ToString()));
		}
		
		Debug.Log("Print KakaotalkFriend Information.");
		foreach( var pair in kakaotalkFriends ) {
			KakaotalkFriend friend = pair.Value;
			if( friend==null )
				continue;
			
			Debug.Log(string.Format("Name:{0} / messageBlocked:{1}",friend.nickname, friend.messageBlocked==true?"true":"false"));
		}
	}
	
	
	public LeaderboardFriend getLeaderboardFriend(string key) {
		if( leaderboardFriends==null )
			return null;
		
		if( key==null || key.Length==0 )
			return null;
		
		if( leaderboardFriends.ContainsKey(key)==false )
			return null;
		
		return leaderboardFriends[key];
	}
	
	public KakaotalkFriend getKakaotalkFriend(string key) {
		if( kakaotalkFriends==null )
			return null;
		
		if( key==null || key.Length==0 )
			return null;
		
		if( kakaotalkFriends.ContainsKey(key)==false )
			return null;
		
		return kakaotalkFriends[key];
	}
}
