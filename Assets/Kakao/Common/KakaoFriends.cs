﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoFriends {
	public class Friend
	{
		public string nickname { get; private set; }
		public string userid { get; private set; }
		public string profileImageUrl { get; private set; }
		public bool messageBlocked { get; private set; }
		
		public Friend(JSONNode friend) {
			nickname = friend[KakaoStringKeys.Parsers.nickName].Value.ToString();
			userid = friend[KakaoStringKeys.Parsers.userId].Value.ToString();
			profileImageUrl = friend[KakaoStringKeys.Parsers.profileImageUrl].Value.ToString();
			
			string messageBlockedString = friend[KakaoStringKeys.Parsers.messageBlocked].Value.ToString();
			if( messageBlockedString!=null && messageBlockedString.Equals("true")==true )
				messageBlocked = true;
			else 
				messageBlocked = false;
		}
	}
	
	static KakaoFriends _instance;
	public static KakaoFriends Instance
    {
        get
        {
            if (_instance == null)
            {
				_instance = new KakaoFriends();
            }

            return _instance;
        }
    }
	
	public List<Friend> appFriends = new List<Friend>();
	public List<Friend> friends = new List<Friend>();
	
	public void clearFriends()
	{
		if( appFriends!=null )
			appFriends.Clear();
		
		if( friends!=null )
			friends.Clear();
	}
	
	public bool hasFriends() {
		if( appFriends.Count>0 )
			return true;
		
		if( friends.Count>0 )
			return true;
		
		return false;
	}
	public void setFriendsFromJSON(JSONNode root)
	{
		
		appFriends.Clear();
		friends.Clear();
		
		Debug.Log("Parse \"app_friends_info\"!");
		JSONNode appFriendsInfo = root[KakaoStringKeys.Parsers.appFriendsInfo];
		
		int count = appFriendsInfo.Count;
		for( int i=0; i<count; ++i ) {
			JSONNode data = appFriendsInfo[i];
			if( data==null ) {
				Debug.LogWarning("app_friends_info is null");
				continue;
			}
			
			 
			Friend friend = new Friend(data);
			appFriends.Add(friend);
		}
		
		Debug.Log("Parse \"friends_info\"!");
		JSONNode friendsInfo = root[KakaoStringKeys.Parsers.friendsInfo];
		count = friendsInfo.Count;
		for( int i=0; i<count; ++i ) {
			JSONNode data = friendsInfo[i];
			if( data==null ) {
				Debug.LogWarning("friends_info data is null");
				continue;
			}
			
			Friend friend = new Friend(data);
			friends.Add(friend);
		}
	}
}
