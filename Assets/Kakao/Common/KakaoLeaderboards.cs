﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoLeaderboards {

	public class cGameFriendInfo
	{
		public string UserId;
		public string UserUrl;

		public List<int> _nLeaderBoard;
	}

	public class Leaderboard {

		public class Score {
			public int best_score;
			public int season_score;
			public int last_season_score;
			
			public Score( int bestScore, int seasonScore, int lastSeasonScore ) {
				best_score = bestScore;
				season_score = seasonScore;
				last_season_score = lastSeasonScore;
			}
		}
		
		public Dictionary<string, Score> scores = new Dictionary<string, Score>();
		
		public void setScore(string userId, int bestScore, int seasonScore, int lastSeasonScore) {
			if( scores.ContainsKey(userId)==true )
				scores.Remove(userId);
			scores.Add(userId, new Score(bestScore, seasonScore, lastSeasonScore));
		}
		
	}
	
	static KakaoLeaderboards _instance;
	public static KakaoLeaderboards Instance
    {
        get
        {
            if (_instance == null)
            {
				_instance = new KakaoLeaderboards();
            }

            return _instance;
        }
    }
	
	public Dictionary<string, KakaoLeaderboards.Leaderboard> leaderboards = new Dictionary<string, KakaoLeaderboards.Leaderboard>(StringComparer.OrdinalIgnoreCase);
	
	public void setLeaderboard(string key) {
		if( leaderboards.ContainsKey(key)==true ) {
			leaderboards.Remove(key);
		}
		
		Leaderboard leaderboard = new Leaderboard();
		leaderboards.Add(key, leaderboard);
	}

	/*
	public void updateLeaderboard(string key, int bestScore, int seasonScore, int lastSeasonScore ) {
		
		if( leaderboards.ContainsKey(key)==false ) {
			Debug.LogError("Can't find leaderboard!");
			return;
		}
		
		Leaderboard leaderboard = leaderboards[key];
		if( leaderboard!=null ) {
			leaderboard.best_score = bestScore;
			leaderboard.season_score = seasonScore;
			leaderboard.last_season_score = lastSeasonScore;
			leaderboards.Add(key,leaderboard);
		}
	}
	*/
	
	/*
	public class Leaderboard {
		public string name;
		public string key;
		
		public int best_score;
		public int season_score;
		public int last_season_score;
	}
	*/
	public void clear(string leaderboardKey)
	{
		if( leaderboards.ContainsKey(leaderboardKey)==false )
			return;
		
		leaderboards.Remove(leaderboardKey);
	}
	public void setLeaderboardsFromJSON(JSONNode root)
	{
		string leaderboardKey = root[KakaoStringKeys.Parsers.Leaderboard.leaderboardKey].Value.ToString();
		Leaderboard currentLeaderboard = null;
		if( leaderboards.ContainsKey(leaderboardKey)==false ) {
			currentLeaderboard = new Leaderboard();
		}
		else {
			currentLeaderboard = leaderboards[leaderboardKey];
		}
			
		if(leaderboardKey!=null && leaderboardKey.Length>0 )
			clear(leaderboardKey);
		
		JSONArray appFriends = root[KakaoStringKeys.Parsers.Leaderboard.appFriends].AsArray;
		for( int i=0; i<appFriends.Count; ++i ) {
			JSONNode appFriend = appFriends[i];
			if( appFriend==null )
				continue;
			
			string userId 		= appFriend[KakaoStringKeys.Parsers.userId].Value.ToString();
			int bestScore 		= appFriend[KakaoStringKeys.Parsers.Leaderboard.bestScore].AsInt;
			int lastSeasonScore = appFriend[KakaoStringKeys.Parsers.Leaderboard.lastSeasonScore].AsInt;
			int seasonScore 	= appFriend[KakaoStringKeys.Parsers.Leaderboard.seasonScore].AsInt;

			currentLeaderboard.setScore(userId, bestScore, seasonScore, lastSeasonScore);
		}
		
		leaderboards.Add(leaderboardKey, currentLeaderboard);
	}

	public cGameFriendInfo _cGameBase;
	public List<cGameFriendInfo> _cGameFriendInfo = new List<cGameFriendInfo>();

    private int _nIdxStage;

    public void printToConsole()
	{
        foreach (string key in leaderboards.Keys)
        {
            Debug.Log("***********************************************");
            Debug.Log(string.Format("Leaderboard Key : {0}", key));
            Leaderboard leaderboard = leaderboards[key];
            if (leaderboard == null && key == "DEFAULT")
                continue;

            Debug.Log("Scores in Leaderboard");

            foreach (string serviceUserId in leaderboard.scores.Keys)
            {
                if (serviceUserId == null && serviceUserId.Length == 0)
                    continue;

                Leaderboard.Score score = leaderboard.scores[serviceUserId];

                Debug.Log(string.Format("{1} and {0}'s scores : {2}/{3}/{4}", key, serviceUserId, score.season_score, score.best_score, score.last_season_score));

                int _nSeasonScore   = score.season_score;
                int _nBestScore     = score.best_score;
                int _LastScore      = score.last_season_score;

                string[] stidx = key.Split('_');

                int _Par = System.Convert.ToInt32(stidx[1]);

                int _RealStage = (_Par - 1);

                Debug.Log("_Par -1 :" + _RealStage);

                if (_nBestScore > 0)
                {
                    for (int i = 0; i < KakaoFriendsController.Instance._myKakaoKeypetFriends.Count; i++)
                    {
                        if (KakaoFriendsController.Instance._myKakaoKeypetFriends[i].userid == serviceUserId)
                        {
                            KakaoFriendsController.Instance._myKakaoKeypetFriends[i]._nLeaderboardStage[_RealStage] = _nBestScore;

                            Debug.Log("_RealStage :" + _RealStage);

                            KakaoFriendsController.Instance._bLeaderBoard = true;
                        }
                    }
                }
            }
        }
    }

    void FriendScoreSetting(int _BestScore, string _userid, string _idx) 
    {
    }
}
