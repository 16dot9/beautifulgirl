﻿using System.Collections;
using SimpleJSON;

public class KakaoParamTest : KakaoParamBase {
	
	int test;
	
	public KakaoParamTest(int _test) : base(KakaoAction.Test) {
		test = _test;
	}
	
	public override string getParamString() {
		JSONClass json 						= makeDefaultParam();
		json[KakaoStringKeys.Params.test]	= test.ToString();
		return json.ToString();
	}
}