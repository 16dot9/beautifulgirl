﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;

public class KakaoParamImageMessage : KakaoParamBase {

	
	private string templateId = null;
	private string receiverId = null;
	private string imagePath = null;
	private string executeUrl = null;
	private Dictionary<string,string> metaInfo = null;

    public KakaoParamImageMessage(string _templateId, string _receiverId, string _imagePath, string _executeUrl, Dictionary<string, string> _metaInfo) 
	: base(KakaoAction.SendImageMessage) 
	{
		templateId 	= _templateId;
		receiverId 	= _receiverId;
		imagePath 	= _imagePath;
		executeUrl 	= _executeUrl;
		metaInfo 	= _metaInfo;
	}
	
	public override string getParamString() {
		JSONClass json 					= makeDefaultParam();
		
		json[KakaoStringKeys.Params.templateId] = templateId;
		json[KakaoStringKeys.Params.receiverId] = receiverId;
		json[KakaoStringKeys.Params.imagePath] 	= imagePath;
		json[KakaoStringKeys.Params.executeUrl] = executeUrl;
		
		foreach (string key in metaInfo.Keys)
		{
			json[KakaoStringKeys.Params.metaInfo][key] = metaInfo[key];
		}
		
		return json.ToString();
	}
}
