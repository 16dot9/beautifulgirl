﻿using System.Collections;
using SimpleJSON;

public class KakaoParamBase {
	
	protected KakaoAction action;
	
	public KakaoAction getAction() { return action; }
	
	public KakaoParamBase(KakaoAction _action)
	{
		action = _action;
	}

	protected JSONClass makeDefaultParam() {
		JSONClass json 						= new JSONClass();
		json[KakaoStringKeys.Params.action]	= action.ToString();
		return json;
	}
	public virtual string getParamString() {
		JSONClass json = makeDefaultParam();
		return json.ToString();
	}
}
