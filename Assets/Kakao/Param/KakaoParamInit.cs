﻿using System.Collections;
using SimpleJSON;

public class KakaoParamInit : KakaoParamBase {
	
	public string accessToken 	= null;
	public string refreshToken 	= null;
		
	public KakaoParamInit(string _accessToken, string _refreshToken) : base(KakaoAction.Init) {
		accessToken = _accessToken;
		refreshToken = _refreshToken;
	}
	
	public override string getParamString() {
		JSONClass json 					= makeDefaultParam();
		
		if( accessToken!=null && accessToken.Length>0 )
			json[KakaoStringKeys.Params.access_token]	= accessToken;
		
		if( refreshToken!=null && refreshToken.Length>0 )
			json[KakaoStringKeys.Params.refresh_token] 	= refreshToken;
		
		return json.ToString();
	}
}
