﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class KakaoParamBlockMessage : KakaoParamBase {

	bool blockMessage;

	public KakaoParamBlockMessage(bool block) : base(KakaoAction.BlockMessage) 
	{
		blockMessage = block;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();
		json[KakaoStringKeys.Params.Leaderboard.block] 	= (blockMessage==true)?"true":"false";
		return json.ToString();
	}
}
