﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Text;

public class KakaoParamUpdateResult : KakaoParamBase {
	
	private string leaderboardKey = null;
	private int score = 0;
	private int exp = 0;
	private byte[] publicData = null;
	private byte[] privateData = null;
	
	public KakaoParamUpdateResult(string _leaderboardKey, int _score, int _exp, byte[] _publicData, byte[] _privateData) : base(KakaoAction.UpdateResult) 
	{
		leaderboardKey = _leaderboardKey;
		score = _score;
		exp = _exp;
		publicData = _publicData;
		privateData = _privateData;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();

		json[KakaoStringKeys.Params.Leaderboard.leaderboardKey] = leaderboardKey;
		json[KakaoStringKeys.Params.Leaderboard.score] = score.ToString();
		json[KakaoStringKeys.Params.Leaderboard.exp] = exp.ToString();
		
		if( publicData!=null )
			json[KakaoStringKeys.Params.Leaderboard.publicData] = Encoding.UTF8.GetString(publicData);
		if( privateData!=null )
			json[KakaoStringKeys.Params.Leaderboard.privateData] = Encoding.UTF8.GetString(privateData);		
		return json.ToString();
	}
}
