﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class KakaoParamInviteGameMessage : KakaoParamBase {
	
	private string receiverId;
	private string talkMessage;
	private string executeUrl;

	public KakaoParamInviteGameMessage(string _receiverId, string _talkMessage, string _executeUrl) : base(KakaoAction.SendInviteGameMessage) 
	{
		receiverId = _receiverId;
		talkMessage = _talkMessage;
		executeUrl = _executeUrl;
	}

	public override string getParamString() {
		JSONClass json = makeDefaultParam();

		if( receiverId!=null )
			json[KakaoStringKeys.Params.Leaderboard.receiverId] = receiverId;
		
		if( talkMessage!=null )
			json[KakaoStringKeys.Params.Leaderboard.talkMessage] = talkMessage;
		
		if( executeUrl!=null )
			json[KakaoStringKeys.Params.Leaderboard.executeUrl] = executeUrl;
		return json.ToString();
	}
}
