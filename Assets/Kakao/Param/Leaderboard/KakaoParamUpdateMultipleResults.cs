﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;

public class KakaoParamUpdateMultipleResults : KakaoParamBase {
	
	private Dictionary<string,int> scores = null;
	private int exp = 0;
	private byte[] publicData = null;
	private byte[] privateData = null;
		
	public KakaoParamUpdateMultipleResults(Dictionary<string,int> _scores, int _exp, byte[] _publicData, byte[] _privateData) : base(KakaoAction.UpdateMultipleResults) 
	{
		scores = _scores;
		exp = _exp;
		publicData = _publicData;
		privateData = _privateData;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();
		
		if( scores!=null ) {
			foreach( string key in scores.Keys ) {
				json[KakaoStringKeys.Params.Leaderboard.multipleLeaderboards][key] = scores[key].ToString();
			}
		}
		
		json[KakaoStringKeys.Params.Leaderboard.exp] = exp.ToString();
		
		if( publicData!=null )
			json[KakaoStringKeys.Params.Leaderboard.publicData] = System.Convert.ToBase64String(publicData);
		
		if( privateData!=null )
			json[KakaoStringKeys.Params.Leaderboard.privateData] = System.Convert.ToBase64String(privateData);
		
		return json.ToString();
	}
	
}
