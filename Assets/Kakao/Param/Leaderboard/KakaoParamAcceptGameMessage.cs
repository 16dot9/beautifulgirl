﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Text;

public class KakaoParamAcceptGameMessage : KakaoParamBase {

	string id;

	public KakaoParamAcceptGameMessage(string _id) : base(KakaoAction.AcceptGameMessage) 
	{
		id = _id;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();
		if( id!=null )
			json[KakaoStringKeys.Params.Leaderboard.messageId]= id;
		return json.ToString();
	}
}
