﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Text;

public class KakaoParamGameMessage : KakaoParamBase {

	private string receiverId;
	private string talkMessage;
	private string gameMessage;
	private int heart;
	private string executeUrl;
	private byte[] data;

	public KakaoParamGameMessage(string _receiverId, string _talkMessage, string _gameMessage, int _heart, string _executeUrl, byte[] _data) : base(KakaoAction.SendGameMessage) {
		receiverId = _receiverId;
		talkMessage = _talkMessage;
		gameMessage = _gameMessage;
		heart = _heart;
		executeUrl = _executeUrl;
		data = _data;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();

		json [KakaoStringKeys.Params.Leaderboard.receiverId] = receiverId;
		json [KakaoStringKeys.Params.Leaderboard.heart] = heart.ToString();
		
		if( talkMessage!=null )
			json [KakaoStringKeys.Params.Leaderboard.talkMessage] = talkMessage;
		
		if( gameMessage!=null )
			json [KakaoStringKeys.Params.Leaderboard.gameMessage] = gameMessage;

		if( executeUrl!=null )
			json [KakaoStringKeys.Params.Leaderboard.executeUrl] = executeUrl;
		
		if( data!=null )
			json [KakaoStringKeys.Params.Leaderboard.data] = Encoding.UTF8.GetString(data);

		return json.ToString();
	}

}
