﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Text;

public class KakaoParamUpdateUser : KakaoParamBase {
	
	private int additionalHeart = 0;
	private byte[] publicData = null;
	private byte[] privateData = null;
		
	public KakaoParamUpdateUser(int _additionalHeart, byte[] _publicData, byte[] _privateData) : base(KakaoAction.UpdateUser) 
	{
		additionalHeart = _additionalHeart;
		publicData = _publicData;
		privateData = _privateData;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();

		json[KakaoStringKeys.Params.Leaderboard.additionalHeart] = additionalHeart.ToString();
		json[KakaoStringKeys.Params.Leaderboard.currentHeart] = KakaoGameUserInfo.Instance.heart.ToString();
		
		if( publicData!=null )
			json[KakaoStringKeys.Params.Leaderboard.publicData] = Encoding.UTF8.GetString(publicData);
		if( privateData!=null )
			json[KakaoStringKeys.Params.Leaderboard.privateData] = Encoding.UTF8.GetString(privateData);
		
		return json.ToString();
	}
}
