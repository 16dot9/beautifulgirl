﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class KakaoParamUseHeart : KakaoParamBase {
	
	private int useHeart;
		
	public KakaoParamUseHeart(int _useHeart) : base(KakaoAction.UseHeart) 
	{
		useHeart = _useHeart;
	}

	public override string getParamString() {
		JSONClass json 							= makeDefaultParam();

		json[KakaoStringKeys.Params.Leaderboard.useHeart] = useHeart.ToString();
		return json.ToString();
	}
}
