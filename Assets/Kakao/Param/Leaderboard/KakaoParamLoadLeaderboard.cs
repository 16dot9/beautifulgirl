﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class KakaoParamLoadLeaderboard : KakaoParamBase {
	
	string leaderboardKey = null;
	
	public KakaoParamLoadLeaderboard(string _leaderboardKey) : base(KakaoAction.LoadLeaderboard) 
	{
		leaderboardKey = _leaderboardKey;
	}

	public override string getParamString() {
		JSONClass json 										= makeDefaultParam();
		
		if( leaderboardKey!=null )
			json[KakaoStringKeys.Params.Leaderboard.leaderboardKey] = leaderboardKey;
				
		return json.ToString();
	}
	
}
