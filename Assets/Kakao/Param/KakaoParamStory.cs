﻿using System.Collections;
using SimpleJSON;

public class KakaoParamStory : KakaoParamBase {
	private string message = null;
	private string imagePath = null;
	private string executeUrl = null;
	
	public KakaoParamStory(string _message, string _imagePath, string _executeUrl) 
	: base(KakaoAction.PostToKakaoStory) 
	{
		message 	= _message;
		imagePath 	= _imagePath;
		executeUrl 	= _executeUrl;
	}
	
	public override string getParamString() {
		JSONClass json 					= makeDefaultParam();
		
		json[KakaoStringKeys.Params.message] 	= message;
		json[KakaoStringKeys.Params.imagePath] 	= imagePath;
		json[KakaoStringKeys.Params.executeUrl] = executeUrl;
		
		return json.ToString();
	}
}
