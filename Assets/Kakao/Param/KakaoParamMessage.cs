﻿using System.Collections;
using SimpleJSON;

public class KakaoParamMessage : KakaoParamBase {
	
	private string message = null;
	private string receiverId = null;
	private string executeUrl = null;
	
	public KakaoParamMessage(string _message, string _receiverId, string _executeUrl) : base(KakaoAction.SendMessage) {
		message = _message;
		receiverId = _receiverId;
		executeUrl = _executeUrl;
	}
	
	public override string getParamString() {
		JSONClass json 						= makeDefaultParam();
		
		if( message!=null )
			json[KakaoStringKeys.Params.message] 	= message;
		
		if( receiverId!=null )
			json[KakaoStringKeys.Params.receiverId] = receiverId;
		
		if( executeUrl!=null )
			json[KakaoStringKeys.Params.executeUrl] = executeUrl;
		
		return json.ToString();
	}
}
