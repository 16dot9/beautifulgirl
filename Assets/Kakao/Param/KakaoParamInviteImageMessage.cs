﻿using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class KakaoParamInviteImageMessage : KakaoParamBase {
	
	private string templateId = null;
	private string receiverId = null;
	private string executeUrl = null;
	private Dictionary<string,string> metaInfo = null;

    public KakaoParamInviteImageMessage(string _templateId, string _receiverId, string _executeUrl, Dictionary<string, string> _metaInfo) 
	: base(KakaoAction.SendInviteImageMessage) 
	{
		templateId 	= _templateId;
		receiverId 	= _receiverId;
		executeUrl 	= _executeUrl;
		metaInfo 	= _metaInfo;
	}
	
	public override string getParamString() {
		JSONClass json 					= makeDefaultParam();
		
		json[KakaoStringKeys.Params.templateId] 	= templateId;
		json[KakaoStringKeys.Params.receiverId] 	= receiverId;
		json[KakaoStringKeys.Params.executeUrl] 	= executeUrl;
		
		foreach (string key in metaInfo.Keys)
		{
			json[KakaoStringKeys.Params.metaInfo][key] = metaInfo[key];
		}
		
		return json.ToString();
	}
}
