﻿using System.Collections;
using SimpleJSON;

public class KakaoParamShowAlertMessage : KakaoParamBase {
	private string message;
	
	public KakaoParamShowAlertMessage(string _message) : base(KakaoAction.ShowAlertMessage) {
		message = _message;
	}
	
	public override string getParamString() {
		JSONClass json 					= makeDefaultParam();
		
		if( message!=null )
			json[KakaoStringKeys.Params.message]= message;
		
		return json.ToString();
	}
}
