﻿using UnityEngine;
using System.Collections;

public class KakaoPluginEditor : KakaoPluginBase {
	
	public KakaoPluginEditor() {
	}
	
	public override void request(KakaoParamBase param) {
//		Debug.Log(param.getParamString());
		
		KakaoAction action = param.getAction();
		switch (action)
		{
		case KakaoAction.Init:
			if( KakaoResponseHandler.Instance.initComplete==null )
				return;
			KakaoResponseHandler.Instance.initComplete();
			break;
		case KakaoAction.Authorized:
			if( KakaoResponseHandler.Instance.authorized==null )
				return;
			KakaoResponseHandler.Instance.authorized(KakaoNativeExtension.Instance.hasValidTokenCache());
			break;
		case KakaoAction.Login:
		case KakaoAction.LoginWebview:
			if( KakaoResponseHandler.Instance.tokens==null )
				return;
			KakaoResponseHandler.Instance.tokens("test_access_token", "test_refresh_token");
			
			if( KakaoResponseHandler.Instance.loginComplete==null )
				return;
			KakaoResponseHandler.Instance.loginComplete();
			break;
			
		case KakaoAction.LocalUser:
			if( KakaoResponseHandler.Instance.localUserComplete==null )
				return;
			KakaoResponseHandler.Instance.localUserComplete();
			break;
		case KakaoAction.Friends:
			if( KakaoResponseHandler.Instance.friendsComplete==null )
				return;
			KakaoResponseHandler.Instance.friendsComplete();
			break;
		case KakaoAction.SendMessage:
			if( KakaoResponseHandler.Instance.sendMessageComplete==null )
				return;
			KakaoResponseHandler.Instance.sendMessageComplete();
			break;
        case KakaoAction.SendImageMessage:
			if( KakaoResponseHandler.Instance.sendImageMessageComplete==null )
				return;
            KakaoResponseHandler.Instance.sendImageMessageComplete();
			break;
        case KakaoAction.SendInviteImageMessage:
			if( KakaoResponseHandler.Instance.sendImageMessageComplete==null )
				return;
            KakaoResponseHandler.Instance.sendImageMessageComplete();
			break;
		case KakaoAction.PostToKakaoStory:
			if( KakaoResponseHandler.Instance.postStoryComplete==null )
				return;
			KakaoResponseHandler.Instance.postStoryComplete();
			break;
		case KakaoAction.Logout:
			if( KakaoResponseHandler.Instance.tokens==null )
				return;
			KakaoResponseHandler.Instance.tokens(null, null);
			
			if( KakaoResponseHandler.Instance.logoutComplete==null )
				return;
			KakaoResponseHandler.Instance.logoutComplete();
			break;
		case KakaoAction.Unregister:
			if( KakaoResponseHandler.Instance.tokens==null )
				return;
			KakaoResponseHandler.Instance.tokens(null, null);
			
			if( KakaoResponseHandler.Instance.unregisterComplete==null )
				return;
			KakaoResponseHandler.Instance.unregisterComplete();
			break;
		case KakaoAction.ShowAlertMessage:
			Debug.Log("KakaoAction.ShowAlertMessage");
			break;
		case KakaoAction.LoadGameInfo:
			break;
		case KakaoAction.LoadGameUserInfo:
			break;
		case KakaoAction.UpdateUser:
			break;
		case KakaoAction.UseHeart:
			break;
		case KakaoAction.UpdateResult:
			break;
		case KakaoAction.UpdateMultipleResults:
			break;
		case KakaoAction.LoadLeaderboard:
			break;
		case KakaoAction.BlockMessage:
			break;
		case KakaoAction.SendGameMessage:
			break;
		case KakaoAction.SendInviteGameMessage:
			break;
		case KakaoAction.LoadGameFriends:
			break;
		case KakaoAction.LoadGameMessages:
			break;
		case KakaoAction.AcceptGameMessage:
			break;
		case KakaoAction.AcceptAllGameMessages:
			break;
		case KakaoAction.DeleteUser:
			break;
		};
	}
}
