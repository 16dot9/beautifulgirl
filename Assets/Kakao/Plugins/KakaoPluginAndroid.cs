﻿using UnityEngine;
using System.Collections;

#if UNITY_ANDROID
public class KakaoPluginAndroid : KakaoPluginBase {
	
	public AndroidJavaObject activity;
	
	public KakaoPluginAndroid() {
		AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        activity = javaClass.GetStatic<AndroidJavaObject>("currentActivity");
	}
	
	public override void request(KakaoParamBase param) {
		activity.Call("kakaoUnityExtension", param.getParamString());
	}
}
#endif