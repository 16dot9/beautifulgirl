using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class KakaoNativeExtension : MonoBehaviour {
	static public string version = "1.1.3";
	
	private static KakaoPluginBase plugin = null;
	
	private static KakaoNativeExtension _instance;
    public static KakaoNativeExtension Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(KakaoNativeExtension)) as KakaoNativeExtension;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "KakaoNativeExtension";
                    _instance = container.AddComponent(typeof(KakaoNativeExtension)) as KakaoNativeExtension;
					DontDestroyOnLoad(_instance);
                }
            }
 
            return _instance;
        }
    }
	
	public void Init(KakaoResponseHandler.delegateInitComplete complete, KakaoResponseHandler.delegateTokens tokens) 
	{
		string accessToken = PlayerPrefs.GetString(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs);
		string refreshToken = PlayerPrefs.GetString(KakaoStringKeys.Commons.refreshTokenKeyForPlayerPrefs);

		KakaoResponseHandler.Instance.Init();
		KakaoResponseHandler.Instance.initComplete = complete;
		KakaoResponseHandler.Instance.tokens = tokens;
		
		if( plugin==null ) {
#if UNITY_EDITOR
			plugin = ScriptableObject.CreateInstance<KakaoPluginEditor>();
#elif UNITY_ANDROID
			plugin = ScriptableObject.CreateInstance<KakaoPluginAndroid>();
#elif UNITY_IPHONE
			plugin = ScriptableObject.CreateInstance<KakaoPluginiOS>();
#else
			plugin = ScriptableObject.CreateInstance<KakaoPluginEditor>();
#endif
		}
		
		KakaoParamBase param = new KakaoParamInit(accessToken,refreshToken);
		plugin.request(param);
	}
	
	public void Authorized(KakaoResponseHandler.delegateAuthorized complete) {
		Debug.Log("Authorized");
		KakaoResponseHandler.Instance.authorized = complete;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.Authorized);
		plugin.request(param);
	}
	
	public void Login(KakaoResponseHandler.delegateLoginComplete complete, KakaoResponseHandler.delegateLoginError error) {
		Debug.Log("Login");
		KakaoResponseHandler.Instance.loginComplete = complete;
		KakaoResponseHandler.Instance.loginError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.Login);
		plugin.request(param);
	}
	
	public void LoginWithWebview(KakaoResponseHandler.delegateLoginComplete complete, KakaoResponseHandler.delegateLoginError error) {
		Debug.Log("Login with Webview");
		KakaoResponseHandler.Instance.loginComplete = complete;
		KakaoResponseHandler.Instance.loginError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LoginWebview);
		plugin.request(param);
	}
	
	public void LocalUser(KakaoResponseHandler.delegateLocalUserComplete complete, KakaoResponseHandler.delegateLocalUserError error) {
		Debug.Log("LocalUser");
		KakaoResponseHandler.Instance.localUserComplete = complete;
		KakaoResponseHandler.Instance.localUserError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LocalUser);
		plugin.request(param);
	}
	
	public void Friends(KakaoResponseHandler.delegateFriendsComplete complete, KakaoResponseHandler.delegateFriendsError error) {
		Debug.Log("Friends");
		KakaoResponseHandler.Instance.friendsComplete = complete;
		KakaoResponseHandler.Instance.friendsError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.Friends);
		plugin.request(param);
	}
	
	public void SendMessage(string message, string receiverId, string executeUrl, KakaoResponseHandler.delegateSendMessageComplete complete, KakaoResponseHandler.delegateSendMessageError error) {
		Debug.Log("SendMessage");
		KakaoResponseHandler.Instance.sendMessageComplete = complete;
		KakaoResponseHandler.Instance.sendMessageError = error;
		KakaoParamBase param = new KakaoParamMessage(message,receiverId,executeUrl);
		plugin.request(param);
	}
	
	public void SendImageMessage(string templateId, string receiverId, string imagePath, string executeUrl, Dictionary<string,string> metaInfo, KakaoResponseHandler.delegateSendImageMessageComplete complete, KakaoResponseHandler.delegateSendImageMessageError error) {
		Debug.Log("SendImageMessage");
		KakaoResponseHandler.Instance.sendImageMessageComplete = complete;
		KakaoResponseHandler.Instance.sendImageMessageError = error;
		KakaoParamBase param = new KakaoParamImageMessage(templateId,receiverId,imagePath,executeUrl,metaInfo);
		plugin.request(param);
	}
	
	public void SendInviteImageMessage(string templateId, string receiverId, string executeUrl, Dictionary<string,string> metaInfo, KakaoResponseHandler.delegateSendInviteImageMessageComplete complete, KakaoResponseHandler.delegateSendInviteImageMessageError error)
	{
		Debug.Log("SendInviteImageMessage");
		KakaoResponseHandler.Instance.sendInviteImageMessageComplete = complete;
		KakaoResponseHandler.Instance.sendInviteImageMessageError = error;
		KakaoParamBase param = new KakaoParamInviteImageMessage(templateId,receiverId,executeUrl,metaInfo);
		plugin.request(param);
	}
	
	public void PostToKakaoStory(string message, string imagePath, string executeUrl, KakaoResponseHandler.delegatePostStoryComplete complete, KakaoResponseHandler.delegatePostStoryError error)
	{
		Debug.Log("PostToKakaoStory");
		KakaoResponseHandler.Instance.postStoryComplete = complete;
		KakaoResponseHandler.Instance.postStoryError = error;
		KakaoParamBase param = new KakaoParamStory(message, imagePath, executeUrl);
		plugin.request(param);
	}
	
	public void Logout(KakaoResponseHandler.delegateLogoutComplete complete, KakaoResponseHandler.delegateLogoutError error) {
		Debug.Log("Logout");
		KakaoResponseHandler.Instance.logoutComplete = complete;
		KakaoResponseHandler.Instance.logoutError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.Logout);
		plugin.request(param);
	}
	
	public void Unregister(KakaoResponseHandler.delegateUnregisterComplete complete, KakaoResponseHandler.delegateUnregisterError error) {
		Debug.Log("Unregister");
		KakaoResponseHandler.Instance.unregisterComplete = complete;
		KakaoResponseHandler.Instance.unregisterError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.Unregister);
		plugin.request(param);
	}
	
	public void ShowAlertMessage(string message) {
		Debug.Log("ShowAlertMessage");
		KakaoParamBase param = new KakaoParamShowAlertMessage(message);
		plugin.request(param);
	}
	
	public void loadGameInfo(KakaoResponseHandler.delegateLoadGameInfoComplete complete, KakaoResponseHandler.delegateLoadGameInfoError error) {
		Debug.Log("loadGameInfo");
		KakaoResponseHandler.Instance.loadGameInfoComplete = complete;
		KakaoResponseHandler.Instance.loadGameInfoError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LoadGameInfo);
		plugin.request(param);
	}
	
	public void loadGameUserInfo(KakaoResponseHandler.delegateLoadGameUserInfoComplete complete, KakaoResponseHandler.delegateLoadGameUserInfoError error) {
		Debug.Log("loadGameUserInfo");
		KakaoResponseHandler.Instance.loadGameUserInfoComplete = complete;
		KakaoResponseHandler.Instance.loadGameUserInfoError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LoadGameUserInfo);
		plugin.request(param);
	}
	
	//UpdateUser,
	public void updateUser(int additionalHeart, byte[] publicData, byte[] privateData, KakaoResponseHandler.delegateUpdateUserComplete complete, KakaoResponseHandler.delegateUpdateUserError error)
	{
		Debug.Log ("updateUser");
		KakaoResponseHandler.Instance.updateUserComplete = complete;
		KakaoResponseHandler.Instance.updateUserError = error;
		KakaoParamBase param = new KakaoParamUpdateUser(additionalHeart,publicData,privateData);
		plugin.request(param);
	}
	
	//UseHeart,
	public void useHeart(int useHeart, KakaoResponseHandler.delegateUseHeartComplete complete, KakaoResponseHandler.delegateUseHeartError error)
	{
		Debug.Log ("useHeart");
		KakaoResponseHandler.Instance.useHeartComplete = complete;
		KakaoResponseHandler.Instance.useHeartError = error;
		KakaoParamBase param = new KakaoParamUseHeart(useHeart);
		plugin.request(param);
	}

	//UpdateResult,
	public void updateResult(string leaderboardKey, int score, int exp, byte[] publicData, byte[] privateData, KakaoResponseHandler.delegateUpdateResultComplete complete, KakaoResponseHandler.delegateUpdateResultError error)
	{
		Debug.Log ("updateResult");
		KakaoResponseHandler.Instance.updateResultComplete = complete;
		KakaoResponseHandler.Instance.updateResultError = error;
		KakaoParamBase param = new KakaoParamUpdateResult(leaderboardKey, score, exp, publicData, privateData);
		plugin.request(param);
	}

	//UpdateResults,
	public void updateMultipleResults(Dictionary<string,int> scores, int exp, byte[] publicData, byte[] privateData, KakaoResponseHandler.delegateUpdateMultipleResultsComplete complete, KakaoResponseHandler.delegateUpdateMultipleResultsError error)
	{
		Debug.Log ("updateMultipleResults");
		KakaoResponseHandler.Instance.updateMultipleResultsComplete = complete;
		KakaoResponseHandler.Instance.updateMultipleResultsError = error;
		KakaoParamBase param = new KakaoParamUpdateMultipleResults(scores,exp,publicData,privateData);
		plugin.request(param);
	}

	//GetLeaderboard,
	public void loadLeaderboard(string leaderboardKey, KakaoResponseHandler.delegateLoadLeaderboardComplete complete, KakaoResponseHandler.delegateLoadLeaderboardError error)
	{
		Debug.Log ("loadLeaderboard");
		KakaoResponseHandler.Instance.loadLeaderboardComplete = complete;
		KakaoResponseHandler.Instance.loadLeaderboardError = error;
		KakaoParamBase param = new KakaoParamLoadLeaderboard(leaderboardKey);
		plugin.request(param);
	}

	//	BlockMessage,
	public void blockMessage(bool block, KakaoResponseHandler.delegateBlockMessageComplete complete, KakaoResponseHandler.delegateBlockMessageError error) 
	{
		Debug.Log("blockMessage");
		KakaoResponseHandler.Instance.blockMessageComplete = complete;
		KakaoResponseHandler.Instance.blockMessageError = error;
		KakaoParamBase param = new KakaoParamBlockMessage(block);
		plugin.request(param);
	}
	
	//	SendGameMessage,
	public void sendGameMessage(string receiverId, string talkMessage, string gameMessage, int heart, string executeUrl, byte[] data, KakaoResponseHandler.delegateSendGameMessageComplete complete, KakaoResponseHandler.delegateSendGameMessageError error) {
		Debug.Log("sendGameMessage");
		KakaoResponseHandler.Instance.sendGameMessageComplete = complete;
		KakaoResponseHandler.Instance.sendGameMessageError = error;
		KakaoParamBase param = new KakaoParamGameMessage(receiverId, talkMessage, gameMessage, heart, executeUrl, data);
		plugin.request(param);
	}
	
	// sendInviteGameMessage
	public void sendInviteGameMessage(string receiverId, string talkMessage, string executeUrl, KakaoResponseHandler.delegateSendInviteGameMessageComplete complete, KakaoResponseHandler.delegateSendInviteGameMessageError error) {
		Debug.Log("sendInviteGameMessage");
		KakaoResponseHandler.Instance.sendInviteGameMessageComplete = complete;
		KakaoResponseHandler.Instance.sendInviteGameMessageError = error;
		KakaoParamBase param = new KakaoParamInviteGameMessage(receiverId, talkMessage,executeUrl);
		plugin.request(param);
	}

	//	AcceptGameMessage,
	public void acceptGameMessage(string id,KakaoResponseHandler.delegateAcceptGameMessageComplete complete, KakaoResponseHandler.delegateAcceptGameMessageError error) {
		Debug.Log("acceptGameMessage");
		KakaoResponseHandler.Instance.acceptGameMessageComplete = complete;
		KakaoResponseHandler.Instance.acceptGameMessageError = error;
		KakaoParamBase param = new KakaoParamAcceptGameMessage(id);
		plugin.request(param);
	}

	//	LoadGameFriends,
	public void loadGameFriends(KakaoResponseHandler.delegateLoadGameFriendsComplete complete, KakaoResponseHandler.delegateLoadGameFriendsError error) {
		Debug.Log("loadGameFriends");
		KakaoResponseHandler.Instance.loadGameFriendsComplete = complete;
		KakaoResponseHandler.Instance.loadGameFriendsError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LoadGameFriends);
		plugin.request(param);

	}

	//	LoadGameMessages,
	public void loadGameMessages(KakaoResponseHandler.delegateLoadGameMessagesComplete complete, KakaoResponseHandler.delegateLoadGameMessagesError error) {
		Debug.Log("loadGameMessages");
		KakaoResponseHandler.Instance.loadGameMessagesComplete = complete;
		KakaoResponseHandler.Instance.loadGameMessagesError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.LoadGameMessages);
		plugin.request(param);
	}
	
	//	AcceptAllGameMessages,
	public void acceptAllGameMessages(KakaoResponseHandler.delegateAcceptAllGameMessagesComplete complete, KakaoResponseHandler.delegateAcceptAllGameMessagesError error) {
		Debug.Log("acceptAllGameMessages");
		KakaoResponseHandler.Instance.acceptAllGameMessagesComplete = complete;
		KakaoResponseHandler.Instance.acceptAllGameMessagesError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.AcceptAllGameMessages);
		plugin.request(param);
	}
	
	//	DeleteUser,
	public void deleteUser(KakaoResponseHandler.delegateDeleteUserComplete complete, KakaoResponseHandler.delegateDeleteUserError error) {
		Debug.Log("deleteUser");
		KakaoResponseHandler.Instance.deleteUserComplete = complete;
		KakaoResponseHandler.Instance.deleteUserError = error;
		KakaoParamBase param = new KakaoParamBase(KakaoAction.DeleteUser);
		plugin.request(param);
	}

	public void updateTokenCache(string accessToken, string refreshToken) {
		if( accessToken!=null && refreshToken!=null && accessToken.Length>0 && refreshToken.Length>0 ) {
			PlayerPrefs.SetString(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs, 	accessToken);
			PlayerPrefs.SetString(KakaoStringKeys.Commons.refreshTokenKeyForPlayerPrefs, 	refreshToken);
			Debug.Log("Archived tokens.");
		}
		else {
			PlayerPrefs.DeleteKey(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs);
			PlayerPrefs.DeleteKey(KakaoStringKeys.Commons.refreshTokenKeyForPlayerPrefs);
			Debug.Log("Token is invaldate, Because logout or unregister or expired token.");
		}

		PlayerPrefs.Save();
	}
	public bool hasValidTokenCache() {
		string accessToken = PlayerPrefs.GetString(KakaoStringKeys.Commons.accessTokenKeyForPlayerPrefs, 	null);
		string refreshToken = PlayerPrefs.GetString(KakaoStringKeys.Commons.refreshTokenKeyForPlayerPrefs,	null);
		
		if( accessToken!=null && refreshToken!=null && accessToken.Length>0 && refreshToken.Length>0 ) {
			return true;
		}
		return false;
	}

	public void test(int testNumber) {
		KakaoParamTest param = new KakaoParamTest(testNumber);
		plugin.request(param);
	}

}
