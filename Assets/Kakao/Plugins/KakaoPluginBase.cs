﻿using UnityEngine;
using System.Collections;

public abstract class KakaoPluginBase : ScriptableObject {
	abstract public void request(KakaoParamBase param);
}
