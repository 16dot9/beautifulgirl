﻿using UnityEngine;
using System.Collections;

public class KakaoPluginiOS : KakaoPluginBase {
	
	public KakaoPluginiOS() {
	}
	
	override public void request(KakaoParamBase param) {
		PlayerPrefs.SetString("kakaoUnityExtension",param.getParamString());
	}
}
